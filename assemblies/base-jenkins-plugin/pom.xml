<?xml version="1.0" encoding="UTF-8"?>
<!--

    Copyright (c) 2012-present Marvelution Holding B.V.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.marvelution.jenkins.jira.assemblies</groupId>
        <artifactId>assemblies</artifactId>
        <version>6.1.0-SNAPSHOT</version>
    </parent>
    <artifactId>base-jenkins-plugin</artifactId>

    <dependencies>
        <dependency>
            <groupId>org.marvelution.jenkins.jira.components</groupId>
            <artifactId>core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.marvelution.jenkins.jira.components</groupId>
            <artifactId>activity-streams</artifactId>
        </dependency>
        <dependency>
            <groupId>org.marvelution.jenkins.jira.components</groupId>
            <artifactId>build-panels</artifactId>
        </dependency>
        <dependency>
            <groupId>org.marvelution.jenkins.jira.components</groupId>
            <artifactId>data-access</artifactId>
        </dependency>
        <dependency>
            <groupId>org.marvelution.jenkins.jira.components</groupId>
            <artifactId>data-synchronization</artifactId>
        </dependency>
        <dependency>
            <groupId>org.marvelution.jenkins.jira.components</groupId>
            <artifactId>indexing</artifactId>
        </dependency>
        <dependency>
            <groupId>org.marvelution.jenkins.jira.components</groupId>
            <artifactId>jql</artifactId>
        </dependency>
        <dependency>
            <groupId>org.marvelution.jenkins.jira.components</groupId>
            <artifactId>release-report</artifactId>
        </dependency>
        <dependency>
            <groupId>org.marvelution.jenkins.common.components</groupId>
            <artifactId>sync-token</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-crypto</artifactId>
        </dependency>

        <dependency>
            <groupId>org.marvelution.jenkins.common.components</groupId>
            <artifactId>resources</artifactId>
        </dependency>
        <dependency>
            <groupId>org.marvelution.jenkins.common.components</groupId>
            <artifactId>templating</artifactId>
        </dependency>

        <dependency>
            <groupId>org.marvelution.jenkins.common.components</groupId>
            <artifactId>model</artifactId>
            <classifier>tests</classifier>
        </dependency>
        <dependency>
            <groupId>org.marvelution.jenkins.common.testsuite</groupId>
            <artifactId>testkit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.marvelution.testing</groupId>
            <artifactId>testkit</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- Other components -->
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-core</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.json</groupId>
            <artifactId>atlassian-json-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-annotation</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-runtime</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webresource-common</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-v2-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.soy</groupId>
            <artifactId>soy-template-renderer-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.templaterenderer</groupId>
            <artifactId>atlassian-template-renderer-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi-bridge</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.felix</groupId>
            <artifactId>org.apache.felix.framework</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.upm</groupId>
            <artifactId>licensing-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.upm</groupId>
            <artifactId>upm-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.thymeleaf</groupId>
            <artifactId>thymeleaf-spring5</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-scss-resources</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <includeGroupIds>org.marvelution.jenkins.common.components</includeGroupIds>
                            <includeArtifactIds>resources</includeArtifactIds>
                            <includes>**/sass/**</includes>
                            <outputDirectory>${project.build.directory}/common-resources/</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
                <configuration>
                    <overWriteReleases>true</overWriteReleases>
                    <overWriteSnapshots>true</overWriteSnapshots>
                </configuration>
            </plugin>
            <plugin>
                <groupId>io.github.cleydyr</groupId>
                <artifactId>dart-sass-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>generate-css-using-sass</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>compile-sass</goal>
                        </goals>
                        <configuration>
                            <version>${dart.sass.version}</version>
                            <cachedFilesDirectory>${settings.localRepository}/dart-sass-cache/</cachedFilesDirectory>
                            <style>COMPRESSED</style>
                            <loadPaths>
                                <loadPath>${project.build.directory}/common-resources/sass/</loadPath>
                            </loadPaths>
                            <outputFolder>${project.build.outputDirectory}/static/css</outputFolder>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-shade-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <keepDependenciesWithProvidedScope>true</keepDependenciesWithProvidedScope>
                            <promoteTransitiveDependencies>true</promoteTransitiveDependencies>
                            <artifactSet>
                                <includes>
                                    <include>org.springframework*:*</include>
                                </includes>
                            </artifactSet>
                            <filters>
                                <filter>
                                    <artifact>org.springframework*:*</artifact>
                                    <includes>
                                        <include>org/springframework/security/crypto/codec/Hex*</include>
                                    </includes>
                                </filter>
                            </filters>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
