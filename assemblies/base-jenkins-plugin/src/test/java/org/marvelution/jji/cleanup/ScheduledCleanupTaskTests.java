/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.cleanup;

import java.time.*;
import java.util.*;
import java.util.stream.*;

import org.marvelution.testing.*;

import com.atlassian.scheduler.*;
import com.atlassian.scheduler.config.*;
import org.assertj.core.api.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import static java.util.stream.Collectors.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

abstract class ScheduledCleanupTaskTests<T extends ScheduledCleanupTask>
		extends TestSupport
{

	@Mock
	private SchedulerService schedulerService;
	T task;

	@BeforeEach
	void setUp()
	{
		task = createTask(schedulerService);
	}

	abstract T createTask(SchedulerService schedulerService);

	abstract void testRunJob();

	abstract void testRunJob_Failed();

	@Test
	void testOnStart()
			throws Exception
	{
		when(schedulerService.getRegisteredJobRunnerKeys()).thenReturn(Collections.emptySet());

		task.onStart();

		verify(schedulerService).registerJobRunner(task.JOB_KEY, task);
		ArgumentCaptor<JobConfig> jobConfigArgumentCaptor = ArgumentCaptor.forClass(JobConfig.class);
		verify(schedulerService).scheduleJob(eq(task.JOB_ID), jobConfigArgumentCaptor.capture());
		assertJobConfig(jobConfigArgumentCaptor.getValue());
	}

	@Test
	void testOnStart_AlreadyRegisteredJob()
			throws Exception
	{
		when(schedulerService.getRegisteredJobRunnerKeys()).thenReturn(Stream.of(task.JOB_KEY).collect(toSet()));

		task.onStart();

		verify(schedulerService, never()).registerJobRunner(task.JOB_KEY, task);
		ArgumentCaptor<JobConfig> jobConfigArgumentCaptor = ArgumentCaptor.forClass(JobConfig.class);
		verify(schedulerService).scheduleJob(eq(task.JOB_ID), jobConfigArgumentCaptor.capture());
		assertJobConfig(jobConfigArgumentCaptor.getValue());
	}

	private void assertJobConfig(JobConfig jobConfig)
	{
		assertThat(jobConfig).extracting(JobConfig::getJobRunnerKey, JobConfig::getRunMode)
				.containsOnly(task.JOB_KEY, RunMode.RUN_ONCE_PER_CLUSTER);
		Schedule schedule = jobConfig.getSchedule();
		assertThat(schedule.getType()).isEqualTo(Schedule.Type.INTERVAL);
		OffsetDateTime now = OffsetDateTime.now();
		OffsetDateTime firstRun = now.withHour(22)
				.withMinute(0)
				.withSecond(0);
		if (firstRun.isBefore(now))
		{
			firstRun = firstRun.plusDays(1);
		}
		assertThat(schedule.getIntervalScheduleInfo()).isNotNull()
				.extracting(IntervalScheduleInfo::getIntervalInMillis, IntervalScheduleInfo::getFirstRunTime)
				.contains(Duration.ofDays(1)
						.toMillis())
				.element(1, InstanceOfAssertFactories.DATE)
				.isCloseTo(Date.from(firstRun.toInstant()), 5000);
	}

	@Test
	void testOnStart_SchedulingException()
			throws Exception
	{
		when(schedulerService.getRegisteredJobRunnerKeys()).thenReturn(Collections.emptySet());
		doThrow(new SchedulerServiceException("")).when(schedulerService).scheduleJob(eq(task.JOB_ID), any());

		task.onStart();

		verify(schedulerService).registerJobRunner(task.JOB_KEY, task);
		verify(schedulerService).scheduleJob(eq(task.JOB_ID), any());
	}

	@Test
	void testOnStop()
	{
		task.onStop();

		verify(schedulerService).unregisterJobRunner(task.JOB_KEY);
		verify(schedulerService).unscheduleJob(task.JOB_ID);
	}
}
