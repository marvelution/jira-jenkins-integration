/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.cleanup;

import java.time.*;
import java.time.temporal.*;

import org.marvelution.jji.data.access.*;
import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.services.api.*;

import com.atlassian.scheduler.*;
import com.atlassian.scheduler.status.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import static org.marvelution.jji.data.services.api.ConfigurationService.DEFAULT_DELETED_DATA_RETENTION_PERIOD;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class CleanupOldAuditDataTest
		extends ScheduledCleanupTaskTests<CleanupOldAuditData>
{

	@Mock
	private ConfigurationService configurationService;
	@Mock
	private SynchronizationResultDAO synchronizationResultDAO;

	@Override
	CleanupOldAuditData createTask(SchedulerService schedulerService)
	{
		return new CleanupOldAuditData(schedulerService, configurationService, synchronizationResultDAO);
	}

	@Test
	@Override
	void testRunJob()
	{
		when(configurationService.getAuditDataRetentionTimestamp()).thenReturn(LocalDateTime.now().minusMonths(1));

		assertThat(task.runJob(null)).extracting(JobRunnerResponse::getRunOutcome).isEqualTo(RunOutcome.SUCCESS);

		ArgumentCaptor<LocalDateTime> timestampCaptor = ArgumentCaptor.forClass(LocalDateTime.class);
		verify(synchronizationResultDAO).deleteAllQueuedBefore(timestampCaptor.capture());
		assertThat(timestampCaptor.getValue()).isCloseTo(LocalDateTime.now().minusMonths(1), within(5, ChronoUnit.SECONDS));
	}

	@Test
	void testRunJob_ZeroRetention()
	{
		when(configurationService.getAuditDataRetentionTimestamp()).thenReturn(LocalDateTime.now().minusDays(1));

		assertThat(task.runJob(null)).extracting(JobRunnerResponse::getRunOutcome).isEqualTo(RunOutcome.SUCCESS);

		ArgumentCaptor<LocalDateTime> timestampCaptor = ArgumentCaptor.forClass(LocalDateTime.class);
		verify(synchronizationResultDAO).deleteAllQueuedBefore(timestampCaptor.capture());
		assertThat(timestampCaptor.getValue()).isCloseTo(LocalDateTime.now().minusDays(1), within(5, ChronoUnit.SECONDS));
	}

	@Test
	@Override
	void testRunJob_Failed()
	{
		when(configurationService.getAuditDataRetentionTimestamp()).thenReturn(LocalDateTime.now().minusMonths(1));

		doThrow(new IllegalStateException("OOPS!")).when(synchronizationResultDAO).deleteAllQueuedBefore(any());

		assertThat(task.runJob(null)).extracting(JobRunnerResponse::getRunOutcome, JobRunnerResponse::getMessage)
				.containsOnly(RunOutcome.FAILED, "Failed to cleanup old audit data, see logs for details.");

		verify(synchronizationResultDAO).deleteAllQueuedBefore(any());
	}
}
