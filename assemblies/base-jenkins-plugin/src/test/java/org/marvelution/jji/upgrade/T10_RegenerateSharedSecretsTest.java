/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.net.*;
import java.util.*;

import org.marvelution.jji.api.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.testing.*;

import org.junit.jupiter.api.*;
import org.mockito.*;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * Tests for {@link T10_RegenerateSharedSecrets}.
 *
 * @author Mark Rekveld
 * @since 3.1.0
 */
public class T10_RegenerateSharedSecretsTest
		extends TestSupport
{

	@Mock
	private AppState appState;
	@Mock
	private SiteService siteService;
	@Mock
	private SynchronizationService synchronizationService;
	private T10_RegenerateSharedSecrets task;

	@BeforeEach
	void setUp()
	{
		task = new T10_RegenerateSharedSecrets(appState, siteService, synchronizationService);
	}

	@Test
	void testGetBuildNumber()
	{
		assertThat(task.getBuildNumber()).isEqualTo(10);
	}

	@Test
	void testGetShortDescription()
	{
		assertThat(task.getShortDescription()).isEqualTo("Regenerate Shared Secrets");
	}

	@Test
	public void testDoUpgrade()
	{
		List<Site> sites = Arrays.asList(new Site().setId("site-1")
				                                 .setName("Site 1")
				                                 .setRpcUrl(URI.create("http://localhost:8080"))
				                                 .setJenkinsPluginInstalled(true)
				                                 .setSharedSecret("old-shared-secret-1")
				                                 .setUseCrumbs(false), new Site().setId("site-2")
				                                 .setName("Site 2")
				                                 .setRpcUrl(URI.create("http://localhost:8082"))
				                                 .setJenkinsPluginInstalled(false)
				                                 .setSharedSecret("old-shared-secret-2")
				                                 .setUseCrumbs(true));
		when(siteService.getAll()).thenReturn(sites);
		when(siteService.save(any(Site.class))).then(invocation -> invocation.getArgument(0));

		task.doUpgrade();

		verify(siteService).getAll();
		ArgumentCaptor<Site> argumentCaptor = ArgumentCaptor.forClass(Site.class);
		verify(synchronizationService, times(2)).synchronize(argumentCaptor.capture(), any(UpgradeTaskTrigger.class));
		assertThat(argumentCaptor.getAllValues()).hasSize(2)
				.usingElementComparatorOnFields("id", "name", "rpcUrl", "jenkinsPluginInstalled", "useCrumbs")
				.containsOnly(new Site().setId("site-1")
						              .setName("Site 1")
						              .setRpcUrl(URI.create("http://localhost:8080"))
						              .setJenkinsPluginInstalled(false)
						              .setUseCrumbs(true), new Site().setId("site-2")
						              .setName("Site 2")
						              .setRpcUrl(URI.create("http://localhost:8082"))
						              .setJenkinsPluginInstalled(false)
						              .setUseCrumbs(true));
	}
}
