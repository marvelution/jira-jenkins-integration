/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.cleanup;

import java.time.*;
import java.time.temporal.*;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.services.api.*;

import com.atlassian.scheduler.*;
import com.atlassian.scheduler.status.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import static org.marvelution.jji.data.services.api.ConfigurationService.*;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class CleanupOldDeletedDataTest
		extends ScheduledCleanupTaskTests<CleanupOldDeletedData>
{

	@Mock
	private ConfigurationService configurationService;
	@Mock
	private JobDAO jobDAO;
	@Mock
	private BuildDAO buildDAO;

	@Override
	CleanupOldDeletedData createTask(SchedulerService schedulerService)
	{
		return new CleanupOldDeletedData(schedulerService, configurationService, jobDAO, buildDAO);
	}

	@Test
	@Override
	void testRunJob()
	{
		when(configurationService.getDeletedDataRetentionTimestamp()).thenReturn(
				LocalDateTime.now().minusMonths(DEFAULT_DELETED_DATA_RETENTION_PERIOD));

		LocalDateTime expectedTimestamp = LocalDateTime.now().minusMonths(DEFAULT_DELETED_DATA_RETENTION_PERIOD);

		assertThat(task.runJob(null)).extracting(JobRunnerResponse::getRunOutcome).isEqualTo(RunOutcome.SUCCESS);

		ArgumentCaptor<LocalDateTime> timestampCaptor = ArgumentCaptor.forClass(LocalDateTime.class);
		verify(jobDAO).cleanupOldDeletedJobs(timestampCaptor.capture());
		verify(buildDAO).cleanupOldDeletedBuilds(timestampCaptor.capture());
		assertThat(timestampCaptor.getAllValues()).hasSize(2).allSatisfy(timestamp -> {
			assertThat(timestamp).isCloseTo(expectedTimestamp, within(5, ChronoUnit.SECONDS));
		});
	}

	@Test
	@Override
	void testRunJob_Failed()
	{
		doThrow(new IllegalStateException("OOPS!")).when(buildDAO).cleanupOldDeletedBuilds(any());

		assertThat(task.runJob(null)).extracting(JobRunnerResponse::getRunOutcome, JobRunnerResponse::getMessage)
				.containsOnly(RunOutcome.FAILED, "IllegalStateException: OOPS!");

		verify(jobDAO).cleanupOldDeletedJobs(any());
		verify(buildDAO).cleanupOldDeletedBuilds(any());
	}
}
