/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.Collections;
import java.util.Set;

import org.marvelution.jji.api.AppState;
import org.marvelution.jji.api.features.FeatureFlag;
import org.marvelution.jji.api.features.FeatureFlags;
import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.api.features.InMemoryFeatureStore;
import org.marvelution.testing.TestSupport;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.marvelution.jji.upgrade.T14_SyncAllBuildFeatureFlag.ONLY_SYNC_UNKNOWN_BUILDS_KEY;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class T14_SyncAllBuildFeatureFlagTest
        extends TestSupport
{

    @Mock
    private AppState appState;
    @Mock
    private PluginSettingsFactory pluginSettingsFactory;
    @Mock
    private PluginSettings pluginSettings;
    private Features features;
    private T14_SyncAllBuildFeatureFlag task;

    @BeforeEach
    void setUp()
    {
        when(pluginSettingsFactory.createGlobalSettings()).thenReturn(pluginSettings);
        features = new Features(new InMemoryFeatureStore())
        {
            @Override
            protected Set<FeatureFlag> loadFeatureFlags()
            {
                return Collections.singleton(new FeatureFlag(FeatureFlags.SYNC_ALL_BUILDS, false, true));
            }
        };
        task = new T14_SyncAllBuildFeatureFlag(appState, features, pluginSettingsFactory);
    }

    @Test
    void testUpgrade_SyncAllBuilds()
    {
        when(pluginSettings.get(ONLY_SYNC_UNKNOWN_BUILDS_KEY)).thenReturn("false");

        task.doUpgrade();

        assertThat(features.isFeatureEnabled(FeatureFlags.SYNC_ALL_BUILDS)).isTrue();
        verify(pluginSettings).get(ONLY_SYNC_UNKNOWN_BUILDS_KEY);
        verify(pluginSettings).remove(ONLY_SYNC_UNKNOWN_BUILDS_KEY);
    }

    @Test
    void testUpgrade_SyncUnknownOnlyBuilds()
    {
        when(pluginSettings.get(ONLY_SYNC_UNKNOWN_BUILDS_KEY)).thenReturn("true");

        task.doUpgrade();

        assertThat(features.isFeatureEnabled(FeatureFlags.SYNC_ALL_BUILDS)).isFalse();
        verify(pluginSettings).get(ONLY_SYNC_UNKNOWN_BUILDS_KEY);
        verify(pluginSettings).remove(ONLY_SYNC_UNKNOWN_BUILDS_KEY);
    }
}
