/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
require(['jira/util/events'], function (Events) {
    Events.bind('before-submit', function (e, event, dialog) {
        if (dialog.options.id === 'migration-import-dialog') {
            dialog.options.dataProcessor = function (fields) {
                fields['importFile'] = undefined;
                return fields;
            };
            dialog.options.submitAjaxOptions = {
                jqueryAjaxFn: function (options) {
                    options.data['inline'] = undefined;
                    options.data['decorator'] = undefined;

                    const data = new FormData();
                    data.append('options', new Blob([JSON.stringify(options.data)], {type: "application/json"}),
                        'options.json');
                    data.append('importFile', $('#importFile')[0].files[0]);
                    options.data = data;

                    return jQuery.ajax(options);
                },
                cache: false,
                processData: false,
                contentType: false,
                dataType: false
            }
        }
    });
});
