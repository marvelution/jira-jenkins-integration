/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define('marvelution/form-dialog', ['jira/dialog/form-dialog', 'jquery', 'jira/flag', 'jira/message', 'jira/util/browser',
        'jira/util/events'],
    function (FormDialog, jQuery, Flags, Messages, Browser, Events) {
        return FormDialog.extend({
            _isRefreshAfterSubmit: function _isRefreshAfterSubmit() {
                if (this.options.refreshAfterSubmit === false || this.options.refreshAfterSubmit === 'false') {
                    return false;
                } else {
                    return true;
                }
            },
            _getDefaultOptions: function _getDefaultOptions() {
                return jQuery.extend(this._super(), {
                    autoClose: true,
                    widthClass: 'large',
                    refreshAfterSubmit: true,
                    ajaxOptions: {
                        dataType: 'html'
                    },
                    onDialogFinished: function () {
                        const targetUrl = this.options.targetUrl;
                        const refresh = this._isRefreshAfterSubmit();
                        const message = this.options.message;
                        if (message) {
                            this.showMessage(message, 'success', targetUrl || refresh);
                        }
                        if (targetUrl) {
                            Events.trigger('page-unload.location-change.from-dialog', [this.$popup]);
                            window.location.href = targetUrl;
                        } else if (refresh) {
                            Events.trigger('page-unload.refresh.from-dialog', [this.$popup]);
                            Browser.reloadViaWindowLocation();
                        }
                    },
                    submitAjaxOptions: {
                        jqueryAjaxFn: function (options) {
                            if (options.data) {
                                options.data['inline'] = undefined;
                                options.data['decorator'] = undefined;
                                options.data = JSON.stringify(options.data);
                            }
                            return jQuery.ajax(options);
                        },
                        contentType: 'application/json',
                        dataType: 'json'
                    }
                });
            },
            _getPath: function _getPath(action) {
                return action;
            },
            _submitForm: function _submitForm(e) {
                jQuery('.error', this.$form).empty();
                this.options.submitAjaxOptions.type = this.$form.attr('method') || 'post';
                this.options.submitAjaxOptions.headers = {
                    'X-Dialog-Id': this.options.id
                };
                this._super(e);
            },
            _getFormDataAsObject: function () {
                let fields = this._super();
                if (this.options.dataProcessor) {
                    fields = this.options.dataProcessor.call(this, fields);
                }
                return fields;
            },
            _showMessagesFromXhrResponse: function _showMessagesFromXhrResponse(xhr) {
                const message = xhr.getResponseHeader('X-Dialog-Message');
                if (message) {
                    if (this._isRefreshAfterSubmit()) {
                        this.showMessage(message, xhr.getResponseHeader('X-Dialog-Message-Type') || 'success', false);
                    }
                    this.options.message = message;
                }
            },
            showMessage: function showMessage(message, type, afterReload) {
                if (afterReload) {
                    Messages.showMsgOnReload(message, {
                        type: type.toUpperCase(),
                        closeable: true
                    });
                } else {
                    Flags.showMsg('', message, {type: type, close: 'auto'});
                }
            },
            _handleServerError: function _handleServerError(xhr, textStatus, errorThrown, smartAjaxResult) {
                if (this.options.onUnSuccessfulSubmit) {
                    this.options.onUnSuccessfulSubmit.call(xhr, textStatus, errorThrown, smartAjaxResult);
                }
                let data;
                try {
                    data = jQuery.parseJSON(xhr.responseText);
                } catch (e) {
                    data = {};
                }
                if (data.errors !== undefined) {
                    const $form = this.$form;
                    jQuery.each(data.errors, function (a, error) {
                        jQuery('input[name=' + error.field + ']:not([disabled])', $form).parent().find('.error').html(error.message);
                    });
                } else {
                    AJS.messages.error(this.$form, {
                        insert: 'prepend',
                        title: data.message !== undefined ? data.message : AJS.I18n.getText('unexpected.error.occurred')
                    });
                }
                jQuery(':submit', this.$form).removeAttr('disabled');
            },
            _handleServerSuccess: function _handleServerSuccess(data, xhr, textStatus, smartAjaxResult) {
                this.serverIsDone = true;
            },
            get$popupContent: function get$popupContent() {
                $popupContent = this._super();
                $popupContent.html = function(html) {
                    this.addRawHTML(html, 'append');
                };
                return $popupContent;
            }
        });
    });
