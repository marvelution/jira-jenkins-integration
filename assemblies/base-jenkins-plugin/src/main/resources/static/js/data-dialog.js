/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
require(['marvelution/form-dialog', 'jquery', 'require'],
    function (FormDialog, jQuery, require) {
        const wrmContextPath = require('wrm/context-path');
        const contextPath = wrmContextPath();

        jQuery(document).on('click', '[data-dialog-module]', function (event) {
            const $this = jQuery(this);
            if ($this.is(':disabled') || $this.attr('disabled')) {
                return;
            }

            event.preventDefault();

            const dialogId = $this.attr('data-dialog-module') + '-dialog';

            let dialogUrl = contextPath + '/rest/jenkins/latest/panel/' + dialogId;
            const data = {
                decorator: "dialog",
                inline: "false",
                dialog: dialogId
            };
            jQuery.each(this.attributes, function (index, attr) {
                if (attr.name === 'data-dialog-url') {
                    dialogUrl = attr.value;
                }
                if (attr.name.startsWith('data-dialog-module-')) {
                    data[attr.name.substring('data-dialog-module-'.length)] = attr.value;
                }
            });

            let options = {
                id: dialogId,
                targetUrl: $this.attr('data-dialog-target'),
                refreshAfterSubmit: $this.attr('data-dialog-refresh'),
                message: $this.attr('data-dialog-message'),
                ajaxOptions: {
                    url: dialogUrl,
                    data: data,
                }
            };
            const width = $this.attr('data-dialog-width');
            if (width) {
                options.width = width;
            } else {
                options.widthClass = $this.attr('data-dialog-size') || 'large';
            }
            const dialog = new FormDialog(options);
            dialog.show();
        });
    });
