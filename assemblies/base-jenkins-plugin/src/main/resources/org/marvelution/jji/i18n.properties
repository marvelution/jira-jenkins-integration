#
# Copyright (c) 2012-present Marvelution Holding B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

unknown.error.message = An unknown error occurred. Please try again, and if the error persists, contact your Jira Administrator.

# Configuration

url.invalid = Invalid URL, missing {0}.

jira.base.rpc.url = Jira Sync URL
jira.base.rpc.url.description = The URL that Jenkins should use for background communication with this Jira instance. This can be useful \
   if this Jira instance is running behind a proxy that he Jenkins server cannot access. Defaults to the Jira Base URL {0}.
use.all.explicit.builds.when.indexing = Use all explicit related builds.
use.job.latest.build.when.indexing = Only use the latest job build.
use.job.latest.build.when.indexing.description = The default behaviour is to only index the builds that are directly related to an Issue \
  in Jira. When this is checked then only the latest build of related jobs are used vs. all explicit related builds.
use.job.latest.build.when.indexing.warning.title = Changing this will require issue Reindexing!
use.job.latest.build.when.indexing.warning = Changing whether all builds vs latest build are indexing will require that issues are \
  reindexed. This is not done automatically since it may have a big impact. When changing this, please manually reindex Jira.
latest.vs.all.related = Latest Build vs. All Related Builds

all.vs.unknown = All Builds vs. Unknown Only Builds
sync.unknown.builds = Synchronize unknown builds only.
sync.all.builds = Synchronize all builds.
only.sync.unknown.builds.description = The default behaviour is to synchronize unknown builds only when synchronizing a job. This behaviour \
  can be altered to synchronize all builds available on Jenkins, by selecting ''Synchronize all builds''.

site.not.accessible=Site {0} ({1}) is not accessible for synchronization. Verify that any credentials are valid and that the ''Jenkins \
  Integration for Jira'' app can access the Jenkins site. If you''re Jenkins site is behind a firewall then select ''Inaccessible'' \
  accessibility.

cloud.migration.checks.jenkins-plugin=Jenkins Plugin Installed
cloud.migration.checks.jenkins-plugin.description=Checks whether the configured Jenkins sites have the required ''Jira \
  Integration'' plugin installed and of the minimum required version.
cloud.migration.checks.jenkins-plugin.verify-plugin=<paragraph>Some Jenkins sites are missing, or have an old version of the \
  required ''Jira Integration'' plugin. Follow these steps to fix those issues:</><unordered-item>Go to the Jenkins site mentioned in the \
  csv file.</><unordered-item>Navigate to ''Manage Jenkins'' > ''Manage Plugins''</><unordered-item>Install or Upgrade the ''Jira \
  Integration'' plugin to version 5.2.0 or newer.</>
