/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import org.marvelution.jji.data.synchronization.api.trigger.*;

import com.atlassian.sal.api.upgrade.*;

/**
 * {@link TriggerReason} for {@link PluginUpgradeTask}s.
 *
 * @author Mark Rekveld
 * @since 3.9.0
 */
public class UpgradeTaskTrigger extends TriggerReason {

	private int buildNumber;

	public UpgradeTaskTrigger(int buildNumber) {
		this.buildNumber = buildNumber;
	}

	public int getBuildNumber() {
		return buildNumber;
	}

	@Override
	public String describe() {
		return "upgrade task " + buildNumber;
	}
}
