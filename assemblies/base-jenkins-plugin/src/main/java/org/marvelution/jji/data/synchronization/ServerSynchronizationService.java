/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.api.AppState;
import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.api.utils.ExecutorUtils;
import org.marvelution.jji.data.access.DefaultSynchronizationResult;
import org.marvelution.jji.data.access.api.SynchronizationResultDAO;
import org.marvelution.jji.data.services.BaseSynchronizationService;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.synchronization.api.SynchronizationOperation;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.jji.data.synchronization.api.context.SynchronizationContextBuilder;
import org.marvelution.jji.data.synchronization.api.result.ImmutableSynchronizationResult;
import org.marvelution.jji.data.synchronization.api.trigger.TriggerReason;
import org.marvelution.jji.model.OperationId;
import org.marvelution.jji.model.Syncable;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.jira.cluster.ClusterInfo;
import com.atlassian.jira.cluster.ClusterMessageConsumer;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.util.thread.JiraThreadLocalUtil;
import com.atlassian.jira.util.thread.JiraThreadLocalUtilImpl;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import org.apache.log4j.Logger;

import static io.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static org.marvelution.jji.api.features.FeatureFlags.DC_LOCAL_QUEUEING_DISABLED;
import static org.marvelution.jji.api.features.FeatureFlags.DC_QUEUEING;

/**
 * Synchronization Service for Jira Server and Data Center deployments.
 *
 * @author Mark Rekveld
 * @since 3.6.0, refactored in 4.0.0
 */
@Named
@ExportAsService({SynchronizationService.class,
        LifecycleAware.class})
public class ServerSynchronizationService
        extends BaseSynchronizationService<DefaultSynchronizationResult>
        implements LifecycleAware
{
    static final String SYNC_CHANNEL = "jji.sync";
    private static final Logger log = Logger.getLogger(ServerSynchronizationService.class);
    private static final String STOP_SYNC_CHANNEL = "jji.stop.sync";
    private final List<SynchronizationOperation<?>> synchronizationOperations;
    private final SynchronizationResultDAO synchronizationResultDAO;
    private final SynchronizationContextBuilder<?, ?> synchronizationContextBuilder;
    private final ClusterInfo clusterInfo;
    private final Features features;
    private final ClusterMessagingService messagingService;
    private final ClusterLockService lockService;
    private final JiraThreadLocalUtil jiraThreadLocalUtil;
    private ExecutorService executorService;

    @Inject
    public ServerSynchronizationService(
            AppState appState,
            SiteClient siteClient,
            Executor asyncExecutor,
            List<SynchronizationOperation<?>> synchronizationOperations,
            SynchronizationResultDAO synchronizationResultDAO,
            OnPremiseSynchronizationContextBuilder synchronizationContextBuilder,
            @ComponentImport
            ClusterInfo clusterInfo,
            Features features,
            @ComponentImport
            ClusterMessagingService messagingService,
            @ComponentImport
            ClusterLockService lockService)
    {
        super(appState, siteClient, asyncExecutor);
        this.synchronizationOperations = synchronizationOperations;
        this.synchronizationResultDAO = synchronizationResultDAO;
        this.synchronizationContextBuilder = synchronizationContextBuilder.synchronizationService(this);
        this.clusterInfo = clusterInfo;
        this.features = features;
        this.messagingService = messagingService;
        this.lockService = lockService;
        jiraThreadLocalUtil = new JiraThreadLocalUtilImpl();
    }

    @Override
    public void onStart()
    {
        if (isExecutorShutdown())
        {
            logger.debug("Initializing new executor Service");
            executorService = newFixedThreadPool(5, namedThreadFactory("JenkinsSynchronizerExecutorServiceThread"));
            synchronizationResultDAO.findAllNonFinishedResultIds()
                    .forEach(resultId -> enqueueTask(new DefaultSynchronizationOperationTask(resultId)));
        }
    }

    @Override
    public void onStop()
    {
        if (!isExecutorShutdown())
        {
            logger.debug("Shutting down executor service");
            ExecutorUtils.shutdownExecutor(executorService);
        }
    }

    private boolean isExecutorShutdown()
    {
        return executorService == null || executorService.isShutdown();
    }

    private void enqueueTask(SynchronizationOperationTask operationTask)
    {
        if (!isExecutorShutdown())
        {
            logger.debug("Submitting synchronization request for {}, task type: {}",
                    operationTask.getResultId(),
                    operationTask.getClass()
                            .getName());
            executorService.submit(() -> {
                jiraThreadLocalUtil.preCall();
                try
                {
                    operationTask.run();
                }
                finally
                {
                    jiraThreadLocalUtil.postCall(log);
                }
            });
        }
        else
        {
            logger.debug("Ignoring synchronization request for {}, executor service is not ready.", operationTask.getResultId());
        }
    }

    @Override
    public Optional<ImmutableSynchronizationResult> getSynchronizationResult(String reference)
    {
        return synchronizationResultDAO.get(reference)
                .map(ImmutableSynchronizationResult.class::cast);
    }

    @Override
    public List<ImmutableSynchronizationResult> findLastResults(int limit)
    {
        return synchronizationResultDAO.findLastResults(limit);
    }

    @Override
    protected void stopSynchronization(DefaultSynchronizationResult result)
    {
        if (result.hasStarted())
        {
            if (clusterInfo.isClustered() && features.isFeatureEnabled(DC_QUEUEING))
            {
                messagingService.sendRemote(STOP_SYNC_CHANNEL, result.getId());
            }
            notifyStopRequest(result);
        }
        else
        {
            super.stopSynchronization(result);
            synchronizationResultDAO.persistStopRequest(result);
        }
    }

    @Override
    protected void enqueueSyncable(
            Syncable<?> syncable,
            DefaultSynchronizationResult result)
    {
        if (clusterInfo.isClustered())
        {
            if (!features.isFeatureEnabled(DC_LOCAL_QUEUEING_DISABLED))
            {
                enqueueSynchronizationRequest(result.getId());
            }
            else
            {
                logger.warn("Local queueing is disabled!");
            }
            if (features.isFeatureEnabled(DC_QUEUEING))
            {
                messagingService.sendRemote(SYNC_CHANNEL, result.getId());
            }
            else
            {
                logger.warn("DC queueing is disabled!");
            }
        }
        else
        {
            enqueueTask(new InMemorySynchronizationOperationTask(syncable, result));
        }
    }

    @Override
    protected Optional<DefaultSynchronizationResult> findLatestResult(OperationId operationId)
    {
        return synchronizationResultDAO.findLatestResult(operationId);
    }

    @Override
    protected DefaultSynchronizationResult createResult(
            OperationId operationId,
            TriggerReason triggerReason)
    {
        return synchronizationResultDAO.create(operationId, triggerReason);
    }

    private void notifyStopRequest(DefaultSynchronizationResult result)
    {
        ((ThreadPoolExecutor) executorService).getQueue()
                .stream()
                .filter(SynchronizationOperationTask.class::isInstance)
                .map(SynchronizationOperationTask.class::cast)
                .forEach(task -> {
                    DefaultSynchronizationResult current = task.current.get();
                    if (current != null && Objects.equals(current.getId(), result.getId()))
                    {
                        current.shouldStop();
                    }
                });
    }

    void enqueueSynchronizationRequest(String resultId)
    {
        enqueueTask(new ServerSynchronizationOperationTask(resultId));
    }

    private class InMemorySynchronizationOperationTask
            extends SynchronizationOperationTask
    {

        private final Syncable<?> syncable;
        private final DefaultSynchronizationResult result;

        private InMemorySynchronizationOperationTask(
                Syncable<?> syncable,
                DefaultSynchronizationResult result)
        {
            this.syncable = syncable;
            this.result = result;
        }

        @Override
        public String getResultId()
        {
            return result.getId();
        }

        @Override
        protected Optional<DefaultSynchronizationResult> result()
        {
            return Optional.of(result);
        }

        @Override
        @SuppressWarnings("unchecked")
        protected <S extends Syncable<S>> SynchronizationContextBuilder<S, ?> createSynchronizationContext(
                SynchronizationOperation<S> operation,
                OperationId operationId)
        {
            return synchronizationContextBuilder.syncable((S) syncable);
        }

        @Override
        protected Stream<SynchronizationOperation<?>> synchronizationOperations()
        {
            return synchronizationOperations.stream();
        }
    }

    protected class DefaultSynchronizationOperationTask
            extends SynchronizationOperationTask
    {

        private final String resultId;

        protected DefaultSynchronizationOperationTask(String resultId)
        {
            this.resultId = resultId;
        }

        @Override
        public String getResultId()
        {
            return resultId;
        }

        @Override
        protected Optional<DefaultSynchronizationResult> result()
        {
            return synchronizationResultDAO.get(resultId);
        }

        @Override
        protected <S extends Syncable<S>> SynchronizationContextBuilder<S, ?> createSynchronizationContext(
                SynchronizationOperation<S> operation,
                OperationId operationId)
        {
            return operation.locateSyncable(null, operationId)
                    .map(synchronizationContextBuilder::syncable)
                    .orElseThrow(() -> new UnsupportedOperationException("Unable to locate syncable for " + operationId));
        }

        @Override
        protected Stream<SynchronizationOperation<?>> synchronizationOperations()
        {
            return synchronizationOperations.stream();
        }
    }

    private class ServerSynchronizationOperationTask
            extends DefaultSynchronizationOperationTask
            implements ClusterMessageConsumer
    {

        private ClusterLock lock;

        private ServerSynchronizationOperationTask(String resultId)
        {
            super(resultId);
        }

        @Override
        protected Optional<DefaultSynchronizationResult> result()
        {
            String resultId = getResultId();
            lock = lockService.getLockForName(SYNC_CHANNEL + "." + resultId);

            return super.result()
                    .filter(result -> {
                        if (lock.tryLock())
                        {
                            return true;
                        }
                        else
                        {
                            logger.info("Skipping operation for {}, its already being processed by another node.", resultId);
                            return false;
                        }
                    });
        }

        @Override
        protected void beforeStarting()
        {
            messagingService.registerListener(STOP_SYNC_CHANNEL, this);
        }

        @Override
        protected void afterFinishing()
        {
            messagingService.unregisterListener(STOP_SYNC_CHANNEL, this);
            if (lock != null)
            {
                lock.unlock();
            }
        }

        @Override
        public void receive(
                String channel,
                String message,
                String senderId)
        {
            DefaultSynchronizationResult result = current.get();
            if (result != null)
            {
                if (Objects.equals(STOP_SYNC_CHANNEL, channel) && Objects.equals(result.getId(), message))
                {
                    logger.info("Received stop request for {} ({}) from {}", result.getId(), result.getOperationId(), senderId);
                    result.shouldStop();
                }
            }
        }
    }
}
