/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.cleanup;

import java.time.*;
import javax.annotation.*;
import javax.inject.*;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.services.api.*;

import com.atlassian.plugin.spring.scanner.annotation.export.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import com.atlassian.sal.api.lifecycle.*;
import com.atlassian.scheduler.*;

@Named
@ExportAsService(value = LifecycleAware.class)
public class CleanupOldDeletedData
		extends ScheduledCleanupTask
{

	private final ConfigurationService configurationService;
	private final JobDAO jobDAO;
	private final BuildDAO buildDAO;

	@Inject
	public CleanupOldDeletedData(
			@ComponentImport SchedulerService schedulerService,
			ConfigurationService configurationService,
			JobDAO jobDAO,
			BuildDAO buildDAO)
	{
		super(schedulerService);
		this.configurationService = configurationService;
		this.jobDAO = jobDAO;
		this.buildDAO = buildDAO;
	}

	@Override
	public JobRunnerResponse runJob(@Nonnull JobRunnerRequest request)
	{
		try
		{
			LocalDateTime timestamp = configurationService.getDeletedDataRetentionTimestamp();
			logger.info("Cleaning up data marked as deleted older then {}", timestamp);
			jobDAO.cleanupOldDeletedJobs(timestamp);
			buildDAO.cleanupOldDeletedBuilds(timestamp);
			return JobRunnerResponse.success("Cleaned up data marked as deleted older then " + timestamp);
		}
		catch (Exception e)
		{
			return JobRunnerResponse.failed(e);
		}
	}
}
