/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.admin;

import java.util.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.templating.*;

import com.atlassian.jira.security.request.*;

import static java.util.stream.Collectors.*;
import static org.apache.commons.lang3.StringUtils.*;

@SupportedMethods(RequestMethod.GET)
public class ConfigureJenkinsIntegrationSites
		extends JenkinsWebActionSupport
{

	private final SiteService siteService;
	private final JobService jobService;
	private String id;

	public ConfigureJenkinsIntegrationSites(
			SiteService siteService,
			JobService jobService,
			ThymeleafTemplateRenderer thymeleafTemplateRenderer)
	{
		super(thymeleafTemplateRenderer);
		this.siteService = siteService;
		this.jobService = jobService;
	}

	public String doSite()
	{
		if (!hasPermissions())
		{
			return PERMISSION_VIOLATION_RESULT;
		}
		else if (isBlank(id) || !siteService.get(id).isPresent())
		{
			return "unknown-site";
		}
		else
		{
			return "site";
		}
	}

	public String doJob()
	{
		if (!hasPermissions())
		{
			return PERMISSION_VIOLATION_RESULT;
		}
		else if (isBlank(id) || !jobService.get(id).isPresent())
		{
			return "unknown-job";
		}
		else
		{
			return "job";
		}
	}

	public List<Site> getSites()
	{
		return siteService.getAll().stream().sorted(Comparator.comparing(Site::getName)).collect(toList());
	}

	public Site getSite()
	{
		return siteService.getExisting(id, true);
	}

	public Job getJob()
	{
		return jobService.getExisting(id, true);
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}
}
