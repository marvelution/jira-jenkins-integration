/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.ConfigurationService;

import org.apache.commons.lang3.StringUtils;
import org.thymeleaf.context.IExpressionContext;

import static org.marvelution.jji.data.services.DefaultConfigurationService.JIRA_BASE_RPC_URL;

@Named
public class ServerConfig
        extends Config
{

    private static final String[] ADVANCED_KEYS = {JIRA_BASE_RPC_URL};

    @Inject
    public ServerConfig(ConfigurationService configurationService)
    {
        this(configurationService, null);
    }

    private ServerConfig(
            ConfigurationService configurationService,
            IExpressionContext expressionContext)
    {
        super(configurationService, expressionContext);
    }

    @Override
    public Config withContext(IExpressionContext context)
    {
        return new ServerConfig(configurationService, context);
    }

    @Override
    protected String[] additionalSettingKeys(
            String scope,
            String section)
    {
        return (StringUtils.isBlank(scope) && ADVANCED.equals(section)) ? ADVANCED_KEYS : null;
    }
}
