/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.*;
import javax.inject.*;

import org.marvelution.jji.api.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.*;
import org.marvelution.jji.synctoken.utils.*;

import com.atlassian.plugin.spring.scanner.annotation.export.*;
import com.atlassian.sal.api.message.*;
import com.atlassian.sal.api.upgrade.*;
import org.slf4j.*;

@Named
@ExportAsService(PluginUpgradeTask.class)
public class T10_RegenerateSharedSecrets
		extends AbstractUpgradeTask
{

	private static final Logger LOGGER = LoggerFactory.getLogger(T10_RegenerateSharedSecrets.class);
	private final SiteService siteService;
	private final SynchronizationService synchronizationService;

	@Inject
	public T10_RegenerateSharedSecrets(
			AppState appState,
			SiteService siteService,
			SynchronizationService synchronizationService)
	{
		super(appState);
		this.siteService = siteService;
		this.synchronizationService = synchronizationService;
	}

	@Override
	public Collection<Message> doUpgrade()
	{
		siteService.getAll()
				.stream()
				.peek(site -> {
					LOGGER.info("Regenerating shared secret for site {} ({})", site.getName(), site.getId());
					site.setSharedSecret(SharedSecretGenerator.generate());
					site.setJenkinsPluginInstalled(false);
					site.setUseCrumbs(true);
				})
				.map(siteService::save)
				.forEach(syncable -> synchronizationService.synchronize(syncable, new UpgradeTaskTrigger(getBuildNumber())));
		return null;
	}
}
