/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.services.BulkRequestService;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.jji.data.synchronization.api.trigger.ManualSynchronizationTrigger;
import org.marvelution.jji.data.synchronization.api.trigger.SiteConnectedTrigger;
import org.marvelution.jji.data.synchronization.api.trigger.SiteUpdatedTrigger;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteAuthentication;
import org.marvelution.jji.model.request.ConnectSiteRequest;
import org.marvelution.jji.model.request.EnabledState;
import org.marvelution.jji.rest.security.RequiresPermission;
import org.marvelution.jji.data.services.ServerPermissionService;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static org.marvelution.jji.data.services.api.model.PermissionKeys.ADMINISTER_PROJECTS;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.marvelution.jji.Headers.DIALOG_MESSAGE_HEADER;
import static org.marvelution.jji.Headers.SYNC_RESULT_HEADER;

@Named
@RequiresPermission(ADMINISTER_PROJECTS)
@Path("site")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SiteResource
{

    private final SiteService siteService;
    private final SiteClient siteClient;
    private final SynchronizationService synchronizationService;
    private final TextResolver textResolver;
    private final BulkRequestService bulkRequestService;
    private final JiraAuthenticationContext authenticationContext;
    private final ServerPermissionService permissions;

    @Inject
    public SiteResource(
            SiteService siteService,
            SiteClient siteClient,
            SynchronizationService synchronizationService,
            TextResolver textResolver,
            BulkRequestService bulkRequestService,
            @ComponentImport
            JiraAuthenticationContext authenticationContext,
            ServerPermissionService permissions)
    {
        this.siteService = siteService;
        this.siteClient = siteClient;
        this.synchronizationService = synchronizationService;
        this.textResolver = textResolver;
        this.bulkRequestService = bulkRequestService;
        this.authenticationContext = authenticationContext;
        this.permissions = permissions;
    }

    @GET
    public List<Site> getAll(
            @QueryParam("includeJobs")
            @DefaultValue("false")
            boolean includeJobs)
    {
        return permissions.filterPermittedSites(authenticationContext.getLoggedInUser(), siteService.getAll(includeJobs));
    }

    @GET
    @Path("{siteId}")
    public Response get(
            @PathParam("siteId")
            String siteId,
            @QueryParam("includeJobs")
            @DefaultValue("false")
            boolean includeJobs,
            @QueryParam("jobSearchTerm")
            @DefaultValue("")
            String term)
    {
        Site site = siteService.getExisting(siteId, includeJobs, term);
        if (!permissions.isAdministrator(authenticationContext.getLoggedInUser(), site))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        return Response.ok(site)
                .build();
    }

    @POST
    public Response addSite(
            @QueryParam("validateFields")
            List<String> validateFields,
            Site site,
            @HeaderParam("X-Dialog-Id")
            @DefaultValue("")
            String dialog)
    {
        if (!permissions.isAdministrator(authenticationContext.getLoggedInUser(), site))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        if (validateFields != null && !validateFields.isEmpty())
        {
            siteService.validateSite(site, validateFields.toArray(new String[0]));
            return Response.noContent()
                    .build();
        }
        else
        {
            Site added = siteService.add(site);
            Response.ResponseBuilder response = Response.ok(added);
            if (isNotBlank(dialog))
            {
                response.header(DIALOG_MESSAGE_HEADER, textResolver.getText("created.site.successful", added.getName()));
            }
            return response.build();
        }
    }

    @POST
    @Path("{siteId}")
    public Response updateSite(
            @PathParam("siteId")
            String siteId,
            @QueryParam("validateFields")
            List<String> validateFields,
            Site site,
            @HeaderParam("X-Dialog-Id")
            @DefaultValue("")
            String dialog)
    {
        Site oldSite = siteService.getExisting(siteId);
        if (!permissions.isAdministrator(authenticationContext.getLoggedInUser(), oldSite) || !permissions.isAdministrator(
                authenticationContext.getLoggedInUser(),
                site))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        if (validateFields != null && !validateFields.isEmpty())
        {
            siteService.validateSite(site, validateFields.toArray(new String[0]));
            return Response.noContent()
                    .build();
        }
        else
        {
            Site updated = siteService.update(siteId, site);
            Optional<String> resultId = synchronizationService.synchronize(updated,
                    new SiteUpdatedTrigger(site.getId(),
                            authenticationContext.getLoggedInUser()
                                    .getKey()));
            Response.ResponseBuilder response = Response.ok(updated);
            if (isNotBlank(dialog))
            {
                response.header(DIALOG_MESSAGE_HEADER, textResolver.getText("updated.site.successful", site.getName()))
                        .header(SYNC_RESULT_HEADER, resultId.orElse(null));
            }
            return response.build();
        }
    }

    @DELETE
    @Path("{siteId}")
    public Response deleteSite(
            @PathParam("siteId")
            String siteId)
    {
        Site site = siteService.getExisting(siteId);
        if (!permissions.isAdministrator(authenticationContext.getLoggedInUser(), site))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        bulkRequestService.deleteSite(site);
        return Response.noContent()
                .build();
    }

    @POST
    @Path("/{siteId}/connect")
    public Response connectSite(
            @PathParam("siteId")
            String siteId,
            ConnectSiteRequest request)
    {
        Site site = siteService.getExisting(siteId);
        if (!permissions.isAdministrator(authenticationContext.getLoggedInUser(), site))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        int status;
        switch (request.getMethod())
        {
            case automatic:
                if (siteClient.registerWithSite(site,
                        new SiteAuthentication.BasicSiteAuthentication(request.getAdminUser(), request.getAdminToken())))
                {
                    siteService.save(site.setJenkinsPluginInstalled(true)
                            .setRegistrationComplete(true));
                    status = Response.Status.ACCEPTED.getStatusCode();
                }
                else
                {
                    status = 424; //FAILED_DEPENDENCY
                }
                break;
            case manual:
                if (site.isRegistrationComplete())
                {
                    status = Response.Status.ACCEPTED.getStatusCode();
                }
                else
                {
                    status = 417; //EXPECTATION_FAILED
                }
                break;
            case casc:
                siteService.save(site.setJenkinsPluginInstalled(true)
                        .setRegistrationComplete(true));
                status = Response.Status.ACCEPTED.getStatusCode();
                break;
            default:
                status = Response.Status.BAD_REQUEST.getStatusCode();
                break;
        }
        Response.ResponseBuilder responseBuilder = Response.status(status);
        if (status == Response.Status.ACCEPTED.getStatusCode())
        {
            Optional<String> resultId = synchronizationService.synchronize(site,
                    new SiteConnectedTrigger(site.getId(),
                            authenticationContext.getLoggedInUser()
                                    .getKey()));
            return responseBuilder.header(SYNC_RESULT_HEADER, resultId.orElse(null))
                    .build();
        }
        else
        {
            return responseBuilder.build();
        }
    }

    @POST
    @Path("{siteId}/sync")
    public Response synchronizeSite(
            @PathParam("siteId")
            String siteId)
    {
        Site site = siteService.getExisting(siteId);
        if (!permissions.isAdministrator(authenticationContext.getLoggedInUser(), site))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        Optional<String> resultId = synchronizationService.synchronize(site,
                new ManualSynchronizationTrigger(authenticationContext.getLoggedInUser()
                        .getKey()));
        return Response.noContent()
                .header(SYNC_RESULT_HEADER, resultId.orElse(null))
                .build();
    }

    @POST
    @Path("{siteId}/autolink/{enabled}")
    public Response enableAutoLinkNewJobs(
            @PathParam("siteId")
            String siteId,
            @PathParam("enabled")
            boolean enabled)
    {
        Site site = siteService.getExisting(siteId);
        if (!permissions.isAdministrator(authenticationContext.getLoggedInUser(), site))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        siteService.automaticallyEnableNewJobs(site.getId(), enabled);
        return Response.noContent()
                .build();
    }

    @POST
    @Path("{siteId}/autolink")
    public Response enableAutoLinkNewJobs(
            @PathParam("siteId")
            String siteId,
            EnabledState enabled)
    {
        return enableAutoLinkNewJobs(siteId, enabled.isEnabled());
    }

    @POST
    @Path("{siteId}/enable/{enabled}")
    public Response enableSite(
            @PathParam("siteId")
            String siteId,
            @PathParam("enabled")
            boolean enabled)
    {
        Site site = siteService.getExisting(siteId);
        if (!permissions.isAdministrator(authenticationContext.getLoggedInUser(), site))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        siteService.enableSite(site.getId(), enabled);
        return Response.noContent()
                .build();
    }

    @POST
    @Path("{siteId}/enable")
    public Response enableSite(
            @PathParam("siteId")
            String siteId,
            EnabledState enabled)
    {
        return enableSite(siteId, enabled.isEnabled());
    }

    @GET
    @Path("{siteId}/status")
    public Response siteStatus(
            @PathParam("siteId")
            String siteId)
    {
        Site site = siteService.getExisting(siteId);
        if (!permissions.isAdministrator(authenticationContext.getLoggedInUser(), site))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        return Response.ok(siteService.getSiteStatus(site))
                .build();
    }

    @DELETE
    @Path("{siteId}/jobs")
    public Response removeJobs(
            @PathParam("siteId")
            String siteId)
    {
        Site site = siteService.getExisting(siteId);
        if (!permissions.isAdministrator(authenticationContext.getLoggedInUser(), site))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        bulkRequestService.deleteJobs(site);
        return Response.noContent()
                .build();
    }

    @GET
    @Path("wizard")
    @Produces(MediaType.TEXT_HTML)
    public String addSiteForm(
            @Context
            ResourceContext context,
            @Context
            UriInfo uriInfo,
            @Context
            HttpServletRequest request)
            throws IOException
    {
        return render("add-site", context, uriInfo, request);
    }

    @GET
    @Path("wizard/{siteId}")
    @Produces(MediaType.TEXT_HTML)
    public String editSiteForm(
            @Context
            ResourceContext context,
            @Context
            UriInfo uriInfo,
            @Context
            HttpServletRequest request)
            throws IOException
    {
        return render("edit-site", context, uriInfo, request);
    }

    @GET
    @Path("wizard/{siteId}/connect")
    @Produces(MediaType.TEXT_HTML)
    public String connectSite(
            @Context
            ResourceContext context,
            @Context
            UriInfo uriInfo,
            @Context
            HttpServletRequest request)
            throws IOException
    {
        return render("connect-site", context, uriInfo, request);
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String siteDialog(
            @QueryParam("dialog")
            String dialog,
            @Context
            ResourceContext context,
            @Context
            UriInfo uriInfo,
            @Context
            HttpServletRequest request)
            throws IOException
    {
        return render(dialog, context, uriInfo, request);
    }

    private String render(
            String location,
            @Context
            ResourceContext context,
            @Context
            UriInfo uriInfo,
            @Context
            HttpServletRequest request)
            throws IOException
    {
        return context.getResource(WebPanelResource.class)
                .render(location, uriInfo, request);
    }
}
