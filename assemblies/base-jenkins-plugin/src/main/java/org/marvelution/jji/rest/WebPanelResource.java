/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.marvelution.jji.data.services.api.SiteService;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.model.WebPanel;

@Named
@Path("panel")
@Produces(MediaType.TEXT_HTML)
public class WebPanelResource
{

    private final DynamicWebInterfaceManager webInterfaceManager;
    private final JiraAuthenticationContext authenticationContext;
    private final ProjectManager projectManager;
    private final IssueManager issueManager;
    private final SiteService siteService;

    @Inject
    public WebPanelResource(
            @ComponentImport
            DynamicWebInterfaceManager webInterfaceManager,
            @ComponentImport
            JiraAuthenticationContext authenticationContext,
            @ComponentImport
            ProjectManager projectManager,
            @ComponentImport
            IssueManager issueManager,
            SiteService siteService)
    {
        this.webInterfaceManager = webInterfaceManager;
        this.authenticationContext = authenticationContext;
        this.projectManager = projectManager;
        this.issueManager = issueManager;
        this.siteService = siteService;
    }

    @GET
    @Path("{location}")
    public String render(
            @PathParam("location")
            String location,
            @Context
            UriInfo uriInfo,
            @Context
            HttpServletRequest request)
            throws IOException
    {
        Map<String, Object> context = new HashMap<>();
        context.put("request", request);
        context.put("user", authenticationContext.getLoggedInUser());

        BiConsumer<String, List<String>> parameterConsumer = (key, values) -> {
            if (values.isEmpty())
            {
                context.put(key, Boolean.TRUE);
            }
            else if (values.size() == 1)
            {
                context.put(key, values.get(0));
            }
            else
            {
                context.put(key, values);
            }
        };
        uriInfo.getQueryParameters()
                .forEach(parameterConsumer);
        uriInfo.getPathParameters()
                .forEach(parameterConsumer);

        Project project = null;
        if (context.containsKey("issuekey"))
        {
            MutableIssue issue = issueManager.getIssueObject(String.valueOf(context.get("issuekey")));
            context.put("issue", issue);
            project = issue.getProjectObject();
            context.put("project", project);
        }
        else if (context.containsKey("projectkey"))
        {
            project = projectManager.getProjectObjByKey(String.valueOf(context.get("projectkey")));
            context.put("project", project);
        }
        context.put("helper", new JiraHelper(request, project));

        context.entrySet()
                .stream()
                .filter(entry -> "siteId".equalsIgnoreCase(entry.getKey()))
                .findFirst()
                .map(Map.Entry::getValue)
                .filter(String.class::isInstance)
                .map(String.class::cast)
                .flatMap(siteService::get)
                .ifPresent(site -> context.put("site", site));

        StringWriter writer = new StringWriter();
        for (WebPanel panel : webInterfaceManager.getDisplayableWebPanels(location, context))
        {
            panel.writeHtml(writer, context);
        }
        return writer.toString();
    }
}
