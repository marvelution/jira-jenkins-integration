/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf;

import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.marvelution.jji.templating.ThymeleafTemplateRenderer;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.templaterenderer.RenderingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.TemplateSpec;
import org.thymeleaf.context.AbstractExpressionContext;
import org.thymeleaf.context.ExpressionContext;
import org.thymeleaf.context.IContext;
import org.thymeleaf.context.WebExpressionContext;
import org.thymeleaf.exceptions.TemplateEngineException;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.spring5.expression.ThymeleafEvaluationContext;
import org.thymeleaf.standard.expression.FragmentExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.web.IWebExchange;
import org.thymeleaf.web.servlet.JavaxServletWebApplication;

import static org.marvelution.jji.templating.thymeleaf.dialect.ServerDialect.CONTAINER_ACCESSOR;
import static org.thymeleaf.standard.expression.FragmentExpression.ExecutedFragmentExpression;
import static org.thymeleaf.standard.expression.FragmentExpression.createExecutedFragmentExpression;
import static org.thymeleaf.standard.expression.StandardExpressions.getExpressionParser;

/**
 * @author Mark Rekveld
 * @since 3.7.0
 */
public class DefaultThymeleafTemplateRenderer
        implements ThymeleafTemplateRenderer
{

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultThymeleafTemplateRenderer.class);
    private final TemplateEngine templateEngine;
    private final HttpContext httpContext;
    private final JiraAuthenticationContext authenticationContext;

    DefaultThymeleafTemplateRenderer(
            TemplateEngine templateEngine,
            HttpContext httpContext,
            JiraAuthenticationContext authenticationContext)
    {
        this.templateEngine = templateEngine;
        this.httpContext = httpContext;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public void render(
            String templateName,
            Writer writer)
            throws RenderingException
    {
        render(templateName, new HashMap<>(), writer);
    }

    @Override
    public void render(
            String templateName,
            Map<String, Object> context,
            Writer writer)
            throws RenderingException
    {
        AbstractExpressionContext expressionContext = createContext(context);
        TemplateSpec templateSpec = resolveTemplate(templateName, expressionContext);
        render(templateSpec, expressionContext, writer);
    }

    @Override
    public boolean resolve(String templateName)
    {
        try
        {
            resolveTemplate(templateName, createContext(new HashMap<>()));
            return true;
        }
        catch (Exception ignored)
        {
            return false;
        }
    }

    private void render(
            TemplateSpec templateSpec,
            IContext expressionContext,
            Writer writer)
            throws RenderingException
    {
        try
        {
            templateEngine.process(templateSpec, expressionContext, writer);
        }
        catch (TemplateEngineException e)
        {
            throw new RenderingException(e);
        }
    }

    @Override
    public String renderFragment(
            String fragment,
            Map<String, Object> context)
            throws RenderingException
    {
        StringWriter writer = new StringWriter();
        render(fragment, context, writer);
        return writer.toString();
    }

    @Override
    public String renderFragment(String fragment)
            throws RenderingException
    {
        return renderFragment(fragment, new HashMap<>());
    }

    @Override
    public void render(
            TemplateSpec templateSpec,
            Map<String, Object> context,
            Writer writer)
            throws RenderingException
    {
        try
        {
            render(templateSpec, createContext(context), writer);
        }
        catch (TemplateEngineException e)
        {
            throw new RenderingException(e);
        }
    }

    private TemplateSpec resolveTemplate(
            String templateName,
            AbstractExpressionContext expressionContext)
    {
        String template;
        Set<String> markupSelectors;
        if (!templateName.contains("::"))
        {
            template = templateName;
            markupSelectors = null;
        }
        else
        {
            IStandardExpressionParser parser = getExpressionParser(expressionContext.getConfiguration());
            FragmentExpression fragmentExpression;
            try
            {
                fragmentExpression = (FragmentExpression) parser.parseExpression(expressionContext, "~{" + templateName + "}");
            }
            catch (TemplateProcessingException e)
            {
                throw new RenderingException("Invalid template name specification: '" + templateName + "'");
            }
            ExecutedFragmentExpression fragment = createExecutedFragmentExpression(expressionContext, fragmentExpression);
            template = FragmentExpression.resolveTemplateName(fragment);
            markupSelectors = FragmentExpression.resolveFragments(fragment);
            Map<String, Object> nameFragmentParameters = fragment.getFragmentParameters();
            if (nameFragmentParameters != null)
            {
                if (fragment.hasSyntheticParameters())
                {
                    throw new RenderingException(
                            "Parameters in a view specification must be named (non-synthetic): '" + templateName + "'");
                }
                expressionContext.setVariables(nameFragmentParameters);
            }
        }
        return new TemplateSpec(template, markupSelectors, TemplateMode.HTML, null);
    }

    private AbstractExpressionContext createContext(Map<String, Object> attributes)
    {
        HttpServletRequest request = httpContext.getRequest();
        HttpServletResponse response = httpContext.getResponse();

        ContainerAccessor containerAccessor = (ContainerAccessor) templateEngine.getConfiguration()
                .getExecutionAttributes()
                .get(CONTAINER_ACCESSOR);
        if (containerAccessor != null)
        {
            ThymeleafEvaluationContext evaluationContext = containerAccessor.createBean(ThymeleafEvaluationContextFactory.class)
                    .getObject();
            attributes.put(ThymeleafEvaluationContext.THYMELEAF_EVALUATION_CONTEXT_CONTEXT_VARIABLE_NAME, evaluationContext);
        }

        if (request != null && response != null)
        {
            LOGGER.debug("Creating new web context for request {}", request);
            IWebExchange webExchange = JavaxServletWebApplication.buildApplication(request.getServletContext())
                    .buildExchange(request, response);
            return new WebExpressionContext(templateEngine.getConfiguration(), webExchange, authenticationContext.getLocale(), attributes);
        }
        else
        {
            LOGGER.debug("No request, response and servletContext available, creating basic context");
            return new ExpressionContext(templateEngine.getConfiguration(), authenticationContext.getLocale(), attributes);
        }
    }

    private static class ThymeleafEvaluationContextFactory
            implements FactoryBean<ThymeleafEvaluationContext>
    {

        private final ApplicationContext applicationContext;

        public ThymeleafEvaluationContextFactory(ApplicationContext applicationContext)
        {
            this.applicationContext = applicationContext;
        }

        @Override
        public ThymeleafEvaluationContext getObject()
        {
            ConversionService conversionService = new DefaultConversionService();
            return new ThymeleafEvaluationContext(applicationContext, conversionService);
        }

        @Override
        public Class<?> getObjectType()
        {
            return ThymeleafEvaluationContext.class;
        }

        @Override
        public boolean isSingleton()
        {
            return false;
        }
    }
}
