/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.migration;

import java.time.*;
import java.util.concurrent.*;
import javax.inject.*;

import org.marvelution.jji.api.migration.*;
import org.marvelution.jji.api.text.*;
import org.marvelution.jji.api.utils.*;
import org.marvelution.jji.data.access.*;
import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.data.migration.*;

import com.atlassian.jira.util.thread.*;
import com.atlassian.plugin.spring.scanner.annotation.export.*;
import com.atlassian.sal.api.lifecycle.*;
import org.slf4j.*;

import static io.atlassian.util.concurrent.ThreadFactories.*;
import static java.util.concurrent.Executors.*;

@Named
@ExportAsService(LifecycleAware.class)
public class ServerAppMigrationManager
		extends BaseAppMigrationManager<ServerAppMigration, ServerAppMigrationDAO>
		implements LifecycleAware
{

	private static final Logger LOGGER = LoggerFactory.getLogger(ServerAppMigrationManager.class);
	private ExecutorService executorService;

	@Inject
	public ServerAppMigrationManager(
			AppMigrationService appMigrationService,
			ServerAppMigrationDAO appMigrationDAO,
			TextResolver textResolver)
	{
		super(appMigrationService, appMigrationDAO, textResolver);
	}

	@Override
	public void onStart()
	{
		initialize();
	}

	@Override
	public void onStop()
	{
		destroy();
	}

	protected void initialize()
	{
		if (isExecutorShutdown())
		{
			LOGGER.debug("Initializing new executor Service");
			executorService = newFixedThreadPool(5, namedThreadFactory("JenkinsMigratorExecutorServiceThread"));
		}
	}

	protected void destroy()
	{
		if (!isExecutorShutdown())
		{
			LOGGER.debug("Shutting down executor service");
			ExecutorUtils.shutdownExecutor(executorService);
		}
	}

	private boolean isExecutorShutdown()
	{
		return executorService == null || executorService.isShutdown();
	}

	@Override
	protected <O extends MigrationOptions<O>> ServerAppMigration createMigrationEvent(
			String id,
			LocalDateTime createdAt,
			AppMigrationType type,
			ProgressStatus progressStatus,
			O options)
	{
		return new ServerAppMigration(id, options.getName(), createdAt, type, progressStatus, options);
	}

	@Override
	protected void performMigration(ServerAppMigration migration)
	{
		executorService.submit(JiraThreadLocalUtils.wrap(() -> {
			FutureTask<Void> task = new FutureTask<>(() -> {
				executeMigration(migration);
				return null;
			});
			new Thread(task, "migration" + migration.getId()).start();
			try
			{
				task.get(1, TimeUnit.HOURS);
			}
			catch (Exception e)
			{
				LOGGER.error("Failed to execute migration within 1 hour", e);
			}
		}));
	}
}
