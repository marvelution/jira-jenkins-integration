/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.*;
import javax.inject.*;

import org.marvelution.jji.api.*;
import org.marvelution.jji.data.services.api.*;

import com.atlassian.plugin.spring.scanner.annotation.export.*;
import com.atlassian.sal.api.message.*;
import com.atlassian.sal.api.upgrade.*;
import org.slf4j.*;

/**
 * {@link PluginUpgradeTask} to generate link statistics for all links.
 *
 * @author Mark Rekveld
 * @since 4.5.0
 */
@Named
@ExportAsService(PluginUpgradeTask.class)
public class T11_LinkStatistics
		extends AbstractUpgradeTask
{

	private static final Logger LOGGER = LoggerFactory.getLogger(T11_LinkStatistics.class);
	private final IssueLinkService issueLinkService;

	@Inject
	public T11_LinkStatistics(
			AppState appState,
			IssueLinkService issueLinkService)
	{
		super(appState);
		this.issueLinkService = issueLinkService;
	}

	@Override
	public Collection<Message> doUpgrade()
	{
		issueLinkService.rebuildLinkStatistics();
		LOGGER.warn("Reindexing of Jira issues is required in order to utilize the new Jenkins build link statistics!");
		return null;
	}
}
