/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.workflow;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;

import org.marvelution.jji.data.services.JobTriggerService;
import org.marvelution.jji.model.request.TriggerBuildsRequest;
import org.marvelution.jji.model.request.WorkflowPostFunctionConfiguration;
import org.marvelution.jji.templating.ThymeleafTemplateRenderer;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;

public class TriggerBuildPostFunctionFactory
        extends AbstractWorkflowPluginFactory
        implements WorkflowPluginFunctionFactory
{

    private final JobTriggerService jobTriggerService;
    private final ThymeleafTemplateRenderer thymeleaf;

    @Inject
    public TriggerBuildPostFunctionFactory(
            JobTriggerService jobTriggerService,
            ThymeleafTemplateRenderer thymeleaf)
    {
        this.jobTriggerService = jobTriggerService;
        this.thymeleaf = thymeleaf;
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> params)
    {
        params.put("thymeleaf", thymeleaf);
        params.put("config", WorkflowPostFunctionConfiguration.blank());
    }

    @Override
    protected void getVelocityParamsForEdit(
            Map<String, Object> params,
            AbstractDescriptor descriptor)
    {
        populateVelocityParams(params, getFunctionDescriptor(descriptor));
    }

    @Override
    protected void getVelocityParamsForView(
            Map<String, Object> params,
            AbstractDescriptor descriptor)
    {
        populateVelocityParams(params, getFunctionDescriptor(descriptor));
    }

    private void populateVelocityParams(
            Map<String, Object> params,
            FunctionDescriptor descriptor)
    {
        params.put("thymeleaf", thymeleaf);
        String config = (String) descriptor.getArgs()
                .get("config");
        WorkflowPostFunctionConfiguration configuration = WorkflowPostFunctionConfiguration.parse("DUMMY-1",
                config,
                jobTriggerService::getTriggerableJob);
        params.put("config", configuration);
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> params)
    {
        Map<String, String> descriptorParams = new HashMap<>();
        String type = extractSingleParam(params, "type");
        String config;
        if (TriggerBuildsRequest.SPECIFIC_JOB.equals(type))
        {
            config = extractSingleParam(params, "specific-job");
        }
        else if (TriggerBuildsRequest.BY_ISSUE_FIELD.equals(type))
        {
            boolean exactMatch = false;
            Object matching = params.get("exact-match");
            if (matching instanceof String[]) {
                exactMatch = Boolean.parseBoolean(((String[])matching)[0]);
            }
            config = TriggerBuildsRequest.BY_ISSUE_FIELD + (exactMatch ? ";;" : "::") + extractSingleParam(params, "issue-field") +
                     extractSingleParam(params, "issue-field-mapping");
        }
        else
        {
            config = type;
        }
        descriptorParams.put("config", config);
        return descriptorParams;
    }

    private FunctionDescriptor getFunctionDescriptor(AbstractDescriptor descriptor)
    {
        if (!(descriptor instanceof FunctionDescriptor))
        {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }
        return (FunctionDescriptor) descriptor;
    }
}
