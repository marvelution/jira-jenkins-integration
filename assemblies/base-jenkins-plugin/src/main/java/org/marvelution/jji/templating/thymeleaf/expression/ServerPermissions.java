/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import javax.inject.*;

import org.marvelution.jji.data.services.ServerPermissionService;
import org.marvelution.jji.data.services.api.*;

import com.atlassian.jira.security.*;
import com.atlassian.jira.user.*;
import org.thymeleaf.context.*;

@Named
public class ServerPermissions
		extends Permissions<ApplicationUser>
{

	private final JiraAuthenticationContext authenticationContext;

	@Inject
	public ServerPermissions(
			ServerPermissionService permissionService,
			JiraAuthenticationContext authenticationContext)
	{
		this(permissionService, authenticationContext, null);
	}

	private ServerPermissions(
			PermissionService<ApplicationUser> permissionService,
			JiraAuthenticationContext authenticationContext,
			IExpressionContext expressionContext)
	{
		super(permissionService, expressionContext);
		this.authenticationContext = authenticationContext;
	}

	@Override
	public Permissions withContext(IExpressionContext context)
	{
		return new ServerPermissions(permissionService, authenticationContext, context);
	}

	@Override
	protected ApplicationUser getLoggedInUser()
	{
		return authenticationContext.getLoggedInUser();
	}
}
