/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf;

import javax.inject.*;
import java.io.*;
import java.util.*;
import java.util.function.Supplier;

import org.marvelution.jji.templating.*;

import com.atlassian.plugin.osgi.bridge.external.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import com.atlassian.templaterenderer.*;
import com.google.common.base.*;
import org.thymeleaf.*;

@Named
public class DelegatingThymeleafTemplateRenderer
        implements ThymeleafTemplateRenderer
{

    private final Supplier<ThymeleafTemplateRenderer> delegate;

    @Inject
    public DelegatingThymeleafTemplateRenderer(
            ThymeleafTemplateRendererFactory templateRendererFactory,
            @ComponentImport
            PluginRetrievalService pluginRetrievalService)
    {
        delegate = Suppliers.memoize(() -> templateRendererFactory.getTemplateEngine(pluginRetrievalService.getPlugin()));
    }

    @Override
    public void render(
            String templateName,
            Writer writer)
            throws RenderingException, IOException
    {
        delegate.get()
                .render(templateName, writer);
    }

    @Override
    public void render(
            String templateName,
            Map<String, Object> context,
            Writer writer)
            throws RenderingException, IOException
    {
        delegate.get()
                .render(templateName, context, writer);
    }

    @Override
    public String renderFragment(
            String fragment,
            Map<String, Object> context)
            throws RenderingException
    {
        return delegate.get()
                .renderFragment(fragment, context);
    }

    @Override
    public String renderFragment(String fragment)
            throws RenderingException
    {
        return delegate.get()
                .renderFragment(fragment);
    }

    @Override
    public void render(
            TemplateSpec templateSpec,
            Map<String, Object> context,
            Writer writer)
            throws RenderingException, IOException
    {
        delegate.get()
                .render(templateSpec, context, writer);
    }

    @Override
    public boolean resolve(String templateName)
    {
        return delegate.get()
                .resolve(templateName);
    }
}
