/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.context;

import java.util.*;

import org.marvelution.jji.api.migration.*;
import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.migration.*;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.*;

public class MigrationContextProvider
        implements ContextProvider
{

    private final ServerAppMigrationManager appMigrationManager;

    public MigrationContextProvider(ServerAppMigrationManager appMigrationManager)
    {
        this.appMigrationManager = appMigrationManager;
    }

    @Override
    public void init(Map<String, String> params)
            throws PluginParseException
    {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context)
    {
        String migrationId = (String) context.get("id");
        ServerAppMigration appMigration = appMigrationManager.getMigration(migrationId)
                .orElseThrow(() -> new IllegalArgumentException("unknown migration id"));
        Map<String, Object> templateContext = new HashMap<>();
        templateContext.put("migration", appMigration);
        Optional.ofNullable(appMigration.getFeedback())
                .map(Feedback::getDetails)
                .ifPresent(templateContext::putAll);
        templateContext.put("downloadLink", appMigrationManager.getDownloadLink(appMigration));
        return templateContext;
    }
}
