/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.dialect;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.dialect.IExecutionAttributeDialect;

/**
 * @author Mark Rekveld
 * @since 3.7.0
 */
public class ServerDialect extends AbstractDialect implements IExecutionAttributeDialect {

	public static final String CONTAINER_ACCESSOR = "containerAccessor";
	public static final String PLUGIN = "plugin";
	private final Plugin plugin;

	public ServerDialect(Plugin plugin) {
		super("server");
		this.plugin = plugin;
	}

	@Override
	public Map<String, Object> getExecutionAttributes() {
		Map<String, Object> attributes = new HashMap<>();
		attributes.put(PLUGIN, plugin);
		Optional.of(plugin)
		        .filter(ContainerManagedPlugin.class::isInstance)
		        .map(ContainerManagedPlugin.class::cast)
		        .map(ContainerManagedPlugin::getContainerAccessor)
		        .ifPresent(containerAccessor -> attributes.put(CONTAINER_ACCESSOR, containerAccessor));
		return attributes;
	}
}
