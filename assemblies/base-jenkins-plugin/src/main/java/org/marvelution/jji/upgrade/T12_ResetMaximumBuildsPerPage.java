/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.*;
import javax.inject.*;

import org.marvelution.jji.api.*;
import org.marvelution.jji.data.services.*;
import org.marvelution.jji.model.*;

import com.atlassian.plugin.spring.scanner.annotation.export.*;
import com.atlassian.sal.api.message.*;
import com.atlassian.sal.api.upgrade.*;

import static org.marvelution.jji.data.services.api.ConfigurationService.MAX_BUILDS_PER_PAGE;

/**
 * {@link PluginUpgradeTask} to reset the maximum builds per page setting.
 *
 * @author Mark Rekveld
 * @since 4.7.1
 */
@Named
@ExportAsService(PluginUpgradeTask.class)
public class T12_ResetMaximumBuildsPerPage
		extends AbstractUpgradeTask
{

	private final DefaultConfigurationService configurationService;

	@Inject
	public T12_ResetMaximumBuildsPerPage(
			AppState appState,
			DefaultConfigurationService configurationService)
	{
		super(appState);
		this.configurationService = configurationService;
	}

	@Override
	public Collection<Message> doUpgrade()
	{
		ConfigurationSetting setting = configurationService.getConfigurationSetting(MAX_BUILDS_PER_PAGE);
		configurationService.saveConfigurationSetting(setting);
		return null;
	}
}
