/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.marvelution.jji.Headers;
import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.services.JobTriggerService;
import org.marvelution.jji.data.services.BulkRequestService;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.api.exceptions.UnknownJobException;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.jji.data.synchronization.api.trigger.ManualSynchronizationTrigger;
import org.marvelution.jji.data.synchronization.api.trigger.ResetBuildCacheTrigger;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.request.BulkRequest;
import org.marvelution.jji.model.request.EnabledState;
import org.marvelution.jji.model.request.TriggerBuildsRequest;
import org.marvelution.jji.model.response.TriggerBuildsResponse;
import org.marvelution.jji.rest.security.RequiresPermission;
import org.marvelution.jji.data.services.ServerPermissionService;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static org.marvelution.jji.data.services.api.model.PermissionKeys.ADMINISTER_PROJECTS;
import static org.marvelution.jji.data.services.api.model.PermissionKeys.TRIGGER_JENKINS_BUILD;

@Named
@Path("job")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JobResource
{

    private final JobService jobService;
    private final SynchronizationService synchronizationService;
    private final BulkRequestService bulkRequestService;
    private final JobTriggerService jobTriggerService;
    private final TextResolver textResolver;
    private final ServerPermissionService permissionService;
    private final JiraAuthenticationContext authenticationContext;

    @Inject
    public JobResource(
            JobService jobService,
            SynchronizationService synchronizationService,
            BulkRequestService bulkRequestService,
            JobTriggerService jobTriggerService,
            TextResolver textResolver,
            ServerPermissionService permissionService,
            @ComponentImport
            JiraAuthenticationContext authenticationContext)
    {
        this.jobService = jobService;
        this.synchronizationService = synchronizationService;
        this.bulkRequestService = bulkRequestService;
        this.jobTriggerService = jobTriggerService;
        this.textResolver = textResolver;
        this.permissionService = permissionService;
        this.authenticationContext = authenticationContext;
    }

    @GET
    @Path("{jobId}")
    @RequiresPermission(ADMINISTER_PROJECTS)
    public Response getJob(
            @PathParam("jobId")
            String jobId)
    {
        Job job = jobService.getExisting(jobId);
        if (!permissionService.isAdministrator(authenticationContext.getLoggedInUser(), job))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        return Response.ok(job)
                .build();
    }

    @GET
    @Path("search")
    @RequiresPermission(ADMINISTER_PROJECTS)
    public List<Job> search(
            @QueryParam("term")
            String term)
    {
        return permissionService.filterPermittedJobs(authenticationContext.getLoggedInUser(), jobService.search(term));
    }

    @DELETE
    @Path("{jobId}")
    @RequiresPermission(ADMINISTER_PROJECTS)
    public Response deleteJob(
            @PathParam("jobId")
            String jobId)
    {
        Job job = jobService.getExisting(jobId);
        if (!permissionService.isAdministrator(authenticationContext.getLoggedInUser(), job))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        bulkRequestService.deleteJob(job);
        return Response.noContent()
                .build();
    }

    @PUT
    @Path("{jobId}/sync")
    @RequiresPermission(ADMINISTER_PROJECTS)
    public Response synchronizeJob(
            @PathParam("jobId")
            String jobId)
    {
        Job job = jobService.getExisting(jobId);
        if (!permissionService.isAdministrator(authenticationContext.getLoggedInUser(), job))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        synchronizationService.synchronize(job,
                new ManualSynchronizationTrigger(authenticationContext.getLoggedInUser()
                        .getKey()));
        return Response.noContent()
                .build();
    }

    @POST
    @Path("{jobId}/link/{enabled}")
    @RequiresPermission(ADMINISTER_PROJECTS)
    public Response enableLink(
            @PathParam("jobId")
            String jobId,
            @PathParam("enabled")
            boolean enabled)
    {
        Job job = jobService.getExisting(jobId);
        if (!permissionService.isAdministrator(authenticationContext.getLoggedInUser(), job))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        jobService.enable(job, enabled);
        return Response.noContent()
                .build();
    }

    @POST
    @Path("{jobId}/link")
    @RequiresPermission(ADMINISTER_PROJECTS)
    public Response enableLink(
            @PathParam("jobId")
            String jobId,
            EnabledState enabled)
    {
        return enableLink(jobId, enabled.isEnabled());
    }

    @DELETE
    @Path("{jobId}/builds")
    @RequiresPermission(ADMINISTER_PROJECTS)
    public Response deleteAllBuilds(
            @PathParam("jobId")
            String jobId)
    {
        Job job = jobService.getExisting(jobId);
        if (!permissionService.isAdministrator(authenticationContext.getLoggedInUser(), job))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        bulkRequestService.deleteBuilds(job);
        return Response.noContent()
                .build();
    }

    @POST
    @Path("{jobId}/rebuild")
    @RequiresPermission(ADMINISTER_PROJECTS)
    public Response rebuildJobCache(
            @PathParam("jobId")
            String jobId)
    {
        Job job = jobService.getExisting(jobId);
        if (!permissionService.isAdministrator(authenticationContext.getLoggedInUser(), job))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        jobService.resetBuildCache(job);
        synchronizationService.synchronize(job,
                new ResetBuildCacheTrigger(authenticationContext.getLoggedInUser()
                        .getKey()));
        return Response.noContent()
                .build();
    }

    @GET
    @Path("build")
    @Produces(MediaType.TEXT_HTML)
    @RequiresPermission(TRIGGER_JENKINS_BUILD)
    public String triggerBuildDialog(
            @Context
            ResourceContext context,
            @Context
            UriInfo uriInfo,
            @Context
            HttpServletRequest request)
            throws IOException
    {
        return context.getResource(WebPanelResource.class)
                .render("jenkins-trigger-build-dialog", uriInfo, request);
    }

    @GET
    @Path("buildable")
    @RequiresPermission(TRIGGER_JENKINS_BUILD)
    public List<Job> findTriggerableJobs(
            @QueryParam("term")
            String term)
    {
        return jobTriggerService.findTriggerableJobs(term);
    }

    @GET
    @Path("buildable/{jobId}")
    @RequiresPermission(TRIGGER_JENKINS_BUILD)
    public Job getTriggerableJob(
            @PathParam("jobId")
            String jobId)
    {
        return jobTriggerService.getTriggerableJob(jobId)
                .orElseThrow(() -> new UnknownJobException(jobId));
    }

    @POST
    @Path("build")
    @RequiresPermission(TRIGGER_JENKINS_BUILD)
    public TriggerBuildsResponse triggerBuild(TriggerBuildsRequest request)
    {
        return jobTriggerService.triggerBuilds(request);
    }

    @POST
    @Path("bulk")
    @RequiresPermission(ADMINISTER_PROJECTS)
    public Response bulkRequest(BulkRequest request)
    {
        if (!permissionService.isSiteAdministrator(authenticationContext.getLoggedInUser()))
        {
            Set<String> projectKeys = permissionService.getPermittedProjects(authenticationContext.getLoggedInUser(), ADMINISTER_PROJECTS);
            if (projectKeys.isEmpty())
            {
                return Response.status(Response.Status.FORBIDDEN)
                        .build();
            }
            else
            {
                Set<Job> jobs = request.getJobIds()
                        .stream()
                        .map(jobService::get)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .filter(job -> projectKeys.contains(job.getSite()
                                .getScope()) || ConfigurationService.GLOBAL_SCOPE.equals(job.getSite()
                                .getScope()))
                        .collect(Collectors.toSet());
                request.setJobs(jobs);
            }
        }
        bulkRequestService.process(request);
        return Response.noContent()
                .header(Headers.DIALOG_MESSAGE_HEADER, textResolver.getText("bulk.request.started"))
                .build();
    }
}
