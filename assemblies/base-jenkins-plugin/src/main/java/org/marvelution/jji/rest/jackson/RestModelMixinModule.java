/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest.jackson;

import javax.inject.Named;

import org.marvelution.jji.data.services.api.model.AvatarUrls;
import org.marvelution.jji.data.services.api.model.IssueField;
import org.marvelution.jji.data.services.api.model.IssueType;
import org.marvelution.jji.data.services.api.model.Project;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;

@Named
@ExportAsService(Module.class)
public class RestModelMixinModule
        extends Module
{

    private static final Version VERSION = new Version(1, 0, 0, null, null, null);

    @Override
    public String getModuleName()
    {
        return "rest-model-mixins";
    }

    @Override
    public Version version()
    {
        return VERSION;
    }

    @Override
    public void setupModule(SetupContext context)
    {
        context.setMixInAnnotations(Project.class, ProjectMixin.class);
        context.setMixInAnnotations(AvatarUrls.class, AvatarUrlsMixin.class);
        context.setMixInAnnotations(IssueType.class, IssueTypeMixin.class);
        context.setMixInAnnotations(IssueField.class, IssueFieldMixin.class);
    }
}
