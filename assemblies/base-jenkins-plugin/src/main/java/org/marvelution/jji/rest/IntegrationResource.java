/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Providers;

import org.marvelution.jji.Headers;
import org.marvelution.jji.data.services.IntegrationApiService;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.events.JobNotificationType;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.response.SiteTunnelDetails;
import org.marvelution.jji.synctoken.SyncTokenAuthenticator;
import org.marvelution.jji.synctoken.SyncTokenRequest;
import org.marvelution.jji.synctoken.exceptions.InvalidSyncTokenException;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

import static org.marvelution.jji.data.services.IntegrationApiService.SYNC_RESULT_HEADER;

/**
 * REST resource for the Jenkins plugin, with endpoints to trigger the synchronization of new builds and get issue link information.
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
@Named
@UnrestrictedAccess
@Path("{p:integration|build}")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class IntegrationResource
{

    private final IntegrationApiService integrationApiService;
    private final SyncTokenAuthenticator syncTokenAuthenticator;
    private final SiteService siteService;

    @Inject
    public IntegrationResource(
            IntegrationApiService integrationApiService,
            SyncTokenAuthenticator syncTokenAuthenticator,
            SiteService siteService)
    {
        this.integrationApiService = integrationApiService;
        this.syncTokenAuthenticator = syncTokenAuthenticator;
        this.siteService = siteService;
    }

    /**
     * @since 5.16.0
     */
    @GET
    @Path("/tunnel/{siteId}")
    public Response getTunnelDetails(
            @PathParam("siteId")
            String siteId,
            @Context
            HttpServletRequest request)
    {
        Optional<SiteTunnelDetails> tunnelDetails = integrationApiService.getTunnelDetails(authenticateRequest(request, siteId));
        if (tunnelDetails.isPresent())
        {
            return Response.ok()
                    .entity(tunnelDetails.get())
                    .build();
        }
        else
        {
            return Response.status(Response.Status.NOT_FOUND)
                    .build();
        }
    }

    /**
     * @since 5.16.0
     */
    @GET
    @Path("/register/{siteId}")
    public Response getRegistrationDetails(
            @PathParam("siteId")
            String siteId,
            @Context
            HttpServletRequest request)
    {
        return Response.ok()
                .entity(integrationApiService.getRegistrationDetails(authenticateRequest(request, siteId))
                        .toString())
                .build();
    }

    /**
     * @since 3.10.0
     */
    @POST
    @Path("/register/{siteId}")
    public Response completeRegistration(
            @PathParam("siteId")
            String siteId,
            @Context
            HttpServletRequest request)
    {
        integrationApiService.completeRegistration(authenticateRequest(request, siteId));
        return Response.status(Response.Status.ACCEPTED)
                .build();
    }

    @DELETE
    @Path("/register/{siteId}")
    public Response incompleteRegistration(
            @PathParam("siteId")
            String siteId,
            @Context
            HttpServletRequest request)
    {
        integrationApiService.incompleteRegistration(authenticateRequest(request, siteId));
        return Response.status(Response.Status.ACCEPTED)
                .build();
    }

    /**
     * Trigger the synchronization of a specific {@link Job}.
     */
    @PUT
    @Path("{jobHash}")
    public Response synchronizeJob(
            @PathParam("jobHash")
            String jobHash,
            @Context
            HttpHeaders headers,
            @Context
            HttpServletRequest request)
    {
        return synchronizationResponse(() -> integrationApiService.synchronizeJob(authenticateRequest(request),
                jobHash,
                getJobNotificationType(headers)));
    }

    /**
     * Synchronize a specific {@link Job} from the data posted.
     *
     * @since 3.10.0
     */
    @POST
    @Path("{jobHash}")
    public Response synchronizeJob(
            @PathParam("jobHash")
            String jobHash,
            InputStream body,
            @Context
            HttpHeaders headers,
            @Context
            HttpServletRequest request)
    {
        return synchronizationResponse(() -> integrationApiService.synchronizeJob(authenticateRequest(request),
                jobHash,
                getJobNotificationType(headers),
                body,
                getCharset(headers)));
    }

    /**
     * Trigger the synchronization of a specific {@link Build}.
     *
     * @since 3.4.0
     */
    @PUT
    @Path("{jobHash}/{buildNumber}")
    @SuppressWarnings({"rawtypes",
            "unchecked"})
    public Response synchronizeBuild(
            @PathParam("jobHash")
            String jobHash,
            @PathParam("buildNumber")
            Integer buildNumber,
            @Context
            Providers providers,
            @Context
            ContainerRequestContext requestContext,
            @Context
            HttpServletRequest request)
    {
        return synchronizationResponse(() -> {
            List<String> parentHashes;
            try
            {
                MessageBodyReader<List> reader = providers.getMessageBodyReader(List.class,
                        List.class,
                        new Annotation[]{},
                        MediaType.APPLICATION_JSON_TYPE);
                parentHashes = (List<String>) reader.readFrom(List.class,
                        List.class,
                        new Annotation[]{},
                        MediaType.APPLICATION_JSON_TYPE,
                        requestContext.getHeaders(),
                        requestContext.getEntityStream());
            }
            catch (Exception e)
            {
                parentHashes = null;
            }

            return integrationApiService.synchronizeBuild(authenticateRequest(request), jobHash, buildNumber, parentHashes);
        });
    }

    /**
     * Synchronize a specific {@link Build} from the data posted.
     *
     * @since 3.10.0
     */
    @POST
    @Path("{jobHash}/{buildNumber}")
    public Response synchronizeBuild(
            @PathParam("jobHash")
            String jobHash,
            @PathParam("buildNumber")
            Integer buildNumber,
            InputStream body,
            @Context
            HttpHeaders headers,
            @Context
            HttpServletRequest request)
    {
        return synchronizationResponse(() -> integrationApiService.synchronizeBuild(authenticateRequest(request),
                jobHash,
                buildNumber,
                body,
                getCharset(headers)));
    }

    private JobNotificationType getJobNotificationType(HttpHeaders headers)
    {
        return Optional.ofNullable(headers.getRequestHeader(Headers.NOTIFICATION_TYPE))
                .flatMap(values -> values.stream()
                        .findFirst())
                .flatMap(header -> Stream.of(JobNotificationType.values())
                        .filter(type -> type.name()
                                                .equalsIgnoreCase(header) || type.value()
                                                .equalsIgnoreCase(header))
                        .findFirst())
                .orElse(null);
    }

    private Optional<Charset> getCharset(HttpHeaders headers)
    {
        return Optional.ofNullable(headers.getMediaType()
                        .getParameters()
                        .get("charset"))
                .map(Charset::forName);
    }

    private Response synchronizationResponse(Supplier<Optional<String>> function)
    {
        Optional<String> resultId = function.get();
        if (resultId.isPresent())
        {
            return Response.status(Response.Status.ACCEPTED)
                    .header(SYNC_RESULT_HEADER, resultId.get())
                    .build();
        }
        else
        {
            return Response.noContent()
                    .build();
        }
    }

    /**
     * Returns a list of issues linked to the specific {@link Build}.
     */
    @GET
    @Path("{jobHash}/{buildNumber}/links")
    public Map<String, String> getBuildLinks(
            @PathParam("jobHash")
            String jobHash,
            @PathParam("buildNumber")
            int buildNumber,
            @Context
            HttpServletRequest request)
    {
        return integrationApiService.getBuildLinks(authenticateRequest(request), jobHash, buildNumber);
    }

    /**
     * Mark the {@link Job} with the specified {@code jobHash} as deleted.
     *
     * @since 3.9.0
     */
    @DELETE
    @Path("{jobHash}")
    public void markJobAsDeleted(
            @PathParam("jobHash")
            String jobHash,
            @Context
            HttpServletRequest request)
    {
        integrationApiService.markJobAsDeleted(authenticateRequest(request), jobHash);
    }

    /**
     * Mark the specific {@link Build} of the {@link Job} with the specified {@code jobHash} as deleted.
     *
     * @since 3.9.0
     */
    @DELETE
    @Path("{jobHash}/{buildNumber}")
    public void markBuildAsDeleted(
            @PathParam("jobHash")
            String jobHash,
            @PathParam("buildNumber")
            int buildNumber,
            @Context
            HttpServletRequest request)
    {
        integrationApiService.markBuildAsDeleted(authenticateRequest(request), jobHash, buildNumber);
    }

    private Site authenticateRequest(HttpServletRequest request)
    {
        String issuer = syncTokenAuthenticator.authenticate(new SyncTokenRequest(request))
                .getIssuer();
        return siteService.getExisting(issuer);
    }

    private Site authenticateRequest(
            HttpServletRequest request,
            String siteId)
    {
        Site issuer = authenticateRequest(request);
        if (!Objects.equals(issuer.getId(), siteId))
        {
            throw new InvalidSyncTokenException("Site id mismatch");
        }
        return issuer;
    }
}
