/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import javax.inject.*;

import org.marvelution.jji.cluster.*;

import com.atlassian.jira.cluster.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;

import static org.marvelution.jji.data.synchronization.ServerSynchronizationService.*;

/**
 * {@link ClusterMessageConsumer} implementation that listens for synchronization requests.
 *
 * @author Mark Rekveld
 * @since 3.6.0
 */
@Named
public class SynchronizationRequestConsumer extends AbstractClusterMessageConsumer {

	private final ServerSynchronizationService synchronizationService;

	@Inject
	public SynchronizationRequestConsumer(@ComponentImport ClusterMessagingService messagingService,
	                                      ServerSynchronizationService synchronizationService) {
		super(messagingService, SYNC_CHANNEL);
		this.synchronizationService = synchronizationService;
	}

	@Override
	protected void onReceive(String message, String senderId) {
		synchronizationService.enqueueSynchronizationRequest(message);
	}
}
