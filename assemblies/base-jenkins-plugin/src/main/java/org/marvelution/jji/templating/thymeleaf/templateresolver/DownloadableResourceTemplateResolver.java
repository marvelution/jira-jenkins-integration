/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.templateresolver;

import java.io.*;
import java.util.*;

import com.atlassian.plugin.elements.*;
import com.atlassian.plugin.servlet.*;
import org.thymeleaf.*;
import org.thymeleaf.templateresolver.*;
import org.thymeleaf.templateresource.*;

/**
 * {@link ITemplateResolver} specific for rendering {@link DownloadableResource}s.
 *
 * @author Mark Rekveld
 * @since 4.0.0
 */
public class DownloadableResourceTemplateResolver extends AbstractConfigurableTemplateResolver {

	public static final String RESOURCE_LOCATION = "downloadable.resource.location";
	public static final String RESOURCE = "downloadable.resource";

	public DownloadableResourceTemplateResolver() {
		setOrder(1);
	}

	@Override
	protected boolean computeResolvable(IEngineConfiguration configuration, String ownerTemplate, String template,
	                                    Map<String, Object> templateResolutionAttributes) {
		return templateResolutionAttributes != null && templateResolutionAttributes.containsKey(
				RESOURCE_LOCATION) && templateResolutionAttributes.containsKey(RESOURCE);
	}

	@Override
	protected ITemplateResource computeTemplateResource(IEngineConfiguration configuration, String ownerTemplate, String template,
	                                                    String resourceName, String characterEncoding,
	                                                    Map<String, Object> templateResolutionAttributes) {
		ResourceLocation location = (ResourceLocation) templateResolutionAttributes.get(RESOURCE_LOCATION);
		DownloadableResource resource = (DownloadableResource) templateResolutionAttributes.get(RESOURCE);
		return new DownloadableTemplateResource(location, resource);
	}

	private static class DownloadableTemplateResource implements ITemplateResource {

		private final ResourceLocation location;
		private final DownloadableResource resource;

		private DownloadableTemplateResource(ResourceLocation location, DownloadableResource resource) {
			this.location = location;
			this.resource = resource;
		}

		@Override
		public String getDescription() {
			return location.getLocation();
		}

		@Override
		public String getBaseName() {
			return location.getName();
		}

		@Override
		public boolean exists() {
			return location != null && resource != null;
		}

		@Override
		public Reader reader() throws IOException {
			try {
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				resource.streamResource(output);
				return new InputStreamReader(new ByteArrayInputStream(output.toByteArray()));
			} catch (DownloadException e) {
				throw new IOException(e);
			}
		}

		@Override
		public ITemplateResource relative(String relativeLocation) {
			return new InvalidTemplateResource(location, relativeLocation);
		}
	}

	private static class InvalidTemplateResource implements ITemplateResource {

		private final ResourceLocation location;
		private final String relativeLocation;

		private InvalidTemplateResource(ResourceLocation location, String relativeLocation) {
			this.location = location;
			this.relativeLocation = relativeLocation;
		}

		@Override
		public String getDescription() {
			return "Invalid relative location '" + relativeLocation + "' and resource '" + location.getLocation() + "'";
		}

		@Override
		public String getBaseName() {
			return getDescription();
		}

		@Override
		public boolean exists() {
			return false;
		}

		@Override
		public Reader reader() throws IOException {
			throw new IOException(getDescription());
		}

		@Override
		public ITemplateResource relative(String relativeLocation) {
			return this;
		}
	}
}
