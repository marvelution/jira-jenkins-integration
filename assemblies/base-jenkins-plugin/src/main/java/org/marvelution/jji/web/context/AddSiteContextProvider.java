/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.context;

import javax.inject.*;
import java.util.*;

import org.marvelution.jji.data.services.api.*;

import com.atlassian.jira.permission.*;
import com.atlassian.jira.security.*;
import com.atlassian.plugin.*;
import com.atlassian.plugin.web.*;

/**
 * @author Mark Rekveld
 * @since 3.10.0
 */
public class AddSiteContextProvider
        implements ContextProvider
{

    private final GlobalPermissionManager permissionManager;
    private final JiraAuthenticationContext authenticationContext;
    private final SiteService siteService;

    @Inject
    public AddSiteContextProvider(
            GlobalPermissionManager permissionManager,
            JiraAuthenticationContext authenticationContext,
            SiteService siteService)
    {
        this.permissionManager = permissionManager;
        this.authenticationContext = authenticationContext;
        this.siteService = siteService;
    }

    @Override
    public void init(Map<String, String> params)
            throws PluginParseException
    {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context)
    {
        String scope = (String) context.get("scope");
        Map<String, Object> templateContext = new HashMap<>();
        templateContext.put("site", siteService.getNewSiteStartingPoint(scope));
        templateContext.put("scopeSelector",
                permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, authenticationContext.getLoggedInUser()));
        return templateContext;
    }
}
