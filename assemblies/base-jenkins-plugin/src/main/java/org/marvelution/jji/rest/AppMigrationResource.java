/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.io.IOException;
import java.io.InputStream;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.marvelution.jji.api.migration.ExportOptions;
import org.marvelution.jji.api.migration.ImportOptions;
import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.ServerAppMigrationDAO;
import org.marvelution.jji.jackson.ObjectMapperFactory;
import org.marvelution.jji.migration.ServerAppMigrationManager;
import org.marvelution.jji.rest.security.AdminRequired;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugins.rest.api.multipart.FilePart;
import com.atlassian.plugins.rest.api.multipart.MultipartForm;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;

@Named
@AdminRequired
@Path("migration")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AppMigrationResource
{

    private final ServerAppMigrationManager migrationManager;
    private final ServerAppMigrationDAO appMigrationDAO;
    private final TextResolver textResolver;
    private final ObjectMapper objectMapper;

    @Inject
    public AppMigrationResource(
            ServerAppMigrationManager migrationManager,
            ServerAppMigrationDAO appMigrationDAO,
            TextResolver textResolver,
            ObjectMapperFactory objectMapperFactory)
    {
        this.migrationManager = migrationManager;
        this.appMigrationDAO = appMigrationDAO;
        this.textResolver = textResolver;
        this.objectMapper = objectMapperFactory.create();
    }

    @POST
    @Path("export")
    public void exportData(ExportOptions options)
    {
        migrationManager.migrate(options);
    }

    @POST
    @Path("import")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @XsrfProtectionExcluded
    public void importData(MultipartForm form)
    {
        FilePart importFilePart = form.getFilePart("importFile");
        if (importFilePart.getSize() < 10)
        {
            throw new IllegalArgumentException(textResolver.getText("import.file.required"));
        }
        try (InputStream importFile = importFilePart.getInputStream();
             InputStream options = form.getFilePart("options")
                     .getInputStream())
        {
            ImportOptions importOptions = objectMapper.readValue(options, ImportOptions.class);
            migrationManager.migrate(importOptions, importFile);
        }
        catch (IOException | WebApplicationException e)
        {
            throw new IllegalStateException("Failed to import data", e);
        }
    }

    @GET
    @Path("{id}")
    public Response getFile(
            @PathParam("id")
            String id)
    {
        return migrationManager.getMigration(id)
                .map(migration -> Response.ok((StreamingOutput) output -> {
                            try (InputStream input = appMigrationDAO.getMigrationData(migration))
                            {
                                IOUtils.copy(input, output);
                            }
                        }, MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + migration.getId())
                        .build())
                .orElseGet(() -> Response.status(Response.Status.NOT_FOUND)
                        .build());
    }
}
