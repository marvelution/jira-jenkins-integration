/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.util.*;

import org.marvelution.jji.templating.thymeleaf.dialect.*;

import com.atlassian.plugin.*;
import org.thymeleaf.context.*;

/**
 * Helper for Expression Objects.
 *
 * @author Mark Rekveld
 * @since 4.1.0
 */
class ExpressionObjectHelper
{

	static Plugin getPlugin(IExpressionContext context)
	{
		return Optional.ofNullable(context.getConfiguration().getExecutionAttributes().get(ServerDialect.PLUGIN))
				.filter(Plugin.class::isInstance)
				.map(Plugin.class::cast)
				.orElseThrow(() -> new IllegalArgumentException("missing plugin in execution variables"));
	}
}
