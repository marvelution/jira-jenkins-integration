/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.templating.ThymeleafWebTemplateRenderer;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.web.api.renderer.RendererException;

/**
 * Delegating {@link ThymeleafWebTemplateRenderer} implementation.
 *
 * @author Mark Rekveld
 * @since 3.5.0
 */
@Named
@ExportAsService(ThymeleafWebTemplateRenderer.class)
public class DefaultThymeleafWebTemplateRenderer
        implements ThymeleafWebTemplateRenderer
{

    private final ThymeleafTemplateRendererFactory templateEngineFactory;

    @Inject
    public DefaultThymeleafWebTemplateRenderer(ThymeleafTemplateRendererFactory templateEngineFactory)
    {
        this.templateEngineFactory = templateEngineFactory;
    }

    @Override
    public String getResourceType()
    {
        return "thymeleaf";
    }

    @Override
    public void render(
            String templateName,
            Plugin plugin,
            Map<String, Object> context,
            Writer writer)
            throws RendererException
    {
        try
        {
            templateEngineFactory.getTemplateEngine(plugin)
                    .render(templateName, context, writer);
        }
        catch (IOException e)
        {
            throw new RendererException(e);
        }
    }

    @Override
    public String renderFragment(
            String fragment,
            Plugin plugin,
            Map<String, Object> context)
            throws RendererException
    {
        return templateEngineFactory.getTemplateEngine(plugin)
                .renderFragment(fragment, context);
    }

    @Override
    public void renderFragment(
            Writer writer,
            String fragment,
            Plugin plugin,
            Map<String, Object> context)
            throws RendererException
    {
        try
        {
            templateEngineFactory.getTemplateEngine(plugin)
                    .render(fragment, context, writer);
        }
        catch (IOException e)
        {
            throw new RendererException(e);
        }
    }
}
