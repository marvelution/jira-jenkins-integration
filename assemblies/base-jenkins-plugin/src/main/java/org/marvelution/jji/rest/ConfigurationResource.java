/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.util.List;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.data.services.JobTriggerService;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.JiraService;
import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.model.request.EnabledState;
import org.marvelution.jji.model.request.EvaluateParameterIssueFieldsRequest;
import org.marvelution.jji.rest.security.AdminRequired;
import org.marvelution.jji.rest.security.RequiresPermission;
import org.marvelution.jji.data.services.ServerPermissionService;
import org.marvelution.jji.data.services.api.model.IssueField;
import org.marvelution.jji.data.services.api.model.Project;

import com.atlassian.jira.security.JiraAuthenticationContext;
import org.apache.commons.lang3.StringUtils;

import static org.marvelution.jji.data.services.api.model.PermissionKeys.ADMINISTER_PROJECTS;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Named
@AdminRequired
@Path("configuration")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ConfigurationResource
{

    private final ConfigurationService configurationService;
    private final Features features;
    private final JiraService jiraService;
    private final JobTriggerService jobTriggerService;
    private final ServerPermissionService permissions;
    private final JiraAuthenticationContext authenticationContext;

    @Inject
    public ConfigurationResource(
            ConfigurationService configurationService,
            Features features,
            JiraService jiraService,
            JobTriggerService jobTriggerService,
            ServerPermissionService permissions,
            JiraAuthenticationContext authenticationContext)
    {
        this.configurationService = configurationService;
        this.features = features;
        this.jiraService = jiraService;
        this.jobTriggerService = jobTriggerService;
        this.permissions = permissions;
        this.authenticationContext = authenticationContext;
    }

    @GET
    @RequiresPermission(ADMINISTER_PROJECTS)
    public Response getConfiguration(
            @QueryParam("scope")
            @DefaultValue("")
            String scope)
    {
        if (!permissions.isScopeAdministrator(authenticationContext.getLoggedInUser(), scope))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        return Response.ok(configurationService.getConfiguration(scope))
                .build();
    }

    @POST
    @RequiresPermission(ADMINISTER_PROJECTS)
    public Response saveConfiguration(Configuration configuration)
    {
        if (!permissions.isScopeAdministrator(authenticationContext.getLoggedInUser(), configuration.getScope()))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        configurationService.saveConfiguration(configuration);
        return Response.noContent()
                .build();
    }

    @DELETE
    @RequiresPermission(ADMINISTER_PROJECTS)
    public Response resetConfiguration(
            @QueryParam("scope")
            @DefaultValue("")
            String scope)
    {
        if (!permissions.isScopeAdministrator(authenticationContext.getLoggedInUser(), scope))
        {
            return Response.status(Response.Status.FORBIDDEN)
                    .build();
        }
        else if (StringUtils.isBlank(scope))
        {
            configurationService.resetToDefaults();
        }
        else
        {
            configurationService.resetToDefaults(scope);
        }
        return Response.noContent()
                .build();
    }

    @GET
    @Path("/fields")
    @RequiresPermission(ADMINISTER_PROJECTS)
    public List<IssueField> getIssueFields(
            @QueryParam("ids")
            String ids,
            @QueryParam("q")
            String query)
    {
        if (isNotBlank(ids))
        {
            return jiraService.getIssueFields(Stream.of(ids.split(","))
                    .collect(toSet()));
        }
        else if (isNotBlank(query))
        {
            return jiraService.findIssueFields(query);
        }
        else
        {
            return null;
        }
    }

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Path("parameter-issue-fields")
    @RequiresPermission(ADMINISTER_PROJECTS)
    public String evaluateParameterIssueFields(EvaluateParameterIssueFieldsRequest request)
    {
        return jobTriggerService.evaluateParameterIssueFields(request);
    }

    @GET
    @Path("/projects")
    public List<Project> getProjects(
            @QueryParam("keys")
            String keys,
            @QueryParam("q")
            String query)
    {
        if (isNotBlank(keys))
        {
            return jiraService.getProjects(Stream.of(keys.split(","))
                    .collect(toSet()));
        }
        else if (isNotBlank(query))
        {
            return jiraService.getProjects(query);
        }
        else
        {
            return null;
        }
    }

    @POST
    @Path("/features/{feature}")
    public void toggleFeature(
            @PathParam("feature")
            String feature,
            EnabledState enabledState)
    {
        features.setFeatureState(feature, enabledState.isEnabled());
    }
}
