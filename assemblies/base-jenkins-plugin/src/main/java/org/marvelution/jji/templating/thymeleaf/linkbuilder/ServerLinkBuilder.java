/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.linkbuilder;

import java.util.Map;

import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.plugin.Plugin;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.linkbuilder.StandardLinkBuilder;

/**
 * Customer {@link StandardLinkBuilder} that can handle specific Server deployment link requirements.
 *
 * @author Mark Rekveld
 * @since 3.5.0
 */
public class ServerLinkBuilder
        extends StandardLinkBuilder
{
    private final WebResourceUrlProvider webResourceUrlProvider;
    private final Plugin plugin;

    public ServerLinkBuilder(
            WebResourceUrlProvider webResourceUrlProvider,
            Plugin plugin)
    {
        this.webResourceUrlProvider = webResourceUrlProvider;
        this.plugin = plugin;
        setOrder(10);
    }

    @Override
    protected String computeContextPath(
            IExpressionContext context,
            String base,
            Map<String, Object> parameters)
    {
        // Get the context path of Jira itself
        String contextPath = super.computeContextPath(context, base, parameters);
        // Compute the contextPath for images if needed
        if (base.startsWith("/images/"))
        {
            contextPath += "/download/resources/" + plugin.getKey();
        }
        else if (base.startsWith("/js/"))
        {
            contextPath = String.format("%s/download/resources/%s:js",
                    webResourceUrlProvider.getStaticResourcePrefix(UrlMode.RELATIVE),
                    plugin.getKey());
        }
        return contextPath;
    }
}
