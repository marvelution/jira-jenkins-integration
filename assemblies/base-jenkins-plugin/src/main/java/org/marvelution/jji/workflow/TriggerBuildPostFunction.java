/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.workflow;

import javax.inject.*;
import java.util.*;

import org.marvelution.jji.api.text.*;
import org.marvelution.jji.data.services.JobTriggerService;
import org.marvelution.jji.model.request.*;
import org.marvelution.jji.model.response.*;

import com.atlassian.jira.issue.*;
import com.atlassian.jira.security.*;
import com.atlassian.jira.user.*;
import com.atlassian.jira.workflow.function.issue.*;
import com.opensymphony.module.propertyset.*;
import com.opensymphony.workflow.loader.*;
import com.opensymphony.workflow.spi.*;
import org.slf4j.*;

public class TriggerBuildPostFunction
        extends AbstractJiraFunctionProvider
{

    private static final Logger LOGGER = LoggerFactory.getLogger(TriggerBuildPostFunction.class);
    private final JobTriggerService jobTriggerService;
    private final TextResolver textResolver;
    private final JiraAuthenticationContext authenticationContext;

    @Inject
    public TriggerBuildPostFunction(
            JobTriggerService jobTriggerService,
            TextResolver textResolver,
            JiraAuthenticationContext authenticationContext)
    {
        this.jobTriggerService = jobTriggerService;
        this.textResolver = textResolver;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public void execute(
            Map transientVars,
            Map args,
            PropertySet ps)
    {
        Issue issue = getIssue(transientVars);
        ApplicationUser user = getCallerUser(transientVars, args);

        Object config = args.get("config");

        TriggerBuildsResponse response;

        if (config instanceof String)
        {
            TriggerBuildsRequest request = WorkflowPostFunctionConfiguration.parse(issue.getKey(), (String) config);
            ApplicationUser currentUser = authenticationContext.getLoggedInUser();
            try
            {
                authenticationContext.setLoggedInUser(user);
                response = jobTriggerService.triggerBuilds(request);
            }
            finally
            {
                if (currentUser != null)
                {
                    authenticationContext.setLoggedInUser(currentUser);
                }
                else
                {
                    authenticationContext.clearLoggedInUser();
                }
            }
        }
        else
        {
            WorkflowEntry entry = (WorkflowEntry) transientVars.get("entry");
            Integer actionId = (Integer) transientVars.get("actionId");
            WorkflowDescriptor workflowDescriptor = (WorkflowDescriptor) transientVars.get("descriptor");
            ActionDescriptor actionDescriptor = workflowDescriptor.getAction(actionId);
            response = new TriggerBuildsResponse().addFailure(textResolver.getText("invalid.trigger.build.post.function.configuration",
                    actionDescriptor.getName(),
                    actionId,
                    entry.getWorkflowName(),
                    entry.getId()), null);
        }

        if (response.hasFailures())
        {
            response.getFailures()
                    .forEach(failure -> LOGGER.error("Failed to trigger build, {}", failure.getMessage()));
        }
        if (response.hasWarnings())
        {
            response.getWarnings()
                    .forEach(warning -> LOGGER.warn("Unable to trigger build {}", warning.getMessage()));
        }
    }
}
