/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.templating.ThymeleafTemplateRenderer;
import org.marvelution.jji.templating.thymeleaf.dialect.ServerDialect;
import org.marvelution.jji.templating.thymeleaf.linkbuilder.ServerLinkBuilder;
import org.marvelution.jji.templating.thymeleaf.templateresolver.DownloadableResourceTemplateResolver;
import org.marvelution.jji.templating.thymeleaf.templateresolver.PluginTemplateResolver;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;
import org.thymeleaf.templateresolver.ITemplateResolver;

import static java.util.Objects.requireNonNull;

/**
 * @author Mark Rekveld
 * @since 3.7.0
 */
@Named
@ExportAsService(ThymeleafTemplateRenderer.class)
public class ThymeleafTemplateRendererFactory
        implements ServiceFactory<ThymeleafTemplateRenderer>
{

    private final Map<String, DefaultThymeleafTemplateRenderer> templateEngines = new ConcurrentHashMap<>();
    private final TemplateEngineFactory templateEngineFactory;
    private final JiraAuthenticationContext authenticationContext;
    private final HttpContext httpContext;
    private final PluginAccessor pluginAccessor;
    private final WebResourceUrlProvider webResourceUrlProvider;

    @Inject
    public ThymeleafTemplateRendererFactory(
            TemplateEngineFactory templateEngineFactory,
            @ComponentImport
            JiraAuthenticationContext authenticationContext,
            @ComponentImport
            HttpContext httpContext,
            @ComponentImport
            PluginAccessor pluginAccessor,
            @ComponentImport
            WebResourceUrlProvider webResourceUrlProvider)
    {
        this.templateEngineFactory = templateEngineFactory;
        this.authenticationContext = authenticationContext;
        this.httpContext = httpContext;
        this.pluginAccessor = pluginAccessor;
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    @Override
    public ThymeleafTemplateRenderer getService(
            Bundle bundle,
            ServiceRegistration<ThymeleafTemplateRenderer> registration)
    {
        String pluginKey = requireNonNull(bundle.getHeaders()
                .get("Atlassian-Plugin-Key"), "no plugin key found in bundle");
        Plugin plugin = requireNonNull(pluginAccessor.getEnabledPlugin(pluginKey), "plugin with key" + pluginKey + " is not enabled");
        return getTemplateEngine(plugin);
    }

    @Override
    public void ungetService(
            Bundle bundle,
            ServiceRegistration<ThymeleafTemplateRenderer> registration,
            ThymeleafTemplateRenderer service)
    {}

    ThymeleafTemplateRenderer getTemplateEngine(Plugin plugin)
    {
        return templateEngines.computeIfAbsent(plugin.getKey(), pluginKey -> {
            List<ITemplateResolver> templateResolvers = new ArrayList<>();
            templateResolvers.add(new DownloadableResourceTemplateResolver());
            templateResolvers.add(PluginTemplateResolver.htmlTemplateResolver(plugin));
            templateResolvers.add(PluginTemplateResolver.jsTemplateResolver(plugin));
            List<IDialect> dialects = new ArrayList<>();
            dialects.add(new ServerDialect(plugin));
            dialects.add(new Java8TimeDialect());
            Optional.of(plugin)
                    .filter(ContainerManagedPlugin.class::isInstance)
                    .map(ContainerManagedPlugin.class::cast)
                    .map(ContainerManagedPlugin::getContainerAccessor)
                    .map(accessor -> accessor.getBeansOfType(IDialect.class))
                    .ifPresent(dialects::addAll);
            TemplateEngine templateEngine = templateEngineFactory.createTemplateEngine(templateResolvers, dialects);
            templateEngine.addLinkBuilder(new ServerLinkBuilder(webResourceUrlProvider, plugin));
            return new DefaultThymeleafTemplateRenderer(templateEngine, httpContext, authenticationContext);
        });
    }

    @PreDestroy
    public void destroy()
    {
        templateEngines.clear();
    }

    @PluginEventListener
    public void onPluginDisabled(PluginDisabledEvent event)
    {
        templateEngines.remove(event.getPlugin()
                .getKey());
    }

    @PluginEventListener
    public void onPluginFrameworkShutdown(PluginFrameworkShutdownEvent event)
    {
        destroy();
    }
}
