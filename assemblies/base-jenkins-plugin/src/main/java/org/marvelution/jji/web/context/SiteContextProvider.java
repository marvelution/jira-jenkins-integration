/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.context;

import javax.inject.*;
import java.util.*;

import org.marvelution.jji.data.services.api.*;

import com.atlassian.jira.permission.*;
import com.atlassian.jira.security.*;
import com.atlassian.plugin.*;
import com.atlassian.plugin.web.*;

/**
 * @author Mark Rekveld
 * @since 3.10.0
 */
public class SiteContextProvider
        implements ContextProvider
{

    private final SiteService siteService;
    private final GlobalPermissionManager permissionManager;
    private final JiraAuthenticationContext authenticationContext;

    @Inject
    public SiteContextProvider(
            SiteService siteService,
            GlobalPermissionManager permissionManager,
            JiraAuthenticationContext authenticationContext)
    {
        this.siteService = siteService;
        this.permissionManager = permissionManager;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public void init(Map<String, String> params)
            throws PluginParseException
    {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context)
    {
        String siteIdContextKey = context.keySet()
                .stream()
                .filter("siteId"::equalsIgnoreCase)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("missing siteId context parameter"));
        String siteId = (String) context.get(siteIdContextKey);
        Map<String, Object> templateContext = new HashMap<>();
        templateContext.put("site", siteService.getExisting(siteId));
        templateContext.put("scopeSelector",
                permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, authenticationContext.getLoggedInUser()));
        return templateContext;
    }
}
