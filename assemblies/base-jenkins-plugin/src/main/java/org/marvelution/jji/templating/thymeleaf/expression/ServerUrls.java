/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import javax.inject.*;
import java.net.*;
import java.util.*;

import org.marvelution.jji.data.services.api.*;

import com.atlassian.plugin.*;
import org.apache.commons.lang3.*;
import org.thymeleaf.context.*;
import org.thymeleaf.web.servlet.*;

import static org.apache.commons.lang3.StringUtils.*;

/**
 * Server specific implementation of {@link Urls}.
 *
 * @author Mark Rekveld
 * @since 3.10.0
 */
@Named
public class ServerUrls
        extends Urls
{

    private final ConfigurationService configurationService;

    @Inject
    public ServerUrls(ConfigurationService configurationService)
    {
        this(null, configurationService);
    }

    private ServerUrls(
            IExpressionContext expressionContext,
            ConfigurationService configurationService)
    {
        super(expressionContext);
        this.configurationService = configurationService;
    }

    @Override
    public String baseUrl()
    {
        return configurationService.getBaseUrl()
                .toASCIIString();
    }

    @Override
    public String restBaseUrl(boolean absolute)
    {
        Optional<String> contextPath = getContext(IWebContext.class).map(IWebContext::getExchange)
                .filter(IServletWebExchange.class::isInstance)
                .map(IServletWebExchange.class::cast)
                .map(IServletWebExchange::getRequest)
                .map(IServletWebRequest::getContextPath);
        String restApiPath = getPluginInformation().getOrDefault("rest.api.path", "");
        restApiPath = startsWith(restApiPath, "/") ? restApiPath : ("/" + restApiPath);
        String restPath = contextPath.map(path -> stripEnd(path, "/"))
                                  .orElse("") + restApiPath;
        if (absolute)
        {
            return resolvePath(baseUrl(), restPath);
        }
        else
        {
            return restPath;
        }
    }

    @Override
    public String docsBaseUrl()
    {
        return getPluginInformation().getOrDefault("documentation.url", "");
    }

    private Map<String, String> getPluginInformation()
    {
        return getContext().map(ExpressionObjectHelper::getPlugin)
                .map(Plugin::getPluginInformation)
                .map(PluginInformation::getParameters)
                .orElseGet(HashMap::new);
    }

    public String adminBaseUrl()
    {
        return getContext().map(context -> context.getVariable("projectKey"))
                .map(projectKey -> "/jji-config/" + projectKey)
                .orElse("/admin/jji");
    }

    @Override
    protected Optional<String> contextPath()
    {
        return Optional.of(configurationService.getBaseUrl())
                .map(URI::getPath)
                .filter(StringUtils::isNotBlank);
    }

    @Override
    public Urls withContext(IExpressionContext context)
    {
        return new ServerUrls(context, configurationService);
    }
}
