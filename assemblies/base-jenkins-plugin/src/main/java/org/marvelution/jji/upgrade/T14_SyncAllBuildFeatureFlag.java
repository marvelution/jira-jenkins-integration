/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.*;
import javax.inject.*;

import org.marvelution.jji.api.*;
import org.marvelution.jji.api.features.*;

import com.atlassian.plugin.spring.scanner.annotation.export.*;
import com.atlassian.sal.api.message.*;
import com.atlassian.sal.api.pluginsettings.*;
import com.atlassian.sal.api.upgrade.*;

@Named
@ExportAsService(PluginUpgradeTask.class)
public class T14_SyncAllBuildFeatureFlag
		extends AbstractUpgradeTask
{

	static final String ONLY_SYNC_UNKNOWN_BUILDS_KEY = "jji.";
	private final Features features;
	private final PluginSettingsFactory pluginSettingsFactory;

	@Inject
	public T14_SyncAllBuildFeatureFlag(
			AppState appState,
			Features features,
			PluginSettingsFactory pluginSettingsFactory)
	{
		super(appState);
		this.features = features;
		this.pluginSettingsFactory = pluginSettingsFactory;
	}

	@Override
	public Collection<Message> doUpgrade()
	{
		PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
		Object setting = pluginSettings.get(ONLY_SYNC_UNKNOWN_BUILDS_KEY);
		if (setting instanceof String)
		{
			features.setFeatureState(FeatureFlags.SYNC_ALL_BUILDS, !Boolean.parseBoolean((String) setting));
		}
		pluginSettings.remove(ONLY_SYNC_UNKNOWN_BUILDS_KEY);
		return null;
	}
}
