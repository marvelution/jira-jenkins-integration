/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.templateresolver;

import java.net.*;
import java.nio.charset.*;
import java.util.*;

import com.atlassian.plugin.*;
import org.thymeleaf.*;
import org.thymeleaf.templatemode.*;
import org.thymeleaf.templateresolver.*;
import org.thymeleaf.templateresource.*;

/**
 * {@link ITemplateResolver} for resolving templates within a {@link Plugin}.
 *
 * @author Mark Rekveld
 * @since 3.7.0
 */
public class PluginTemplateResolver extends AbstractConfigurableTemplateResolver {

	private final Plugin plugin;

	public PluginTemplateResolver(Plugin plugin) {
		this.plugin = plugin;
	}

	public static PluginTemplateResolver htmlTemplateResolver(Plugin plugin) {
		PluginTemplateResolver templateResolver = new PluginTemplateResolver(plugin);
		templateResolver.setPrefix("/templates/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode(TemplateMode.HTML);
		templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
		templateResolver.setCacheable(true);
		templateResolver.setCheckExistence(true);
		templateResolver.setOrder(5);
		return templateResolver;
	}

	public static PluginTemplateResolver jsTemplateResolver(Plugin plugin) {
		PluginTemplateResolver templateResolver = new PluginTemplateResolver(plugin);
		templateResolver.setPrefix("/static/js/");
		templateResolver.setTemplateMode(TemplateMode.JAVASCRIPT);
		templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
		templateResolver.setCacheable(true);
		templateResolver.setCheckExistence(true);
		templateResolver.setOrder(6);
		return templateResolver;
	}

	@Override
	protected ITemplateResource computeTemplateResource(IEngineConfiguration configuration, String ownerTemplate, String template,
	                                                    String resourceName, String characterEncoding,
	                                                    Map<String, Object> templateResolutionAttributes) {
		URL resource = plugin.getResource(resourceName);
		if (resource != null) {
			return new UrlTemplateResource(resource, characterEncoding);
		} else {
			return new ClassLoaderTemplateResource(plugin.getClassLoader(), resourceName, characterEncoding);
		}
	}
}
