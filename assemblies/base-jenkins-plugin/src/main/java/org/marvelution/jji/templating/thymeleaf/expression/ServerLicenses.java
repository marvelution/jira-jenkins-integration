/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import javax.inject.*;

import org.marvelution.jji.license.*;

import org.thymeleaf.context.*;

/**
 * Server specific {@link Licenses} implementation.
 *
 * @author Mark Rekveld
 * @since 3.7.0
 */
@Named
public class ServerLicenses
		extends Licenses
{

	private final LicenseHelper.Factory licenseHelperFactory;
	private LicenseHelper licenseHelper;

	@Inject
	public ServerLicenses(LicenseHelper.Factory licenseHelperFactory)
	{
		this(licenseHelperFactory, null);
	}

	private ServerLicenses(
			LicenseHelper.Factory licenseHelperFactory,
			IExpressionContext expressionContext)
	{
		super(expressionContext);
		this.licenseHelperFactory = licenseHelperFactory;
	}

	@Override
	public boolean active()
	{
		return getLicenseHelper().isActive();
	}

	@Override
	public boolean expired()
	{
		return getLicenseHelper().isExpired();
	}

	@Override
	public boolean nearlyExpired()
	{
		return getLicenseHelper().isNearlyExpired();
	}

	@Override
	public Licenses withContext(IExpressionContext context)
	{
		return new ServerLicenses(licenseHelperFactory, context);
	}

	private LicenseHelper getLicenseHelper()
	{
		if (licenseHelper == null)
		{
			licenseHelper = licenseHelperFactory.create(getContext().map(ExpressionObjectHelper::getPlugin).get());
		}
		return licenseHelper;
	}
}
