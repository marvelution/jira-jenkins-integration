/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.cleanup;

import java.time.*;
import java.util.*;

import com.atlassian.sal.api.lifecycle.*;
import com.atlassian.scheduler.*;
import com.atlassian.scheduler.config.*;
import org.slf4j.*;

import static com.atlassian.scheduler.config.JobConfig.*;
import static com.atlassian.scheduler.config.Schedule.*;

public abstract class ScheduledCleanupTask
		implements JobRunner, LifecycleAware
{

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	protected final JobRunnerKey JOB_KEY = JobRunnerKey.of(getClass().getName());
	protected final JobId JOB_ID = JobId.of(getClass().getName());
	private final SchedulerService schedulerService;

	protected ScheduledCleanupTask(SchedulerService schedulerService)
	{
		this.schedulerService = schedulerService;
	}

	@Override
	public void onStart()
	{
		if (!schedulerService.getRegisteredJobRunnerKeys()
				.contains(JOB_KEY))
		{
			logger.debug("Registering job runner {} for task {}", JOB_KEY, getClass().getSimpleName());
			schedulerService.registerJobRunner(JOB_KEY, this);
		}
		OffsetDateTime now = OffsetDateTime.now();
		OffsetDateTime firstRun = now.withHour(22)
				.withMinute(0)
				.withSecond(0);
		if (firstRun.isBefore(now))
		{
			firstRun = firstRun.plusDays(1);
		}
		logger.info("Job first run: {}", firstRun);
		JobConfig jobConfig = forJobRunnerKey(JOB_KEY).withRunMode(RunMode.RUN_ONCE_PER_CLUSTER)
				.withSchedule(forInterval(Duration.ofDays(1)
						.toMillis(), Date.from(firstRun.toInstant())));
		try
		{
			logger.debug("Scheduling {} with id: {}, config: {}", getClass().getSimpleName(), JOB_ID, jobConfig);
			schedulerService.scheduleJob(JOB_ID, jobConfig);
		}
		catch (SchedulerServiceException e)
		{
			logger.error("Failed to schedule cleanup task {}", getClass().getSimpleName(), e);
		}
	}

	@Override
	public void onStop()
	{
		schedulerService.unregisterJobRunner(JOB_KEY);
		schedulerService.unscheduleJob(JOB_ID);
	}
}
