/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.context;

import java.util.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.request.*;

import com.atlassian.jira.security.*;

/**
 * @author Mark Rekveld
 * @since 4.0.0
 */
public class ManageJobsContextProvider
        extends SiteContextProvider
{

    public ManageJobsContextProvider(
            SiteService siteService,
            GlobalPermissionManager permissionManager,
            JiraAuthenticationContext authenticationContext)
    {
        super(siteService, permissionManager, authenticationContext);
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context)
    {
        Map<String, Object> templateContext = super.getContextMap(context);
        templateContext.put("operations", BulkRequest.Operation.values());
        return templateContext;
    }
}
