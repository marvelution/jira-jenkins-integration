/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.cleanup;

import java.time.*;
import javax.annotation.*;
import javax.inject.*;

import org.marvelution.jji.data.access.*;
import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.services.api.*;

import com.atlassian.plugin.spring.scanner.annotation.export.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import com.atlassian.sal.api.lifecycle.*;
import com.atlassian.scheduler.*;

@Named
@ExportAsService(value = LifecycleAware.class)
public class CleanupOldAuditData
		extends ScheduledCleanupTask
{

	private final ConfigurationService configurationService;
	private final SynchronizationResultDAO synchronizationResultDAO;

	@Inject
	public CleanupOldAuditData(
			@ComponentImport SchedulerService schedulerService,
			ConfigurationService configurationService,
			SynchronizationResultDAO synchronizationResultDAO)
	{
		super(schedulerService);
		this.configurationService = configurationService;
		this.synchronizationResultDAO = synchronizationResultDAO;
	}

	@Override
	public JobRunnerResponse runJob(@Nonnull JobRunnerRequest request)
	{
		LocalDateTime timestamp = configurationService.getAuditDataRetentionTimestamp();
		boolean cleanupOldResults = cleanupOldResults(timestamp);
		if (cleanupOldResults)
		{
			return JobRunnerResponse.success("Cleaned up audit data older then " + timestamp);
		}
		else
		{
			return JobRunnerResponse.failed("Failed to cleanup old audit data, see logs for details.");
		}
	}

	private boolean cleanupOldResults(LocalDateTime timestamp)
	{
		try
		{
			logger.info("Cleaning up synchronization results older queued before {}", timestamp);
			synchronizationResultDAO.deleteAllQueuedBefore(timestamp);
			return true;
		}
		catch (Throwable e)
		{
			logger.error("Failed to clean up synchronization results older queued before {}", timestamp, e);
			return false;
		}
	}
}
