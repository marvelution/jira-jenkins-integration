/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.time.LocalDate;
import java.time.*;
import java.time.temporal.*;
import java.util.*;
import javax.inject.*;

import com.atlassian.jira.datetime.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import org.thymeleaf.context.*;

import static com.atlassian.jira.datetime.DateTimeStyle.*;
import static java.time.temporal.ChronoField.*;

/**
 * {@link Times} implementation specific for the Server deployment of the add-on.
 *
 * @author Mark Rekveld
 * @since 3.5.0
 */
@Named
public class ServerTimes
		extends Times
{

	private final DateTimeFormatter formatter;

	@Inject
	public ServerTimes(@ComponentImport DateTimeFormatter formatter)
	{
		this(formatter, null);
	}

	private ServerTimes(
			DateTimeFormatter formatter,
			IExpressionContext context)
	{
		super(context);
		this.formatter = formatter;
	}

	@Override
	public Times withContext(IExpressionContext context)
	{
		return new ServerTimes(formatter, context);
	}

	@Override
	public String format(Date date)
	{
		return formatter.forLoggedInUser().withStyle(RELATIVE).format(date);
	}

	@Override
	public String format(Temporal temporal)
	{
		Instant instant;

		if (temporal instanceof Instant)
		{
			instant = (Instant) temporal;
		}
		else if (temporal.isSupported(INSTANT_SECONDS))
		{
			instant = temporal.query(Instant::from);
		}
		else
		{
			LocalDate date = temporal.query(TemporalQueries.localDate());
			LocalTime time = temporal.query(TemporalQueries.localTime());
			if (time == null)
			{
				time = LocalTime.MIDNIGHT;
			}
			ZoneId zoneId = temporal.query(TemporalQueries.zone());
			if (zoneId == null)
			{
				zoneId = Optional.ofNullable(formatter.forLoggedInUser().getZone())
						.map(TimeZone::toZoneId)
						.orElseGet(ZoneId::systemDefault);
			}
			instant = date.atTime(time).atZone(zoneId).toInstant();
		}

		return format(Date.from(instant));
	}
}
