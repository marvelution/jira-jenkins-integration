/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import javax.inject.*;

import org.marvelution.jji.api.features.*;
import org.marvelution.jji.features.*;

import com.atlassian.jira.config.properties.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import org.thymeleaf.context.*;

/**
 * Server {@link FeaturesExpressionObjectFactory}.
 * 
 * @author Mark Rekveld
 * @since 4.1.0
 */
@Named
public class ServerFeaturesExpressionObjectFactory
		implements FeaturesExpressionObjectFactory
{

	private final FeatureStore featureStore;
	private final JiraProperties jiraProperties;

	@Inject
	public ServerFeaturesExpressionObjectFactory(
			FeatureStore featureStore,
			@ComponentImport JiraProperties jiraProperties)
	{
		this.featureStore = featureStore;
		this.jiraProperties = jiraProperties;
	}

	@Override
	public Features create(IExpressionContext context)
	{
		return new ServerFeatures(featureStore, jiraProperties, () -> ExpressionObjectHelper.getPlugin(context));
	}
}
