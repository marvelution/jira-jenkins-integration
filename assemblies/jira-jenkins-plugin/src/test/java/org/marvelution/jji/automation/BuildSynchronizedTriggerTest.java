/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.automation;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Optional;
import java.util.stream.Stream;

import org.marvelution.jji.automation.model.SmartValueBuild;
import org.marvelution.jji.data.services.api.IssueLinkService;
import org.marvelution.jji.events.BuildDeletedEvent;
import org.marvelution.jji.events.BuildSynchronizedEvent;
import org.marvelution.jji.events.JobSynchronizedEvent;
import org.marvelution.jji.jackson.ObjectMapperFactory;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.HasId;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Result;
import org.marvelution.testing.TestSupport;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.codebarrel.automation.api.config.ComponentConfigBean;
import com.codebarrel.automation.api.thirdparty.audit.AuditItemAssociatedType;
import com.codebarrel.automation.api.thirdparty.audit.AuditLog;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult;
import com.codebarrel.automation.api.thirdparty.context.result.QueuedExecution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.*;
import static org.mockito.Mockito.*;

class BuildSynchronizedTriggerTest
        extends TestSupport
{
    private final ObjectMapperFactory objectMapperFactory = new ObjectMapperFactory(Collections.emptyList());
    @Mock
    private IssueLinkService issueLinkService;
    @Mock
    private IssueService issueService;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private RuleContext ruleContext;
    @Mock
    private AuditLog auditLog;
    @Mock
    private ComponentInputs componentInputs;
    private BuildSynchronizedTrigger trigger;

    @BeforeEach
    void setUp()
    {
        trigger = new BuildSynchronizedTrigger(issueLinkService, objectMapperFactory, issueService, authenticationContext);
    }

    @ParameterizedTest
    @MethodSource("serializeEventParameters")
    void serializeEvent(
            Object event,
            String expectedJson)
    {
        assertThat(trigger.serializeEvent(event)).isEqualTo(expectedJson);
    }

    @Test
    void getHandledEventTypes()
    {
        assertThat(trigger.getHandledEventTypes()).containsOnly(BuildSynchronizedEvent.class.getName());
    }

    @Test
    void getSmartValueContextProvider()
    {
        assertThat(trigger.getSmartValueContextProvider()).isPresent();
    }

    @Test
    void execute_CannotReadEventInput()
    {
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(componentInputs.getSerializedEvent()).thenReturn("");

        assertThat(trigger.execute(ruleContext, componentInputs)).returns(false, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(auditLog).addError("com.codebarrel.automation.jenkins.failed.to.read.event");
        verify(ruleContext).getAuditLog();
        verify(componentInputs).getSerializedEvent();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, issueLinkService, issueService);
    }

    @Test
    void execute_CannotFilterEvent()
            throws Exception
    {
        Job job = new Job().setName(getTestMethodName())
                .setId(HasId.newId());
        Build build = new Build().setId(HasId.newId())
                .setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS);
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(componentInputs.getSerializedEvent()).thenReturn(objectMapperFactory.create()
                .writeValueAsString(new BuildSynchronizedEvent(build)));
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("dif", "config");
                }})
                .build());

        assertThat(trigger.execute(ruleContext, componentInputs)).returns(false, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(auditLog).addAssociatedItem(AuditItemAssociatedType.OTHER, build.getId(), build.getDisplayName());
        verify(auditLog).addError("com.codebarrel.automation.jenkins.failed.to.read.config");
        verify(ruleContext, times(2)).getAuditLog();
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, issueLinkService, issueService);
    }

    @Test
    void execute_SpecificJob_JobMismatch()
            throws Exception
    {
        Job job = new Job().setName(getTestMethodName())
                .setId(HasId.newId());
        Build build = new Build().setId(HasId.newId())
                .setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS);
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(componentInputs.getSerializedEvent()).thenReturn(objectMapperFactory.create()
                .writeValueAsString(new BuildSynchronizedEvent(build)));
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("onlyLinked", false);
                    put("operand", null);
                    put("result", null);
                    put("jobId", "different-job-id");
                }})
                .build());

        assertThat(trigger.execute(ruleContext, componentInputs)).returns(false, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(auditLog).addAssociatedItem(AuditItemAssociatedType.OTHER, build.getId(), build.getDisplayName());
        verify(auditLog).addMessage("com.codebarrel.automation.build.synchronized.trigger.of.job.no.match");
        verify(componentInputs).getSerializedEvent();
        verify(ruleContext, times(2)).getAuditLog();
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, issueLinkService, issueService);
    }

    @Test
    void execute_SpecificJob_JobMatch()
            throws Exception
    {
        Job job = new Job().setName(getTestMethodName())
                .setId(HasId.newId());
        Build build = new Build().setId(HasId.newId())
                .setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS);
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(componentInputs.getSerializedEvent()).thenReturn(objectMapperFactory.create()
                .writeValueAsString(new BuildSynchronizedEvent(build)));
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("onlyLinked", false);
                    put("operand", null);
                    put("result", null);
                    put("jobId", job.getId());
                }})
                .build());
        when(issueLinkService.getRelatedIssueKeys(build)).thenReturn(Collections.emptySet());

        assertThat(trigger.execute(ruleContext, componentInputs)).returns(true, ExecutionResult::isContinueRule)
                .returns(1,
                        result -> result.getQueuedExecutions()
                                .size())
                .extracting(result -> result.getQueuedExecutions()
                        .iterator()
                        .next())
                .returns(Optional.empty(), QueuedExecution::getAdditionalIssues)
                .returns(1,
                        execution -> execution.getAdditionalInputs()
                                .size())
                .extracting(QueuedExecution::getAdditionalInputs)
                .asInstanceOf(MAP)
                .containsOnlyKeys("build")
                .satisfies(inputs -> assertThat(inputs.get("build")).isInstanceOf(SmartValueBuild.class)
                        .asInstanceOf(type(SmartValueBuild.class))
                        .returns(build.getId(), smartValueBuild -> smartValueBuild.id)
                        .returns(build.getNumber(), smartValueBuild -> smartValueBuild.number)
                        .returns(build.getResult(), smartValueBuild -> smartValueBuild.result)
                        .returns(job.getId(), smartValueBuild -> smartValueBuild.job.id)
                        .returns(job.getName(), smartValueBuild -> smartValueBuild.job.name));

        verify(auditLog).addAssociatedItem(AuditItemAssociatedType.OTHER, build.getId(), build.getDisplayName());
        verify(auditLog).addMessage("com.codebarrel.automation.jenkins.synchronized.trigger.no.issues");
        verify(componentInputs).getSerializedEvent();
        verify(ruleContext, times(2)).getAuditLog();
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, issueLinkService, issueService);
    }

    @Test
    void execute_OnlyLinked_NoLinks()
            throws Exception
    {
        Job job = new Job().setName(getTestMethodName())
                .setId(HasId.newId());
        Build build = new Build().setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS);
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(componentInputs.getSerializedEvent()).thenReturn(objectMapperFactory.create()
                .writeValueAsString(new BuildSynchronizedEvent(build)));
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("onlyLinked", true);
                    put("operand", null);
                    put("result", null);
                    put("jobId", "");
                }})
                .build());
        when(issueLinkService.getRelatedIssueKeys(build)).thenReturn(Collections.emptySet());

        assertThat(trigger.execute(ruleContext, componentInputs)).returns(false, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(auditLog).addAssociatedItem(AuditItemAssociatedType.OTHER, build.getId(), build.getDisplayName());
        verify(componentInputs).getSerializedEvent();
        verify(ruleContext).getAuditLog();
        verify(ruleContext).getComponentConfigBean();
        verify(issueLinkService).getRelatedIssueKeys(build);
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, issueLinkService, issueService);
    }

    @Test
    void execute_OnlyLinked_WithLinks()
            throws Exception
    {
        Job job = new Job().setName(getTestMethodName())
                .setId(HasId.newId());
        Build build = new Build().setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS);
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(componentInputs.getSerializedEvent()).thenReturn(objectMapperFactory.create()
                .writeValueAsString(new BuildSynchronizedEvent(build)));
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("onlyLinked", true);
                    put("operand", null);
                    put("result", null);
                    put("jobId", "");
                }})
                .build());
        when(issueLinkService.getRelatedIssueKeys(build)).thenReturn(Collections.singleton("TEST-1"));
        when(authenticationContext.getLoggedInUser()).thenReturn(mock(ApplicationUser.class));
        MutableIssue issue = mock(MutableIssue.class);
        when(issue.getKey()).thenReturn("TEST-1");
        when(issueService.getIssue(any(ApplicationUser.class), eq("TEST-1"))).thenReturn(new IssueService.IssueResult(issue));

        assertThat(trigger.execute(ruleContext, componentInputs)).returns(true, ExecutionResult::isContinueRule)
                .returns(false,
                        result -> result.getQueuedExecutions()
                                .isEmpty())
                .extracting(result -> result.getQueuedExecutions()
                        .iterator()
                        .next())
                .satisfies(execution -> assertThat(execution.getAdditionalIssues()).isPresent()
                        .get()
                        .asInstanceOf(LIST)
                        .hasSize(1)
                        .containsOnly(issue))
                .returns(1,
                        execution -> execution.getAdditionalInputs()
                                .size())
                .extracting(QueuedExecution::getAdditionalInputs)
                .asInstanceOf(MAP)
                .containsOnlyKeys("build")
                .satisfies(inputs -> assertThat(inputs.get("build")).isInstanceOf(SmartValueBuild.class)
                        .asInstanceOf(type(SmartValueBuild.class))
                        .returns(build.getId(), smartValueBuild -> smartValueBuild.id)
                        .returns(build.getNumber(), smartValueBuild -> smartValueBuild.number)
                        .returns(build.getResult(), smartValueBuild -> smartValueBuild.result)
                        .returns(job.getId(), smartValueBuild -> smartValueBuild.job.id)
                        .returns(job.getName(), smartValueBuild -> smartValueBuild.job.name));

        verify(auditLog).addAssociatedItem(AuditItemAssociatedType.OTHER, build.getId(), build.getDisplayName());
        verify(auditLog).addMessage("com.codebarrel.automation.jenkins.synchronized.trigger.issues", "TEST-1");
        verify(componentInputs).getSerializedEvent();
        verify(ruleContext, times(2)).getAuditLog();
        verify(ruleContext).getComponentConfigBean();
        verify(issueLinkService).getRelatedIssueKeys(build);
        verify(issueService).getIssue(any(ApplicationUser.class), eq("TEST-1"));
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, issueLinkService, issueService);
    }

    @Test
    void execute_Result_NoOperand()
            throws Exception
    {
        Job job = new Job().setName(getTestMethodName())
                .setId(HasId.newId());
        Build build = new Build().setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS);
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(componentInputs.getSerializedEvent()).thenReturn(objectMapperFactory.create()
                .writeValueAsString(new BuildSynchronizedEvent(build)));
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("onlyLinked", true);
                    put("operand", null);
                    put("result", Result.SUCCESS);
                    put("jobId", "");
                }})
                .build());

        assertThat(trigger.execute(ruleContext, componentInputs)).returns(false, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(auditLog).addAssociatedItem(AuditItemAssociatedType.OTHER, build.getId(), build.getDisplayName());
        verify(auditLog).addMessage("com.codebarrel.automation.build.synchronized.trigger.with.result.mis.config");
        verify(componentInputs).getSerializedEvent();
        verify(ruleContext, times(2)).getAuditLog();
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, issueLinkService, issueService);
    }

    @Test
    void execute_Result_NoResult()
            throws Exception
    {
        Job job = new Job().setName(getTestMethodName())
                .setId(HasId.newId());
        Build build = new Build().setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS);
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(componentInputs.getSerializedEvent()).thenReturn(objectMapperFactory.create()
                .writeValueAsString(new BuildSynchronizedEvent(build)));
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("onlyLinked", true);
                    put("operand", BuildSynchronizedTrigger.Operand.worseThan);
                    put("result", null);
                    put("jobId", "");
                }})
                .build());

        assertThat(trigger.execute(ruleContext, componentInputs)).returns(false, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(auditLog).addAssociatedItem(AuditItemAssociatedType.OTHER, build.getId(), build.getDisplayName());
        verify(auditLog).addMessage("com.codebarrel.automation.build.synchronized.trigger.with.result.mis.config");
        verify(componentInputs).getSerializedEvent();
        verify(ruleContext, times(2)).getAuditLog();
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, issueLinkService, issueService);
    }

    @Test
    void execute_Result_Mismatch()
            throws Exception
    {
        Job job = new Job().setName(getTestMethodName())
                .setId(HasId.newId());
        Build build = new Build().setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS);
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(componentInputs.getSerializedEvent()).thenReturn(objectMapperFactory.create()
                .writeValueAsString(new BuildSynchronizedEvent(build)));
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("onlyLinked", true);
                    put("operand", BuildSynchronizedTrigger.Operand.worseThan);
                    put("result", Result.SUCCESS);
                    put("jobId", "");
                }})
                .build());

        assertThat(trigger.execute(ruleContext, componentInputs)).returns(false, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(auditLog).addAssociatedItem(AuditItemAssociatedType.OTHER, build.getId(), build.getDisplayName());
        verify(auditLog).addMessage("com.codebarrel.automation.build.synchronized.trigger.with.result.no.match");
        verify(componentInputs).getSerializedEvent();
        verify(ruleContext, times(2)).getAuditLog();
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, issueLinkService, issueService);
    }

    @Test
    void execute_Result_Match()
            throws Exception
    {
        Job job = new Job().setName(getTestMethodName())
                .setId(HasId.newId());
        Build build = new Build().setJob(job)
                .setNumber(1)
                .setResult(Result.FAILURE);
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(componentInputs.getSerializedEvent()).thenReturn(objectMapperFactory.create()
                .writeValueAsString(new BuildSynchronizedEvent(build)));
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("onlyLinked", true);
                    put("operand", BuildSynchronizedTrigger.Operand.worseThan);
                    put("result", Result.SUCCESS);
                    put("jobId", "");
                }})
                .build());
        when(issueLinkService.getRelatedIssueKeys(build)).thenReturn(Collections.singleton("TEST-1"));
        when(authenticationContext.getLoggedInUser()).thenReturn(mock(ApplicationUser.class));
        MutableIssue issue = mock(MutableIssue.class);
        when(issue.getKey()).thenReturn("TEST-1");
        when(issueService.getIssue(any(ApplicationUser.class), eq("TEST-1"))).thenReturn(new IssueService.IssueResult(issue));

        assertThat(trigger.execute(ruleContext, componentInputs)).returns(true, ExecutionResult::isContinueRule)
                .returns(false,
                        result -> result.getQueuedExecutions()
                                .isEmpty())
                .extracting(result -> result.getQueuedExecutions()
                        .iterator()
                        .next())
                .satisfies(execution -> assertThat(execution.getAdditionalIssues()).isPresent()
                        .get()
                        .asInstanceOf(LIST)
                        .hasSize(1)
                        .containsOnly(issue))
                .returns(1,
                        execution -> execution.getAdditionalInputs()
                                .size())
                .extracting(QueuedExecution::getAdditionalInputs)
                .asInstanceOf(MAP)
                .containsOnlyKeys("build")
                .satisfies(inputs -> assertThat(inputs.get("build")).isInstanceOf(SmartValueBuild.class)
                        .asInstanceOf(type(SmartValueBuild.class))
                        .returns(build.getId(), smartValueBuild -> smartValueBuild.id)
                        .returns(build.getNumber(), smartValueBuild -> smartValueBuild.number)
                        .returns(build.getResult(), smartValueBuild -> smartValueBuild.result)
                        .returns(job.getId(), smartValueBuild -> smartValueBuild.job.id)
                        .returns(job.getName(), smartValueBuild -> smartValueBuild.job.name));

        verify(auditLog).addAssociatedItem(AuditItemAssociatedType.OTHER, build.getId(), build.getDisplayName());
        verify(auditLog).addMessage("com.codebarrel.automation.jenkins.synchronized.trigger.issues", "TEST-1");
        verify(componentInputs).getSerializedEvent();
        verify(ruleContext, times(2)).getAuditLog();
        verify(ruleContext).getComponentConfigBean();
        verify(issueLinkService).getRelatedIssueKeys(build);
        verify(issueService).getIssue(any(ApplicationUser.class), eq("TEST-1"));
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, issueLinkService, issueService);
    }

    static Stream<Arguments> serializeEventParameters()
    {
        LocalDateTime now = LocalDateTime.parse("2024-11-07T16:57:48.41242");
        return Stream.of(Arguments.of(new JobSynchronizedEvent(new Job()), null),
                Arguments.of(new BuildDeletedEvent(new Build()), null),
                Arguments.of(new BuildSynchronizedEvent(new Build(), now),
                        "{\"timestamp\":\"2024-11-07T16:57:48.41242\",\"build\":{\"number\":0,\"deleted\":false,\"building\":false," +
                        "\"duration\":0,\"timestamp\":0,\"builtOn\":\"master\"}}"));
    }
}
