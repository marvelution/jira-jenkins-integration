/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.automation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.marvelution.jji.data.services.JobTriggerService;
import org.marvelution.jji.jackson.ObjectMapperFactory;
import org.marvelution.jji.model.request.TriggerBuildsRequest;
import org.marvelution.jji.model.response.TriggerBuildsResponse;
import org.marvelution.testing.TestSupport;

import com.atlassian.jira.issue.MutableIssue;
import com.codebarrel.automation.api.config.ComponentConfigBean;
import com.codebarrel.automation.api.thirdparty.audit.AuditLog;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class BuildJobActionTest
        extends TestSupport
{
    private final ObjectMapperFactory objectMapperFactory = new ObjectMapperFactory(Collections.emptyList());
    @Mock
    private JobTriggerService jobTriggerService;
    @Captor
    private ArgumentCaptor<TriggerBuildsRequest> requestArgumentCaptor;
    @Mock
    private RuleContext ruleContext;
    @Mock
    private AuditLog auditLog;
    @Mock
    private ComponentInputs componentInputs;
    private BuildJobAction action;

    @BeforeEach
    void setUp()
    {
        action = new BuildJobAction(jobTriggerService, objectMapperFactory);
    }

    @Test
    void executeConfigError()
    {
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("dif", "config");
                }})
                .build());
        assertThat(action.execute(ruleContext, componentInputs)).returns(false, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(auditLog).addError("com.codebarrel.automation.jenkins.failed.to.read.config");
        verify(ruleContext).getAuditLog();
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, jobTriggerService);
    }

    @Test
    void executeSpecificJobAndNoParameters()
    {
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("jobSelector", TriggerBuildsRequest.JobSelector.specificJob);
                    put("specificJob", "job-1");
                    put("lookupJob", "");
                    put("useParameters", TriggerBuildsRequest.UseParameters.none);
                    put("parameters", null);
                }})
                .build());
        when(componentInputs.getIssues()).thenReturn(Collections.emptyList());
        when(jobTriggerService.triggerBuilds(any(TriggerBuildsRequest.class))).thenReturn(new TriggerBuildsResponse().addSuccess(
                "successfully.scheduled.build",
                null));

        assertThat(action.execute(ruleContext, componentInputs)).returns(true, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(jobTriggerService).triggerBuilds(requestArgumentCaptor.capture());
        assertThat(requestArgumentCaptor.getValue()).returns(TriggerBuildsRequest.JobSelector.specificJob,
                        TriggerBuildsRequest::getJobSelector)
                .satisfies(request -> assertThat(request.getJobs()).hasSize(1)
                        .containsOnly("job-1"))
                .returns(null, TriggerBuildsRequest::getIssueKey)
                .returns(null, TriggerBuildsRequest::getByIssueField)
                .returns(null, TriggerBuildsRequest::getByIssueFieldMapping)
                .returns(null, TriggerBuildsRequest::getLookupJob)
                .returns(TriggerBuildsRequest.UseParameters.none, TriggerBuildsRequest::getUseParameters)
                .returns(null, TriggerBuildsRequest::getParameters);

        verify(ruleContext).getAuditLog();
        verify(auditLog).addMessage("com.codebarrel.automation.jenkins.catch.all", "successfully.scheduled.build");
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, jobTriggerService);
    }

    @Test
    void executeLookupJobAndNoParameters()
    {
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("jobSelector", TriggerBuildsRequest.JobSelector.lookupJob);
                    put("specificJob", "");
                    put("lookupJob", "test");
                    put("useParameters", TriggerBuildsRequest.UseParameters.none);
                    put("parameters", null);
                }})
                .build());
        when(componentInputs.getIssues()).thenReturn(Collections.emptyList());
        when(ruleContext.renderSmartValues(anyString())).then(invocation -> invocation.getArgument(0));
        when(jobTriggerService.triggerBuilds(any(TriggerBuildsRequest.class))).thenReturn(new TriggerBuildsResponse().addSuccess(
                "successfully.scheduled.build",
                null));

        assertThat(action.execute(ruleContext, componentInputs)).returns(true, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(jobTriggerService).triggerBuilds(requestArgumentCaptor.capture());
        assertThat(requestArgumentCaptor.getValue()).returns(TriggerBuildsRequest.JobSelector.lookupJob,
                        TriggerBuildsRequest::getJobSelector)
                .returns("test", TriggerBuildsRequest::getLookupJob)
                .satisfies(request -> assertThat(request.getJobs()).isEmpty())
                .returns(null, TriggerBuildsRequest::getIssueKey)
                .returns(null, TriggerBuildsRequest::getByIssueField)
                .returns(null, TriggerBuildsRequest::getByIssueFieldMapping)
                .returns(TriggerBuildsRequest.UseParameters.none, TriggerBuildsRequest::getUseParameters)
                .returns(null, TriggerBuildsRequest::getParameters);

        verify(ruleContext).getAuditLog();
        verify(auditLog).addMessage("com.codebarrel.automation.jenkins.catch.all", "successfully.scheduled.build");
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, jobTriggerService);
    }

    @Test
    void executeLinkedAndReferenceParameters()
    {
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("jobSelector", TriggerBuildsRequest.JobSelector.linked);
                    put("specificJob", "");
                    put("lookupJob", "");
                    put("useParameters", TriggerBuildsRequest.UseParameters.reference);
                    put("parameters", null);
                }})
                .build());
        MutableIssue issue = mock(MutableIssue.class);
        when(issue.getKey()).thenReturn("TEST-1");
        when(componentInputs.getIssues()).thenReturn(Collections.singletonList(issue));
        when(jobTriggerService.triggerBuilds(any(TriggerBuildsRequest.class))).thenReturn(new TriggerBuildsResponse().addWarning(
                "job.not.buidable",
                null));

        assertThat(action.execute(ruleContext, componentInputs)).returns(true, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(jobTriggerService).triggerBuilds(requestArgumentCaptor.capture());
        assertThat(requestArgumentCaptor.getValue()).returns(TriggerBuildsRequest.JobSelector.linked, TriggerBuildsRequest::getJobSelector)
                .returns("TEST-1", TriggerBuildsRequest::getIssueKey)
                .returns(null, TriggerBuildsRequest::getLookupJob)
                .satisfies(request -> assertThat(request.getJobs()).isEmpty())
                .returns(null, TriggerBuildsRequest::getByIssueField)
                .returns(null, TriggerBuildsRequest::getByIssueFieldMapping)
                .returns(TriggerBuildsRequest.UseParameters.reference, TriggerBuildsRequest::getUseParameters)
                .returns(null, TriggerBuildsRequest::getParameters);

        verify(ruleContext).getAuditLog();
        verify(auditLog).addMessage("com.codebarrel.automation.jenkins.catch.all", "job.not.buidable");
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, jobTriggerService);
    }

    @Test
    void executeByConventionAndReferenceParameters()
    {
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("jobSelector", TriggerBuildsRequest.JobSelector.byConvention);
                    put("specificJob", "");
                    put("lookupJob", "");
                    put("useParameters", TriggerBuildsRequest.UseParameters.reference);
                    put("parameters", null);
                }})
                .build());
        MutableIssue issue = mock(MutableIssue.class);
        when(issue.getKey()).thenReturn("TEST-1");
        when(componentInputs.getIssues()).thenReturn(Collections.singletonList(issue));
        when(jobTriggerService.triggerBuilds(any(TriggerBuildsRequest.class))).thenReturn(new TriggerBuildsResponse().addSuccess(
                "successfully.scheduled.build",
                null));

        assertThat(action.execute(ruleContext, componentInputs)).returns(true, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(jobTriggerService).triggerBuilds(requestArgumentCaptor.capture());
        assertThat(requestArgumentCaptor.getValue()).returns(TriggerBuildsRequest.JobSelector.byConvention,
                        TriggerBuildsRequest::getJobSelector)
                .returns(null, TriggerBuildsRequest::getLookupJob)
                .satisfies(request -> assertThat(request.getJobs()).isEmpty())
                .returns("TEST-1", TriggerBuildsRequest::getIssueKey)
                .returns(null, TriggerBuildsRequest::getByIssueField)
                .returns(null, TriggerBuildsRequest::getByIssueFieldMapping)
                .returns(TriggerBuildsRequest.UseParameters.reference, TriggerBuildsRequest::getUseParameters)
                .returns(null, TriggerBuildsRequest::getParameters);

        verify(ruleContext).getAuditLog();
        verify(auditLog).addMessage("com.codebarrel.automation.jenkins.catch.all", "successfully.scheduled.build");
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, jobTriggerService);
    }

    @Test
    void executeSpecificParameters()
    {
        when(ruleContext.getAuditLog()).thenReturn(auditLog);
        when(ruleContext.getComponentConfigBean()).thenReturn(ComponentConfigBean.builder()
                .setValue(new HashMap<String, Object>()
                {{
                    put("jobSelector", TriggerBuildsRequest.JobSelector.lookupJob);
                    put("specificJob", "");
                    put("lookupJob", "test");
                    put("useParameters", TriggerBuildsRequest.UseParameters.specified);
                    put("parameters", new ArrayList<Object>()
                    {{
                        add(new HashMap<String, Object>()
                        {{
                            put("name", "param-1");
                            put("value", "value-1");
                        }});
                        add(new HashMap<String, Object>()
                        {{
                            put("name", "param-2");
                            put("value", "value-2");
                        }});
                    }});
                }})
                .build());
        when(componentInputs.getIssues()).thenReturn(Collections.emptyList());
        when(ruleContext.renderSmartValues(anyString())).then(invocation -> invocation.getArgument(0));
        when(jobTriggerService.triggerBuilds(any(TriggerBuildsRequest.class))).thenReturn(new TriggerBuildsResponse().addFailure(
                "failed.to.trigger.build",
                null));

        assertThat(action.execute(ruleContext, componentInputs)).returns(false, ExecutionResult::isContinueRule)
                .returns(true,
                        result -> result.getQueuedExecutions()
                                .isEmpty());

        verify(jobTriggerService).triggerBuilds(requestArgumentCaptor.capture());
        assertThat(requestArgumentCaptor.getValue()).returns(TriggerBuildsRequest.JobSelector.lookupJob,
                        TriggerBuildsRequest::getJobSelector)
                .returns("test", TriggerBuildsRequest::getLookupJob)
                .satisfies(request -> assertThat(request.getJobs()).isEmpty())
                .returns(null, TriggerBuildsRequest::getIssueKey)
                .returns(null, TriggerBuildsRequest::getByIssueField)
                .returns(null, TriggerBuildsRequest::getByIssueFieldMapping)
                .returns(TriggerBuildsRequest.UseParameters.specified, TriggerBuildsRequest::getUseParameters)
                .satisfies(request -> assertThat(request.getParameters()).hasSize(2)
                        .containsOnly(TriggerBuildsRequest.Parameter.create("param-1", "value-1"),
                                TriggerBuildsRequest.Parameter.create("param-2", "value-2")));

        verify(ruleContext).getAuditLog();
        verify(auditLog).addError("com.codebarrel.automation.jenkins.catch.all", "failed.to.trigger.build");
        verify(ruleContext).getComponentConfigBean();
        verifyNoMoreInteractions(ruleContext, componentInputs, auditLog, jobTriggerService);
    }
}
