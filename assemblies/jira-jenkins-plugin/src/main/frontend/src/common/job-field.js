/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import PropTypes from 'prop-types';
import {ErrorMessage, Field, HelperMessage} from "@atlaskit/form";
import React, {Fragment, useEffect, useState} from "react";
import {AsyncSelect} from '@atlaskit/select';

export const JobField = (props) => {
    const context = window.CodeBarrel.Automation.getContext();
    const disabled = window.CodeBarrel.Automation.isReadOnly();
    const [job, setJob] = useState(null);

    useEffect(() => {
        if (props.value) {
            fetch(`${context.tenantBaseUrl}/rest/jenkins/latest/${props.getJobResource ?? 'job'}/${props.value}`)
                .then(response => response.json())
                .then(data => {
                    setJob({label: data.displayName, value: data.id});
                });
        }
    }, []);

    const promiseOptions = (inputValue) => {
        if (inputValue.length < 3) {
            return new Promise((resolve) => {
                resolve([])
            });
        } else {
            return fetch(`${context.tenantBaseUrl}/rest/jenkins/latest/${props.searchJobResource ?? 'job/search'}?term=${inputValue}`)
                .then(response => response.json())
                .then(data => {
                    const jobs = [];
                    data.forEach(job => {
                        jobs.push({label: job.displayName, value: job.id});
                    });
                    return jobs;
                });
        }
    };

    return (
        <Field
            name={props.name}
            label={props.label}
            isDisabled={disabled}
            isInvalid={!!props.errorMessage}
        >
            {({fieldProps}) => {
                return (
                    <Fragment>
                        <AsyncSelect
                            {...fieldProps}
                            isClearable={true}
                            value={job}
                            cacheOptions
                            defaultOptions={job ? [job] : null}
                            loadOptions={promiseOptions}
                            onChange={(e) => {
                                setJob(e);
                                props.onChange(e);
                            }}
                        />
                        {props.helpMessage ? <HelperMessage>{props.helpMessage}</HelperMessage> : null}
                        {props.errorMessage ? <ErrorMessage>{props.errorMessage}</ErrorMessage> : null}
                    </Fragment>);
            }}
        </Field>
    );
};

JobField.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    helpMessage: PropTypes.string,
    errorMessage: PropTypes.string,
    getJobResource: PropTypes.string,
    searchJobResource: PropTypes.string
};

export default JobField;
