/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const toOptions = (options) => Object.entries(options).map(([key, value]) => {
    return {label: value, value: key};
});

export const Operands = {
    equalTo: AJS.I18n.getText('automation.build.synchronized.trigger.operand.equalTo'),
    worseThan: AJS.I18n.getText('automation.build.synchronized.trigger.operand.worseThan'),
    worseOrEqualTo: AJS.I18n.getText('automation.build.synchronized.trigger.operand.worseOrEqualTo'),
    betterThan: AJS.I18n.getText('automation.build.synchronized.trigger.operand.betterThan'),
    betterOrEqualTo: AJS.I18n.getText('automation.build.synchronized.trigger.operand.betterOrEqualTo')
};

export const OperandOptions = toOptions(Operands);

export const Results = {
    SUCCESS: AJS.I18n.getText('automation.build.synchronized.trigger.result.success'),
    UNSTABLE: AJS.I18n.getText('automation.build.synchronized.trigger.result.unstable'),
    FAILURE: AJS.I18n.getText('automation.build.synchronized.trigger.result.failure'),
    NOT_BUILT: AJS.I18n.getText('automation.build.synchronized.trigger.result.notbuilt'),
    ABORTED: AJS.I18n.getText('automation.build.synchronized.trigger.result.aborted'),
    UNKNOWN: AJS.I18n.getText('automation.build.synchronized.trigger.result.unknown')
}

export const ResultOptions = toOptions(Results);
