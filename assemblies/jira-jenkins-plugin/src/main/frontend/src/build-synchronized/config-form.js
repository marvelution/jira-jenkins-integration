/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {ErrorMessage, Field, HelperMessage} from '@atlaskit/form';
import Select from '@atlaskit/select';
import {Checkbox} from '@atlaskit/checkbox';
import {Grid} from '@atlaskit/primitives';
import {OperandOptions, Operands, ResultOptions, Results} from "./options";
import JobField from "../common/job-field";

export const ConfigForm = (props) => {
    const config = props.config;
    const componentErrors = window.CodeBarrel.Automation.componentErrors(config);
    const disabled = window.CodeBarrel.Automation.isReadOnly();

    const operandValue = config.value.operand ? {label: Operands[config.value.operand], value: config.value.operand} : null;
    const resultValue = config.value.result ? {label: Results[config.value.result], value: config.value.result} : null;

    return (
        <>
            <p>{AJS.I18n.getText('automation.build.synchronized.trigger.description')}</p>
            <Checkbox
                name="onlyLinked"
                label={AJS.I18n.getText('automation.build.synchronized.trigger.only.linked')}
                value="true"
                isChecked={config.value.onlyLinked}
                isDisabled={disabled}
                onChange={(e) => {
                    window.CodeBarrel.Automation.updateComponentValue(config, {
                        ...config.value,
                        onlyLinked: e.target.checked
                    });
                }}
            />
            <Grid gap="space.100" templateColumns="1fr 1fr" alignContent="stretch" alignItems="center">
                <Field
                    name="operand"
                    label={AJS.I18n.getText('automation.build.synchronized.trigger.operand')}
                    isDisabled={disabled}
                    isInvalid={!!componentErrors.operand}
                >
                    {({fieldProps}) => (
                        <Fragment>
                            <Select
                                {...fieldProps}
                                options={OperandOptions}
                                value={operandValue}
                                isClearable={true}
                                onChange={(e) => {
                                    window.CodeBarrel.Automation.updateComponentValue(config, {
                                        ...config.value,
                                        operand: e != null ? e.value : ''
                                    });
                                }}
                            />
                            <HelperMessage>{AJS.I18n.getText('automation.build.synchronized.trigger.operand.help')}</HelperMessage>
                            {componentErrors.operand ? <ErrorMessage>{componentErrors.operand}</ErrorMessage> : null}
                        </Fragment>)
                    }
                </Field>
                <Field
                    name="result"
                    label=""
                    isDisabled={disabled}
                    isInvalid={!!componentErrors.result}
                >
                    {({fieldProps}) => (
                        <Fragment>
                            <Select
                                {...fieldProps}
                                options={ResultOptions}
                                value={resultValue}
                                isClearable={true}
                                onChange={(e) => {
                                    window.CodeBarrel.Automation.updateComponentValue(config, {
                                        ...config.value,
                                        result: e != null ? e.value : ''
                                    });
                                }}
                            />
                            {componentErrors.result ? <ErrorMessage>{componentErrors.result}</ErrorMessage> : null}
                        </Fragment>)
                    }
                </Field>
            </Grid>
            <JobField name="jobId"
                      label={AJS.I18n.getText('automation.build.synchronized.trigger.job.limit')}
                      value={config.value.jobId}
                      helpMessage={AJS.I18n.getText('automation.build.synchronized.trigger.job.limit.help')}
                      errorMessage={componentErrors.jobId}
                      onChange={(e) => {
                          window.CodeBarrel.Automation.updateComponentValue(config, {
                              ...config.value,
                              jobId: e != null ? e.value : ''
                          });
                      }}/>
        </>
    );
};

ConfigForm.propTypes = {
    config: PropTypes.object.isRequired,
};

export default ConfigForm;
