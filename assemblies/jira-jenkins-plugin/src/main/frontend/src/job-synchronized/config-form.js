/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {Checkbox} from '@atlaskit/checkbox';
import JobField from "../common/job-field";

export const ConfigForm = (props) => {
    const config = props.config;
    const componentErrors = window.CodeBarrel.Automation.componentErrors(config);

    return (
        <>
            <p>{AJS.I18n.getText('automation.job.synchronized.trigger.description')}</p>
            <Checkbox
                name="onlyLinked"
                label={AJS.I18n.getText('automation.job.synchronized.trigger.only.linked')}
                value="true"
                isChecked={config.value.onlyLinked}
                isDisabled={window.CodeBarrel.Automation.isReadOnly()}
                onChange={(e) => {
                    window.CodeBarrel.Automation.updateComponentValue(config, {
                        ...config.value,
                        onlyLinked: e.target.checked
                    });
                }}
            />
            <JobField name="jobId"
                      label={AJS.I18n.getText('automation.job.synchronized.trigger.limit')}
                      value={config.value.jobId}
                      helpMessage={AJS.I18n.getText('automation.job.synchronized.trigger.limit.help')}
                      errorMessage={componentErrors.jobId}
                      onChange={(e) => {
                          window.CodeBarrel.Automation.updateComponentValue(config, {
                              ...config.value,
                              jobId: e != null ? e.value : ''
                          });
                      }}/>
        </>
    );
};

ConfigForm.propTypes = {
    config: PropTypes.object.isRequired,
};

export default ConfigForm;
