/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import ConfigForm from './config-form';
import Summary from "./summary";

const BuildJobActionComponent = {
    type: 'com.marvelution.jira.plugins.jenkins:build-job-action',
    componentType: 'ACTION',
    name: () => AJS.I18n.getText('automation.build.job.action.name'),
    icon: () => '../com.marvelution.jira.plugins.jenkins:automation-components-resources/images/builds.svg',
    initialValue: () => ({
        jobSelector: "linked",
        specificJob: "",
        lookupJob: "",
        exactMatch: false,
        useParameters: "shared",
    }),
    getRenderer: () => ({
        summaryLabel: (config, context, containerEl) => AJS.I18n.getText('automation.build.job.action.summary'),
        renderSummary: (config, context, containerEl) => {
            ReactDOM.render(<Summary config={config}/>, containerEl);
        },
        renderDetailed: (config, context, containerEl) => {
            ReactDOM.render(<ConfigForm config={config}/>, containerEl);
        },
        renderCard: (containerEl) => {
            ReactDOM.render(
                <span>{AJS.I18n.getText('automation.build.job.action.card')}</span>, containerEl);
        },
    }),
    validate: (rule, config) => {
        if (config.value.jobSelector === "lookupJob" && !config.value.lookupJob) {
            return {lookupJob: AJS.I18n.getText('automation.build.job.action.jobSelector.lookupJob.required')};
        }
        if (config.value.jobSelector === "specificJob" && !config.value.specificJob) {
            return {lookupJob: AJS.I18n.getText('automation.build.job.action.jobSelector.specificJob.required')};
        }
        return {}
    },
    isEditable: false,
    categories: ['INTEGRATIONS'],
    labels: ['job', 'build', 'deployment', 'jenkins', 'devops'],
};

export default BuildJobActionComponent;
