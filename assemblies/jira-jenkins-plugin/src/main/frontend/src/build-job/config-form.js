/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, {Fragment, useState} from "react";
import PropTypes from "prop-types";
import JobField from "../common/job-field";
import {Inline, Stack} from "@atlaskit/primitives";
import {ErrorMessage, Field, HelperMessage} from "@atlaskit/form";
import {RadioGroup} from "@atlaskit/radio";
import TextField from "@atlaskit/textfield";
import Button, {IconButton} from "@atlaskit/button/new";
import TrashIcon from "@atlaskit/icon/glyph/trash";
import AddIcon from "@atlaskit/icon/glyph/add";
import {Checkbox} from "@atlaskit/checkbox";

export const ConfigForm = (props) => {
    const config = props.config;
    const componentErrors = window.CodeBarrel.Automation.componentErrors(config);
    const disabled = window.CodeBarrel.Automation.isReadOnly();

    const emptyParameter = {name: '', value: ''};
    const [parameters, setParameters] = useState(config.value.parameters ?? [emptyParameter]);

    const updateParameters = params => {
        setParameters(params);
        window.CodeBarrel.Automation.updateComponentValue(config, {
            ...config.value,
            parameters: params,
        });
    };

    return (
        <>
            <p>{AJS.I18n.getText('automation.build.job.action.description')}</p>
            <Field name="jobSelector"
                   label="Job to build"
                   isDisabled={disabled}
                   isInvalid={!!componentErrors.jobSelector}
            >
                {({fieldProps}) => (
                    <RadioGroup
                        {...fieldProps}
                        options={[
                            {value: "linked", label: AJS.I18n.getText('automation.build.job.action.jobSelector.linked')},
                            {
                                value: "byConvention",
                                label: AJS.I18n.getText('automation.build.job.action.jobSelector.byConvention'),
                            },
                            {value: "lookupJob", label: AJS.I18n.getText('automation.build.job.action.jobSelector.lookupJob')},
                            {value: "specificJob", label: AJS.I18n.getText('automation.build.job.action.jobSelector.specificJob')}
                        ]}
                        value={config.value.jobSelector}
                        onChange={(e) => {
                            window.CodeBarrel.Automation.updateComponentValue(config, {
                                ...config.value,
                                jobSelector: e.currentTarget.value
                            });
                        }}
                    />
                )}
            </Field>

            {config.value.jobSelector === "lookupJob" && (
                <>
                    <Field name="lookupJob"
                           label=""
                           isDisabled={disabled}
                           isInvalid={!!componentErrors.lookupJob}
                    >
                        {({fieldProps}) => (
                            <Fragment>
                                <TextField
                                    {...fieldProps}
                                    value={config.value.lookupJob}
                                    placeholder=""
                                    onChange={(e) => {
                                        window.CodeBarrel.Automation.updateComponentValue(config, {
                                            ...config.value,
                                            lookupJob: e.currentTarget.value
                                        });
                                    }}
                                />
                                <HelperMessage>{AJS.I18n.getText('automation.build.job.action.jobSelector.lookupJob.help')}</HelperMessage>
                                {componentErrors.lookupJob ? <ErrorMessage>{componentErrors.lookupJob}</ErrorMessage> : null}
                            </Fragment>
                        )}
                    </Field>
                    <Checkbox
                        name="exactMatch"
                        label={AJS.I18n.getText('automation.build.job.action.jobSelector.lookupJob.exactMatch')}
                        value="true"
                        isChecked={config.value.exactMatch}
                        isDisabled={disabled}
                        onChange={(e) => {
                            window.CodeBarrel.Automation.updateComponentValue(config, {
                                ...config.value,
                                exactMatch: e.target.checked
                            });
                        }}
                    />
                </>
            )}
            {config.value.jobSelector === "specificJob" && (
                <JobField
                    name="specificJob"
                    label=""
                    value={config.value.specificJob}
                    errorMessage={componentErrors.specificJob}
                    onChange={(e) => {
                        window.CodeBarrel.Automation.updateComponentValue(config, {
                            ...config.value,
                            specificJob: e != null ? e.value : ''
                        });
                    }}
                    getJobResource="job/buildable"
                    searchJobResource="job/buildable"
                />
            )}

            <Field name="useParameters"
                   label={AJS.I18n.getText('automation.build.job.action.useParameters')}
                   isDisabled={disabled}
                   isInvalid={!!componentErrors.useParameters}
            >
                {({fieldProps}) => (
                    <RadioGroup
                        {...fieldProps}
                        options={[
                            {value: "reference", label: AJS.I18n.getText('automation.build.job.action.useParameters.reference')},
                            {value: "none", label: AJS.I18n.getText('automation.build.job.action.useParameters.none')},
                            {value: "specified", label: AJS.I18n.getText('automation.build.job.action.useParameters.specified')}
                        ]}
                        value={config.value.useParameters}
                        onChange={(e) => {
                            window.CodeBarrel.Automation.updateComponentValue(config, {
                                ...config.value,
                                useParameters: e.currentTarget.value
                            });
                        }}
                    />
                )}
            </Field>
            {config.value.useParameters === "specified" && (
                <Stack space="space.100" alignInline="start">
                    {parameters.map((parameter, index) => (
                        <Inline space="space.100" alignInline="stretch" alignBlock="center" key={index}>
                            <TextField
                                key={`paramname-${index}`}
                                name={`paramname-${index}`}
                                value={parameter.name}
                                isDisabled={disabled}
                                placeholder={AJS.I18n.getText('automation.build.job.action.parameter.name.placeholder')}
                                onChange={(e) => {
                                    updateParameters(parameters.map((param, i) => {
                                        if (i === index) {
                                            return {...param, name: e.currentTarget.value};
                                        } else {
                                            return param;
                                        }
                                    }));
                                }}
                            />
                            <TextField
                                key={`paramvalue-${index}`}
                                name={`paramvalue-${index}`}
                                value={parameter.value}
                                isDisabled={disabled}
                                placeholder={AJS.I18n.getText('automation.build.job.action.parameter.value.placeholder')}
                                onChange={(e) => {
                                    updateParameters(parameters.map((param, i) => {
                                        if (i === index) {
                                            return {...param, value: e.currentTarget.value};
                                        } else {
                                            return param;
                                        }
                                    }));
                                }}
                            />
                            <IconButton
                                key={`remove-${index}`}
                                appearance="subtle"
                                icon={TrashIcon}
                                label={AJS.I18n.getText('automation.build.job.action.parameter.remove')}
                                onClick={() => {
                                    const updated = [
                                        ...parameters.slice(0, index),
                                        ...parameters.slice(index + 1)
                                    ];
                                    updateParameters(updated);
                                }}/>
                        </Inline>
                    ))}
                    <Button appearance="subtle"
                            iconBefore={AddIcon}
                            onClick={() => {
                                updateParameters([...parameters, emptyParameter]);
                            }}>{AJS.I18n.getText('automation.build.job.action.parameter.add')}</Button>
                </Stack>
            )}
        </>
    );
};

ConfigForm.propTypes = {
    config: PropTypes.object.isRequired,
};

export default ConfigForm;
