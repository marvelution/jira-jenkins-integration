/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.automation;

import java.io.IOException;
import java.io.Serializable;
import java.io.UncheckedIOException;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.marvelution.jji.data.services.api.IssueLinkService;
import org.marvelution.jji.events.AbstractSynchronizedEvent;
import org.marvelution.jji.jackson.ObjectMapperFactory;
import org.marvelution.jji.model.Syncable;

import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.codebarrel.automation.api.thirdparty.EventTriggerRuleComponent;
import com.codebarrel.automation.api.thirdparty.audit.AuditItemAssociatedType;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.context.result.AdditionalInputsResultBuilder;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResultBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractJenkinsEventTriggerRuleComponent<S extends Syncable<S>, E extends AbstractSynchronizedEvent<S>>
        implements EventTriggerRuleComponent
{
    protected final IssueLinkService issueLinkService;
    protected final ObjectMapper objectMapper;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Class<E> eventType;
    private final IssueService issueService;
    private final JiraAuthenticationContext authenticationContext;

    protected AbstractJenkinsEventTriggerRuleComponent(
            Class<E> eventType,
            IssueLinkService issueLinkService,
            ObjectMapperFactory objectMapperFactory,
            IssueService issueService,
            JiraAuthenticationContext authenticationContext)
    {
        this.eventType = eventType;
        this.issueLinkService = issueLinkService;
        this.objectMapper = objectMapperFactory.create();
        this.issueService = issueService;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public String serializeEvent(Object event)
    {
        if (eventType.isInstance(event))
        {
            try
            {
                return objectMapper.writeValueAsString(event);
            }
            catch (IOException e)
            {
                logger.error("Failed to serialize event", e);
                throw new UncheckedIOException(e);
            }
        }
        return null;
    }

    @Override
    public Set<String> getHandledEventTypes()
    {
        return Stream.of(eventType)
                .map(Class::getName)
                .collect(Collectors.toSet());
    }

    @Override
    public ExecutionResult execute(
            RuleContext context,
            ComponentInputs inputs)
    {
        E event;
        try
        {
            event = getEvent(inputs);
        }
        catch (IOException e)
        {
            context.getAuditLog()
                    .addError("com.codebarrel.automation.jenkins.failed.to.read.event");
            return ExecutionResultBuilder.stopRule();
        }
        context.getAuditLog()
                .addAssociatedItem(AuditItemAssociatedType.OTHER,
                        event.getSyncable()
                                .getId(),
                        event.getSyncable()
                                .getDisplayName());

        Filter filter;
        try
        {
            filter = filterEvent(event, context);
        }
        catch (Exception e)
        {
            context.getAuditLog()
                    .addError("com.codebarrel.automation.jenkins.failed.to.read.config");
            return ExecutionResultBuilder.stopRule();
        }

        if (filter == Filter.STOP)
        {
            return ExecutionResultBuilder.stopRule();
        }

        Map<String, Serializable> additionalInputs = getAdditionalInputs(event);

        Set<MutableIssue> issues = getRelatedIssueKeys(event).stream()
                .map(key -> issueService.getIssue(authenticationContext.getLoggedInUser(), key))
                .filter(ServiceResultImpl::isValid)
                .map(IssueService.IssueValidationResult::getIssue)
                .collect(Collectors.toSet());

        if (issues.isEmpty() && filter == Filter.CONTINUE_WITHOUT_ISSUES)
        {
            context.getAuditLog()
                    .addMessage("com.codebarrel.automation.jenkins.synchronized.trigger.no.issues");
            return ExecutionResultBuilder.continueRuleWithAdditionalInputs(additionalInputs);
        }
        else if (!issues.isEmpty())
        {
            context.getAuditLog()
                    .addMessage("com.codebarrel.automation.jenkins.synchronized.trigger.issues",
                            issues.stream()
                                    .map(MutableIssue::getKey)
                                    .collect(Collectors.joining(", ")));
            AdditionalInputsResultBuilder resultBuilder = new AdditionalInputsResultBuilder();
            issues.forEach(issue -> resultBuilder.with(additionalInputs, issue));
            return resultBuilder.executeConcurrently();
        }
        else
        {
            return ExecutionResultBuilder.stopRule();
        }
    }

    protected E getEvent(ComponentInputs inputs)
            throws IOException
    {
        return objectMapper.readValue(inputs.getSerializedEvent(), eventType);
    }

    protected abstract Map<String, Serializable> getAdditionalInputs(E event);

    protected abstract Set<String> getRelatedIssueKeys(E event);

    protected Filter filterEvent(
            E event,
            RuleContext context)
    {
        return Filter.CONTINUE;
    }

    protected enum Filter
    {
        STOP,
        CONTINUE,
        CONTINUE_WITHOUT_ISSUES
    }
}
