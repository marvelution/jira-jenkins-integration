/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.admin;

import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.templating.*;

import com.atlassian.jira.permission.*;
import com.atlassian.jira.project.*;
import com.atlassian.jira.security.request.*;
import org.apache.commons.lang3.*;

@SupportedMethods(RequestMethod.GET)
public class ConfigureJenkinsIntegrationProject
        extends JenkinsWebActionSupport
{

    private final SiteService siteService;
    private final JobService jobService;
    private String projectKey;
    private String tab;
    private String siteId;
    private String jobId;
    private Project project;

    public ConfigureJenkinsIntegrationProject(
            ThymeleafTemplateRenderer thymeleafTemplateRenderer,
            SiteService siteService,
            JobService jobService)
    {
        super(thymeleafTemplateRenderer);
        this.siteService = siteService;
        this.jobService = jobService;
    }

    @Override
    protected boolean hasPermissions()
    {
        return hasProjectPermission(ProjectPermissions.ADMINISTER_PROJECTS, getProject());
    }

    public String getProjectKey()
    {
        return projectKey;
    }

    public void setProjectKey(String projectKey)
    {
        this.projectKey = projectKey;
    }

    public String getTab()
    {
        return tab;
    }

    public void setTab(String tab)
    {
        this.tab = tab;
    }

    public String getSiteId()
    {
        return siteId;
    }

    public void setSiteId(String siteId)
    {
        this.siteId = siteId;
    }

    public String getJobId()
    {
        return jobId;
    }

    public void setJobId(String jobId)
    {
        this.jobId = jobId;
    }

    public Project getProject()
    {
        if (project == null)
        {
            project = getProjectManager().getProjectObjByKey(getProjectKey());
        }
        return project;
    }

    public Map<String, Object> getTemplateContext()
    {
        HashMap<String, Object> context = new HashMap<>();
        context.put("scope", project.getKey());
        context.put("projectKey", project.getKey());
        context.put("projectId", project.getId());

        if ("sites".equals(tab))
        {
            context.put("tab", "sites");
            if (StringUtils.isNotBlank(jobId))
            {
                context.put("jobId", jobId);
                Optional<Job> job = jobService.get(jobId, true);
                if (job.isPresent())
                {
                    context.put("job", job.get());
                    context.put("template", "single");
                }
                else
                {
                    context.put("template", "unknown");
                }
            }
            else if (StringUtils.isNotBlank(siteId))
            {
                context.put("siteId", siteId);
                Optional<Site> site = siteService.get(siteId, true);
                if (site.isPresent())
                {
                    context.put("site", site.get());
                    context.put("template", "single");
                }
                else
                {
                    context.put("template", "unknown");
                }
            }
            else
            {
                Set<Site> sites = siteService.getAll()
                        .stream()
                        .filter(site -> StringUtils.isBlank(site.getScope()) || Objects.equals(projectKey, site.getScope()))
                        .collect(Collectors.toSet());
                context.put("sites", sites);
                context.put("template", "list");
            }
        }
        else
        {
            context.put("tab", "config");
        }

        return context;
    }
}
