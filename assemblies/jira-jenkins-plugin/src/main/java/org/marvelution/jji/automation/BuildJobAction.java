/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.automation;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.JobTriggerService;
import org.marvelution.jji.jackson.ObjectMapperFactory;
import org.marvelution.jji.model.request.TriggerBuildsRequest;
import org.marvelution.jji.model.response.TriggerBuildsResponse;

import com.codebarrel.automation.api.thirdparty.AutomationRuleComponent;
import com.codebarrel.automation.api.thirdparty.audit.AuditLog;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResultBuilder;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

@Named
public class BuildJobAction
        implements AutomationRuleComponent
{
    private final JobTriggerService jobTriggerService;
    private final ObjectMapper objectMapper;

    @Inject
    public BuildJobAction(
            JobTriggerService jobTriggerService,
            ObjectMapperFactory objectMapperFactory)
    {
        this.jobTriggerService = jobTriggerService;
        this.objectMapper = objectMapperFactory.create();
    }

    @Override
    public ExecutionResult execute(
            RuleContext context,
            ComponentInputs inputs)
    {
        Config config;
        try
        {
            config = objectMapper.copy()
                    .enable(FAIL_ON_UNKNOWN_PROPERTIES)
                    .convertValue(context.getComponentConfigBean()
                            .getValue(), Config.class);
        }
        catch (IllegalArgumentException e)
        {
            context.getAuditLog()
                    .addError("com.codebarrel.automation.jenkins.failed.to.read.config");
            return ExecutionResultBuilder.stopRule();
        }

        TriggerBuildsRequest request = new TriggerBuildsRequest().setJobSelector(config.jobSelector);
        inputs.getIssues()
                .stream()
                .findFirst()
                .ifPresent(issue -> request.setIssueKey(issue.getKey()));
        if (config.jobSelector == TriggerBuildsRequest.JobSelector.specificJob)
        {
            request.setJobs(Stream.of(config.specificJob)
                    .collect(Collectors.toSet()));
        }
        else if (config.jobSelector == TriggerBuildsRequest.JobSelector.lookupJob)
        {
            request.setLookupJob(context.renderSmartValues(config.lookupJob))
                    .setExactMatch(config.exactMatch);
        }
        request.setUseParameters(config.useParameters);
        if (config.useParameters == TriggerBuildsRequest.UseParameters.specified)
        {
            request.setParameters(config.parameters.stream()
                    .map(parameter -> TriggerBuildsRequest.Parameter.create(context.renderSmartValues(parameter.getName()),
                            context.renderSmartValues(parameter.getValue())))
                    .collect(Collectors.toSet()));
        }
        AuditLog auditLog = context.getAuditLog();
        TriggerBuildsResponse response = jobTriggerService.triggerBuilds(request);
        response.getTriggers()
                .forEach(trigger -> {
                    switch (trigger.getType())
                    {
                        case SUCCESS:
                        case WARNING:
                            auditLog.addMessage("com.codebarrel.automation.jenkins.catch.all", trigger.getMessage());
                            break;
                        case ERROR:
                            auditLog.addError("com.codebarrel.automation.jenkins.catch.all", trigger.getMessage());
                            break;
                    }
                });
        if (!response.getTriggers(TriggerBuildsResponse.TriggerResultType.ERROR)
                .isEmpty())
        {
            return ExecutionResultBuilder.stopRule();
        }

        return ExecutionResultBuilder.continueRule();
    }

    private static class Config
    {
        @JsonProperty
        public TriggerBuildsRequest.JobSelector jobSelector;
        @JsonProperty
        public String specificJob;
        @JsonProperty
        public String lookupJob;
        @JsonProperty
        public boolean exactMatch;
        @JsonProperty
        public TriggerBuildsRequest.UseParameters useParameters;
        @JsonProperty
        public List<TriggerBuildsRequest.Parameter> parameters;
    }
}
