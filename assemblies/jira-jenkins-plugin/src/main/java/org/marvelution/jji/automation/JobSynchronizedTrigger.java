/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.automation;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.automation.model.SmartValueJob;
import org.marvelution.jji.data.services.api.IssueLinkService;
import org.marvelution.jji.events.JobSynchronizedEvent;
import org.marvelution.jji.jackson.ObjectMapperFactory;
import org.marvelution.jji.model.Job;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.smartvalues.SmartValueContextProvider;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

@Named
public class JobSynchronizedTrigger
        extends AbstractJenkinsEventTriggerRuleComponent<Job, JobSynchronizedEvent>
{
    @Inject
    public JobSynchronizedTrigger(
            IssueLinkService issueLinkService,
            ObjectMapperFactory objectMapperFactory,
            @ComponentImport
            IssueService issueService,
            @ComponentImport
            JiraAuthenticationContext authenticationContext)
    {
        super(JobSynchronizedEvent.class, issueLinkService, objectMapperFactory, issueService, authenticationContext);
    }

    @Override
    public Optional<SmartValueContextProvider> getSmartValueContextProvider()
    {
        return Optional.of((unmodifiableContext, inputs) -> {
            try
            {
                JobSynchronizedEvent event = getEvent(inputs);
                HashMap<String, Object> context = new HashMap<String, Object>() {};
                context.put("job", objectMapper.convertValue(event.getJob(), SmartValueJob.class));
                return context;
            }
            catch (IOException e)
            {
                throw new IllegalStateException("Failed to read input event", e);
            }
        });
    }

    @Override
    protected Map<String, Serializable> getAdditionalInputs(JobSynchronizedEvent event)
    {
        Map<String, Serializable> additionalInputs = new HashMap<>();
        additionalInputs.put("job", objectMapper.convertValue(event.getJob(), SmartValueJob.class));
        return additionalInputs;
    }

    @Override
    protected Set<String> getRelatedIssueKeys(JobSynchronizedEvent event)
    {
        return issueLinkService.getRelatedIssueKeys(event.getJob());
    }

    @Override
    protected Filter filterEvent(
            JobSynchronizedEvent event,
            RuleContext context)
    {
        Config config = objectMapper.copy()
                .enable(FAIL_ON_UNKNOWN_PROPERTIES)
                .convertValue(context.getComponentConfigBean()
                        .getValue(), Config.class);

        if (StringUtils.isNotBlank(config.jobId) && !Objects.equals(config.jobId,
                event.getJob()
                        .getId()))
        {
            context.getAuditLog()
                    .addMessage("com.codebarrel.automation.job.synchronized.trigger.no.match");
            return Filter.STOP;
        }

        return config.onlyLinked ? Filter.CONTINUE : Filter.CONTINUE_WITHOUT_ISSUES;
    }

    private static class Config
    {
        @JsonProperty
        public boolean onlyLinked;
        @JsonProperty
        public String jobId;
    }
}
