/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.migration;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.Version;

import com.atlassian.migration.app.check.*;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.apache.commons.lang3.StringUtils;

import static java.util.Arrays.asList;

@Named
@ExportAsService
public class JenkinsCloudPreMigrationChecks
        implements PreMigrationCheckRepository
{
    private static final String JENKINS_PLUGIN = "jenkins-plugin";
    private final TextResolver textResolver;
    private final SiteService siteService;
    private final SiteClient siteClient;
    private final Set<CheckSpec> checkSpecs;

    @Inject
    public JenkinsCloudPreMigrationChecks(
            TextResolver textResolver,
            SiteService siteService,
            SiteClient siteClient)
    {
        this.textResolver = textResolver;
        this.siteService = siteService;
        this.siteClient = siteClient;
        checkSpecs = validateChecks(createJenkinsPluginCheckSpec());
    }

    @Override
    public Set<CheckSpec> getAvailableChecks()
    {
        return checkSpecs;
    }

    @Override
    public CheckResult executeCheck(
            String checkId,
            MigrationPlanContext context)
    {
        if (checkId.equals(JENKINS_PLUGIN))
        {
            return executeJenkinsPluginCheck();
        }
        throw new IllegalStateException("Unknown check: " + checkId);
    }

    private CheckSpec createJenkinsPluginCheckSpec()
    {
        return new CheckSpec(JENKINS_PLUGIN,
                textResolver.getText("cloud.migration.checks.jenkins-plugin"),
                textResolver.getText("cloud.migration.checks.jenkins-plugin.description"),
                Collections.singletonMap("verify-plugin", textResolver.getText("cloud.migration.checks.jenkins-plugin.verify-plugin")));
    }

    private Set<CheckSpec> validateChecks(CheckSpec... checkSpecs)
    {
        Map<CheckSpec, List<String>> validatedCheckSpecs = Stream.of(checkSpecs)
                .collect(Collectors.toMap(Function.identity(), CheckSpec::validate));
        String invalidErrors = validatedCheckSpecs.entrySet()
                .stream()
                .filter(entry -> !entry.getValue()
                        .isEmpty())
                .map(entry -> entry.getValue()
                        .stream()
                        .collect(Collectors.joining("\n - ",
                                entry.getKey()
                                        .getCheckId() + " is invalid:\n - ",
                                "")))
                .collect(Collectors.joining("\n"));
        if (StringUtils.isNotBlank(invalidErrors))
        {
            throw new IllegalStateException("Found some invalid pre-migration checkspecs:\n\n" + invalidErrors);
        }
        return validatedCheckSpecs.keySet();
    }

    private CheckResult executeJenkinsPluginCheck()
    {
        CheckStatus status = CheckStatus.SUCCESS;
        CsvFileContent pluginStatus = new CsvFileContent(asList("Site", "Plugin Version"));
        for (Site site : siteService.getAll())
        {
            if (!site.isInaccessibleSite())
            {
                Optional<Version> pluginVersion = siteClient.getJenkinsPluginVersion(site);
                if (pluginVersion.isEmpty() || pluginVersion.filter(version -> version.isOlder(Version.of("5.2.0")))
                        .isPresent())
                {
                    status = CheckStatus.BLOCKING;
                }
                pluginStatus.addRow(asList(site.getName(),
                        pluginVersion.map(Version::toString)
                                .orElse("Unknown")));
            }
        }
        CheckResultBuilder builder = CheckResultBuilder.resultBuilder(status)
                .withCsvFileContent(pluginStatus);
        if (status != CheckStatus.SUCCESS)
        {
            builder.withStepsToResolveKey("verify-plugin");
        }
        return builder.build();
    }
}
