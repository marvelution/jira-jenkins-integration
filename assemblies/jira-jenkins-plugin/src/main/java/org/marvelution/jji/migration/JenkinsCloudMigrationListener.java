/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.migration;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.api.AppState;
import org.marvelution.jji.api.Environment;
import org.marvelution.jji.api.migration.AppMigrationService;
import org.marvelution.jji.api.migration.ExportOptions;
import org.marvelution.jji.api.migration.OutputStreamProvider;
import org.marvelution.jji.data.migration.MigrationPartsList;

import com.atlassian.migration.app.AccessScope;
import com.atlassian.migration.app.gateway.AppCloudMigrationGateway;
import com.atlassian.migration.app.gateway.MigrationDetailsV1;
import com.atlassian.migration.app.listener.DiscoverableListener;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.migration.app.AccessScope.*;
import static org.marvelution.jji.api.migration.AppMigrationService.PARTS_LIST_LABEL;

@Named
@ExportAsService
public class JenkinsCloudMigrationListener
        implements DiscoverableListener
{

    private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsCloudMigrationListener.class);
    private static final String PROPERTY_KEY = "jenkins.migration.target.environment";
    private final AppState appState;
    private final AppMigrationService migrationService;
    private final Environment targetEnvironment;

    @Inject
    public JenkinsCloudMigrationListener(
            AppState appState,
            AppMigrationService migrationService)
    {
        this(appState,
                migrationService,
                Optional.ofNullable(System.getProperty(PROPERTY_KEY))
                        .map(Environment::valueOf)
                        .orElse(Environment.Production));
    }

    JenkinsCloudMigrationListener(
            AppState appState,
            AppMigrationService migrationService,
            Environment targetEnvironment)
    {
        this.appState = appState;
        this.migrationService = migrationService;
        this.targetEnvironment = targetEnvironment;
    }

    @Override
    public void onStartAppMigration(
            AppCloudMigrationGateway migrationGateway,
            String transferId,
            MigrationDetailsV1 migrationDetails)
    {
        try
        {
            LOGGER.info("Exporting and uploading configuration and site data for cloud import.");
            migrationService.export(new TransferOutputStreamProvider(transferId, migrationGateway), new ExportOptions().setName(
                            migrationDetails.getName())
                    .setIncludeConfiguration(true)
                    .setIncludeIntegration(true));
        }
        catch (Exception e)
        {
            LOGGER.error("Failed to upload data export to cloud instance {}.", migrationDetails.getCloudUrl(), e);
        }
    }

    @Override
    public String getServerAppKey()
    {
        return appState.getKey();
    }

    @Override
    public String getCloudAppKey()
    {
        return targetEnvironment.addonKey(getServerAppKey());
    }

    @Override
    public Set<AccessScope> getDataAccessScopes()
    {
        return Stream.of(APP_DATA_SECURITY, APP_DATA_OTHER, PRODUCT_DATA_OTHER, MIGRATION_TRACING_IDENTITY, MIGRATION_TRACING_PRODUCT)
                .collect(Collectors.toSet());
    }

    private static class TransferOutputStreamProvider
            implements OutputStreamProvider
    {
        private final String transferId;
        private final AppCloudMigrationGateway migrationGateway;
        private final List<String> labels = new ArrayList<>();
        private OutputStream current;

        private TransferOutputStreamProvider(
                String transferId,
                AppCloudMigrationGateway migrationGateway)
        {
            this.transferId = transferId;
            this.migrationGateway = migrationGateway;
        }

        @Override
        public OutputStream getOutputStream(String label)
                throws IOException
        {
            labels.add(label);
            if (current != null)
            {
                current.close();
            }
            current = migrationGateway.createAppData(transferId, label);
            return current;
        }

        @Override
        public void finish()
                throws IOException
        {
            if (current != null)
            {
                current.close();
            }
            try (OutputStream partsList = migrationGateway.createAppData(transferId, PARTS_LIST_LABEL))
            {
                MigrationPartsList.Builder builder = MigrationPartsList.newBuilder();
                builder.addAllLabel(labels);
                builder.build()
                        .writeTo(partsList);
            }
        }
    }
}
