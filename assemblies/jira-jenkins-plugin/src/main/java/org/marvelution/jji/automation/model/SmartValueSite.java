/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.automation.model;

import java.io.Serializable;

import org.marvelution.jji.model.SiteType;

import com.codebarrel.automation.api.thirdparty.smartvalues.SmartValueSafe;

public class SmartValueSite
        implements Serializable
{
    @SmartValueSafe
    public String id;
    @SmartValueSafe
    public String name;
    @SmartValueSafe
    public SiteType type;
    @SmartValueSafe
    public String displayUrl;
    @SmartValueSafe
    public String scope;
}
