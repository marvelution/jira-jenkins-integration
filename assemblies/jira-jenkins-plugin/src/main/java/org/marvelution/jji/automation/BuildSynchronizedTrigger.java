/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.automation;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.function.BiFunction;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.automation.model.SmartValueBuild;
import org.marvelution.jji.data.services.api.IssueLinkService;
import org.marvelution.jji.events.BuildSynchronizedEvent;
import org.marvelution.jji.jackson.ObjectMapperFactory;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Result;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.smartvalues.SmartValueContextProvider;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;

import static com.fasterxml.jackson.databind.DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

@Named
public class BuildSynchronizedTrigger
        extends AbstractJenkinsEventTriggerRuleComponent<Build, BuildSynchronizedEvent>
{
    @Inject
    public BuildSynchronizedTrigger(
            IssueLinkService issueLinkService,
            ObjectMapperFactory objectMapperFactory,
            @ComponentImport
            IssueService issueService,
            @ComponentImport
            JiraAuthenticationContext authenticationContext)
    {
        super(BuildSynchronizedEvent.class, issueLinkService, objectMapperFactory, issueService, authenticationContext);
    }

    @Override
    public Optional<SmartValueContextProvider> getSmartValueContextProvider()
    {
        return Optional.of((unmodifiableContext, inputs) -> {
            try
            {
                BuildSynchronizedEvent event = getEvent(inputs);
                HashMap<String, Object> context = new HashMap<String, Object>() {};
                context.put("build", objectMapper.convertValue(event.getBuild(), SmartValueBuild.class));
                return context;
            }
            catch (IOException e)
            {
                throw new IllegalStateException("Failed to read input event", e);
            }
        });
    }

    @Override
    protected Map<String, Serializable> getAdditionalInputs(BuildSynchronizedEvent event)
    {
        Map<String, Serializable> additionalInputs = new HashMap<>();
        additionalInputs.put("build", objectMapper.convertValue(event.getBuild(), SmartValueBuild.class));
        return additionalInputs;
    }

    @Override
    protected Set<String> getRelatedIssueKeys(BuildSynchronizedEvent event)
    {
        return issueLinkService.getRelatedIssueKeys(event.getBuild());
    }

    @Override
    protected Filter filterEvent(
            BuildSynchronizedEvent event,
            RuleContext context)
    {
        Config config = objectMapper.copy()
                .enable(FAIL_ON_UNKNOWN_PROPERTIES)
                .enable(ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
                .convertValue(context.getComponentConfigBean()
                        .getValue(), Config.class);


        if ((config.operand == null && config.result != null) || (config.operand != null && config.result == null))
        {
            context.getAuditLog()
                    .addMessage("com.codebarrel.automation.build.synchronized.trigger.with.result.mis.config");
            return Filter.STOP;
        }
        else if (config.operand != null && !config.operand.evaluate(config.result,
                event.getBuild()
                        .getResult()))
        {
            context.getAuditLog()
                    .addMessage("com.codebarrel.automation.build.synchronized.trigger.with.result.no.match");
            return Filter.STOP;
        }

        if (StringUtils.isNotBlank(config.jobId) && !Objects.equals(config.jobId,
                event.getBuild()
                        .getJob()
                        .getId()))
        {
            context.getAuditLog()
                    .addMessage("com.codebarrel.automation.build.synchronized.trigger.of.job.no.match");
            return Filter.STOP;
        }
        return config.onlyLinked ? Filter.CONTINUE : Filter.CONTINUE_WITHOUT_ISSUES;
    }

    public enum Operand
    {
        equalTo((expected, actual) -> expected == actual),
        worseThan((expected, actual) -> actual.isWorseThan(expected)),
        worseOrEqualTo((expected, actual) -> actual.isWorseOrEqualTo(expected)),
        betterThan((expected, actual) -> actual.isBetterThan(expected)),
        betterOrEqualTo((expected, actual) -> actual.isBetterOrEqualTo(expected));

        private final BiFunction<Result, Result, Boolean> function;

        Operand(BiFunction<Result, Result, Boolean> function)
        {
            this.function = function;
        }

        boolean evaluate(
                Result expected,
                Result actual)
        {
            return function.apply(expected, actual);
        }
    }

    private static class Config
    {
        @JsonProperty
        public Operand operand;
        @JsonProperty
        public Result result;
        @JsonProperty
        public boolean onlyLinked;
        @JsonProperty
        public String jobId;
    }
}
