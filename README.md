This projects features a Jira app to integrate with Jenkins, more details on <https://docs.marvelution.com/jenkins-for-jira/latest/>

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/JJI>

Continuous Builder
==================
<https://bitbucket.org/marvelution/jira-jenkins-integration/addon/pipelines/home>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
