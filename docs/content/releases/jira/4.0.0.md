---
title: "4.0.0"
date: 2019-09-04T00:00:00Z
draft: false  
layout: none
outputs:
- note
---

*    Support for Rule based Automation Extension

*    Bulk job operations to perform operations, like clear/rebuild build cache, on multiple jobs.

*    Load incomplete synchronization operations on startup.
