---
title: "5.23.0"
date: 2024-11-10T00:00:00Z
draft: false
layout: none
outputs:
- note
---

* Hello Automation for Jira, new triggers and actions to integrate Jenkins in your Automation rules of Jira.

{{< note >}}
Triggering of builds from automation rules requires the installation of
[Jira Integration version 5.4.0](https://plugins.jenkins.io/jira-integration/releases/)
{{< /note >}}
