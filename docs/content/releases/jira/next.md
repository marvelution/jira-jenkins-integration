---
title: "next"
date: 2024-12-18T00:00:00Z
draft: true
layout: none
outputs:
- note
---

* Removed legacy automation rules.
* Allow adding Jenkins sites that are offline.
* New documentation site, https://docs.marvelution.com/jenkins-for-jira/
