---
title: "3.4.0"
date: 2018-09-23T00:00:00Z
draft: false  
layout: none
outputs:
- note
---

*    Synchronization Optimalizations.

*    Improved Administration UI.

{{< note >}}
The first feature requires the installation of [Jira Integration for Jenkins version 3.2.0]({{< relref "/releases/jenkins/#r3-2-0" >}})
{{< /note >}}
