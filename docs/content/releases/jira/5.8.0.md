---
title: "5.8.0"
date: 2022-04-14T00:00:00Z
draft: false
layout: none
outputs:
- note
---

* Security updates.

* Restrict app to specific projects.

* Improved branch detection for issue extraction.
