---
title: "Jenkins for Jira"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
  - release-notes/jira/

layout: releases
menu:
  docs:
    parent: releases
    weight: 1
---

The app is available on the [Atlassian Marketplace](https://marketplace.atlassian.com/apps/1211376/jenkins-for-jira) and 
historical versions of the app can be downloaded from the marketplace 
[Version History](https://marketplace.atlassian.com/apps/1211376/jenkins-for-jira/version-history) page.

{{< include "upgrade-paths" >}}
