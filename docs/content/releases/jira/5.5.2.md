---
title: "5.5.2"
date: 2021-10-29T00:00:00Z
draft: false
layout: none
outputs:
- note
---

*    Addressed bug in JQL issue selector used by automation rule actions

