---
title: "JCMA Migrations"
date: 2023-04-21T00:00:00Z
draft: false

menu:
  docs:
    parent: administration
    weight: 9
---

The Jenkins for Jira app support migrations via the Jira Cloud Migration Assistant, or JCMA for short. Migrations scheduled 
and performed using this method are listed as an Import migration under [Export/Import]({{< relref "/administration/import-export" >}}) 
in the target cloud instance.

From there you can keep track of the progress of the migration as well as review and next steps or errors.
