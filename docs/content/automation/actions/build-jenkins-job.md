---
title: "Build Jenkins Job"
date: 2024-11-04T00:00:00Z
draft: false

menu:
  docs:
    parent: automation-actions

intro: Automation action to build a Jenkins job.

---

Trigger a build of one or more Jenkins jobs. You have the option to trigger a specific job, use smart-values to look up the job(s) to 
trigger, or trigger jobs related to the issue in scope of the rule.
Build parameters are also supported, either specify the specific parameters, this supports smart-values, or use the 
[parameter issue fields]({{< relref "/administration/app-configuration#parameter-issue-fields-" >}}) feature.  

* **Use smart values here:** Yes
* **Rule actor permission:** Trigger Jenkins Builds

{{< screenshot "automation/build-jenkins-job.jpg" >}}
