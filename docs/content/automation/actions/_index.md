---
title: "Actions"
date: 2020-07-01T14:07:00Z
draft: false
layout: merged-list

weight: 3

menu:
  docs:
    identifier: automation-actions
    parent: automation

---

Actions are the doers of your rule. They allow you to automate tasks and make changes within your site, and can perform many tasks, such 
as editing an issue, triggering a Jenkins build:
