---
title: "Build Processed"
date: 2024-11-04T00:00:00Z
draft: false

menu:
  docs:
    parent: automation-triggers

intro: Start a rule when a build is processed.

---

This rule runs whenever a Jenkins build has been processed and stored.
You can limit the trigger to match only builds of a specific job, any build that has links to one or more issues or filter by result of
the build.

**Related smart values:** [{{build}}]({{< relref "/automation/smart-values#build" >}})

{{< screenshot "automation/build-processed.jpg" >}}
