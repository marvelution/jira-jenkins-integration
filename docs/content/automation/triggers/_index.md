---
title: "Triggers"
date: 2020-07-01T14:07:00Z
draft: false
layout: merged-list

weight: 1

menu:
  docs:
    identifier: automation-triggers
    parent: automation

---

Every rule starts with a trigger. They kick off the execution of your rules. Triggers will listen for events in Jira, such as when an 
issue is created or when a build is processed.
