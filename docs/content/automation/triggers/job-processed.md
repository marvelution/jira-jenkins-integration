---
title: "Job Processed"
date: 2024-11-04T00:00:00Z
draft: false

menu:
  docs:
    parent: automation-triggers

intro: Start a rule when a job is processed.

---

This rule runs whenever a Jenkins job has been processed and stored.
You can limit the trigger to match only a specific job, or any job that has links to one or more issues.

**Related smart values:** [{{job}}]({{< relref "/automation/smart-values#job" >}})

{{< screenshot "automation/job-processed.jpg" >}}
