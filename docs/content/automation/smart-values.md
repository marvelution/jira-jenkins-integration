---
title: "Smart Values"
date: 2020-07-01T14:07:00Z
draft: false

weight: 4

menu:
  docs:
    parent: automation
    weight: 4

---

Smart values are placeholders that let you pull in dynamic data. You can use them to access and manipulate almost any issue data from Jira.

## Syntax and Formatting

For more info on the syntax, formatting, and how to access issue fields,
see [Smart values - syntax and formatting](https://confluence.atlassian.com/display/AUTOMATION/Smart+values+-+syntax+and+formatting).

## View available smart values

### site

|                       |                                                |
|-----------------------|------------------------------------------------|
| `{{site.id}}`         | The ID of the site.                            |
| `{{site.name}}`       | The name of the site.                          |
| `{{site.type}}`       | The type of the site.                          |
| `{{site.displayUrl}}` | The URL of the site that can be used by users. |
| `{{site.scope}}`      | The scope (Project) of the site.               |

### job

Available when using the [Job Processed]({{< relref "/automation/triggers#job-processed" >}}) rule trigger.

|                       |                                                                                                                     |
|-----------------------|---------------------------------------------------------------------------------------------------------------------|
| `{{job.id}}`          | The ID of the job.                                                                                                  |
| `{{job.site}}`        | References the site the job is hosted on, see [site](#site). Use `{{job.site.name}}` to print the name of the site. |
| `{{job.name}}`        | The name of the job.                                                                                                |
| `{{job.parentName}}`  | The name of the parent job.                                                                                         |
| `{{job.fullName}}`    | The name of the job.                                                                                                |
| `{{job.urlName}}`     | The name of the job.                                                                                                |
| `{{job.displayName}}` | The display name of the job.                                                                                        |
| `{{job.displayUrl}}`  | The URL of the job that can be used by users.                                                                       |
| `{{job.description}}` | The description of the job.                                                                                         |
| `{{job.lastBuild}}`   | The number of the last known build.                                                                                 |
| `{{job.oldestBuild}}` | The number of the first known build.                                                                                |
| `{{job.linked}}`      | Boolean value that indicates that the job is enabled for build-to-issue indexing.                                   |
| `{{job.deleted}}`     | Boolean value that indicates that the job is marked as deleted in the app.                                          |

### build

Available when using the [Build Processed]({{< relref "/automation/triggers#build-processed" >}}) rule trigger.

|                         |                                                                                                                                                                        |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `{{build.id}}`          | The ID of the build.                                                                                                                                                   |
| `{{build.job}}`         | References the job of the build, see [job](#job). Use `{{build.job.name}}` to print the name of the job.                                                               |
| `{{build.number}}`      | The build number.                                                                                                                                                      |
| `{{build.displayName}}` | The display name of the build, defaults to `{{build.job.displayName}} #{{build.number}}`                                                                               | 
| `{{build.displayUrl}}`  | The URL of the build that can be used by users.                                                                                                                        |
| `{{build.description}}` | The description of the build.                                                                                                                                          |
| `{{build.deleted}}`     | Boolean value that indicates that the build is marked as deleted in the app.                                                                                           |
| `{{build.cause}}`       | The cause of the build.                                                                                                                                                |
| `{{build.result}}`      | References the result type of the build.{{< newline >}}Known results include, from best to worst: `SUCCESS`, `UNSTABLE`, `FAILURE`, `NOT_BUILT`, `ABORTED`, `UNKNOWN`. | 
| `{{build.builtOn}}`     | The name of the node where the build was executed on.                                                                                                                  |
| `{{build.duration}}`    | The duration of the build in milliseconds.                                                                                                                             |
| `{{build.timestamp}}`   | The date the build was scheduled, see [Jira smart values - date and time](https://confluence.atlassian.com/display/AUTOMATION/Jira+smart+values+-+date+and+time).      |
