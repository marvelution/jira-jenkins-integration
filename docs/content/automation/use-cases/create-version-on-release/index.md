---
title: "Use case: Release version after the release build"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
- common-use-cases/release-a-new-version-when-the-release-build-is-successful/

menu:
  docs:
    parent: automation
    weight: 21

---

Automatically release a new version in Jira when ever a release build successfully completes.

## The Rule

After you installed the app, you are ready to configure your Rule.

* For a global rule:
    * From the top navigation in Jira, choose **Gear Icon** > **System**.
    * Choose **Automation for Jira** > **Automation rules** and click on **Create Rule**.
* For a project rule:
    * Navigate to your project and choose **Project Settings**
    * Choose **Automation** and click on **Create Rule**.
* Select [Build Processed]({{< relref "/automation/triggers/#build-processed" >}}) as the trigger that executes the rule.
* Configure the trigger by selecting **equal to** and **Success** to only trigger the rule if the build succeeded.
* Select the **job** to limit the rule to only your release job and click **Save**.
* Select **Release version** action.
* Select the **project** to release the version in.
* Specify the **name** of the version
  ```
  Release {{build.number}}
  ```
* Specify the **release date**
  ```
  {{now.jiraDate}}
  ```
* click on **Save**
* You should have a rule that looks similar to the one in screenshot below.
* Specify a meaningful name, like **Release new Version**.
* Click on **Turn it on** to store and activate the rule.

{{< screenshot "screenshot.jpg" >}}
