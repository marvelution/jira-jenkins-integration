---
title: "Use case: Create issue for failing builds"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
- common-use-cases/create-an-issue-if-a-build-is-unsuccessful/

menu:
  docs:
    parent: automation
    weight: 20

---

Some builds get more eyes on them than others, but builds that are not successful should generally be looked at to determine if actions
are needed. Automatically creating an issue can make sure all unsuccessful builds get eyes on them.

## The Rule

After you installed the app, you are ready to configure your Rule.

* For a global rule:
  * From the top navigation in Jira, choose **Gear Icon** > **System**.
  * Choose **Automation for Jira** > **Automation rules** and click on **Create Rule**.
* For a project rule:
  * Navigate to your project and choose **Project Settings**
  * Choose **Automation** and click on **Create Rule**.
* Select [Build Processed]({{< relref "/automation/triggers/#build-processed" >}}) as the trigger that executes the rule.
* Configure the trigger by selecting **worse than** and **Success** to only trigger the rule if the build did not succeed and click 
  **Save**.
* Click on **New Action** to add an action to the rule.
* Select **Create issue** action.
* Select the **project** to create the issue in.
* Select the **issue type** off the to be created issue.
* Specify the **summary** of the issue
  ```
  Build {{build.displayName}} did not succeed
  ```
* Optionally specify a **description** with additional event data
  ```
  {{build.displayName}} resulted in {{build.result}}
  
  See {{build.displayUrl}}
  ```
* Click on **Save**
* You should have a rule that looks similar to the one in screenshot below.
* Give the rule a meaningful name, like **Create issue for failing builds**.
* Click on **Turn it on** to store and activate the rule.

{{< screenshot "screenshot.jpg" >}}
