---
title: "Use case: Transition issue when work starts"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
- common-use-cases/transition-issue-to-in-progress-when-development-has-started/

menu:
  docs:
    parent: automation
    weight: 22

---

We have all seen this from time to time, developers that eagerly start work on a new story but don't update them to reflect this.
Automatically transitioning issues linked to a build can make sure issues in Jira reflect the actual state, even when developers forget.

## The Rule

After you installed the add-ons, you are ready to configure your Rule.

* For a global rule:
    * From the top navigation in Jira, choose **Gear Icon** > **System**.
    * Choose **Automation for Jira** > **Automation rules** and click on **Create Rule**.
* For a project rule:
    * Navigate to your project and choose **Project Settings**
    * Choose **Automation** and click on **Create Rule**.
* Select [Build Processed]({{< relref "/automation/triggers/#build-processed" >}}) as the trigger that executes the rule.
* Check **Only execute the rule if the build has issue(s) linked.** to limit the rule execute to builds that have an issue related and 
  click on **Save**.
* Select **Transition Issue**.
* Configure how the action should transition the issue, by either configuring a target status, or what specific transition to use.
* You should have a rule that looks similar to the one in screenshot below.
* Specify a meaningful name, like **Transition issue on build**.
* Click on **Turn it on** to store and activate the rule.

{{< screenshot "screenshot.jpg" >}}
