---
title: "Use case: Publish version on release"
date: 2024-11-05T00:00:00Z
draft: false

menu:
  docs:
    parent: automation
    weight: 22

---

Automatically trigger the release build whenever a version in Jira is released.

## The Rule

After you installed the add-ons, you are ready to configure your Rule.

* For a global rule:
    * From the top navigation in Jira, choose **Gear Icon** > **System**.
    * Choose **Automation for Jira** > **Automation rules** and click on **Create Rule**.
* For a project rule:
    * Navigate to your project and choose **Project Settings**
    * Choose **Automation** and click on **Create Rule**.
* Select **Version released** as the trigger that executes the rule.
* Click on **Save**.
* Select [Build Jenkins Job]({{< relref "/automation/actions/#build-jenkins-job" >}}).
* Configure what job(s) should be triggers, e.g. Select **Build the selected job** and select your release job.
* Configure what parameter(s) should be sent to Jenkins, e.g. a specific parameter with the name of the version being released.
* Specify a meaningful name, like **Publish version on release**.
* Click on **Turn it on** to store and activate the rule.

{{< screenshot "screenshot.jpg" >}}
