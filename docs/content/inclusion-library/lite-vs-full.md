---
title: "Lite vs Full"
date: 2020-07-01T14:07:00Z
draft: false
---

## Lite vs Full

Version 4.0.0 introduces a new flavour of the app, namely Jenkins for Jira - Lite, this new lite version is functionally the 
same as the existing server version of the app but with some key differences.

|                      | Jenkins for Jira (Lite) | Jenkins for Jira  |
|----------------------|:-----------------------:|:-----------------:|
| **Paid vs Free**     |          Free           |       Paid        |
| **Support**          |   Best effort support   | SLA based support |
| **Extensibility**    |  {{< icon "remove" >}}  | {{< icon "ok" >}} |
| **Jira Server**      |    {{< icon "ok" >}}    | {{< icon "ok" >}} |
| **Jira Data Center** |  {{< icon "remove" >}}  | {{< icon "ok" >}} |
| **Jira Cloud**       |  {{< icon "remove" >}}  | {{< icon "ok" >}} |

There are also some features differences highlighted on the [Features]({{< relref "/administration/app-configuration/#features" >}}) page.

The new lite version of the app was introduces to make it easier for users to get an SLA based support subscription on the Server
version of the app while at the same time keeping it available for those that don't want/need SLA based support. It also makes it
possible to add paid extensions to the integration app that would require a support subscription on the main 'Jenkins Integration
for Jira' app.
