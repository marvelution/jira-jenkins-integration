---
title: "Upgrade Paths"
date: 2020-07-01T14:07:00Z
draft: false
---

####  Before you upgrade

##### Determine the upgrade path

* When upgrading from earlier than 3.0.0, follow this path:{{< newline >}}
  {{< badge "danger" >}}OLDER VERSIONS{{< /badge >}} → {{< badge "warning" >}}2.3.5{{< /badge >}} → {{< badge "warning" >}}3.0.0
  {{< /badge >}} → {{< badge "warning" >}}4.0.1{{< /badge >}} → {{< badge "warning" >}}4.6.1{{< /badge >}} →
  {{< badge "warning" >}}5.0.0{{< /badge >}} → {{< badge "success" >}}LATEST{{< /badge >}}  
{{< warning >}} A new data model was introduced in version 3.0.0 which required data migration that depending on your data set can take
anywhere from a couple of seconds to multiple hours to complete. **It is advised to test this upgrade in a test/staging environment 
first!**{{< /warning >}}
* When upgrading from earlier than 4.7.0, follow this path:{{< newline >}}
  {{< badge "warning" >}}3.0.0{{< /badge >}} → {{< badge "warning" >}}4.0.1{{< /badge >}} → {{< badge "warning" >}}4.6.1{{< /badge >}} →
  {{< badge "warning" >}}5.0.0{{< /badge >}} → {{< badge "success" >}}LATEST{{< /badge >}} 
* When upgrading from 4.6.1 and newer, follow this path:{{< newline >}}
  {{< badge "warning" >}}4.6.1{{< /badge >}} → {{< badge "warning" >}}5.0.0{{< /badge >}} → {{< badge "success" >}}LATEST{{< /badge >}}    
