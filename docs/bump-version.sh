#!/bin/bash
#
# Copyright (c) 2012-present Marvelution Holding B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

DOCSPATH=`dirname $0`

sed_i()
{
  if [[ "$OSTYPE" == "darwin"* ]]; then
    sed -i '' "$@"
  else
    sed -i "$@"
  fi
}

version=$1
version=${version%-*}
today=$(date "+%Y-%m-%d")
tomorrow=$(date +%Y-%m-%d -d "$DATE + 1 day")

echo "Bumping version in configuration..."

sed_i -e "s+/jenkins-for-jira/.*/+/jenkins-for-jira/${version}/+g" ${DOCSPATH}/config/_default/config.yaml
sed_i -e "s+/jenkins-for-jira/.*/+/jenkins-for-jira/${version}/+g" ${DOCSPATH}/config/staging/config.yaml
sed_i -e "s+/jenkins-for-jira/.*/+/jenkins-for-jira/${version}/+g" ${DOCSPATH}/config/production/config.yaml

nextNotes="${DOCSPATH}/content/releases/jira/next.md"
versionNotes="${DOCSPATH}/content/releases/jira/${version}.md"

if [[ -f $nextNotes ]]
then
  echo "Moving ${nextNotes} to ${versionNotes}"
  mv $nextNotes $versionNotes
fi

if [[ -f $versionNotes ]]
then
  echo "Updating title and publish date in ${versionNotes}"
  sed_i -e "s+next+${version}+g" \
    -e "s+date:.*T+date: ${today}T+g" \
    -e "s+draft: true+draft: false+g" \
    $versionNotes

    git add $versionNotes
else
  echo "Generating next release notes file: ${nextNotes}"
  cat <<EOF > $nextNotes
---
title: "next"
date: ${tomorrow}T00:00:00Z
draft: true
layout: none
outputs:
- note
---

*    What was added?

EOF
  git add $nextNotes
fi
