module bitbucket.org/marvelution/jira-jenkins-integration/docs

go 1.14

//replace bitbucket.org/marvelution/jira-jenkins-integration-common/docs => ../../jira-jenkins-integration-common/docs
//replace bitbucket.org/marvelution/marvelution-docs-theme => ../../../Sites/marvelution-docs-theme

require (
	bitbucket.org/marvelution/jira-jenkins-integration-common/docs v0.0.0-20250107153107-6ad2d4b19819
	bitbucket.org/marvelution/marvelution-docs-theme v0.2.11
)
