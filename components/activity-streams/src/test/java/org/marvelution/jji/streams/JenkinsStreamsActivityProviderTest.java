/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.streams;

import java.util.*;

import org.marvelution.jji.DefaultAppState;
import org.marvelution.jji.data.services.api.IssueLinkService;
import org.marvelution.jji.data.services.api.IssueLinkService.LinkRequest;
import org.marvelution.testing.TestSupport;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.StandardStreamsFilterOption;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mock;

import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.marvelution.jji.data.helper.FeaturesEnabledHelper.VIEW_JENKINS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class JenkinsStreamsActivityProviderTest
        extends TestSupport
{

    @Mock
    private IssueLinkService issueLinkService;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private TemplateRenderer templateRenderer;
    @Mock
    private WebResourceUrlProvider urlProvider;
    private JenkinsStreamsActivityProvider streamsActivityProvider;

    @BeforeEach
    void setUp()
    {
        DefaultAppState addonHelper = new DefaultAppState(null, null);
        streamsActivityProvider = new JenkinsStreamsActivityProvider(addonHelper,
                issueLinkService,
                projectManager,
                permissionManager,
                authenticationContext,
                i18nResolver,
                templateRenderer,
                urlProvider);
    }

    @Test
    void testGetLinkRequest()
    {
        Map<String, Collection<Pair<StreamsFilterType.Operator, Iterable<String>>>> filters = new HashMap<>();
        filters.put(StandardStreamsFilterOption.PROJECT_KEY, new ArrayList<>()
        {{
            add(Pair.pair(StreamsFilterType.Operator.IS, singletonList("PROJECTA")));
            add(Pair.pair(StreamsFilterType.Operator.NOT, singletonList("PROJECTB")));
        }});
        filters.put(StandardStreamsFilterOption.ISSUE_KEY.getKey(), new ArrayList<>()
        {{
            add(Pair.pair(StreamsFilterType.Operator.IS, singletonList("PROJECTA-1")));
            add(Pair.pair(StreamsFilterType.Operator.NOT, singletonList("PROJECTB-1")));
        }});

        Project projectA = mock(Project.class);
        when(projectA.getKey()).thenReturn("PROJECTA");
        when(projectManager.getProjectObjByKey("PROJECTA")).thenReturn(projectA);
        when(permissionManager.hasPermission(eq(VIEW_DEV_TOOLS), eq(projectA), any(ApplicationUser.class))).thenReturn(true);

        LinkRequest buildIssueFilter = testGetLinkRequest(filters);
        assertThat(buildIssueFilter.getInProjectKeys()).hasSize(1)
                .containsOnly("PROJECTA");
        assertThat(buildIssueFilter.getNotInProjectKeys()).hasSize(1)
                .containsOnly("PROJECTB");
        assertThat(buildIssueFilter.getInIssueKeys()).hasSize(1)
                .containsOnly("PROJECTA-1");
        assertThat(buildIssueFilter.getNotInIssueKeys()).hasSize(1)
                .containsOnly("PROJECTB-1");

        verify(projectManager).getProjectObjByKey("PROJECTA");
        verify(permissionManager).hasPermission(eq(VIEW_DEV_TOOLS), eq(projectA), any(ApplicationUser.class));
        verifyNoMoreInteractions(projectManager, permissionManager);
    }

    @Test
    void testGetLinkRequest_CustomPermission()
    {
        Map<String, Collection<Pair<StreamsFilterType.Operator, Iterable<String>>>> filters = new HashMap<>();
        filters.put(StandardStreamsFilterOption.PROJECT_KEY, new ArrayList<>()
        {{
            add(Pair.pair(StreamsFilterType.Operator.IS, singletonList("PROJECTA")));
            add(Pair.pair(StreamsFilterType.Operator.NOT, singletonList("PROJECTB")));
        }});
        filters.put(StandardStreamsFilterOption.ISSUE_KEY.getKey(), new ArrayList<>()
        {{
            add(Pair.pair(StreamsFilterType.Operator.IS, singletonList("PROJECTA-1")));
            add(Pair.pair(StreamsFilterType.Operator.NOT, singletonList("PROJECTB-1")));
        }});

        Project projectA = mock(Project.class);
        when(projectA.getKey()).thenReturn("PROJECTA");
        when(projectManager.getProjectObjByKey("PROJECTA")).thenReturn(projectA);
        when(permissionManager.hasPermission(any(ProjectPermissionKey.class),
                eq(projectA),
                any(ApplicationUser.class))).then(invocation -> {
            ProjectPermissionKey key = invocation.getArgument(0);
            return VIEW_JENKINS.equals(key);
        });

        LinkRequest buildIssueFilter = testGetLinkRequest(filters);
        assertThat(buildIssueFilter.getInProjectKeys()).hasSize(1)
                .containsOnly("PROJECTA");
        assertThat(buildIssueFilter.getNotInProjectKeys()).hasSize(1)
                .containsOnly("PROJECTB");
        assertThat(buildIssueFilter.getInIssueKeys()).hasSize(1)
                .containsOnly("PROJECTA-1");
        assertThat(buildIssueFilter.getNotInIssueKeys()).hasSize(1)
                .containsOnly("PROJECTB-1");

        verify(projectManager).getProjectObjByKey("PROJECTA");
        verify(permissionManager).hasPermission(eq(VIEW_DEV_TOOLS), eq(projectA), any(ApplicationUser.class));
        verify(permissionManager).hasPermission(eq(VIEW_JENKINS), eq(projectA), any(ApplicationUser.class));
    }

    @Test
    void testGetLinkRequest_WithUsersInFilter()
    {
        Map<String, Collection<Pair<StreamsFilterType.Operator, Iterable<String>>>> filters = new HashMap<>();
        filters.put(StandardStreamsFilterOption.USER.getKey(),
                Collections.singletonList(Pair.pair(StreamsFilterType.Operator.IS, singletonList("admin"))));

        LinkRequest buildIssueFilter = testGetLinkRequest(filters);
        assertThat(buildIssueFilter.getInProjectKeys()).isEmpty();
        assertThat(buildIssueFilter.getNotInProjectKeys()).isEmpty();
        assertThat(buildIssueFilter.getInIssueKeys()).isEmpty();
        assertThat(buildIssueFilter.getNotInIssueKeys()).isEmpty();

        verifyNoMoreInteractions(projectManager, permissionManager);
    }

    @Test
    void testGetLinkRequest_WithUsersNotFilter()
    {
        Map<String, Collection<Pair<StreamsFilterType.Operator, Iterable<String>>>> filters = new HashMap<>();
        filters.put(StandardStreamsFilterOption.USER.getKey(),
                Collections.singletonList(Pair.pair(StreamsFilterType.Operator.NOT, singletonList("admin"))));

        Project projectA = mock(Project.class);
        when(projectA.getKey()).thenReturn("PROJECTA");
        Project projectB = mock(Project.class);
        when(projectManager.getProjectObjects()).thenReturn(Arrays.asList(projectA, projectB));
        when(permissionManager.hasPermission(eq(VIEW_DEV_TOOLS), eq(projectA), any(ApplicationUser.class))).thenReturn(true);

        LinkRequest buildIssueFilter = testGetLinkRequest(filters);
        assertThat(buildIssueFilter.getInProjectKeys()).hasSize(1)
                .containsOnly("PROJECTA");
        assertThat(buildIssueFilter.getNotInProjectKeys()).isEmpty();
        assertThat(buildIssueFilter.getInIssueKeys()).isEmpty();
        assertThat(buildIssueFilter.getNotInIssueKeys()).isEmpty();

        verify(projectManager).getProjectObjects();
        verify(permissionManager).hasPermission(eq(VIEW_DEV_TOOLS), eq(projectA), any(ApplicationUser.class));
        verify(permissionManager).hasPermission(eq(VIEW_DEV_TOOLS), eq(projectB), any(ApplicationUser.class));
    }

    private LinkRequest testGetLinkRequest(Map<String, Collection<Pair<StreamsFilterType.Operator, Iterable<String>>>> filters)
    {
        ActivityRequest activityRequest = mock(ActivityRequest.class);
        when(activityRequest.getStandardFiltersMap()).thenReturn(filters);
        return streamsActivityProvider.getLinkRequest(activityRequest);
    }
}
