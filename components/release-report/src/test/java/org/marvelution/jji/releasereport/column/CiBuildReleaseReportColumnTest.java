/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.releasereport.column;

import java.util.Arrays;
import java.util.Map;

import org.marvelution.jji.DefaultAppState;
import org.marvelution.jji.events.IssueLinksDeletedEvent;
import org.marvelution.jji.events.IssueLinksUpdatedEvent;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.releasereport.ReleaseReportHelper;
import org.marvelution.testing.TestSupport;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.devstatus.api.VersionWarningCategoryRequest;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.marvelution.jji.model.Result.*;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class CiBuildReleaseReportColumnTest
        extends TestSupport
{

    @Mock
    private VersionWarningCategoryRequest context;
    @Mock
    private ReleaseReportHelper reportHelper;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    private CiBuildReleaseReportColumn ciBuildReleaseReportColumn;
    @Mock
    private Issue issue;
    @Captor
    private ArgumentCaptor<Map<String, Object>> contextCaptor;

    @BeforeEach
    void setUp()
    {
        DefaultAppState appState = new DefaultAppState(null, null)
        {
            @Override
            public String getCompleteModuleKey(String moduleKey)
            {
                return moduleKey;
            }
        };
        ciBuildReleaseReportColumn = new CiBuildReleaseReportColumn(appState,
                reportHelper,
                i18nHelper,
                new MemoryCacheManager(),
                soyTemplateRenderer);
    }

    @Test
    void testRenderIssues()
    {
        when(issue.getKey()).thenReturn("JJI-1");
        when(reportHelper.getBuildInformation("JJI-1")).thenAnswer(invocation -> {
            Job foo = new Job().setId("1")
                    .setName("foo");
            Build build1 = new Build().setId("1")
                    .setJob(foo)
                    .setNumber(1);
            build1.setResult(SUCCESS);
            foo.getBuilds()
                    .add(build1);
            Build build2 = new Build().setId("2")
                    .setJob(foo)
                    .setNumber(2);
            build2.setResult(UNSTABLE);
            foo.getBuilds()
                    .add(build2);
            Job bar = new Job().setId("2")
                    .setName("bar");
            Build build3 = new Build().setId("3")
                    .setJob(bar)
                    .setNumber(1);
            build3.setResult(FAILURE);
            bar.getBuilds()
                    .add(build3);
            Build build4 = new Build().setId("4")
                    .setJob(bar)
                    .setNumber(2);
            build4.setResult(ABORTED);
            bar.getBuilds()
                    .add(build4);
            return Arrays.asList(foo, bar);
        });
        when(soyTemplateRenderer.render(anyString(), anyString(), anyMap())).thenReturn("JJI-1");
        ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
        verify(reportHelper).getBuildInformation("JJI-1");
        verify(soyTemplateRenderer).render(anyString(), anyString(), contextCaptor.capture());
        Map<String, Object> context = contextCaptor.getValue();
        assertThat(context.keySet(), hasSize(6));
        assertThat(context.keySet(), hasItem("issueKey"));
        assertThat(context.get("issueKey"), is("JJI-1"));
        assertThat(context.keySet(), hasItem("count"));
        assertThat(context.get("count"), is(2));
        assertThat(context.keySet(), hasItem("successfulBuildCount"));
        assertThat(context.get("successfulBuildCount"), is(0));
        assertThat(context.keySet(), hasItem("unstableBuildCount"));
        assertThat(context.get("unstableBuildCount"), is(1));
        assertThat(context.keySet(), hasItem("failedBuildCount"));
        assertThat(context.get("failedBuildCount"), is(0));
        assertThat(context.keySet(), hasItem("unknownBuildCount"));
        assertThat(context.get("unknownBuildCount"), is(1));
        Map<String, String> issueHtml = ciBuildReleaseReportColumn.getIssueHtml();
        assertThat(issueHtml.keySet(), hasSize(1));
        assertThat(issueHtml.keySet(), hasItem("JJI-1"));
        assertThat(issueHtml.get("JJI-1"), is("JJI-1"));
    }

    @Test
    void testRenderIssues_IssueLinksUpdateEvent()
    {
        when(issue.getKey()).thenReturn("JJI-1");
        when(reportHelper.getBuildInformation("JJI-1")).thenAnswer(invocation -> {
            Job foo = new Job().setId("1")
                    .setName("foo");
            Build build1 = new Build().setId("1")
                    .setJob(foo)
                    .setNumber(1);
            build1.setResult(SUCCESS);
            foo.getBuilds()
                    .add(build1);
            return singletonList(foo);
        });
        when(soyTemplateRenderer.render(anyString(), anyString(), anyMap())).thenReturn("JJI-1");
        // Initial call to fill the cache
        ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
        verify(reportHelper, times(1)).getBuildInformation("JJI-1");
        verify(soyTemplateRenderer, times(1)).render(anyString(), anyString(), anyMap());
        // Calling renderIssues should return from cache
        ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
        verify(reportHelper, times(1)).getBuildInformation("JJI-1");
        verify(soyTemplateRenderer, times(1)).render(anyString(), anyString(), anyMap());
        // IssueLinksUpdateEvent should clear cache
        ciBuildReleaseReportColumn.onIssueLinksUpdatedEvent(new IssueLinksUpdatedEvent("1", "JJI-1"));
        ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
        verify(reportHelper, times(2)).getBuildInformation("JJI-1");
        verify(soyTemplateRenderer, times(2)).render(anyString(), anyString(), anyMap());
    }

    @Test
    void testRenderIssues_IssueLinksDeletedEvent_IssueKey()
    {
        when(issue.getKey()).thenReturn("JJI-1");
        when(reportHelper.getBuildInformation("JJI-1")).thenAnswer(invocation -> {
            Job foo = new Job().setId("1")
                    .setName("foo");
            Build build1 = new Build().setId("1")
                    .setJob(foo)
                    .setNumber(1);
            build1.setResult(SUCCESS);
            foo.getBuilds()
                    .add(build1);
            return singletonList(foo);
        });
        when(soyTemplateRenderer.render(anyString(), anyString(), anyMap())).thenReturn("JJI-1");
        // Initial call to fill the cache
        ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
        verify(reportHelper, times(1)).getBuildInformation("JJI-1");
        verify(soyTemplateRenderer, times(1)).render(anyString(), anyString(), anyMap());
        // Calling renderIssues should return from cache
        ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
        verify(reportHelper, times(1)).getBuildInformation("JJI-1");
        verify(soyTemplateRenderer, times(1)).render(anyString(), anyString(), anyMap());
        // IssueLinksDeletedEvent should clear cache
        ciBuildReleaseReportColumn.onIssueLinksDeletedEvent(new IssueLinksDeletedEvent(null, "JJI-1"));
        ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
        verify(reportHelper, times(2)).getBuildInformation("JJI-1");
        verify(soyTemplateRenderer, times(2)).render(anyString(), anyString(), anyMap());
    }

    @Test
    void testRenderIssues_IssueLinksDeletedEvent_ProjectKey()
    {
        when(issue.getKey()).thenReturn("JJI-1");
        when(reportHelper.getBuildInformation("JJI-1")).thenAnswer(invocation -> {
            Job foo = new Job().setId("1")
                    .setName("foo");
            Build build1 = new Build().setId("1")
                    .setJob(foo)
                    .setNumber(1);
            build1.setResult(SUCCESS);
            foo.getBuilds()
                    .add(build1);
            return singletonList(foo);
        });
        when(soyTemplateRenderer.render(anyString(), anyString(), anyMap())).thenReturn("JJI-1");
        // Initial call to fill the cache
        ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
        verify(reportHelper, times(1)).getBuildInformation("JJI-1");
        verify(soyTemplateRenderer, times(1)).render(anyString(), anyString(), anyMap());
        // Calling renderIssues should return from cache
        ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
        verify(reportHelper, times(1)).getBuildInformation("JJI-1");
        verify(soyTemplateRenderer, times(1)).render(anyString(), anyString(), anyMap());
        // IssueLinksDeletedEvent should clear cache
        ciBuildReleaseReportColumn.onIssueLinksDeletedEvent(new IssueLinksDeletedEvent("JJI"));
        ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
        verify(reportHelper, times(2)).getBuildInformation("JJI-1");
        verify(soyTemplateRenderer, times(2)).render(anyString(), anyString(), anyMap());
    }
}
