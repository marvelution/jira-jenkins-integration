/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.releasereport.warning;

import java.io.*;
import java.util.*;
import javax.annotation.*;

import org.marvelution.jji.data.helper.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.releasereport.common.*;

import com.atlassian.jira.avatar.*;
import com.atlassian.jira.bc.issue.search.*;
import com.atlassian.jira.issue.*;
import com.atlassian.jira.issue.search.*;
import com.atlassian.jira.jql.builder.*;
import com.atlassian.jira.plugin.devstatus.api.*;
import com.atlassian.jira.user.*;
import com.atlassian.jira.util.*;
import com.atlassian.jira.web.bean.*;
import com.atlassian.query.*;
import com.atlassian.query.operator.*;
import com.atlassian.query.order.*;
import com.atlassian.soy.renderer.*;

import static java.util.Collections.*;
import static java.util.stream.Collectors.*;

abstract class WorstBuildResultVersionWarningCategory
		implements VersionWarningCategory
{

	private static final PagerFilter FIRST_PAGE = PagerFilter.newPageAlignedFilter(0, 20);
	private static final String RELEASE_REPORT_TEMPLATE_COMPLETE_MODULE_KEY = "com.atlassian.jira.plugins.jira-development-integration-plugin:release-report-soy-templates";
	private static final String RELEASE_WARNING_TEMPLATE_NAME = "JIRA.Templates.ReleaseReport.warningCategoryList";
	private final FeaturesEnabledHelper featuresEnabledHelper;
	private final SearchService searchService;
	private final I18nHelper.BeanFactory i18nHelperFactory;
	private final SoyTemplateRenderer soyTemplateRenderer;
	private final AvatarService avatarService;
	private final Result result;

	WorstBuildResultVersionWarningCategory(
			FeaturesEnabledHelper featuresEnabledHelper,
			SearchService searchService,
			I18nHelper.BeanFactory i18nHelperFactory,
			SoyTemplateRenderer soyTemplateRenderer,
			AvatarService avatarService,
			Result result)
	{
		this.featuresEnabledHelper = featuresEnabledHelper;
		this.searchService = searchService;
		this.i18nHelperFactory = i18nHelperFactory;
		this.soyTemplateRenderer = soyTemplateRenderer;
		this.avatarService = avatarService;
		this.result = result;
	}

	@Override
	public boolean shouldDisplay(@Nonnull VersionWarningCategoryRequest request)
	{
		return featuresEnabledHelper.areFeaturesEnabledForProject(request.getProject());
	}

	@Override
	public long count(@Nonnull VersionWarningCategoryRequest request)
	{
		try
		{
			return searchService.searchCount(request.getUser(), getQuery(request));
		}
		catch (SearchException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public VersionWarningCategoryResponse getResponse(@Nonnull VersionWarningCategoryRequest request)
			throws VersionWarningCategoryException
	{
		ErrorCollection errorCollection = new SimpleErrorCollection();
		VersionWarningCategoryResponse.Builder builder = new VersionWarningCategoryResponse.Builder();
		try
		{
			Map<String, Object> model = getTemplateModel(request.getUser(), getQuery(request));
			builder.setContent(renderTemplate(model));
		}
		catch (SearchException e)
		{
			errorCollection.addErrorMessage(
					i18nHelperFactory.getInstance(request.getUser()).getText("release.report.warnings.fetch.issues.error"));
		}
		return builder.setSuccessful(!errorCollection.hasAnyErrors()).setErrorCollection(errorCollection).build();
	}

	private Query getQuery(VersionWarningCategoryRequest request)
	{
		return JqlQueryBuilder.newBuilder()
				.where()
				.project(request.getProject().getId())
				.and()
				.fixVersion(request.getVersion().getId())
				.and()
				.statusCategory("done")
				.and()
				.addStringCondition("jenkinsWorstresult", Operator.EQUALS, result.key())
				.endWhere()
				.orderBy()
				.priority(SortOrder.DESC)
				.issueKey(SortOrder.ASC)
				.endOrderBy()
				.buildQuery();
	}

	protected Map<String, Object> getTemplateModel(
			ApplicationUser user,
			Query query)
			throws SearchException
	{
		Map<String, Object> model = new HashMap<>();
		SearchResults<Issue> searchResults = searchService.search(user, query, FIRST_PAGE);
		List<Issue> issues = searchResults.getResults();
		model.put("issues", issues);
		model.put("unassignedAvatar", getAvatarMetaData(user, null));
		model.put("avatars", issues.stream().collect(toMap(Issue::getKey, issue -> getAvatarMetaData(user, issue))));
		model.put("issueCount", searchResults.getTotal());
		model.put("jqlQuery", searchService.getJqlString(query));
		model.put("pluggableColumns", emptyList());
		return model;
	}

	private String renderTemplate(Map<String, Object> model)
			throws VersionWarningCategoryException
	{
		try
		{
			StringWriter writer = new StringWriter();
			soyTemplateRenderer.render(writer, RELEASE_REPORT_TEMPLATE_COMPLETE_MODULE_KEY, RELEASE_WARNING_TEMPLATE_NAME, model);
			return writer.toString();
		}
		catch (SoyException e)
		{
			throw new VersionWarningCategoryException(e);
		}
	}

	private AvatarMetaData getAvatarMetaData(
			ApplicationUser user,
			@Nullable Issue issue)
	{
		String displayName;
		ApplicationUser assignee;
		if (issue == null)
		{
			assignee = null;
			displayName = null;
		}
		else
		{
			assignee = issue.getAssignee();
			displayName = assignee == null ? issue.getAssigneeId() : assignee.getDisplayName();
		}
		return new AvatarMetaData(displayName, avatarService.getAvatarURL(user, assignee, Avatar.Size.NORMAL).toString());
	}

}
