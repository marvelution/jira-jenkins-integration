/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.releasereport.warning;

import org.marvelution.jji.data.helper.*;

import com.atlassian.jira.avatar.*;
import com.atlassian.jira.bc.issue.search.*;
import com.atlassian.jira.util.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import com.atlassian.soy.renderer.*;

import static org.marvelution.jji.model.Result.*;

public class UnstableCiBuildsVersionWarningCategory
		extends WorstBuildResultVersionWarningCategory
{

	public UnstableCiBuildsVersionWarningCategory(
			FeaturesEnabledHelper featuresEnabledHelper,
			@ComponentImport SearchService searchService,
			@ComponentImport I18nHelper.BeanFactory i18nHelperFactory,
			@ComponentImport SoyTemplateRenderer soyTemplateRenderer,
			@ComponentImport AvatarService avatarService)
	{
		super(featuresEnabledHelper, searchService, i18nHelperFactory, soyTemplateRenderer, avatarService, UNSTABLE);
	}
}
