/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.releasereport;

import java.util.*;
import javax.inject.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;

import static java.util.stream.Collectors.*;

/**
 * Helper for the Release Report features
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
@Named
public class ReleaseReportHelper
{

	private final BuildService buildService;
	private final IssueLinkService issueLinkService;

	@Inject
	public ReleaseReportHelper(
			BuildService buildService,
			IssueLinkService issueLinkService)
	{
		this.buildService = buildService;
		this.issueLinkService = issueLinkService;
	}

	public List<Job> getBuildInformation(String issueKey)
	{
		return issueLinkService.getJobsForIssue(false, issueKey).stream().map(match -> {
			Job job = match.copy();
			buildService.getLast(job).ifPresent(job::addBuild);
			return job;
		}).collect(toList());
	}
}
