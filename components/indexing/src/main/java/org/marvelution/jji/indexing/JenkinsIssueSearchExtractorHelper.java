/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.indexing;

import java.time.*;
import java.util.*;
import javax.inject.*;

import org.marvelution.jji.api.features.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;

import com.atlassian.jira.index.*;
import com.atlassian.jira.issue.*;
import com.atlassian.jira.issue.customfields.converters.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import org.slf4j.*;

import static org.marvelution.jji.api.features.FeatureFlags.*;

/**
 * {@link IssueSearchExtractor} specific to add Jenkins builds fields to the Issue Index
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
@Named
public class JenkinsIssueSearchExtractorHelper
{

	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsIssueSearchExtractorHelper.class);
	private static final String ISSUE_PROPERTY_FIELD_PREFIX = "ISSUEPROP_jenkins$";
	static final String TOTAL_FIELD_NAME = ISSUE_PROPERTY_FIELD_PREFIX + "total";
	static final String TOTAL_BUILDS_FIELD_NAME = ISSUE_PROPERTY_FIELD_PREFIX + "buildTotal";
	static final String WORST_RESULT_FIELD_NAME = ISSUE_PROPERTY_FIELD_PREFIX + "worstresult";
	static final String TOTAL_DEPLOYS_FIELD_NAME = ISSUE_PROPERTY_FIELD_PREFIX + "deployTotal";
	static final String WORST_DEPLOY_RESULT_FIELD_NAME = ISSUE_PROPERTY_FIELD_PREFIX + "deployWorstresult";
	private final Features features;
	private final IssueLinkService issueLinkService;
	private final DoubleConverter doubleConverter;
	private Instant lastIndexWarningLogTimestamp;

	@Inject
	public JenkinsIssueSearchExtractorHelper(
			Features features,
			IssueLinkService issueLinkService,
			@ComponentImport DoubleConverter doubleConverter)
	{
		this.features = features;
		this.issueLinkService = issueLinkService;
		this.doubleConverter = doubleConverter;
	}

	static String fieldNameForResult(Result result)
	{
		return fieldNameForResult(result, false);
	}

	static String fieldNameForResult(
			Result result,
			boolean isDeploy)
	{
		String fieldName = result.key();
		if (isDeploy)
		{
			fieldName = "deploy" + fieldName.substring(0, 1).toUpperCase(Locale.ENGLISH) + fieldName.substring(1);
		}
		return ISSUE_PROPERTY_FIELD_PREFIX + fieldName;
	}

	Map<String, String> indexIssue(Issue issue)
	{
		Map<String, String> fields = new HashMap<>();
		if (features.isFeatureEnabled(DISABLE_ISSUE_INDEXING))
		{
			if (lastIndexWarningLogTimestamp == null || lastIndexWarningLogTimestamp.plus(Duration.ofHours(12)).isAfter(Instant.now()))
			{
				setLastIndexWarningLogTimestamp(Instant.now());
				LOGGER.warn("Issue indexing is disabled, disable feature {} to enable indexing again.", DISABLE_ISSUE_INDEXING);
			}
		}
		else
		{
			long startTime = System.currentTimeMillis();

			LinkStatistics linkStatistics = issueLinkService.getLinkStatistics(issue.getKey());
			if (linkStatistics.getTotal() > 0)
			{
				fields.put(TOTAL_FIELD_NAME, doubleConverter.getStringForLucene(Double.valueOf(linkStatistics.getTotal())));
				fields.put(TOTAL_BUILDS_FIELD_NAME, doubleConverter.getStringForLucene(Double.valueOf(linkStatistics.getBuildTotal())));
				fields.put(WORST_RESULT_FIELD_NAME, linkStatistics.getWorstResult().key());
				linkStatistics.getCountByResult()
						.forEach((result, count) -> fields.put(fieldNameForResult(result),
						                                       doubleConverter.getStringForLucene(Double.valueOf(count))));
				fields.put(TOTAL_DEPLOYS_FIELD_NAME, doubleConverter.getStringForLucene(Double.valueOf(linkStatistics.getDeployTotal())));
				fields.put(WORST_DEPLOY_RESULT_FIELD_NAME, linkStatistics.getWorstDeployResult().key());
				linkStatistics.getDeployCountByResult()
						.forEach((result, count) -> fields.put(fieldNameForResult(result, true),
						                                       doubleConverter.getStringForLucene(Double.valueOf(count))));
				LOGGER.debug("Indexing {} took {} ms", issue.getKey(), System.currentTimeMillis() - startTime);
			}
		}
		return fields;
	}

	private void setLastIndexWarningLogTimestamp(Instant lastIndexWarningLogTimestamp)
	{
		this.lastIndexWarningLogTimestamp = lastIndexWarningLogTimestamp;
	}
}
