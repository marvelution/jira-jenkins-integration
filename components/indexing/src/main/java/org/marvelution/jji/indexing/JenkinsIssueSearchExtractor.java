/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.indexing;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import com.atlassian.jira.index.IssueSearchExtractor;
import com.atlassian.jira.issue.Issue;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;

/**
 * {@link IssueSearchExtractor} specific to add Jenkins builds fields to the Issue Index specific for Jira 8.0 and later.
 *
 * @author Mark Rekveld
 * @since 3.7.0
 */
public class JenkinsIssueSearchExtractor implements IssueSearchExtractor {

	private final JenkinsIssueSearchExtractorHelper issueSearchExtractorHelper;

	public JenkinsIssueSearchExtractor(JenkinsIssueSearchExtractorHelper issueSearchExtractorHelper) {
		this.issueSearchExtractorHelper = issueSearchExtractorHelper;
	}

	@Override
	public Set<String> indexEntity(Context<Issue> context, Document document) {
		Map<String, String> fields = issueSearchExtractorHelper.indexIssue(context.getEntity());
		fields.entrySet().stream().map(field -> createField(field.getKey(), field.getValue())).forEach(document::add);
		return Collections.unmodifiableSet(fields.keySet());
	}

	private Field createField(String name, String value) {
		return new StringField(name, value, Field.Store.NO);
	}
}
