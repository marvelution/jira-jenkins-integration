/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.indexing;

import java.util.*;

import org.marvelution.jji.model.*;
import org.marvelution.testing.*;

import com.atlassian.jira.index.*;
import com.atlassian.jira.issue.*;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.assertj.core.api.Condition;
import org.hamcrest.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import static org.marvelution.jji.indexing.JenkinsIssueSearchExtractorHelper.*;

import static java.util.Collections.*;
import static org.apache.lucene.index.IndexOptions.*;
import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.HamcrestCondition.*;
import static org.mockito.Mockito.*;

/**
 * Testcase for {@link JenkinsIssueSearchExtractor}.
 *
 * @author Mark Rekveld
 * @since 3.7.0
 */
class JenkinsIssueSearchExtractorTest
		extends TestSupport
{

	@Mock
	private Issue issue;
	@Mock
	private EntitySearchExtractor.Context<Issue> context;
	@Mock
	private JenkinsIssueSearchExtractorHelper issueSearchExtractorHelper;
	private IssueSearchExtractor issueSearchExtractor;

	@BeforeEach
	void setUp()
	{
		issueSearchExtractor = new JenkinsIssueSearchExtractor(issueSearchExtractorHelper);
		when(context.getEntity()).thenReturn(issue);
	}

	@Test
	void testIndexEntity_NoBuilds()
	{
		when(issueSearchExtractorHelper.indexIssue(issue)).thenReturn(emptyMap());
		Document document = new Document();

		Set<String> fieldNames = issueSearchExtractor.indexEntity(context, document);

		assertThat(fieldNames).hasSize(0);
		assertThat(document.getFields()).hasSize(0);
	}

	@Test
	void testIndexEntity()
	{
		Map<String, String> fields = new HashMap<>();
		fields.put(fieldNameForResult(Result.SUCCESS), "1.0");
		fields.put(fieldNameForResult(Result.FAILURE), "10.0");
		fields.put(WORST_RESULT_FIELD_NAME, Result.FAILURE.key());
		when(issueSearchExtractorHelper.indexIssue(issue)).thenReturn(fields);
		Document document = new Document();

		Set<String> fieldNames = issueSearchExtractor.indexEntity(context, document);
		assertThat(fieldNames).hasSize(fields.keySet().size());
		fields.forEach((field, value) -> {
			assertThat(fieldNames).contains(field);
			assertThat(document.getFields()).haveExactly(1, field(field, value));
		});
	}

	private Condition<IndexableField> field(
			String fieldName,
			String expectedValue)
	{
		return matching(new DiagnosingMatcher<Field>()
		{
			@Override
			protected boolean matches(
					Object item,
					Description mismatchDescription)
			{
				if (item == null)
				{
					mismatchDescription.appendText("null");
					return false;
				}
				else if (!(item instanceof StringField))
				{
					mismatchDescription.appendValue(item).appendValue(" is a " + item.getClass().getName());
					return false;
				}
				StringField stringField = (StringField) item;
				if (!fieldName.equals(stringField.name()))
				{
					mismatchDescription.appendValue("field name does not match ").appendValue(stringField.name());
				}
				else if (stringField.fieldType().stored())
				{
					mismatchDescription.appendValue("field should not be stored");
				}
				else if (stringField.fieldType().indexOptions() != DOCS)
				{
					mismatchDescription.appendValue("field should be indexed");
				}
				else if (stringField.fieldType().tokenized())
				{
					mismatchDescription.appendValue("field should not be tokenized");
				}
				else if (!stringField.fieldType().omitNorms())
				{
					mismatchDescription.appendValue("field should omit norms");
				}
				else if (stringField.fieldType().storeTermVectors())
				{
					mismatchDescription.appendValue("field should not store term vector");
				}
				else
				{
					return expectedValue.equals(stringField.stringValue());
				}
				return false;
			}

			@Override
			public void describeTo(Description description)
			{
				description.appendText("fieldable for field ").appendValue(fieldName).appendText(" with value ").appendValue(expectedValue);
			}
		});
	}
}
