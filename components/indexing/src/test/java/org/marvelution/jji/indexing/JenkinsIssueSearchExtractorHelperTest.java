/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.indexing;

import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.api.features.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.testing.*;

import com.atlassian.jira.issue.*;
import com.atlassian.jira.issue.customfields.converters.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import static org.marvelution.jji.api.features.FeatureFlags.*;
import static org.marvelution.jji.indexing.JenkinsIssueSearchExtractorHelper.*;
import static org.marvelution.jji.model.Result.*;

import static java.util.stream.Collectors.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * Testcase for the {@link JenkinsIssueSearchExtractorHelper}
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
class JenkinsIssueSearchExtractorHelperTest
		extends TestSupport
{

	@Mock
	private IssueLinkService issueLinkService;
	@Mock
	private DoubleConverter doubleConverter;
	@Mock
	private Issue issue;
	private Features features;
	private JenkinsIssueSearchExtractorHelper helper;

	@BeforeEach
	void setUp()
	{
		features = new Features(new InMemoryFeatureStore())
		{
			@Override
			protected Set<FeatureFlag> loadFeatureFlags()
			{
				return Stream.of(new FeatureFlag(DISABLE_ISSUE_INDEXING, false, true)).collect(toSet());
			}
		};
		helper = new JenkinsIssueSearchExtractorHelper(features, issueLinkService, doubleConverter);
	}

	@Test
	void testIndexEntity()
	{
		when(issue.getKey()).thenReturn("JJI-653");
		when(doubleConverter.getStringForLucene(any(Double.class))).then(invocation -> String.valueOf(invocation.getArguments()[0]));
		LinkStatistics linkStatistics = new LinkStatistics().setTotal(1)
				.setBuildTotal(10)
				.setDeployTotal(2)
				.setWorstResult(FAILURE)
				.addCountByResult(SUCCESS, 4)
				.addCountByResult(UNSTABLE, 5)
				.addCountByResult(FAILURE, 1)
				.setWorstDeployResult(FAILURE)
				.addDeployCountByResult(SUCCESS, 1)
				.addDeployCountByResult(FAILURE, 1);

		when(issueLinkService.getLinkStatistics("JJI-653")).thenReturn(linkStatistics);

		Map<String, String> fields = helper.indexIssue(issue);
		assertThat(fields).contains(entry(TOTAL_FIELD_NAME, "1.0"), entry(TOTAL_BUILDS_FIELD_NAME, "10.0"),
		                            entry(WORST_RESULT_FIELD_NAME, FAILURE.key()), entry(fieldNameForResult(SUCCESS), "4.0"),
		                            entry(fieldNameForResult(UNSTABLE), "5.0"), entry(fieldNameForResult(FAILURE), "1.0"),
		                            entry(TOTAL_DEPLOYS_FIELD_NAME, "2.0"), entry(WORST_DEPLOY_RESULT_FIELD_NAME, FAILURE.key()),
		                            entry(fieldNameForResult(SUCCESS, true), "1.0"), entry(fieldNameForResult(FAILURE, true), "1.0"));

		verify(issueLinkService).getLinkStatistics("JJI-653");
		verifyNoMoreInteractions(issueLinkService);
	}

	@Test
	void testIndexEntity_NoStats()
	{
		when(issue.getKey()).thenReturn("JJI-653");
		when(issueLinkService.getLinkStatistics("JJI-653")).thenReturn(new LinkStatistics());

		Map<String, String> fields = helper.indexIssue(issue);
		assertThat(fields.keySet()).isEmpty();

		verify(issueLinkService).getLinkStatistics("JJI-653");
		verifyNoMoreInteractions(issueLinkService);
	}

	@Test
	void testIndexEntity_DisabledFeatureEnabled()
	{
		features.enableFeature(DISABLE_ISSUE_INDEXING);

		Map<String, String> fields = helper.indexIssue(issue);
		assertThat(fields.keySet()).isEmpty();

		verifyNoMoreInteractions(issueLinkService);
	}
}
