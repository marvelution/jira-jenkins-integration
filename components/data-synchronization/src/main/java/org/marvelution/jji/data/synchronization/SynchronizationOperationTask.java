/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.util.*;
import java.util.concurrent.atomic.*;
import java.util.concurrent.locks.*;
import java.util.stream.*;

import org.marvelution.jji.data.access.*;
import org.marvelution.jji.data.synchronization.api.*;
import org.marvelution.jji.data.synchronization.api.context.*;
import org.marvelution.jji.model.*;

import org.slf4j.*;

/**
 * {@link Runnable} for scheduling and executing synchronization operations.
 *
 * @author Mark Rekveld
 * @since 3.9.0
 */
public abstract class SynchronizationOperationTask
		implements Runnable
{

	private static final Logger LOGGER = LoggerFactory.getLogger(SynchronizationOperationTask.class);
	protected final AtomicReference<DefaultSynchronizationResult> current = new AtomicReference<>();

	/**
	 * @since 4.0.0
	 */
	public abstract String getResultId();

	@Override
	public void run()
	{
		result().ifPresent(result -> {
			if (!result.isFinished())
			{
				current.set(result);
				LOGGER.debug("Starting {}", result.getOperationId());
				try
				{
					beforeStarting();
					result.started();
					SynchronizationOperation<?> operation = locateSynchronizationOperation(result.getOperationId());
					executeOperation(operation, result);
				}
				catch (UnsupportedOperationException e)
				{
					LOGGER.warn(e.getMessage());
					result.addError(e.getMessage());
				}
				catch (SynchronizationOperationException e)
				{
					LOGGER.warn("Synchronization failed with message: {}", e.getMessage(), e);
					result.addError(e);
				}
				finally
				{
					result.finished();
					afterFinishing();
					current.set(null);
				}
			}
			else
			{
				LOGGER.debug("{} has already finished, skipping it.", result.getOperationId());
				afterFinishing();
			}
		});
	}

	protected abstract Optional<DefaultSynchronizationResult> result();

	protected abstract <S extends Syncable<S>> SynchronizationContextBuilder<S, ?> createSynchronizationContext(
			SynchronizationOperation<S> operation,
			OperationId operationId);

	protected abstract Stream<SynchronizationOperation<?>> synchronizationOperations();

	protected void beforeStarting() {}

	protected void afterFinishing() {}

	private SynchronizationOperation<?> locateSynchronizationOperation(OperationId operationId)
	{
		return synchronizationOperations().filter(operation -> operation.supports(operationId))
				.findFirst()
				.orElseThrow(() -> new UnsupportedOperationException("Unable to locate synchronization operation for " + operationId));
	}

	private <S extends Syncable<S>> void executeOperation(
			SynchronizationOperation<S> operation,
			DefaultSynchronizationResult result)
			throws SynchronizationOperationException
	{
		operation.synchronize(createSynchronizationContext(operation, result.getOperationId()).result(result).build());
	}
}
