/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.scheduler;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.testing.*;

import org.junit.jupiter.api.*;
import org.mockito.*;

import static org.marvelution.jji.model.Matchers.equalTo;

import static java.util.Arrays.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

/**
 * Tests for {@link JenkinsSynchronizationJob}.
 *
 * @author Mark Rekveld
 * @since 3.4.0
 */
class JenkinsSynchronizationJobTest extends TestSupport {

	@Mock
	private SiteService siteService;
	@Mock
	private SynchronizationService synchronizationService;
	private JenkinsSynchronizationJob synchronizationJob;

	@BeforeEach
	void setUp() {
		synchronizationJob = new JenkinsSynchronizationJob(siteService, synchronizationService);
	}

	@Test
	void runJob() {
		Site site1 = new Site().setName("site-1").setId("site-1").setRegistrationComplete(false);
		Site site2 = new Site().setName("site-2").setId("site-2").setRegistrationComplete(true);

		when(siteService.getAll()).thenReturn(asList(site1, site2));

		synchronizationJob.runJob(null);

		ArgumentCaptor<Site> argumentCaptor = ArgumentCaptor.forClass(Site.class);
		verify(synchronizationService).synchronize(argumentCaptor.capture(), ArgumentMatchers.any(ScheduledJobTrigger.class));

		assertThat(argumentCaptor.getAllValues(), hasItem(equalTo(site1)));
		assertThat(argumentCaptor.getAllValues(), not(hasItem(equalTo(site2))));
	}
}
