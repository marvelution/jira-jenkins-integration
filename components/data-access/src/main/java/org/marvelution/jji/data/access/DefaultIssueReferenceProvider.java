/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.time.ZoneId;
import java.util.*;
import java.util.function.Function;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.access.api.IssueReferenceProvider;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.model.IssueReference;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.KeySystemField;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.RestAwareField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.api.json.JaxbJsonMarshaller;
import com.google.common.collect.ImmutableMap;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriComponentsBuilder;

import static java.util.stream.Collectors.toSet;
import static org.marvelution.jji.data.services.api.model.IssueFields.*;
import static org.marvelution.jji.utils.KeyExtractor.extractProjectKeyFromIssueKey;

@Named
public class DefaultIssueReferenceProvider
        implements IssueReferenceProvider
{

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultIssueReferenceProvider.class);
    private static final Map<String, Function<Issue, ApplicationUser>> USER_FIELDS = ImmutableMap.of(CREATOR,
            Issue::getCreator,
            ASSIGNEE,
            Issue::getAssignee,
            REPORTER,
            Issue::getReporter);
    private final ConfigurationService configurationService;
    private final IssueManager issueManager;
    private final FieldManager fieldManager;
    private final JaxbJsonMarshaller jaxbJsonMarshaller;

    @Inject
    public DefaultIssueReferenceProvider(
            ConfigurationService configurationService,
            @ComponentImport
            IssueManager issueManager,
            @ComponentImport
            FieldManager fieldManager,
            @ComponentImport
            JaxbJsonMarshaller jaxbJsonMarshaller)
    {
        this.configurationService = configurationService;
        this.issueManager = issueManager;
        this.fieldManager = fieldManager;
        this.jaxbJsonMarshaller = jaxbJsonMarshaller;
    }

    @Override
    public void setIssueUrl(IssueReference reference)
    {
        reference.setIssueUrl(UriComponentsBuilder.fromUri(configurationService.getBaseUrl())
                .path("/browse/{issueKey}")
                .build(reference.getIssueKey()));
    }

    @Override
    public Optional<IssueReference> getIssueReference(
            String key,
            FieldContext fieldContext)
    {
        return Optional.of(key)
                .map(issueManager::getIssueObject)
                .map(issue -> toReference(issue, fieldContext));
    }

    @Override
    public Set<IssueReference> getIssueReferences(
            Set<String> keys,
            FieldContext fieldContext)
    {
        return keys.stream()
                .map(issueManager::getIssueObject)
                .filter(Objects::nonNull)
                .map(issue -> toReference(issue, fieldContext))
                .collect(toSet());
    }

    private IssueReference toReference(
            Issue issue,
            FieldContext fieldContext)
    {
        IssueReference reference = new IssueReference().setIssueKey(issue.getKey())
                .setProjectKey(Optional.ofNullable(issue.getProjectObject())
                        .map(Project::getKey)
                        .orElse(extractProjectKeyFromIssueKey(issue.getKey())));
        setIssueUrl(reference);
        if (fieldContext.hasAdditionalFields())
        {
            for (String fieldKey : fieldContext.additionalFields())
            {
                FieldJsonRepresentation jsonRepresentation = null;
                Field field = fieldManager.getField(fieldKey);
                if (field == null)
                {
                    LOGGER.warn("Skipping non-existing field {} for issue {}", fieldKey, issue.getKey());
                    continue;
                }
                else if (field instanceof KeySystemField)
                {
                    LOGGER.debug("Skipping issuekey field for issue {}", issue.getKey());
                    continue;
                }
                else if (USER_FIELDS.containsKey(field.getId()))
                {
                    ApplicationUser user = USER_FIELDS.get(field.getId())
                            .apply(issue);
                    if (user != null)
                    {
                        User userJson = new User();
                        userJson.key = user.getKey();
                        userJson.accountId = String.valueOf(user.getId());
                        userJson.name = user.getName();
                        userJson.displayName = user.getDisplayName();
                        userJson.emailAddress = user.getEmailAddress();
                        jsonRepresentation = new FieldJsonRepresentation(new JsonData(userJson), null);
                    }
                }
                else if (field instanceof RestAwareField)
                {
                    jsonRepresentation = ((RestAwareField) field).getJsonFromIssue(issue, true, null);
                }
                if (jsonRepresentation != null)
                {
                    String value = getFieldValueFromJson(field.getId(), field.getName(), jsonRepresentation, fieldContext);
                    if (value != null)
                    {
                        reference.addField(fieldKey, fieldContext.fieldName(fieldKey, field.getName()), value);
                    }
                    else
                    {
                        LOGGER.debug("Skipping field {} of issue {} as its value is 'null'", fieldKey, issue.getKey());
                    }
                }
                else
                {
                    LOGGER.warn("Unable to get value of field {} for issue {}", fieldKey, issue.getKey());
                }
            }
        }
        return reference;
    }

    @Nullable
    protected String getFieldValueFromJson(
            String fieldKey,
            String fieldName,
            FieldJsonRepresentation jsonRepresentation,
            FieldContext fieldContext)
    {
        try
        {
            Object valueJson = getJson(jsonRepresentation.getStandardData());
            Object renderedValueJson = getJson(jsonRepresentation.getRenderedData());
            LOGGER.debug("Attempting to extract the value for field {} from value: {}; renderedValue: {}",
                    fieldKey,
                    valueJson,
                    renderedValueJson);
            return fieldContext.fieldValue(fieldKey, fieldName, valueJson, renderedValueJson);
        }
        catch (Exception e)
        {
            LOGGER.error("failed to extract field value for field {}", fieldKey, e);
            return null;
        }
    }

    private Object getJson(JsonData jsonData)
    {
        try
        {
            return Optional.ofNullable(jsonData)
                    .map(JsonData::getData)
                    .map(jaxbJsonMarshaller::marshal)
                    .map(JsonPath::parse)
                    .map(DocumentContext::json)
                    .orElse(null);
        }
        catch (Exception e)
        {
            LOGGER.debug("Unable to get json string from json data {}", jsonData);
            return null;
        }
    }

    public static class User
    {
        public String key;
        public String accountId;
        public String name;
        public String displayName;
        public String emailAddress;
    }
}
