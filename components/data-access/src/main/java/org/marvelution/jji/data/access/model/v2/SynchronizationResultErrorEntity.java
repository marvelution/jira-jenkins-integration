/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import javax.annotation.*;

import org.marvelution.jji.data.access.model.Entity;

import net.java.ao.*;
import net.java.ao.schema.*;

import static net.java.ao.schema.StringLength.*;

/**
 * @author Mark Rekveld
 * @since 3.9.0
 */
@Preload
@Table(SynchronizationResultErrorEntity.TABLE_NAME)
public interface SynchronizationResultErrorEntity
		extends Entity
{

	String TABLE_NAME = "SYNC_RESULT_ERRORS";

	String RESULT_ID = "RESULT_ID";
	String MESSAGE = "MESSAGE";
	String STACK_TRACE = "STACK_TRACE";

	@NotNull
	SynchronizationResultEntity getResult();

	void setResult(SynchronizationResultEntity progress);

	@NotNull
	@StringLength(UNLIMITED)
	String getMessage();

	void setMessage(String message);

	@Nullable
	@StringLength(UNLIMITED)
	String getStackTrace();

	void setStackTrace(@Nullable String stackTrace);
}
