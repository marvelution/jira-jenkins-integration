/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import java.time.*;
import java.util.*;

import org.marvelution.jji.api.migration.*;
import org.marvelution.jji.model.*;

import jakarta.json.bind.*;
import org.apache.commons.lang3.*;

public class ServerAppMigration
		implements AppMigration, HasId<ServerAppMigration>
{

	public static final Jsonb JSONB = JsonbBuilder.create();
	private final String id;
	private final String name;
	private final LocalDateTime createdAt;
	private final AppMigrationType type;
	private final ProgressStatus progressStatus;
	private final String eventJson;
	private final String feedbackJson;

	public ServerAppMigration(
			String id,
			String name,
			LocalDateTime createdAt,
			AppMigrationType type,
			ProgressStatus progressStatus,
			String eventJson,
			String feedbackJson)
	{
		this.id = id;
		this.name = name;
		this.createdAt = createdAt;
		this.type = type;
		this.progressStatus = progressStatus;
		this.eventJson = eventJson;
		this.feedbackJson = feedbackJson;
	}

	public ServerAppMigration(
			String id,
			String name,
			LocalDateTime createdAt,
			AppMigrationType type,
			ProgressStatus progressStatus,
			Object event)
	{
		this(id, name, createdAt, type, progressStatus, Optional.ofNullable(event).map(JSONB::toJson).orElse(null), null);
	}

	@Override
	public String getId()
	{
		return id;
	}

	@Override
	public ServerAppMigration setId(String id)
	{
		return this;
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public LocalDateTime getCreatedAt()
	{
		return createdAt;
	}

	@Override
	public ProgressStatus getProgress()
	{
		return progressStatus;
	}

	@Override
	public AppMigrationType getType()
	{
		return type;
	}

	@Override
	public <E> E getEvent(Class<E> type)
	{
		return Optional.ofNullable(eventJson).filter(StringUtils::isNotBlank).map(json -> JSONB.fromJson(json, type)).orElse(null);
	}

	@Override
	public Feedback getFeedback()
	{
		return Optional.ofNullable(feedbackJson)
				.filter(StringUtils::isNotBlank)
				.map(json -> JSONB.fromJson(json, Feedback.class))
				.orElse(null);
	}

	public String getEventJson()
	{
		return eventJson;
	}

	public String getFeedbackJson()
	{
		return feedbackJson;
	}

	@Override
	public ServerAppMigration copy()
	{
		return new ServerAppMigration(id, name, createdAt, type, progressStatus, eventJson, feedbackJson);
	}
}
