/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.upgrade;

import java.util.Collection;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.api.AppState;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.model.ConfigurationSetting;
import org.marvelution.jji.upgrade.AbstractUpgradeTask;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.marvelution.jji.data.services.DefaultConfigurationService.*;

@Named
@ExportAsService(PluginUpgradeTask.class)
public class T14_CopyConfigurationData
        extends AbstractUpgradeTask
{

    private static final Logger LOGGER = LoggerFactory.getLogger(T14_CopyConfigurationData.class);
    private static final String SETTING_PREFIX = "jji.";
    private static final String[] KEYS_TO_COPY = {MAX_BUILDS_PER_PAGE,
            USE_USER_ID,
            PARAMETER_ISSUE_FIELDS,
            PARAMETER_ISSUE_FIELD_MAPPERS,
            DELETED_DATA_RETENTION_PERIOD,
            AUDIT_DATA_RETENTION_PERIOD,
            JIRA_BASE_RPC_URL,
            ENABLED_PROJECT_KEYS};
    private final ConfigurationService configurationService;
    private final PluginSettingsFactory pluginSettingsFactory;


    @Inject
    public T14_CopyConfigurationData(
            AppState appState,
            ConfigurationService configurationService,
            @ComponentImport
            PluginSettingsFactory pluginSettingsFactory)
    {
        super(appState);
        this.configurationService = configurationService;
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    @Override
    public Collection<Message> doUpgrade()
    {
        LOGGER.info("Copying configuration data from plugin settings to app tables");

        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();

        Configuration configuration = new Configuration().setScope(GLOBAL_SCOPE);
        for (String key : KEYS_TO_COPY)
        {
            String settingKey = SETTING_PREFIX + key;
            String value = Optional.ofNullable((String) settings.get(settingKey))
                    .orElse("");
            configuration.addConfiguration(new ConfigurationSetting().setKey(key)
                    .setScope(GLOBAL_SCOPE)
                    .setOverridable(true)
                    .setInternal(false)
                    .setValue(value));
            settings.remove(settingKey);
        }

        configurationService.saveConfiguration(configuration);

        return null;
    }
}
