/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.JiraService;
import org.marvelution.jji.data.services.api.model.AvatarUrls;
import org.marvelution.jji.data.services.api.model.IssueField;
import org.marvelution.jji.data.services.api.model.Project;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Named
public class DefaultJiraService
        implements JiraService
{
    private final ProjectManager projectManager;
    private final FieldManager fieldManager;
    private final AvatarService avatarService;

    @Inject
    public DefaultJiraService(
            @ComponentImport
            ProjectManager projectManager,
            @ComponentImport
            FieldManager fieldManager,
            @ComponentImport
            AvatarService avatarService)
    {
        this.projectManager = projectManager;
        this.fieldManager = fieldManager;
        this.avatarService = avatarService;
    }

    @Override
    public List<Project> getProjects(Set<String> keys)
    {
        return projectManager.getProjectsByArgs(keys)
                .stream()
                .map(this::toRestModel)
                .collect(toList());
    }

    @Override
    public List<Project> getProjects(String query)
    {
        Pattern term = Pattern.compile(Pattern.quote(query), Pattern.CASE_INSENSITIVE);
        Predicate<com.atlassian.jira.project.Project> filter;
        if (isBlank(query))
        {
            filter = project -> true;
        }
        else
        {
            filter = project -> Stream.of(project.getKey(), project.getName())
                    .anyMatch(key -> term.matcher(key)
                            .matches());
        }
        return projectManager.getProjects()
                .stream()
                .filter(filter)
                .map(this::toRestModel)
                .collect(toList());
    }

    @Override
    public List<IssueField> findIssueFields(String query)
    {
        try
        {
            Predicate<Field> filter = field -> field.getId()
                                                       .contains(query) || field.getName()
                                                       .contains(query);
            Set<IssueField> fields = fieldManager.getAllAvailableNavigableFields()
                    .stream()
                    .filter(filter)
                    .map(this::toRestModel)
                    .collect(toSet());
            fieldManager.getAllSearchableFields()
                    .stream()
                    .filter(filter)
                    .map(this::toRestModel)
                    .forEach(fields::add);
            return new ArrayList<>(fields);
        }
        catch (FieldException e)
        {
            throw new IllegalStateException("Failed to get all available issue fields", e);
        }
    }

    @Override
    public List<IssueField> getIssueFields(Set<String> ids)
    {
        try
        {
            return fieldManager.getAllAvailableNavigableFields()
                    .stream()
                    .filter(field -> ids.contains(field.getId()))
                    .map(this::toRestModel)
                    .collect(toList());
        }
        catch (FieldException e)
        {
            throw new IllegalStateException("Failed to get all available issue fields", e);
        }
    }

    private Project toRestModel(com.atlassian.jira.project.Project project)
    {
        Project restProject = new Project();
        restProject.setId(String.valueOf(project.getId()));
        restProject.setKey(project.getKey());
        restProject.setName(project.getName());
        restProject.setProjectTypeKey(project.getProjectTypeKey()
                .getKey());
        restProject.setAvatarUrls(new AvatarUrls());
        for (Avatar.Size size : Avatar.Size.values())
        {
            int pixels = size.getPixels();
            if (pixels <= 48)
            {
                restProject.getAvatarUrls()
                        .set(String.format("%dx%d", pixels, pixels),
                                avatarService.getProjectAvatarAbsoluteURL(project, size)
                                        .toASCIIString());
            }
        }
        return restProject;
    }

    private IssueField toRestModel(Field field)
    {
        IssueField restField = new IssueField();
        restField.setId(field.getId());
        restField.setName(field.getName());
        return restField;
    }
}
