/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import com.atlassian.pocketknife.spi.querydsl.*;
import com.querydsl.core.types.dsl.*;

import static org.marvelution.jji.data.access.model.Entity.*;
import static org.marvelution.jji.data.access.model.v2.SynchronizationResultErrorEntity.*;

public class SynchronizationResultErrorTable
		extends EnhancedRelationalPathBase<SynchronizationResultErrorTable>
{

	private final StringPath id = createString(ID);
	private final StringPath resultId = createString(RESULT_ID);
	private final StringPath message = createString(MESSAGE);
	private final StringPath stackTrace = createString(STACK_TRACE);

	public SynchronizationResultErrorTable(String namespace)
	{
		super(SynchronizationResultErrorTable.class, namespace + TABLE_NAME, "syncResultError");
		createPrimaryKey(id);
	}

	public StringPath id()
	{
		return id;
	}

	public StringPath resultId()
	{
		return resultId;
	}

	public StringPath message()
	{
		return message;
	}

	public StringPath stackTrace()
	{
		return stackTrace;
	}
}
