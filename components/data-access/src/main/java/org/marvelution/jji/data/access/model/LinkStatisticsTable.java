/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import com.atlassian.pocketknife.spi.querydsl.*;
import com.querydsl.core.types.dsl.*;

import static org.marvelution.jji.data.access.model.v2.LinkStatisticsEntity.*;

/**
 * @author Mark Rekveld
 */
public class LinkStatisticsTable
		extends EnhancedRelationalPathBase<LinkStatisticsTable>
{

	private final StringPath id = createString(ID);
	private final StringPath issueKey = createString(ISSUE_KEY);
	private final StringPath json = createString(JSON);
	private final BooleanPath queued = createBoolean(QUEUED);

	public LinkStatisticsTable(String namespace)
	{
		super(LinkStatisticsTable.class, namespace + TABLE_NAME, "linkStats");
	}

	public StringPath id()
	{
		return id;
	}

	public StringPath issueKey()
	{
		return issueKey;
	}

	public StringPath json()
	{
		return json;
	}

	public BooleanPath queued()
	{
		return queued;
	}
}
