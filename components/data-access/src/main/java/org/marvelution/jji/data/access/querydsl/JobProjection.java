/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.querydsl;

import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.model.*;

import static java.util.stream.Collectors.*;

/**
 * QueryDSL {@link Job} projection.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public class JobProjection
		extends ProjectionBase<Job>
{

	public JobProjection()
	{
		super(Job.class, Stream.of(Tables.JOB.getColumns(), Tables.SITE.getColumns()).flatMap(List::stream).collect(toList()));
	}

	@Override
	Job createInstance(ProjectionContext context)
	{
		return createJob(context);
	}

	static Job createJob(ProjectionContext context)
	{
		return new Job().setId(context.get(Tables.JOB.id()))
				.setSite(SiteProjection.createSite(context))
				.setDeleted(context.get(Tables.JOB.deleted()))
				.setLinked(context.get(Tables.JOB.linked()))
				.setParentName(context.get(Tables.JOB.parentName()))
				.setName(context.get(Tables.JOB.name()))
				.setDisplayName(context.get(Tables.JOB.displayName()))
				.setUrlName(context.get(Tables.JOB.urlName()))
				.setDescription(context.get(Tables.JOB.description()))
				.setLastBuild(context.get(Tables.JOB.lastBuild()))
				.setOldestBuild(context.get(Tables.JOB.oldestBuild()));
	}
}
