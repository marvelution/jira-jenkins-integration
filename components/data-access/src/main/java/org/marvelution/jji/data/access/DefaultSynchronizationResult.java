/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import javax.annotation.Nullable;

import org.marvelution.jji.data.access.model.SynchronizationResultHolder;
import org.marvelution.jji.data.synchronization.api.result.ImmutableSynchronizationResult;
import org.marvelution.jji.data.synchronization.api.result.SynchronizationResult;
import org.marvelution.jji.data.synchronization.api.trigger.TriggerReason;
import org.marvelution.jji.data.synchronization.api.trigger.TriggerReasons;
import org.marvelution.jji.model.*;

import org.apache.commons.lang3.exception.ExceptionUtils;

import static org.marvelution.jji.model.HasId.newId;

public class DefaultSynchronizationResult
        implements SynchronizationResult
{

    private final DefaultSynchronizationResultDAO dao;
    private final SynchronizationResultHolder holder;
    private final List<DefaultError> errors;
    private TriggerReason triggerReason;

    DefaultSynchronizationResult(
            DefaultSynchronizationResultDAO dao,
            SynchronizationResultHolder holder,
            List<DefaultError> errors)
    {
        this.dao = dao;
        this.holder = holder;
        this.errors = errors;
    }

    DefaultSynchronizationResult(
            DefaultSynchronizationResultDAO dao,
            SynchronizationResultHolder holder,
            TriggerReason triggerReason)
    {
        this(dao, holder, new ArrayList<>());
        this.triggerReason = triggerReason;
    }

    @Override
    public String getId()
    {
        return holder.getId();
    }

    @Override
    public OperationId getOperationId()
    {
        return OperationId.of(holder.getOperationId());
    }

    @Override
    public TriggerReason triggerReason()
    {
        if (triggerReason == null)
        {
            synchronized (this)
            {
                if (triggerReason == null)
                {
                    triggerReason = TriggerReasons.fromJson(holder.getTriggerReason());
                }
            }
        }
        return triggerReason;
    }

    @Override
    public boolean isQueued()
    {
        return holder.hasTimestampQueued();
    }

    @Override
    public boolean hasStarted()
    {
        return holder.hasTimestampStarted();
    }

    @Override
    public boolean isFinished()
    {
        return holder.hasTimestampFinished();
    }

    @Override
    public boolean stopRequested()
    {
        return holder.getStopRequested();
    }

    @Override
    public Duration queuedDuration()
    {
        if (!isQueued())
        {
            return Duration.ZERO;
        }
        else if (!hasStarted())
        {
            return Duration.ofMillis(System.currentTimeMillis() - holder.getTimestampQueued());
        }
        else
        {
            return Duration.ofMillis(holder.getTimestampStarted() - holder.getTimestampQueued());
        }
    }

    @Override
    public Duration executionDuration()
    {
        if (!hasStarted())
        {
            return Duration.ZERO;
        }
        else if (!isFinished())
        {
            return Duration.ofMillis(System.currentTimeMillis() - holder.getTimestampStarted());
        }
        else
        {
            return Duration.ofMillis(holder.getTimestampFinished() - holder.getTimestampStarted());
        }
    }

    @Override
    public Duration completedDuration()
    {
        if (!isQueued())
        {
            return Duration.ZERO;
        }
        else if (!isFinished())
        {
            return Duration.ofMillis(System.currentTimeMillis() - holder.getTimestampQueued());
        }
        else
        {
            return Duration.ofMillis(holder.getTimestampFinished() - holder.getTimestampQueued());
        }
    }

    @Override
    public SyncProgress asSyncProgress()
    {
        return new SyncProgress().setFinished(isFinished())
                .setJobCount(holder.getJobCount())
                .setBuildCount(holder.getBuildCount())
                .setIssueCount(holder.getIssueCount())
                .setErrorCount(errors.size());
    }

    @Override
    public void queued()
    {
        if (!holder.hasTimestampQueued())
        {
            holder.setTimestampQueued(System.currentTimeMillis());
            dao.save(this);
        }
    }

    @Override
    public void started()
    {
        if (!holder.hasTimestampStarted())
        {
            holder.setTimestampStarted(System.currentTimeMillis());
            dao.save(this);
        }
    }

    @Override
    public void finished()
    {
        if (!holder.hasTimestampFinished())
        {
            holder.setTimestampFinished(System.currentTimeMillis());
            dao.save(this);
        }
    }

    @Override
    public void shouldStop()
    {
        if (!holder.getStopRequested())
        {
            holder.setStopRequested(true);
            dao.save(this);
        }
    }

    @Override
    public void synchronizedJob(Job job)
    {
        holder.incrementJobCount();
    }

    @Override
    public void synchronizedBuild(Build build)
    {
        holder.incrementBuildCount();
    }

    @Override
    public void linkedIssues(Set<IssueReference> references)
    {
        holder.incrementIssueCount(references.size());
    }

    @Override
    public void addError(Throwable error)
    {
        String message = Optional.ofNullable(error.getMessage())
                .orElse(error.getClass()
                        .getName());
        String stackTrace = ExceptionUtils.getStackTrace(error);
        addError(message, stackTrace);
    }

    @Override
    public void addError(String message)
    {
        addError(message, null);
    }

    @Override
    public void addError(
            String message,
            @Nullable
            String details)
    {
        errors.add(new DefaultError(message, details));
    }

    @Override
    public List<Error> getErrors()
    {
        return new ArrayList<>(errors);
    }

    public SynchronizationResultHolder getHolder()
    {
        return holder;
    }

    public void forEachError(Consumer<DefaultError> action)
    {
        errors.forEach(action);
    }

    public static class DefaultError
            implements ImmutableSynchronizationResult.Error
    {

        private final String id;
        private final String message;
        private final String details;

        public DefaultError(
                String message,
                String details)
        {
            this(newId(), message, details);
        }

        public DefaultError(
                String id,
                String message,
                String details)
        {
            this.id = id;
            this.message = message;
            this.details = details;
        }

        public String getId()
        {
            return id;
        }

        @Override
        public String getMessage()
        {
            return message;
        }

        @Override
        @Nullable
        public String getDetails()
        {
            return details;
        }
    }
}
