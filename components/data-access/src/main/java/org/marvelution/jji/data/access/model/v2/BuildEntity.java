/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import javax.annotation.*;

import org.marvelution.jji.data.access.model.Entity;
import org.marvelution.jji.model.*;

import net.java.ao.*;
import net.java.ao.schema.Table;
import net.java.ao.schema.*;

import static net.java.ao.schema.StringLength.*;

@Preload
@Table(BuildEntity.TABLE_NAME)
@Indexes({@Index(name = "builds_job_idx",
                 methodNames = {"getJob"})})
public interface BuildEntity
        extends Entity
{

    String TABLE_NAME = "BUILDS";

    String JOB_ID = "JOB_ID";
    String BUILD_NUMBER = "BUILD_NUMBER";
    String DISPLAY_NAME = "DISPLAY_NAME";
    String DESCRIPTION = "DESCRIPTION";
    String CAUSE = "CAUSE";
    String DELETED = "DELETED";
    String DURATION = "DURATION";
    String TIME_STAMP = "TIME_STAMP";
    String RESULT = "RESULT";

    @NotNull
    JobEntity getJob();

    void setJob(JobEntity job);

    @NotNull
    int getBuildNumber();

    void setBuildNumber(int buildNumber);

    @Nullable
    @StringLength(MAX_LENGTH)
    String getDisplayName();

    void setDisplayName(
            @Nullable
            String displayName);

    @Nullable
    @StringLength(UNLIMITED)
    String getDescription();

    void setDescription(
            @Nullable
            String description);

    @Nullable
    @StringLength(UNLIMITED)
    String getCause();

    void setCause(
            @Nullable
            String cause);

    boolean isDeleted();

    void setDeleted(boolean deleted);

    long getDuration();

    void setDuration(long duration);

    @NotNull
    long getTimeStamp();

    void setTimeStamp(long timestamp);

    @NotNull
    @StringLength(10)
    Result getResult();

    void setResult(Result result);

    @OneToOne(reverse = "getBuild")
    TestResultsEntity getTestResults();

    /**
     * @since 4.1.0
     */
    @ManyToMany(value = BuildToDeploymentEnvironmentEntity.class,
                reverse = "getBuild",
                through = "getEnvironment")
    DeploymentEnvironmentEntity[] getDeploymentEnvironments();
}
