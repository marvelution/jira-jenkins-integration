/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.querydsl;

import java.util.stream.*;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.model.*;

import jakarta.json.bind.*;

public class LinkStatisticianStateProjection
		extends ProjectionBase<LinkStatisticsDAO.LinkStatisticianState>
{

	private final Jsonb jsonb;

	public LinkStatisticianStateProjection(Jsonb jsonb)
	{
		super(State.class,
		      Stream.of(Tables.LINK_STATISTICS_TABLE.issueKey(), Tables.LINK_STATISTICS_TABLE.queued(), Tables.LINK_STATISTICS_TABLE.json())
				      .collect(Collectors.toList()));
		this.jsonb = jsonb;
	}

	@Override
	LinkStatisticsDAO.LinkStatisticianState createInstance(ProjectionContext context)
	{
		String json = context.get(Tables.LINK_STATISTICS_TABLE.json());
		LinkStatistics current = json != null ? jsonb.fromJson(json, LinkStatistics.class) : new LinkStatistics();
		return new State(context.get(Tables.LINK_STATISTICS_TABLE.issueKey()), context.get(Tables.LINK_STATISTICS_TABLE.queued()), current);
	}

	public static class State
			implements LinkStatisticsDAO.LinkStatisticianState
	{

		private final String issueKey;
		private final boolean queued;
		private final LinkStatistics current;

		public State(String issueKey)
		{
			this(issueKey, false, new LinkStatistics());
		}

		private State(
				String issueKey,
				boolean queued,
				LinkStatistics current)
		{
			this.issueKey = issueKey;
			this.queued = queued;
			this.current = current;
		}

		@Override
		public String issueKey()
		{
			return issueKey;
		}

		@Override
		public boolean queued()
		{
			return queued;
		}

		@Override
		public LinkStatistics current()
		{
			return current;
		}
	}
}
