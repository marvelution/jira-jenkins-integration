/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import org.marvelution.jji.data.access.model.v4.*;

import com.atlassian.pocketknife.spi.querydsl.*;
import com.querydsl.core.types.dsl.*;

import static org.marvelution.jji.data.access.model.v4.ConfigurationEntity.*;

public class ConfigurationTable
		extends EnhancedRelationalPathBase<ConfigurationTable>
{

	private final NumberPath<Integer> id = createNumber(ID, Integer.class);
	private final StringPath key = createString(KEY);
	private final StringPath scope = createString(SCOPE);
	private final BooleanPath overridable = createBoolean(OVERRIDABLE);
	private final BooleanPath internal = createBoolean(INTERNAL);
	private final StringPath val = createString(VAL);

	public ConfigurationTable(String namespace)
	{
		super(ConfigurationTable.class, namespace + ConfigurationEntity.TABLE_NAME, "configuration");
		createPrimaryKey(id);
	}

	public NumberPath<Integer> id()
	{
		return id;
	}

	public StringPath key()
	{
		return key;
	}

	public StringPath scope()
	{
		return scope;
	}

	public BooleanPath overridable()
	{
		return overridable;
	}

	public BooleanPath internal()
	{
		return internal;
	}

	public StringPath val()
	{
		return val;
	}
}
