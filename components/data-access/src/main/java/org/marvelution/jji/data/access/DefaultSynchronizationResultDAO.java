/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.access.api.SynchronizationResultDAO;
import org.marvelution.jji.data.access.model.SynchronizationResultHolder;
import org.marvelution.jji.data.access.model.Tables;
import org.marvelution.jji.data.access.querydsl.SynchronizationResultErrorProjection;
import org.marvelution.jji.data.access.querydsl.SynchronizationResultHolderProjection;
import org.marvelution.jji.data.synchronization.api.result.ImmutableSynchronizationResult;
import org.marvelution.jji.data.synchronization.api.trigger.TriggerReason;
import org.marvelution.jji.data.synchronization.api.trigger.TriggerReasons;
import org.marvelution.jji.model.OperationId;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.util.OnRollback;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.sql.dml.SQLInsertClause;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.marvelution.jji.model.HasId.newId;

@Named
public class DefaultSynchronizationResultDAO
        implements SynchronizationResultDAO
{

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSynchronizationResultDAO.class);
    private final DatabaseAccessor databaseAccessor;

    @Inject
    public DefaultSynchronizationResultDAO(DatabaseAccessor databaseAccessor)
    {
        this.databaseAccessor = databaseAccessor;
    }

    @Override
    public Optional<DefaultSynchronizationResult> get(String resultId)
    {
        return find(Tables.SYNCHRONIZATION_RESULT_TABLE.id()
                .eq(resultId));
    }

    @Override
    public Optional<DefaultSynchronizationResult> findLatestResult(OperationId operationId)
    {
        return find(Tables.SYNCHRONIZATION_RESULT_TABLE.operationId()
                .eq(operationId.getId()));
    }

    @Override
    public DefaultSynchronizationResult create(
            OperationId operationId,
            TriggerReason triggerReason)
    {
        return databaseAccessor.runInTransaction(conn -> {
            String id = newId();
            String triggerReasonJson = TriggerReasons.asJson(triggerReason);
            long createTimestamp = System.currentTimeMillis();
            conn.insert(Tables.SYNCHRONIZATION_RESULT_TABLE)
                    .set(Tables.SYNCHRONIZATION_RESULT_TABLE.id(), id)
                    .set(Tables.SYNCHRONIZATION_RESULT_TABLE.operationId(), operationId.getId())
                    .set(Tables.SYNCHRONIZATION_RESULT_TABLE.triggerReason(), triggerReasonJson)
                    .set(Tables.SYNCHRONIZATION_RESULT_TABLE.createTimestamp(), createTimestamp)
                    .execute();
            return new DefaultSynchronizationResult(this,
                    new SynchronizationResultHolder(id, operationId.getId(), triggerReasonJson, createTimestamp),
                    triggerReason);
        }, OnRollback.NOOP);
    }

    @Override
    public void save(DefaultSynchronizationResult result)
    {
        LOGGER.debug("saving {} {}", result.getId(), result.getOperationId());
        databaseAccessor.runInTransaction(conn -> {
            conn.update(Tables.SYNCHRONIZATION_RESULT_TABLE)
                    .populate(result.getHolder(), new SynchronizationResultHolderProjection())
                    .where(Tables.SYNCHRONIZATION_RESULT_TABLE.id()
                            .eq(result.getId()))
                    .execute();
            if (result.hasErrors())
            {
                SQLInsertClause insert = conn.insert(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE);
                result.forEachError(error -> insert.set(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE.id(), error.getId())
                        .set(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE.resultId(), result.getId())
                        .set(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE.message(), error.getMessage())
                        .set(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE.stackTrace(), error.getDetails())
                        .addBatch());
                insert.execute();
            }
            return null;
        }, OnRollback.NOOP);
    }

    @Override
    public void persistStopRequest(DefaultSynchronizationResult result)
    {
        databaseAccessor.runInTransaction(conn -> conn.update(Tables.SYNCHRONIZATION_RESULT_TABLE)
                .set(Tables.SYNCHRONIZATION_RESULT_TABLE.stopRequested(), true)
                .where(Tables.SYNCHRONIZATION_RESULT_TABLE.id()
                        .eq(result.getId()))
                .execute(), OnRollback.NOOP);
    }

    @Override
    public Stream<String> findAllNonFinishedResultIds()
    {
        return databaseAccessor.runInTransaction(conn -> conn.select(Tables.SYNCHRONIZATION_RESULT_TABLE.id())
                .from(Tables.SYNCHRONIZATION_RESULT_TABLE)
                .where(Tables.SYNCHRONIZATION_RESULT_TABLE.timestampFinished()
                        .isNull()
                        .or(Tables.SYNCHRONIZATION_RESULT_TABLE.timestampFinished()
                                .eq(0L)))
                .fetch()
                .stream(), OnRollback.NOOP);
    }

    @Override
    public void deleteAllQueuedBefore(LocalDateTime timestamp)
    {
        DeletionHelper.batchDelete(databaseAccessor,
                conn -> conn.select(Tables.SYNCHRONIZATION_RESULT_TABLE.id())
                        .from(Tables.SYNCHRONIZATION_RESULT_TABLE)
                        .where(Tables.SYNCHRONIZATION_RESULT_TABLE.timestampQueued()
                                .lt(TimeUtils.toEpochMilli(timestamp))),
                (conn, chunk) -> {
                    try
                    {
                        conn.delete(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE)
                                .where(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE.resultId()
                                        .in(chunk))
                                .execute();
                        conn.delete(Tables.SYNCHRONIZATION_RESULT_TABLE)
                                .where(Tables.SYNCHRONIZATION_RESULT_TABLE.id()
                                        .in(chunk))
                                .execute();
                    }
                    catch (Throwable e)
                    {
                        LOGGER.error("Failed to cleanup old synchronization executions: {}", chunk, e);
                    }
                });
    }

    @Override
    public List<ImmutableSynchronizationResult> findLastResults(int limit)
    {
        // TODO Implement when needed
        return new ArrayList<>();
    }

    private Optional<DefaultSynchronizationResult> find(BooleanExpression where)
    {
        return databaseAccessor.runInTransaction(conn -> {
            SynchronizationResultHolder holder = conn.select(new SynchronizationResultHolderProjection())
                    .from(Tables.SYNCHRONIZATION_RESULT_TABLE)
                    .where(where)
                    .orderBy(Tables.SYNCHRONIZATION_RESULT_TABLE.createTimestamp()
                            .desc())
                    .fetchFirst();
            if (holder != null)
            {
                List<DefaultSynchronizationResult.DefaultError> errors = conn.select(new SynchronizationResultErrorProjection())
                        .from(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE)
                        .where(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE.resultId()
                                .eq(holder.getId()))
                        .fetch();
                return Optional.of(new DefaultSynchronizationResult(this, holder, errors));
            }
            else
            {
                return Optional.empty();
            }
        }, OnRollback.NOOP);
    }
}
