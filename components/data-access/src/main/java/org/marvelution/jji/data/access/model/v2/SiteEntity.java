/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import javax.annotation.*;
import java.net.*;

import org.marvelution.jji.data.access.model.Entity;
import org.marvelution.jji.model.*;

import net.java.ao.*;
import net.java.ao.schema.Table;
import net.java.ao.schema.*;

import static net.java.ao.schema.StringLength.*;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
@Preload
@Table(SiteEntity.TABLE_NAME)
public interface SiteEntity
        extends Entity
{

    String TABLE_NAME = "SITES";

    String NAME = "NAME";
    String SCOPE = "SCOPE";
    String SITE_TYPE = "SITE_TYPE";
    String SHARED_SECRET = "SHARED_SECRET";
    String RPC_URL = "RPC_URL";
    String DISPLAY_URL = "DISPLAY_URL";
    String SITE_AUTHENTICATION = "SITE_AUTHENTICATION";
    String SITE_CONNECTION = "SITE_CONNECTION";
    String USER = "USER";
    String USER_TOKEN = "USER_TOKEN";
    String AUTO_LINK_NEW_JOBS = "AUTO_LINK_NEW_JOBS";
    String JENKINS_PLUGIN_INSTALLED = "JENKINS_PLUGIN_INSTALLED";
    String REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE";
    String USE_CRUMBS = "USE_CRUMBS";
    String FIREWALLED = "FIREWALLED";
    String ENABLED = "ENABLED";
    String DELETED = "DELETED";

    @NotNull
    String getName();

    void setName(String name);

    @StringLength(50)
    String getScope();

    void setScope(String scope);

    @NotNull
    @StringLength(MAX_LENGTH)
    String getSharedSecret();

    void setSharedSecret(String sharedSecret);

    @NotNull
    SiteType getSiteType();

    void setSiteType(SiteType siteType);

    @NotNull
    URI getRpcUrl();

    void setRpcUrl(URI rpcUrl);

    @Nullable
    URI getDisplayUrl();

    void setDisplayUrl(
            @Nullable
            URI displayUrl);

	SiteAuthenticationType getSiteAuthentication();

	void setSiteAuthentication(SiteAuthenticationType siteAuthenticationType);

	SiteConnectionType getSiteConnection();

	void setSiteConnection(SiteConnectionType siteConnectionType);

    @Nullable
    String getUser();

    void setUser(
            @Nullable
            String user);

    @Nullable
    String getUserToken();

    void setUserToken(
            @Nullable
            String userToken);

    boolean isUseCrumbs();

    void setUseCrumbs(boolean useCrumbs);

    boolean isAutoLinkNewJobs();

    void setAutoLinkNewJobs(boolean autoLinkNewJobs);

    boolean isJenkinsPluginInstalled();

    void setJenkinsPluginInstalled(boolean jenkinsPluginInstalled);

    boolean isRegistrationComplete();

    void setRegistrationComplete(boolean registrationComplete);

    boolean isFirewalled();

    void setFirewalled(boolean firewalled);

    @Default("true")
    boolean isEnabled();

    void setEnabled(boolean enabled);

    @OneToMany(reverse = "getSite")
    JobEntity[] getJobs();

}
