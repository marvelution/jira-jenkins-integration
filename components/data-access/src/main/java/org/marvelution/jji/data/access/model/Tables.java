/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

public class Tables
{

	public static final String DEFAULT_AO_NAMESPACE = "AO_3FB43F_";

	public static final SiteTable SITE = new SiteTable(DEFAULT_AO_NAMESPACE);
	public static final JobTable JOB = new JobTable(DEFAULT_AO_NAMESPACE);
	public static final BuildTable BUILD = new BuildTable(DEFAULT_AO_NAMESPACE);
	public static final TestResultsTable TEST_RESULTS = new TestResultsTable(DEFAULT_AO_NAMESPACE);
	public static final IssueToBuildTable ISSUE_TO_BUILD = new IssueToBuildTable(DEFAULT_AO_NAMESPACE);
	public static final DeploymentEnvironmentTable DEPLOYMENT_ENVIRONMENT = new DeploymentEnvironmentTable(DEFAULT_AO_NAMESPACE);
	public static final BuildToDeploymentEnvironmentTable BUILD_TO_DEPLOYMENT_ENVIRONMENT = new BuildToDeploymentEnvironmentTable(
			DEFAULT_AO_NAMESPACE);
	public static final LinkStatisticsTable LINK_STATISTICS_TABLE = new LinkStatisticsTable(DEFAULT_AO_NAMESPACE);
	public static final SynchronizationResultTable SYNCHRONIZATION_RESULT_TABLE = new SynchronizationResultTable(DEFAULT_AO_NAMESPACE);
	public static final SynchronizationResultErrorTable SYNCHRONIZATION_RESULT_ERROR_TABLE = new SynchronizationResultErrorTable(
			DEFAULT_AO_NAMESPACE);
	public static final AppMigrationTable APP_MIGRATION_TABLE = new AppMigrationTable(DEFAULT_AO_NAMESPACE);
	public static final ConfigurationTable CONFIGURATION_TABLE = new ConfigurationTable(DEFAULT_AO_NAMESPACE);
}
