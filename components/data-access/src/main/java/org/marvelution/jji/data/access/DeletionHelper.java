/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import org.marvelution.jji.data.access.model.*;

import com.atlassian.pocketknife.api.querydsl.*;
import com.atlassian.pocketknife.api.querydsl.util.*;
import com.querydsl.sql.*;
import org.slf4j.*;

import static java.util.stream.Collectors.*;
import static java.util.stream.IntStream.*;

/**
 * Helper for deleting data using the PocketKnife framework.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public class DeletionHelper
{

    public static final int DELETE_BATCH_SIZE = 10;
    private static final Logger LOGGER = LoggerFactory.getLogger(DeletionHelper.class);

    static long deleteSite(
            DatabaseConnection conn,
            String id)
    {
        long start = System.currentTimeMillis();
        List<String> jobIds = conn.select(Tables.JOB.id())
                .from(Tables.JOB)
                .where(Tables.JOB.siteId()
                        .eq(id))
                .fetch();
        for (String jobId : jobIds)
        {
            deleteJob(conn, jobId);
        }
        long count = conn.delete(Tables.SITE)
                .where(Tables.SITE.id()
                        .eq(id))
                .execute();
        long duration = System.currentTimeMillis() - start;
        LOGGER.debug("Deleted {} sites and {} jobs in {} ms", count, jobIds.size(), duration);
        return count;
    }

    static long deleteJob(
            DatabaseConnection conn,
            String id)
    {
        long start = System.currentTimeMillis();
        long buildCount = deleteJobBuilds(conn, id);
        long count = conn.delete(Tables.JOB)
                .where(Tables.JOB.id()
                        .eq(id))
                .execute();
        long duration = System.currentTimeMillis() - start;
        LOGGER.debug("Deleted {} jobs and {} builds in {} ms", count, buildCount, duration);
        return count;
    }

    static long deleteJobBuilds(
            DatabaseConnection conn,
            String id)
    {
        List<String> buildIds = conn.select(Tables.BUILD.id())
                .from(Tables.BUILD)
                .where(Tables.BUILD.jobId()
                        .eq(id))
                .fetch();
        return batchDelete(chunk -> DeletionHelper.deleteBuild(conn, chunk), buildIds.toArray(new String[0]));
    }

    static long deleteBuild(
            DatabaseConnection conn,
            String... ids)
    {
        long start = System.currentTimeMillis();
        conn.delete(Tables.BUILD_TO_DEPLOYMENT_ENVIRONMENT)
                .where(Tables.BUILD_TO_DEPLOYMENT_ENVIRONMENT.buildId()
                        .in(ids))
                .execute();
        conn.delete(Tables.ISSUE_TO_BUILD)
                .where(Tables.ISSUE_TO_BUILD.buildId()
                        .in(ids))
                .execute();
        conn.delete(Tables.TEST_RESULTS)
                .where(Tables.TEST_RESULTS.buildId()
                        .in(ids))
                .execute();
        long count = conn.delete(Tables.BUILD)
                .where(Tables.BUILD.id()
                        .in(ids))
                .execute();
        long duration = System.currentTimeMillis() - start;
        LOGGER.debug("Deleted {} builds in {} ms", count, duration);
        return count;
    }

    static <T> long batchDelete(
            Consumer<String[]> callback,
            String... ids)
    {
        int chunkCount = ids.length / DELETE_BATCH_SIZE;
        if (ids.length % DELETE_BATCH_SIZE > 0)
        {
            chunkCount++;
        }
        List<String[]> chunks = range(0, chunkCount).mapToObj(chunk -> Stream.of(ids)
                        .sorted()
                        .skip((long) chunk * DELETE_BATCH_SIZE)
                        .limit(DELETE_BATCH_SIZE)
                        .toArray(String[]::new))
                .collect(toList());
        long count = 0;
        for (String[] chunk : chunks)
        {
            try
            {
                callback.accept(chunk);
                count += chunk.length;
            }
            catch (Throwable e)
            {
                LOGGER.debug("Failed to execute batch delete of a chunk", e);
            }
        }
        return count;
    }

    static void batchDelete(
            DatabaseAccessor databaseAccessor,
            Function<DatabaseConnection, SQLQuery<String>> selector,
            BiConsumer<DatabaseConnection, String[]> callback)
    {
        String[] ids;
        do
        {
            ids = databaseAccessor.runInTransaction(conn -> selector.apply(conn)
                    .limit(1000)
                    .fetch()
                    .toArray(new String[0]), OnRollback.NOOP);

            batchDelete(chunk -> databaseAccessor.runInTransaction(conn -> {
                callback.accept(conn, chunk);
                return null;
            }, OnRollback.NOOP), ids);
        }
        while (ids.length == 1000);
    }
}
