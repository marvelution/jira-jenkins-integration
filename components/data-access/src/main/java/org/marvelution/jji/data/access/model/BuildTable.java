/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import java.util.*;

import org.marvelution.jji.model.*;

import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.*;

import static org.marvelution.jji.data.access.model.Entity.*;
import static org.marvelution.jji.data.access.model.v2.BuildEntity.DESCRIPTION;
import static org.marvelution.jji.data.access.model.v2.BuildEntity.DISPLAY_NAME;
import static org.marvelution.jji.data.access.model.v2.BuildEntity.TABLE_NAME;
import static org.marvelution.jji.data.access.model.v2.BuildEntity.*;
import static org.marvelution.jji.data.access.model.v2.JobEntity.DELETED;

/**
 * {@link Build} QueryDSL {@link Table}.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public class BuildTable
		extends Table<Build, BuildTable>
{

	private final StringPath id = createString(ID);
	private final StringPath jobId = createString(JOB_ID);
	private final NumberPath<Integer> buildNumber = createInteger(BUILD_NUMBER);
	private final BooleanPath deleted = createBoolean(DELETED);
	private final StringPath displayName = createString(DISPLAY_NAME);
	private final StringPath description = createString(DESCRIPTION);
	private final StringPath cause = createString(CAUSE);
	private final NumberPath<Long> duration = createLong(DURATION);
	private final NumberPath<Long> timestamp = createLong(TIME_STAMP);
	private final EnumPath<Result> result = createEnum(RESULT, Result.class);

	public BuildTable(String namespace)
	{
		super(BuildTable.class, namespace + TABLE_NAME, "build");
		createPrimaryKey(id);
	}

	@Override
	public StringPath id()
	{
		return id;
	}

	public StringPath jobId()
	{
		return jobId;
	}

	public NumberPath<Integer> buildNumber()
	{
		return buildNumber;
	}

	public BooleanPath deleted()
	{
		return deleted;
	}

	public StringPath displayName()
	{
		return displayName;
	}

	public StringPath description()
	{
		return description;
	}

	public StringPath cause()
	{
		return cause;
	}

	public NumberPath<Long> duration()
	{
		return duration;
	}

	public NumberPath<Long> timestamp()
	{
		return timestamp;
	}

	public EnumPath<Result> result()
	{
		return result;
	}

	@Override
	public Map<Path<?>, Object> mapForInsert(
			RelationalPath<?> path,
			Build object)
	{
		Map<Path<?>, Object> bindings = mapForUpdate(path, object);
		bindings.put(id, object.getId());
		bindings.put(jobId, object.getJob().getId());
		bindings.put(buildNumber, object.getNumber());
		return bindings;
	}

	@Override
	public Map<Path<?>, Object> mapForUpdate(
			RelationalPath<?> path,
			Build object)
	{
		Map<Path<?>, Object> bindings = new HashMap<>();
		bindings.put(deleted, object.isDeleted());
		bindings.put(displayName, object.getDisplayNameOrNull());
		bindings.put(description, object.getDescription());
		bindings.put(cause, object.getCause());
		bindings.put(duration, object.getDuration());
		bindings.put(timestamp, object.getTimestamp());
		bindings.put(result, object.getResult().name());
		return bindings;
	}
}
