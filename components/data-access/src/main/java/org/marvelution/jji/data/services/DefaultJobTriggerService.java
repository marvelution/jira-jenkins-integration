/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.api.IssueReferenceProvider;
import org.marvelution.jji.data.services.api.*;

import com.atlassian.jira.security.JiraAuthenticationContext;

@Named
public class DefaultJobTriggerService
        extends JobTriggerService
{
    private final JiraAuthenticationContext authenticationContext;

    @Inject
    public DefaultJobTriggerService(
            ConfigurationService configurationService,
            IssueReferenceProvider issueReferenceProvider,
            IssueLinkService issueLinkService,
            SiteClient siteClient,
            JobService jobService,
            TextResolver textResolver,
            PermissionService<?> permissionService,
            JiraAuthenticationContext authenticationContext)
    {
        super(configurationService, issueReferenceProvider, issueLinkService, siteClient, jobService, textResolver, permissionService);
        this.authenticationContext = authenticationContext;
    }

    @Override
    protected String getCurrentUserId()
    {
        return String.valueOf(authenticationContext.getLoggedInUser()
                .getId());
    }

    @Override
    protected String getCurrentUserDisplayName()
    {
        return authenticationContext.getLoggedInUser()
                .getDisplayName();
    }
}
