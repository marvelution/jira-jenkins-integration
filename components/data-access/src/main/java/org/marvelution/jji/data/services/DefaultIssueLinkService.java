/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.api.events.EventPublisher;
import org.marvelution.jji.data.access.api.IssueToBuildDAO;
import org.marvelution.jji.data.access.api.LinkStatisticsDAO;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.IssueLinkService;
import org.marvelution.jji.data.access.api.IssueReferenceProvider;
import org.marvelution.jji.data.services.api.LinkStatistician;
import org.marvelution.jji.events.ConfigurationSettingUpdatedEvent;
import org.marvelution.jji.events.EventComponent;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.event.ProjectDeletedEvent;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

@Named
@ExportAsService(IssueLinkService.class)
@EventComponent
public class DefaultIssueLinkService
        extends BaseIssueLinkService
{

    private final Executor asyncExecutor;
    private final LinkStatistician linkStatistician;

    @Inject
    public DefaultIssueLinkService(
            IssueToBuildDAO issueToBuildDAO,
            LinkStatisticsDAO linkStatisticsDAO,
            LinkStatistician linkStatistician,
            IssueReferenceProvider issueReferenceProvider,
            ConfigurationService configurationService,
            EventPublisher eventPublisher,
            Executor asyncExecutor)
    {
        super(issueToBuildDAO, linkStatisticsDAO, issueReferenceProvider, configurationService, eventPublisher);
        this.asyncExecutor = asyncExecutor;
        this.linkStatistician = linkStatistician;
    }

    @EventListener
    public void onIssueEvent(IssueEvent event)
    {
        if (EventType.ISSUE_DELETED_ID.equals(event.getEventTypeId()))
        {
            unlinkForIssue(event.getIssue()
                    .getKey());
        }
    }

    @EventListener
    public void onProjectDeleted(ProjectDeletedEvent event)
    {
        unlinkForProject(event.getKey());
    }

    @Override
    @EventListener
    public void onConfigurationSettingChangedEvent(ConfigurationSettingUpdatedEvent event)
    {
        super.onConfigurationSettingChangedEvent(event);
    }

    @Override
    protected void scheduleStatistician(String issueKey)
    {
        asyncExecutor.execute(() -> {
            LinkStatistician.Response response =
                    linkStatistician.calculate(new LinkStatistician.Request().setIssueKeys(Collections.singleton(issueKey))
                            .setReindexIssue(true));
            if (response.hasUnprocessedIssues())
            {
                scheduleBulkStatistician(response.getUnprocessedIssues());
            }
        });
    }

    @Override
    protected void scheduleBulkStatistician(Set<String> issueKeys)
    {
        asyncExecutor.execute(() -> {
            LinkStatistician.Response response = linkStatistician.calculate(new LinkStatistician.Request().setIssueKeys(issueKeys));
            if (response.hasUnprocessedIssues())
            {
                scheduleBulkStatistician(response.getUnprocessedIssues());
            }
        });
    }
}
