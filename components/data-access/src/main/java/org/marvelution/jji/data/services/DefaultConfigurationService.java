/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.UriBuilder;

import org.marvelution.jji.api.events.EventPublisher;
import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.model.Tables;
import org.marvelution.jji.data.access.querydsl.ConfigurationSettingProjection;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.events.ConfigurationSettingUpdatedEvent;
import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.model.ConfigurationSetting;
import org.marvelution.jji.validation.api.ErrorMessages;
import org.marvelution.jji.validation.api.ValidationException;

import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.api.querydsl.util.OnRollback;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.apache.commons.lang3.StringUtils;

import static org.marvelution.jji.data.services.api.ConfigurationService.getScopeForKey;
import static org.marvelution.jji.model.ConfigurationSetting.forKey;

@Named
@ExportAsService(ConfigurationService.class)
public class DefaultConfigurationService
        implements ConfigurationService
{

    public static final String REST_PATH = "rest/jenkins/latest";
    public static final String JIRA_BASE_URL = "jira_base_url";
    public static final String JIRA_BASE_RPC_URL = "jira_base_rpc_url";
    private final TextResolver textResolver;
    private final EventPublisher eventPublisher;
    private final ApplicationProperties applicationProperties;
    private final DatabaseAccessor databaseAccessor;
    private final UserManager userManager;
    private final ThreadLocal<DatabaseConnection> connection = new ThreadLocal<>();

    @Inject
    public DefaultConfigurationService(
            TextResolver textResolver,
            @ComponentImport
            ApplicationProperties applicationProperties,
            DatabaseAccessor databaseAccessor,
            EventPublisher eventPublisher,
            @ComponentImport
            UserManager userManager)
    {
        this.textResolver = textResolver;
        this.applicationProperties = applicationProperties;
        this.databaseAccessor = databaseAccessor;
        this.eventPublisher = eventPublisher;
        this.userManager = userManager;
    }

    @Override
    public String getInstanceName()
    {
        return applicationProperties.getDisplayName();
    }

    @Override
    public URI getBaseUrl()
    {
        return URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL));
    }

    @Override
    public URI getBaseAPIUrl()
    {
        return getConfigurationSetting(JIRA_BASE_RPC_URL).valueAs(UriBuilder::fromUri)
                .orElseGet(() -> UriBuilder.fromUri(getBaseUrl()))
                .path(REST_PATH)
                .build();
    }

    @Override
    public Configuration getConfiguration(
            @Nullable
            String scope)
    {
        Configuration configuration = new Configuration().addConfiguration(forKey(JIRA_BASE_URL).setValue(getBaseUrl().toASCIIString())
                        .setOverridable(false))
                .addConfiguration(getConfigurationSetting(JIRA_BASE_RPC_URL));

        databaseAccessor.runInTransaction(conn -> {
            fetchAllSettingsInScope(scope, conn).forEach(configuration::addConfiguration);
            fetchAllSettingsInScope(GLOBAL_SCOPE, conn).forEach(setting -> {
                if (configuration.getConfiguration(setting.getKey())
                        .isEmpty())
                {
                    configuration.addConfiguration(setting);
                }
            });
            return null;
        }, OnRollback.NOOP);
        return configuration;
    }

    @Override
    public void saveConfiguration(Configuration configuration)
    {
        databaseAccessor.runInTransaction(conn -> {
            try
            {
                connection.set(conn);
                configuration.getConfiguration()
                        .forEach(setting -> ConfigurationService.super.saveConfigurationSetting(configuration.getScope(), setting));
            }
            finally
            {
                connection.remove();
            }
            return null;
        }, OnRollback.NOOP);

    }

    @Override
    public ConfigurationSetting getConfigurationSetting(
            @Nullable
            String scope,
            String key)
    {
        BooleanExpression scopeWhere;
        if (StringUtils.isNotBlank(scope))
        {
            scopeWhere = Tables.CONFIGURATION_TABLE.scope()
                    .eq(scope)
                    .or(Tables.CONFIGURATION_TABLE.scope()
                            .eq(GLOBAL_SCOPE))
                    .or(Tables.CONFIGURATION_TABLE.scope()
                            .isNull());
        }
        else
        {
            scopeWhere = Tables.CONFIGURATION_TABLE.scope()
                    .eq(GLOBAL_SCOPE)
                    .or(Tables.CONFIGURATION_TABLE.scope()
                            .isNull());
        }

        ConfigurationSetting setting = databaseAccessor.runInTransaction(conn -> conn.select(new ConfigurationSettingProjection())
                .from(Tables.CONFIGURATION_TABLE)
                .where(Tables.CONFIGURATION_TABLE.key()
                        .eq(key)
                        .and(scopeWhere))
                .orderBy(Tables.CONFIGURATION_TABLE.scope()
                        .desc())
                .fetchFirst(), OnRollback.NOOP);

        if (setting == null)
        {
            setting = forKey(key).setScope(getScopeForKey(key, scope));
        }

        if (Objects.equals(JIRA_BASE_RPC_URL, key) && StringUtils.isBlank(setting.getValue()))
        {
            setting.setValue(getBaseUrl().toASCIIString());
        }

        return setting;
    }

    @Override
    public List<ConfigurationSetting> getAllConfigurationSettings()
    {
        return databaseAccessor.runInTransaction(conn -> conn.select(new ConfigurationSettingProjection())
                .from(Tables.CONFIGURATION_TABLE)
                .fetch(), OnRollback.NOOP);
    }

    @Override
    public void saveConfigurationSetting(
            @Nullable
            String scope,
            ConfigurationSetting setting)
    {
        databaseAccessor.runInTransaction(conn -> {
            try
            {
                connection.set(conn);
                ConfigurationService.super.saveConfigurationSetting(scope, setting);
            }
            finally
            {
                connection.remove();
            }
            return null;
        }, OnRollback.NOOP);
    }

    @Override
    public String getStorableConfigurationSettingValue(ConfigurationSetting setting)
    {
        String value = setting.getValue();
        if (Objects.equals(JIRA_BASE_RPC_URL, setting.getKey()))
        {
            if (value != null)
            {
                URI baseAPIUrl = setting.valueAs(URI::create)
                        .orElse(null);
                if (baseAPIUrl == null || baseAPIUrl.equals(getBaseUrl()))
                {
                    value = null;
                }
                else if (baseAPIUrl.getHost() == null)
                {
                    throw new ValidationException(new ErrorMessages().addError(JIRA_BASE_RPC_URL,
                            textResolver.getText("url.invalid", "host")));
                }
                else
                {
                    value = baseAPIUrl.toASCIIString();
                }
            }
        }
        return value;
    }

    @Override
    public void storeConfigurationSetting(
            ConfigurationSetting setting,
            String scopeForKey,
            String value)
    {
        DatabaseConnection conn = connection.get();
        if (conn == null)
        {
            throw new IllegalStateException("No database connection set!");
        }

        BooleanExpression where = Tables.CONFIGURATION_TABLE.key()
                .eq(setting.getKey())
                .and(getScopeWhere(scopeForKey));
        if (conn.select(Tables.CONFIGURATION_TABLE.key())
                    .from(Tables.CONFIGURATION_TABLE)
                    .where(where)
                    .fetchCount() > 0)
        {
            conn.update(Tables.CONFIGURATION_TABLE)
                    .where(where)
                    .set(Tables.CONFIGURATION_TABLE.val(), value)
                    .execute();
        }
        else
        {
            conn.insert(Tables.CONFIGURATION_TABLE)
                    .set(Tables.CONFIGURATION_TABLE.id(), Objects.hash(scopeForKey, setting.getKey()))
                    .set(Tables.CONFIGURATION_TABLE.key(), setting.getKey())
                    .set(Tables.CONFIGURATION_TABLE.scope(), scopeForKey)
                    .set(Tables.CONFIGURATION_TABLE.overridable(), setting.isOverridable())
                    .set(Tables.CONFIGURATION_TABLE.val(), value)
                    .execute();
        }

        eventPublisher.publish(new ConfigurationSettingUpdatedEvent(setting));
    }

    @Override
    public void resetToDefaults()
    {
        databaseAccessor.runInTransaction(conn -> {
            conn.delete(Tables.CONFIGURATION_TABLE)
                    .where(Tables.CONFIGURATION_TABLE.overridable()
                            .isTrue()
                            .and(Tables.CONFIGURATION_TABLE.internal()
                                    .isFalse()))
                    .execute();
            conn.insert(Tables.CONFIGURATION_TABLE)
                    .set(Tables.CONFIGURATION_TABLE.id(), Objects.hash(GLOBAL_SCOPE, MAX_BUILDS_PER_PAGE))
                    .set(Tables.CONFIGURATION_TABLE.key(), MAX_BUILDS_PER_PAGE)
                    .set(Tables.CONFIGURATION_TABLE.scope(), GLOBAL_SCOPE)
                    .set(Tables.CONFIGURATION_TABLE.overridable(), true)
                    .set(Tables.CONFIGURATION_TABLE.internal(), false)
                    .set(Tables.CONFIGURATION_TABLE.val(), Integer.toString(DEFAULT_MAX_BUILDS_PER_PAGE))
                    .addBatch()
                    .set(Tables.CONFIGURATION_TABLE.id(), Objects.hash(GLOBAL_SCOPE, USE_USER_ID))
                    .set(Tables.CONFIGURATION_TABLE.key(), USE_USER_ID)
                    .set(Tables.CONFIGURATION_TABLE.scope(), GLOBAL_SCOPE)
                    .set(Tables.CONFIGURATION_TABLE.overridable(), true)
                    .set(Tables.CONFIGURATION_TABLE.internal(), false)
                    .set(Tables.CONFIGURATION_TABLE.val(), Boolean.toString(DEFAULT_USE_USER_ID))
                    .addBatch()
                    .set(Tables.CONFIGURATION_TABLE.id(), Objects.hash(GLOBAL_SCOPE, PARAMETER_ISSUE_FIELDS))
                    .set(Tables.CONFIGURATION_TABLE.key(), PARAMETER_ISSUE_FIELDS)
                    .set(Tables.CONFIGURATION_TABLE.scope(), GLOBAL_SCOPE)
                    .set(Tables.CONFIGURATION_TABLE.overridable(), true)
                    .set(Tables.CONFIGURATION_TABLE.internal(), false)
                    .set(Tables.CONFIGURATION_TABLE.val(), "")
                    .addBatch()
                    .set(Tables.CONFIGURATION_TABLE.id(), Objects.hash(GLOBAL_SCOPE, PARAMETER_ISSUE_FIELD_NAMES))
                    .set(Tables.CONFIGURATION_TABLE.key(), PARAMETER_ISSUE_FIELD_NAMES)
                    .set(Tables.CONFIGURATION_TABLE.scope(), GLOBAL_SCOPE)
                    .set(Tables.CONFIGURATION_TABLE.overridable(), true)
                    .set(Tables.CONFIGURATION_TABLE.internal(), false)
                    .set(Tables.CONFIGURATION_TABLE.val(), "")
                    .addBatch()
                    .set(Tables.CONFIGURATION_TABLE.id(), Objects.hash(GLOBAL_SCOPE, PARAMETER_ISSUE_FIELD_MAPPERS))
                    .set(Tables.CONFIGURATION_TABLE.key(), PARAMETER_ISSUE_FIELD_MAPPERS)
                    .set(Tables.CONFIGURATION_TABLE.scope(), GLOBAL_SCOPE)
                    .set(Tables.CONFIGURATION_TABLE.overridable(), true)
                    .set(Tables.CONFIGURATION_TABLE.internal(), false)
                    .set(Tables.CONFIGURATION_TABLE.val(), "")
                    .addBatch()
                    .set(Tables.CONFIGURATION_TABLE.id(), Objects.hash(GLOBAL_SCOPE, DELETED_DATA_RETENTION_PERIOD))
                    .set(Tables.CONFIGURATION_TABLE.key(), DELETED_DATA_RETENTION_PERIOD)
                    .set(Tables.CONFIGURATION_TABLE.scope(), GLOBAL_SCOPE)
                    .set(Tables.CONFIGURATION_TABLE.overridable(), true)
                    .set(Tables.CONFIGURATION_TABLE.internal(), false)
                    .set(Tables.CONFIGURATION_TABLE.val(), Integer.toString(DEFAULT_DELETED_DATA_RETENTION_PERIOD))
                    .addBatch()
                    .set(Tables.CONFIGURATION_TABLE.id(), Objects.hash(GLOBAL_SCOPE, AUDIT_DATA_RETENTION_PERIOD))
                    .set(Tables.CONFIGURATION_TABLE.key(), AUDIT_DATA_RETENTION_PERIOD)
                    .set(Tables.CONFIGURATION_TABLE.scope(), GLOBAL_SCOPE)
                    .set(Tables.CONFIGURATION_TABLE.overridable(), true)
                    .set(Tables.CONFIGURATION_TABLE.internal(), false)
                    .set(Tables.CONFIGURATION_TABLE.val(), Integer.toString(DEFAULT_AUDIT_DATA_RETENTION_PERIOD))
                    .addBatch()
                    .set(Tables.CONFIGURATION_TABLE.id(), Objects.hash(GLOBAL_SCOPE, ENABLED_PROJECT_KEYS))
                    .set(Tables.CONFIGURATION_TABLE.key(), ENABLED_PROJECT_KEYS)
                    .set(Tables.CONFIGURATION_TABLE.scope(), GLOBAL_SCOPE)
                    .set(Tables.CONFIGURATION_TABLE.overridable(), true)
                    .set(Tables.CONFIGURATION_TABLE.internal(), false)
                    .set(Tables.CONFIGURATION_TABLE.val(), "")
                    .addBatch()
                    .execute();

            return null;
        }, OnRollback.NOOP);
    }

    @Override
    public void resetToDefaults(
            @Nullable
            String scope)
    {
        if (StringUtils.isBlank(scope))
        {
            resetToDefaults();
        }
        else
        {
            databaseAccessor.runInTransaction(conn -> {
                conn.delete(Tables.CONFIGURATION_TABLE)
                        .where(Tables.CONFIGURATION_TABLE.scope()
                                .eq(scope)
                                .and(Tables.CONFIGURATION_TABLE.overridable()
                                        .isTrue()))
                        .execute();
                return null;
            }, OnRollback.NOOP);
        }
    }

    private List<ConfigurationSetting> fetchAllSettingsInScope(
            String scope,
            DatabaseConnection conn)
    {
        BooleanExpression scopeWhere = getScopeWhere(scope);
        return conn.select(new ConfigurationSettingProjection())
                .from(Tables.CONFIGURATION_TABLE)
                .where(scopeWhere.and(Tables.CONFIGURATION_TABLE.internal()
                                .isFalse())
                        .and(Tables.CONFIGURATION_TABLE.key()
                                .ne(JIRA_BASE_RPC_URL)))
                .fetch();
    }

    private BooleanExpression getScopeWhere(String scope)
    {
        if (StringUtils.isNotBlank(scope))
        {
            return Tables.CONFIGURATION_TABLE.scope()
                    .eq(scope);
        }
        else
        {
            return Tables.CONFIGURATION_TABLE.scope()
                    .eq(GLOBAL_SCOPE)
                    .or(Tables.CONFIGURATION_TABLE.scope()
                            .isNull());
        }
    }
}
