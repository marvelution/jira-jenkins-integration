/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteAuthenticationType;
import org.marvelution.jji.model.SiteConnectionType;
import org.marvelution.jji.model.SiteType;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.RelationalPath;

import static org.marvelution.jji.data.access.model.v2.SiteEntity.*;

/**
 * {@link Site} QueryDSL {@link Table}.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public class SiteTable
        extends Table<Site, SiteTable>
{

    private final StringPath id = createString(ID);
    private final StringPath scope = createString(SCOPE);
    private final EnumPath<SiteType> siteType = createEnum(SITE_TYPE, SiteType.class);
    private final StringPath name = createString(NAME);
    private final StringPath sharedSecret = createString(SHARED_SECRET);
    private final StringPath rpcUrl = createString(RPC_URL);
    private final StringPath displayUrl = createString(DISPLAY_URL);
    private final EnumPath<SiteAuthenticationType> siteAuthentication = createEnum(SITE_AUTHENTICATION, SiteAuthenticationType.class);
    private final EnumPath<SiteConnectionType> siteConnection = createEnum(SITE_CONNECTION, SiteConnectionType.class);
    private final StringPath user = createString(USER);
    private final StringPath userToken = createString(USER_TOKEN);
    private final BooleanPath autoLinkNewJobs = createBoolean(AUTO_LINK_NEW_JOBS);
    private final BooleanPath jenkinsPluginInstalled = createBoolean(JENKINS_PLUGIN_INSTALLED);
    private final BooleanPath registrationComplete = createBoolean(REGISTRATION_COMPLETE);
    private final BooleanPath useCrumbs = createBoolean(USE_CRUMBS);
    private final BooleanPath firewalled = createBoolean(FIREWALLED);
    private final BooleanPath enabled = createBoolean(ENABLED);

    public SiteTable(String namespace)
    {
        super(SiteTable.class, namespace + TABLE_NAME, "site");
        createPrimaryKey(id);
    }

    @Override
    public StringPath id()
    {
        return id;
    }

    @Override
    public Map<Path<?>, Object> mapForInsert(
            RelationalPath<?> path,
            Site object)
    {
        Map<Path<?>, Object> bindings = mapForUpdate(path, object);
        bindings.put(id, object.getId());
        bindings.put(sharedSecret, object.getSharedSecret());
        return bindings;
    }

    @Override
    public Map<Path<?>, Object> mapForUpdate(
            RelationalPath<?> path,
            Site object)
    {
        Map<Path<?>, Object> bindings = new HashMap<>();
        bindings.put(scope, object.getScope());
        bindings.put(siteType,
                object.getType()
                        .name());
        bindings.put(name, object.getName());
        bindings.put(rpcUrl,
                object.getRpcUrl()
                        .toASCIIString());
        bindings.put(displayUrl,
                Optional.ofNullable(object.getDisplayUrlOrNull())
                        .map(URI::toASCIIString)
                        .orElse(null));
        bindings.put(siteAuthentication,
                object.getAuthenticationType()
                        .name());
        bindings.put(user, object.getUser());
        bindings.put(userToken, object.getToken());
        bindings.put(autoLinkNewJobs, object.isAutoLinkNewJobs());
        bindings.put(jenkinsPluginInstalled, object.isJenkinsPluginInstalled());
        bindings.put(registrationComplete, object.isRegistrationComplete());
        bindings.put(useCrumbs, object.isUseCrumbs());
        bindings.put(siteConnection,
                object.getConnectionType()
                        .name());
        bindings.put(enabled, object.isEnabled());
        return bindings;
    }

    public StringPath scope()
    {
        return scope;
    }

    public EnumPath<SiteType> siteType()
    {
        return siteType;
    }

    public StringPath name()
    {
        return name;
    }

    public StringPath sharedSecret()
    {
        return sharedSecret;
    }

    public StringPath rpcUrl()
    {
        return rpcUrl;
    }

    public StringPath displayUrl()
    {
        return displayUrl;
    }

    public EnumPath<SiteAuthenticationType> siteAuthentication()
    {
        return siteAuthentication;
    }

    public EnumPath<SiteConnectionType> siteConnection()
    {
        return siteConnection;
    }

    public StringPath user()
    {
        return user;
    }

    public StringPath userToken()
    {
        return userToken;
    }

    public BooleanPath autoLinkNewJobs()
    {
        return autoLinkNewJobs;
    }

    public BooleanPath jenkinsPluginInstalled()
    {
        return jenkinsPluginInstalled;
    }

    public BooleanPath registrationComplete()
    {
        return registrationComplete;
    }

    public BooleanPath useCrumbs()
    {
        return useCrumbs;
    }

    public BooleanPath firewalled()
    {
        return firewalled;
    }

    public BooleanPath enabled()
    {
        return enabled;
    }
}
