/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.querydsl;

import java.util.*;

import org.marvelution.jji.data.access.model.*;

import com.querydsl.core.types.*;
import com.querydsl.sql.*;
import com.querydsl.sql.dml.*;

public class SynchronizationResultHolderProjection
		extends ProjectionBase<SynchronizationResultHolder>
		implements Mapper<SynchronizationResultHolder>
{

	public SynchronizationResultHolderProjection()
	{
		super(SynchronizationResultHolder.class, new ArrayList<>(Tables.SYNCHRONIZATION_RESULT_TABLE.getColumns()));
	}

	@Override
	SynchronizationResultHolder createInstance(ProjectionContext context)
	{
		return new SynchronizationResultHolder(context.get(Tables.SYNCHRONIZATION_RESULT_TABLE.id()),
		                                       context.get(Tables.SYNCHRONIZATION_RESULT_TABLE.operationId()),
		                                       context.get(Tables.SYNCHRONIZATION_RESULT_TABLE.triggerReason()),
		                                       context.get(Tables.SYNCHRONIZATION_RESULT_TABLE.createTimestamp()),
		                                       context.get(Tables.SYNCHRONIZATION_RESULT_TABLE.timestampQueued()),
		                                       context.get(Tables.SYNCHRONIZATION_RESULT_TABLE.timestampStarted()),
		                                       context.get(Tables.SYNCHRONIZATION_RESULT_TABLE.timestampFinished()),
		                                       context.get(Tables.SYNCHRONIZATION_RESULT_TABLE.stopRequested()),
		                                       context.get(Tables.SYNCHRONIZATION_RESULT_TABLE.jobCount()),
		                                       context.get(Tables.SYNCHRONIZATION_RESULT_TABLE.buildCount()),
		                                       context.get(Tables.SYNCHRONIZATION_RESULT_TABLE.issueCount()));
	}

	@Override
	public Map<Path<?>, Object> createMap(
			RelationalPath<?> path,
			SynchronizationResultHolder object)
	{
		Map<Path<?>, Object> data = new HashMap<>();
		data.put(Tables.SYNCHRONIZATION_RESULT_TABLE.timestampQueued(), object.getTimestampQueued());
		data.put(Tables.SYNCHRONIZATION_RESULT_TABLE.timestampStarted(), object.getTimestampStarted());
		data.put(Tables.SYNCHRONIZATION_RESULT_TABLE.timestampFinished(), object.getTimestampFinished());
		data.put(Tables.SYNCHRONIZATION_RESULT_TABLE.stopRequested(), object.getStopRequested());
		data.put(Tables.SYNCHRONIZATION_RESULT_TABLE.jobCount(), object.getJobCount());
		data.put(Tables.SYNCHRONIZATION_RESULT_TABLE.buildCount(), object.getBuildCount());
		data.put(Tables.SYNCHRONIZATION_RESULT_TABLE.issueCount(), object.getIssueCount());
		return data;
	}

}
