/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.querydsl;

import java.net.*;
import java.util.*;

import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.model.*;

/**
 * QueryDSL {@link Site} projection.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public class SiteProjection
		extends ProjectionBase<Site>
{

	public SiteProjection()
	{
		super(Site.class, new ArrayList<>(Tables.SITE.getColumns()));
	}

	@Override
	Site createInstance(ProjectionContext context)
	{
		return createSite(context);
	}

	static Site createSite(ProjectionContext context)
	{
		String scope = context.get(Tables.SITE.scope());
		return new Site().setId(context.get(Tables.SITE.id()))
				.setType(context.get(Tables.SITE.siteType()))
				.setScope(scope != null ? scope : ConfigurationSetting.GLOBAL_SCOPE)
				.setName(context.get(Tables.SITE.name()))
				.setSharedSecret(context.get(Tables.SITE.sharedSecret()))
				.setRpcUrl(URI.create(context.get(Tables.SITE.rpcUrl())))
				.setDisplayUrl(Optional.ofNullable(context.get(Tables.SITE.displayUrl())).map(URI::create).orElse(null))
				.setAuthenticationType(context.get(Tables.SITE.siteAuthentication()))
				.setUser(context.get(Tables.SITE.user()))
				.setToken(context.get(Tables.SITE.userToken()))
				.setAutoLinkNewJobs(context.get(Tables.SITE.autoLinkNewJobs()))
				.setJenkinsPluginInstalled(context.get(Tables.SITE.jenkinsPluginInstalled()))
				.setRegistrationComplete(context.get(Tables.SITE.registrationComplete()))
				.setUseCrumbs(context.get(Tables.SITE.useCrumbs()))
				.setConnectionType(context.get(Tables.SITE.siteConnection()))
				.setEnabled(context.get(Tables.SITE.enabled()));
	}
}
