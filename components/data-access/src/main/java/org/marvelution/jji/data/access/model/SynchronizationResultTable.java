/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import com.atlassian.pocketknife.spi.querydsl.*;
import com.querydsl.core.types.dsl.*;

import static org.marvelution.jji.data.access.model.Entity.*;
import static org.marvelution.jji.data.access.model.v2.SynchronizationResultEntity.*;

public class SynchronizationResultTable
		extends EnhancedRelationalPathBase<SynchronizationResultTable>
{

	private final StringPath id = createString(ID);
	private final StringPath operationId = createString(OPERATION_ID);
	private final StringPath triggerReason = createString(TRIGGER_REASON);
	private final NumberPath<Long> createTimestamp = createLong(CREATE_TIMESTAMP);
	private final NumberPath<Long> timestampQueued = createLong(TIMESTAMP_QUEUED);
	private final NumberPath<Long> timestampStarted = createLong(TIMESTAMP_STARTED);
	private final NumberPath<Long> timestampFinished = createLong(TIMESTAMP_FINISHED);
	private final BooleanPath stopRequested = createBoolean(STOP_REQUESTED);
	private final NumberPath<Integer> jobCount = createInteger(JOB_COUNT);
	private final NumberPath<Integer> buildCount = createInteger(BUILD_COUNT);
	private final NumberPath<Integer> issueCount = createInteger(ISSUE_COUNT);

	public SynchronizationResultTable(String namespace)
	{
		super(SynchronizationResultTable.class, namespace + TABLE_NAME, "syncResult");
		createPrimaryKey(id);
	}

	public StringPath id()
	{
		return id;
	}

	public StringPath operationId()
	{
		return operationId;
	}

	public StringPath triggerReason()
	{
		return triggerReason;
	}

	public NumberPath<Long> createTimestamp()
	{
		return createTimestamp;
	}

	public NumberPath<Long> timestampQueued()
	{
		return timestampQueued;
	}

	public NumberPath<Long> timestampStarted()
	{
		return timestampStarted;
	}

	public NumberPath<Long> timestampFinished()
	{
		return timestampFinished;
	}

	public BooleanPath stopRequested()
	{
		return stopRequested;
	}

	public NumberPath<Integer> jobCount()
	{
		return jobCount;
	}

	public NumberPath<Integer> buildCount()
	{
		return buildCount;
	}

	public NumberPath<Integer> issueCount()
	{
		return issueCount;
	}
}
