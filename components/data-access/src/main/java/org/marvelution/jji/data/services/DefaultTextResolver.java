/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.*;
import javax.annotation.*;
import javax.inject.Named;
import javax.inject.*;

import org.marvelution.jji.api.text.*;

import com.atlassian.jira.security.*;
import com.atlassian.jira.util.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import com.atlassian.sal.api.message.*;

/**
 * {@link TextResolver} implementation that uses the {@link I18nResolver}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named("textResolver")
public class DefaultTextResolver
		implements TextResolver
{

	private final I18nHelper.BeanFactory beanFactory;
	private final JiraAuthenticationContext authenticationContext;

	@Inject
	public DefaultTextResolver(
			@ComponentImport I18nHelper.BeanFactory beanFactory,
			@ComponentImport JiraAuthenticationContext authenticationContext)
	{
		this.beanFactory = beanFactory;
		this.authenticationContext = authenticationContext;
	}

	@Override
	public String getText(
			String key,
			Object... arguments)
	{
		return getI18nInstance(null).getText(key, arguments);
	}

	@Override
	public String getText(
			Locale locale,
			String key,
			Object... arguments)
	{
		return getI18nInstance(locale).getText(key, arguments);
	}

	@Override
	public Properties getTexts(Locale locale)
	{
		Properties properties = new Properties();
		ResourceBundle resourceBundle = getI18nInstance(locale).getResourceBundle();
		Enumeration<String> resourceBundleKeys = resourceBundle.getKeys();
		while (resourceBundleKeys.hasMoreElements())
		{
			String key = resourceBundleKeys.nextElement();
			String value = resourceBundle.getString(key);
			properties.setProperty(key, value);
		}
		return properties;
	}

	private I18nHelper getI18nInstance(@Nullable Locale locale)
	{
		return beanFactory.getInstance(Optional.ofNullable(locale).orElseGet(authenticationContext::getLocale));
	}
}
