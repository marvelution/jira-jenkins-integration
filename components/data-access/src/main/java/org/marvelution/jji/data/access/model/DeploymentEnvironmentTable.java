/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import java.util.*;

import org.marvelution.jji.model.*;

import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.*;

import static org.marvelution.jji.data.access.model.v2.DeploymentEnvironmentEntity.*;

/**
 * {@link DeploymentEnvironment} QueryDSL {@link Table}.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public class DeploymentEnvironmentTable
		extends Table<DeploymentEnvironment, DeploymentEnvironmentTable>
{

	private final StringPath id = createString(ID);
	private final StringPath name = createString(NAME);
	private final EnumPath<DeploymentEnvironmentType> type = createEnum(TYPE, DeploymentEnvironmentType.class);

	public DeploymentEnvironmentTable(String namespace)
	{
		super(DeploymentEnvironmentTable.class, namespace + TABLE_NAME, "deployEnv");
		createPrimaryKey(id);
	}

	@Override
	public StringPath id()
	{
		return id;
	}

	public StringPath name()
	{
		return name;
	}

	public EnumPath<DeploymentEnvironmentType> type()
	{
		return type;
	}

	@Override
	public Map<Path<?>, Object> mapForInsert(
			RelationalPath<?> path,
			DeploymentEnvironment object)
	{
		Map<Path<?>, Object> bindings = new HashMap<>();
		bindings.put(id, object.getId());
		bindings.put(name, object.getName());
		bindings.put(type, object.getType().name());
		return bindings;
	}

	@Override
	public Map<Path<?>, Object> mapForUpdate(
			RelationalPath<?> path,
			DeploymentEnvironment object)
	{
		return mapForInsert(path, object);
	}
}
