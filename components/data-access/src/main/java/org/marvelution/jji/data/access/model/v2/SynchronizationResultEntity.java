/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import org.marvelution.jji.data.access.model.Entity;

import net.java.ao.*;
import net.java.ao.schema.Table;
import net.java.ao.schema.*;

import static net.java.ao.schema.StringLength.*;

/**
 * @author Mark Rekveld
 * @since 3.9.0
 */
@Preload
@Table(SynchronizationResultEntity.TABLE_NAME)
@Indexes(@Index(name = "sync_result_created_idx",
                methodNames = {"getOperationId",
                        "getCreateTimestamp"}))
public interface SynchronizationResultEntity
        extends Entity
{

    String TABLE_NAME = "SYNC_RESULTS";

    String OPERATION_ID = "OPERATION_ID";
    String TRIGGER_REASON = "TRIGGER_REASON";
    String CREATE_TIMESTAMP = "CREATE_TIMESTAMP";
    String TIMESTAMP_QUEUED = "TIMESTAMP_QUEUED";
    String TIMESTAMP_STARTED = "TIMESTAMP_STARTED";
    String TIMESTAMP_FINISHED = "TIMESTAMP_FINISHED";
    String STOP_REQUESTED = "STOP_REQUESTED";
    String JOB_COUNT = "JOB_COUNT";
    String BUILD_COUNT = "BUILD_COUNT";
    String ISSUE_COUNT = "ISSUE_COUNT";

    @NotNull
    @StringLength(MAX_LENGTH)
    String getOperationId();

    void setOperationId(String operationId);

    @NotNull
    @StringLength(UNLIMITED)
    String getTriggerReason();

    void setTriggerReason(String triggerReason);

    @NotNull
    long getCreateTimestamp();

    void setCreateTimestamp(long createTimestamp);

    Long getTimestampQueued();

    void setTimestampQueued(Long timestampQueued);

    Long getTimestampStarted();

    void setTimestampStarted(Long timestampStarted);

    Long getTimestampFinished();

    void setTimestampFinished(Long timestampFinished);

    boolean isStopRequested();

    void setStopRequested(boolean stopRequested);

    int getJobCount();

    void setJobCount(int jobCount);

    int getBuildCount();

    void setBuildCount(int buildCount);

    int getIssueCount();

    void setIssueCount(int issueCount);

    @OneToMany(reverse = "getResult")
    SynchronizationResultErrorEntity[] getErrors();
}
