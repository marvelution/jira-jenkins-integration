/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.querydsl;

import java.util.*;

import org.marvelution.jji.data.access.*;
import org.marvelution.jji.data.access.model.*;

public class SynchronizationResultErrorProjection
		extends ProjectionBase<DefaultSynchronizationResult.DefaultError>
{

	public SynchronizationResultErrorProjection()
	{
		super(DefaultSynchronizationResult.DefaultError.class, new ArrayList<>(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE.getColumns()));
	}

	@Override
    DefaultSynchronizationResult.DefaultError createInstance(ProjectionContext context)
	{
		return new DefaultSynchronizationResult.DefaultError(context.get(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE.id()),
		                                              context.get(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE.message()),
		                                              context.get(Tables.SYNCHRONIZATION_RESULT_ERROR_TABLE.stackTrace()));
	}
}
