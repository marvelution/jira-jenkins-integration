/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v3;

import org.marvelution.jji.api.migration.*;
import org.marvelution.jji.data.access.model.Entity;

import net.java.ao.*;
import net.java.ao.schema.Table;
import net.java.ao.schema.*;

import static net.java.ao.schema.StringLength.*;

@Preload
@Table(AppMigrationEntity.TABLE_NAME)
public interface AppMigrationEntity
		extends Entity
{

	String TABLE_NAME = "migrations";

	String CREATE_TIMESTAMP = "CREATE_TIMESTAMP";
	String NAME = "NAME";
	String TYPE = "TYPE";
	String PROGRESS_STATUS = "PROGRESS_STATUS";
	String EVENT_JSON = "EVENT_JSON";
	String FEEDBACK_JSON = "FEEDBACK_JSON";

	@NotNull
	long getCreateTimestamp();

	void setCreateTimestamp(long createTimestamp);

	@NotNull
	@StringLength(MAX_LENGTH)
	String getName();

	void setName(String name);

	@NotNull
	@StringLength(10)
	AppMigrationType getType();

	void setType(AppMigrationType type);

	@NotNull
	@StringLength(20)
	ProgressStatus getProgressStatus();

	void setProgressStatus(ProgressStatus progressStatus);

	@NotNull
	@StringLength(UNLIMITED)
	String getEventJson();

	void setEventJson(String eventJson);

	@StringLength(UNLIMITED)
	String getFeedbackJson();

	void setFeedbackJson(String feedbackJson);
}
