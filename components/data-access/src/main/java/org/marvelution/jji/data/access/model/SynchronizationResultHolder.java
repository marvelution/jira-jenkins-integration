/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import java.util.function.*;

public class SynchronizationResultHolder
{

	private final String id;
	private final String operationId;
	private final String triggerReason;
	private final long createTimestamp;
	private Long timestampQueued;
	private Long timestampStarted;
	private Long timestampFinished;
	private Boolean stopRequested;
	private Integer jobCount;
	private Integer buildCount;
	private Integer issueCount;

	public SynchronizationResultHolder(
			String id,
			String operationId,
			String triggerReason,
			long createTimestamp)
	{
		this.id = id;
		this.operationId = operationId;
		this.triggerReason = triggerReason;
		this.createTimestamp = createTimestamp;
	}

	public SynchronizationResultHolder(
			String id,
			String operationId,
			String triggerReason,
			long createTimestamp,
			Long timestampQueued,
			Long timestampStarted,
			Long timestampFinished,
			Boolean stopRequested,
			Integer jobCount,
			Integer buildCount,
			Integer issueCount)
	{
		this(id, operationId, triggerReason, createTimestamp);
		this.timestampQueued = timestampQueued;
		this.timestampStarted = timestampStarted;
		this.timestampFinished = timestampFinished;
		this.stopRequested = stopRequested;
		this.jobCount = jobCount;
		this.buildCount = buildCount;
		this.issueCount = issueCount;
	}

	public String getId()
	{
		return id;
	}

	public String getOperationId()
	{
		return operationId;
	}

	public String getTriggerReason()
	{
		return triggerReason;
	}

	public long getCreateTimestamp()
	{
		return createTimestamp;
	}

	public Long getTimestampQueued()
	{
		return timestampQueued;
	}

	public void setTimestampQueued(Long timestampQueued)
	{
		this.timestampQueued = timestampQueued;
	}

	public boolean hasTimestampQueued()
	{
		return timestampQueued != null && timestampQueued > 0;
	}

	public Long getTimestampStarted()
	{
		return timestampStarted;
	}

	public void setTimestampStarted(Long timestampStarted)
	{
		this.timestampStarted = timestampStarted;
	}

	public boolean hasTimestampStarted()
	{
		return timestampStarted != null && timestampStarted > 0;
	}

	public Long getTimestampFinished()
	{
		return timestampFinished;
	}

	public void setTimestampFinished(Long timestampFinished)
	{
		this.timestampFinished = timestampFinished;
	}

	public boolean hasTimestampFinished()
	{
		return timestampFinished != null && timestampFinished > 0;
	}

	public boolean getStopRequested()
	{
		return stopRequested != null ? stopRequested : false;
	}

	public void setStopRequested(Boolean stopRequested)
	{
		this.stopRequested = stopRequested;
	}

	public int getJobCount()
	{
		return jobCount != null ? jobCount : 0;
	}

	public void setJobCount(int jobCount)
	{
		this.jobCount = jobCount;
	}

	public void incrementJobCount()
	{
		incrementCounter(this::getJobCount, this::setJobCount, 1);
	}

	public int getBuildCount()
	{
		return buildCount != null ? buildCount : 0;
	}

	public void setBuildCount(int buildCount)
	{
		this.buildCount = buildCount;
	}

	public void incrementBuildCount()
	{
		incrementCounter(this::getBuildCount, this::setBuildCount, 1);
	}

	public int getIssueCount()
	{
		return issueCount != null ? issueCount : 0;
	}

	public void setIssueCount(int issueCount)
	{
		this.issueCount = issueCount;
	}

	public void incrementIssueCount(int count)
	{
		incrementCounter(this::getIssueCount, this::setIssueCount, count);
	}

	private void incrementCounter(Supplier<Integer> getter, Consumer<Integer> setter, int amount)
	{
		setter.accept(getter.get() + amount);
	}
}
