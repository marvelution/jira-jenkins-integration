/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import javax.annotation.*;
import javax.inject.*;
import java.time.*;
import java.util.*;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.data.access.querydsl.*;
import org.marvelution.jji.model.*;

import com.atlassian.pocketknife.api.querydsl.*;
import com.atlassian.pocketknife.api.querydsl.stream.*;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.*;
import com.querydsl.sql.dml.*;

import static org.marvelution.jji.model.HasId.*;

@Named
public class DefaultBuildDAO
        extends AbstractPocketKnifeDAO<Build, BuildTable>
        implements BuildDAO
{

    private final IssueReferenceProvider issueReferenceProvider;

    @Inject
    public DefaultBuildDAO(
            DatabaseAccessor databaseAccessor,
            StreamingQueryFactory queryFactory,
            IssueReferenceProvider issueReferenceProvider)
    {
        super(databaseAccessor, queryFactory, Tables.BUILD);
        this.issueReferenceProvider = issueReferenceProvider;
    }

    @Nullable
    @Override
    public Build get(String id)
    {
        return runInTransaction(conn -> findBuilds(conn,
                table.id()
                        .eq(id),
                0,
                1).stream()
                .findFirst()
                .orElse(null));
    }

    @Nullable
    @Override
    public Build get(
            String jobId,
            int buildNumber)
    {
        return runInTransaction(conn -> findBuilds(conn,
                table.jobId()
                        .eq(jobId)
                        .and(table.buildNumber()
                                .eq(buildNumber)),
                0,
                1).stream()
                .findFirst()
                .orElse(null));
    }

    @Override
    public Set<Build> getAllInRange(
            String jobId,
            int start,
            int end)
    {
        return runInTransaction(conn -> {
            BooleanExpression where = table.jobId()
                    .eq(jobId)
                    .and(table.buildNumber()
                            .goe(start));
            if (end > 0)
            {
                where = where.and(table.buildNumber()
                        .loe(end));
            }
            return findBuilds(conn, where, -1, -1);
        });
    }

    @Override
    public Set<Build> getAll(
            String jobId,
            int page,
            int size)
    {
        return runInTransaction(conn -> findBuilds(conn,
                table.jobId()
                        .eq(jobId),
                page,
                size));
    }

    @Override
    public Set<Build> getAllByJob(
            String jobId,
            int limit)
    {
        return runInTransaction(conn -> findBuilds(conn,
                table.jobId()
                        .eq(jobId),
                0,
                limit));
    }

    @Override
    public void deleteAllByJob(String jobId)
    {
        runInTransaction(conn -> DeletionHelper.deleteJobBuilds(conn, jobId));
    }

    @Override
    public void markAsDeleted(String buildId)
    {
        runInTransaction(conn -> conn.update(table)
                .set(table.deleted(), true)
                .where(table.id()
                        .eq(buildId))
                .execute());
    }

    @Override
    public void markAllInJobAsDeleted(
            String jobId,
            int buildNumber)
    {
        runInTransaction(conn -> {
            BooleanExpression where = table.jobId()
                    .eq(jobId);
            if (buildNumber > 0)
            {
                where = where.and(table.buildNumber()
                        .lt(buildNumber));
            }
            conn.update(table)
                    .set(table.deleted(), true)
                    .where(where)
                    .execute();
            return null;
        });
    }

    @Override
    public Set<DeploymentEnvironment> getDeploymentEnvironments()
    {
        return new HashSet<>(runInTransaction(conn -> conn.select(new DeploymentEnvironmentProjection())
                .from(Tables.DEPLOYMENT_ENVIRONMENT)
                .fetch()));
    }

    @Override
    public void cleanupOldDeletedBuilds(LocalDateTime timestamp)
    {
        DeletionHelper.batchDelete(databaseAccessor,
                conn -> conn.select(table.id())
                        .from(table)
                        .where(table.deleted()
                                .isTrue()
                                .and(table.timestamp()
                                        .lt(TimeUtils.toEpochMilli(timestamp)))),
                DeletionHelper::deleteBuild);
    }

    protected Build saveInTransaction(
            Build build,
            DatabaseConnection conn)
    {
        // Create a copy and clear stuff not stored.
        Build copy = build.copy()
                .setBranches(null)
                .setChangeSet(null);

        super.saveInTransaction(copy, conn);

        conn.delete(Tables.TEST_RESULTS)
                .where(Tables.TEST_RESULTS.buildId()
                        .eq(copy.getId()))
                .execute();
        if (copy.getTestResults() != null)
        {
            TestResults testResults = copy.getTestResults();
            conn.insert(Tables.TEST_RESULTS)
                    .set(Tables.TEST_RESULTS.id(), newId())
                    .set(Tables.TEST_RESULTS.buildId(), copy.getId())
                    .set(Tables.TEST_RESULTS.skipped(), testResults.getSkipped())
                    .set(Tables.TEST_RESULTS.failed(), testResults.getFailed())
                    .set(Tables.TEST_RESULTS.total(), testResults.getTotal())
                    .execute();
        }
        conn.delete(Tables.BUILD_TO_DEPLOYMENT_ENVIRONMENT)
                .where(Tables.BUILD_TO_DEPLOYMENT_ENVIRONMENT.buildId()
                        .eq(copy.getId()))
                .execute();
        if (!copy.getDeploymentEnvironments()
                .isEmpty())
        {
            List<String> knownDeployEnvironments = new ArrayList<>(conn.select(Tables.DEPLOYMENT_ENVIRONMENT.id())
                    .from(Tables.DEPLOYMENT_ENVIRONMENT)
                    .fetch());
            SQLInsertClause insert = conn.insert(Tables.BUILD_TO_DEPLOYMENT_ENVIRONMENT);
            List<DeploymentEnvironment> deployEnvs = copy.getDeploymentEnvironments();
            for (DeploymentEnvironment deployEnv : deployEnvs)
            {
                if (!knownDeployEnvironments.contains(deployEnv.getId()))
                {
                    Tables.DEPLOYMENT_ENVIRONMENT.insert(conn, deployEnv)
                            .execute();
                    knownDeployEnvironments.add(deployEnv.getId());
                }
                insert.set(Tables.BUILD_TO_DEPLOYMENT_ENVIRONMENT.id(), newId())
                        .set(Tables.BUILD_TO_DEPLOYMENT_ENVIRONMENT.buildId(), copy.getId())
                        .set(Tables.BUILD_TO_DEPLOYMENT_ENVIRONMENT.environmentId(), deployEnv.getId())
                        .addBatch();
            }
            insert.execute();
        }
        return copy;
    }

    @Override
    public void delete(String id)
    {
        runInTransaction(conn -> DeletionHelper.deleteBuild(conn, id));
    }

    Set<Build> getBuilds(
            DatabaseConnection conn,
            Set<String> buildIds,
            boolean includeIssueReferences)
    {
        Set<Build> builds = findBuilds(conn,
                table.id()
                        .in(buildIds),
                -1,
                -1);
        if (includeIssueReferences)
        {
            for (Build build : builds)
            {
                build.setIssueReferences(conn.select(new IssueReferenceProjection(issueReferenceProvider))
                        .from(Tables.ISSUE_TO_BUILD)
                        .where(Tables.ISSUE_TO_BUILD.buildId()
                                .eq(build.getId()))
                        .fetch());
            }
        }
        return builds;
    }

    Set<Build> findBuilds(
            DatabaseConnection conn,
            Predicate where,
            int page,
            int limit)
    {
        TreeSet<Build> builds = new TreeSet<>(Comparator.comparing(Build::getTimestamp)
                .thenComparing(Build::getNumber));

        try (CloseableIterable<Build> results = queryFactory.stream(conn, () -> {
            SQLQuery<Build> query = conn.select(new BuildProjection())
                    .from(table)
                    .join(Tables.JOB)
                    .on(table.jobId()
                            .eq(Tables.JOB.id()))
                    .join(Tables.SITE)
                    .on(Tables.JOB.siteId()
                            .eq(Tables.SITE.id()))
                    .leftJoin(Tables.TEST_RESULTS)
                    .on(table.id()
                            .eq(Tables.TEST_RESULTS.buildId()))
                    .orderBy(table.timestamp()
                            .desc())
                    .where(where);
            if (page >= 0 && limit > 0)
            {
                return query.offset((long) page * limit)
                        .limit(limit);
            }
            else
            {
                return query;
            }
        }))
        {
            results.foreach(builds::add);
        }

        for (Build build : builds)
        {
            build.setDeploymentEnvironments(conn.select(new DeploymentEnvironmentProjection())
                    .from(Tables.BUILD_TO_DEPLOYMENT_ENVIRONMENT)
                    .join(Tables.DEPLOYMENT_ENVIRONMENT)
                    .on(Tables.BUILD_TO_DEPLOYMENT_ENVIRONMENT.environmentId()
                            .eq(Tables.DEPLOYMENT_ENVIRONMENT.id()))
                    .where(Tables.BUILD_TO_DEPLOYMENT_ENVIRONMENT.buildId()
                            .eq(build.getId()))
                    .fetch());
        }

        return builds.descendingSet();
    }
}
