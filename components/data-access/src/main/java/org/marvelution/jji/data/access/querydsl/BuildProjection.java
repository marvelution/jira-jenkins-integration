/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.querydsl;

import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.model.*;

import static java.util.stream.Collectors.*;

/**
 * QueryDSL {@link Build} projection.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public class BuildProjection
		extends ProjectionBase<Build>
{

	public BuildProjection()
	{
		super(Build.class,
		      Stream.of(Tables.BUILD.getColumns(), Tables.TEST_RESULTS.getColumns(), Tables.JOB.getColumns(), Tables.SITE.getColumns())
				      .flatMap(List::stream)
				      .collect(toList()));
	}

	@Override
	Build createInstance(ProjectionContext context)
	{
		return createBuild(context);
	}

	static Build createBuild(ProjectionContext context)
	{
		TestResults testResults = null;
		int totalTests = context.get(Tables.TEST_RESULTS.total());
		if (totalTests > 0)
		{
			testResults = new TestResults().setTotal(totalTests)
					.setSkipped(context.get(Tables.TEST_RESULTS.skipped()))
					.setFailed(context.get(Tables.TEST_RESULTS.failed()));
		}

		return new Build().setId(context.get(Tables.BUILD.id()))
				.setJob(JobProjection.createJob(context))
				.setNumber(context.get(Tables.BUILD.buildNumber()))
				.setDeleted(context.get(Tables.BUILD.deleted()))
				.setDisplayName(context.get(Tables.BUILD.displayName()))
				.setDescription(context.get(Tables.BUILD.description()))
				.setCause(context.get(Tables.BUILD.cause()))
				.setDuration(context.get(Tables.BUILD.duration()))
				.setTimestamp(context.get(Tables.BUILD.timestamp()))
				.setResult(context.get(Tables.BUILD.result()))
				.setTestResults(testResults);
	}
}
