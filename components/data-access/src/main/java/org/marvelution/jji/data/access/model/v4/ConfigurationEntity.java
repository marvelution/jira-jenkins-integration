/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v4;

import net.java.ao.Preload;
import net.java.ao.RawEntity;
import net.java.ao.schema.*;

import static net.java.ao.schema.StringLength.UNLIMITED;

@Preload
@Table(ConfigurationEntity.TABLE_NAME)
public interface ConfigurationEntity
        extends RawEntity<Integer>
{

    String TABLE_NAME = "configuration";

    String ID = "ID";
    String KEY = "KEY";
    String SCOPE = "SCOPE";
    String OVERRIDABLE = "OVERRIDABLE";
    String INTERNAL = "INTERNAL";
    String VAL = "VAL";

    @NotNull
    @PrimaryKey(ID)
    int getId();

    void setId(int id);

    @NotNull
    @Indexed
    String getKey();

    void setKey(String key);

    @Indexed
    @StringLength(50)
    String getScope();

    void setScope(String scope);

    @Default("true")
    boolean isOverridable();

    void setOverridable(boolean overridable);

    @Default("false")
    boolean isInternal();

    void setInternal(boolean internal);

    @StringLength(UNLIMITED)
    String getVal();

    void setVal(String val);
}
