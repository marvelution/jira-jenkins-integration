/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.querydsl;

import java.util.ArrayList;

import org.marvelution.jji.data.access.model.Tables;
import org.marvelution.jji.model.ConfigurationSetting;

public class ConfigurationSettingProjection
        extends ProjectionBase<ConfigurationSetting>
{

    public ConfigurationSettingProjection()
    {
        super(ConfigurationSetting.class, new ArrayList<>(Tables.CONFIGURATION_TABLE.getColumns()));
    }

    @Override
    ConfigurationSetting createInstance(ProjectionContext context)
    {
        String scope = context.get(Tables.CONFIGURATION_TABLE.scope());
        String value = context.get(Tables.CONFIGURATION_TABLE.val());
        return new ConfigurationSetting().setKey(context.get(Tables.CONFIGURATION_TABLE.key()))
                .setScope(scope != null ? scope : "")
                .setValue(value != null ? value : "")
                .setOverridable(context.get(Tables.CONFIGURATION_TABLE.overridable()))
                .setInternal(context.get(Tables.CONFIGURATION_TABLE.internal()));
    }
}
