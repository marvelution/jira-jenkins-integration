/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.querydsl;

import java.util.*;
import java.util.stream.*;
import javax.annotation.*;

import com.querydsl.core.support.*;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.core.util.*;

abstract class ProjectionBase<T>
		extends FactoryExpressionBase<T>
{

	private final List<Expression<?>> paths;

	ProjectionBase(
			Class<? extends T> type,
			List<Expression<?>> paths)
	{
		super(type);
		this.paths = paths;
	}

	@Override
	public List<Expression<?>> getArgs()
	{
		return paths;
	}

	@Nullable
	@Override
	public T newInstance(Object... args)
	{
		return createInstance(new ProjectionContext(paths, args));
	}

	abstract T createInstance(ProjectionContext context);

	@Nullable
	@Override
	public <R, C> R accept(
			Visitor<R, C> v,
			@Nullable C context)
	{
		return v.visit(this, context);
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof ProjectionBase))
		{
			return false;
		}
		if (!super.equals(o))
		{
			return false;
		}
		ProjectionBase<?> that = (ProjectionBase<?>) o;
		return paths.equals(that.paths) && getType().equals(that.getType());
	}

	static class ProjectionContext
	{

		final List<Expression<?>> paths;
		final Object[] args;

		ProjectionContext(
				List<Expression<?>> paths,
				Object[] args)
		{
			this.paths = paths;
			this.args = args;
		}

		@SuppressWarnings("unchecked")
		<T> T get(Path<T> path)
		{
			int index = paths.indexOf(path);

			Object value = index > -1 ? args[index] : null;

			Class type = path.getType();
			if (Boolean.class.equals(type))
			{
				return (T) Optional.ofNullable(value).map(b -> {
					if (b instanceof Boolean)
					{
						return (Boolean) b;
					}
					else if (b instanceof String)
					{
						return Boolean.valueOf((String) b);
					}
					else if (b instanceof Number)
					{
						return ((Number) b).intValue() > 0;
					}
					else
					{
						return false;
					}
				}).orElse(false);
			}
			else if (Number.class.isAssignableFrom(type))
			{
				return (T) Optional.ofNullable(value).map(n -> MathUtils.cast((Number) n, type)).orElse(MathUtils.cast(0, type));
			}
			else if (Enum.class.isAssignableFrom(type) && value != null)
			{
				return (T) Enum.valueOf(type.asSubclass(Enum.class), (String) value);
			}
			else
			{
				return (T) value;
			}
		}
	}
}
