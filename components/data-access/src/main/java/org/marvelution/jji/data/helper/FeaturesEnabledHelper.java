/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.helper;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.api.AppState;
import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.IssueLinkService;
import org.marvelution.jji.web.conditions.FeaturesEnabledCondition;
import org.marvelution.jji.data.services.api.model.PermissionKeys;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static org.marvelution.jji.api.features.FeatureFlags.DYNAMIC_UI_ELEMENTS;

@Named
public class FeaturesEnabledHelper
        implements FeaturesEnabledCondition.Helper
{

    public static final ProjectPermissionKey VIEW_JENKINS = new ProjectPermissionKey(PermissionKeys.VIEW_JENKINS);
    private final AppState appState;
    private final Features features;
    private final IssueLinkService issueLinkService;
    private final ConfigurationService configurationService;
    private final JiraAuthenticationContext authenticationContext;
    private final PermissionManager permissionManager;

    @Inject
    public FeaturesEnabledHelper(
            AppState appState,
            Features features,
            IssueLinkService issueLinkService,
            ConfigurationService configurationService,
            @ComponentImport
            JiraAuthenticationContext authenticationContext,
            @ComponentImport
            PermissionManager permissionManager)
    {
        this.appState = appState;
        this.features = features;
        this.issueLinkService = issueLinkService;
        this.configurationService = configurationService;
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
    }

    @Override
    public boolean areFeaturesEnabledForIssue(Issue issue)
    {
        return shouldDisplay(issue.getProjectObject()) &&
               (!features.isFeatureEnabled(DYNAMIC_UI_ELEMENTS) || issueLinkService.getBuildCountForIssue(issue.getKey()) > 0);
    }

    @Override
    public boolean areFeaturesEnabledForProject(Project project)
    {
        return shouldDisplay(project) && (!features.isFeatureEnabled(DYNAMIC_UI_ELEMENTS) || issueLinkService.getBuildCountForProject(
                project.getKey()) > 0);
    }

    private boolean shouldDisplay(Project project)
    {
        ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
        return appState.isEnabled() && (permissionManager.hasPermission(VIEW_DEV_TOOLS, project, loggedInUser) ||
                                        permissionManager.hasPermission(VIEW_JENKINS, project, loggedInUser)) &&
               configurationService.isProjectEnabled(project.getKey());
    }
}
