/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import org.marvelution.jji.data.access.model.Entity;
import org.marvelution.jji.model.*;

import net.java.ao.*;
import net.java.ao.schema.*;

/**
 * @author Mark Rekveld
 * @since 4.1.0
 */
@Preload
@Table(DeploymentEnvironmentEntity.TABLE_NAME)
public interface DeploymentEnvironmentEntity
		extends Entity
{

	String TABLE_NAME = "DEPLOY_ENVS";

	String NAME = "NAME";
	String TYPE = "TYPE";

	@NotNull
	@StringLength(255)
	String getName();

	void setName(String name);

	@NotNull
	DeploymentEnvironmentType getType();

	void setType(DeploymentEnvironmentType type);

	@ManyToMany(value = BuildToDeploymentEnvironmentEntity.class,
	            reverse = "getEnvironment",
	            through = "getBuild")
	BuildEntity[] getBuilds();
}
