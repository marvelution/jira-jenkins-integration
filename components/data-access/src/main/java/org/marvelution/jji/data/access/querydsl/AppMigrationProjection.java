/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.querydsl;

import java.time.*;
import java.util.*;

import org.marvelution.jji.data.access.model.*;

public class AppMigrationProjection
		extends ProjectionBase<ServerAppMigration>
{

	public AppMigrationProjection()
	{
		super(ServerAppMigration.class, new ArrayList<>(Tables.APP_MIGRATION_TABLE.getColumns()));
	}

	@Override
	ServerAppMigration createInstance(ProjectionContext context)
	{
		LocalDateTime createdAt = Instant.ofEpochMilli(context.get(Tables.APP_MIGRATION_TABLE.createTimestamp()))
				.atZone(ZoneId.systemDefault())
				.toLocalDateTime();
		return new ServerAppMigration(context.get(Tables.APP_MIGRATION_TABLE.id()), context.get(Tables.APP_MIGRATION_TABLE.name()),
		                              createdAt, context.get(Tables.APP_MIGRATION_TABLE.type()),
		                              context.get(Tables.APP_MIGRATION_TABLE.progressStatus()),
		                              context.get(Tables.APP_MIGRATION_TABLE.eventJson()),
		                              context.get(Tables.APP_MIGRATION_TABLE.feedbackJson()));
	}
}
