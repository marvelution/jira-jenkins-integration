/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.data.services.api.PermissionService;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;

@Named
public class ServerPermissionService
        extends PermissionService<ApplicationUser>
{

    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;
    private final ProjectManager projectManager;
    private final JiraAuthenticationContext authenticationContext;

    @Inject
    public ServerPermissionService(
            Features features,
            GlobalPermissionManager globalPermissionManager,
            PermissionManager permissionManager,
            ProjectManager projectManager,
            JiraAuthenticationContext authenticationContext)
    {
        super(features);
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
        this.projectManager = projectManager;
        this.authenticationContext = authenticationContext;
    }

    @Override
    protected ApplicationUser getCurrentUser()
    {
        return authenticationContext.getLoggedInUser();
    }

    @Override
    public boolean hasPermission(
            ApplicationUser user,
            String permissionKey)
    {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.of(permissionKey), user);
    }

    @Override
    public boolean hasPermission(
            ApplicationUser user,
            String projectKey,
            String permissionKey)
    {
        if (projectKey == null)
        {
            return !getPermittedProjects(user, permissionKey).isEmpty();
        }
        else
        {
            Project project = projectManager.getProjectObjByKey(projectKey);
            return project != null && permissionManager.hasPermission(new ProjectPermissionKey(permissionKey), project, user);
        }
    }

    @Override
    public Set<String> getPermittedProjects(
            ApplicationUser user,
            String permission)
    {
        return permissionManager.getProjects(new ProjectPermissionKey(permission), user)
                .stream()
                .map(Project::getKey)
                .collect(Collectors.toSet());
    }
}
