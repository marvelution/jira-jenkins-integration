/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import com.atlassian.pocketknife.spi.querydsl.*;
import com.querydsl.core.types.dsl.*;

import static org.marvelution.jji.data.access.model.Entity.*;
import static org.marvelution.jji.data.access.model.v2.IssueToBuildEntity.*;

/**
 * Issue Link QueryDSL {@link Table}.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public class IssueToBuildTable
		extends EnhancedRelationalPathBase<IssueToBuildTable>
{

	private final StringPath id = createString(ID);
	private final StringPath buildId = createString(BUILD_ID);
	private final StringPath jobId = createString(JOB_ID);
	private final NumberPath<Long> buildTimestamp = createLong(BUILD_TIMESTAMP);
	private final StringPath projectKey = createString(PROJECT_KEY);
	private final StringPath issueKey = createString(ISSUE_KEY);

	public IssueToBuildTable(String namespace)
	{
		super(IssueToBuildTable.class, namespace + TABLE_NAME, "issueToBuild");
		createPrimaryKey(id);
	}

	public StringPath id()
	{
		return id;
	}

	public StringPath buildId()
	{
		return buildId;
	}

	public StringPath jobId()
	{
		return jobId;
	}

	public NumberPath<Long> buildTimestamp()
	{
		return buildTimestamp;
	}

	public StringPath projectKey()
	{
		return projectKey;
	}

	public StringPath issueKey()
	{
		return issueKey;
	}
}
