/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import org.marvelution.jji.data.access.model.Entity;

import net.java.ao.*;
import net.java.ao.schema.*;

import static net.java.ao.schema.StringLength.*;

/**
 * @author Mark Rekveld
 * @since 4.5.0
 */
@Preload
@Table(LinkStatisticsEntity.TABLE_NAME)
public interface LinkStatisticsEntity
		extends Entity
{

	String TABLE_NAME = "LINK_STATISTICS";

	String ISSUE_KEY = "ISSUE_KEY";
	String JSON = "JSON";
	String QUEUED = "QUEUED";

	@NotNull
	@Indexed
	String getIssueKey();

	void setIssueKey(String issueKey);

	@NotNull
	@StringLength(UNLIMITED)
	String getJson();

	void setJson(String json);

	boolean isQueued();

	void setQueued(boolean queued);
}
