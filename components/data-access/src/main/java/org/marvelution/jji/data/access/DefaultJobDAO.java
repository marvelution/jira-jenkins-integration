/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.time.*;
import java.util.*;
import java.util.stream.*;
import javax.annotation.*;
import javax.inject.*;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.data.access.querydsl.*;
import org.marvelution.jji.model.*;

import com.atlassian.pocketknife.api.querydsl.*;
import com.atlassian.pocketknife.api.querydsl.stream.*;
import com.atlassian.pocketknife.api.querydsl.util.*;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.*;

import static org.apache.commons.lang3.StringUtils.*;

@Named
public class DefaultJobDAO
		extends AbstractPocketKnifeDAO<Job, JobTable>
		implements JobDAO
{

	private final IssueReferenceProvider issueReferenceProvider;

	@Inject
	public DefaultJobDAO(
			DatabaseAccessor databaseAccessor,
			StreamingQueryFactory queryFactory,
			IssueReferenceProvider issueReferenceProvider)
	{
		super(databaseAccessor, queryFactory, Tables.JOB);
		this.issueReferenceProvider = issueReferenceProvider;
	}

	@Override
	public long countBySite(String siteId)
	{
		return runInTransaction(conn -> conn.select(table.id()).from(table).where(table.siteId().eq(siteId)).fetchCount());
	}

	@Override
	public boolean hasDeletedJobs(String siteId)
	{
		return runInTransaction(
				conn -> conn.select(table.id()).from(table).where(table.siteId().eq(siteId).and(table.deleted().isTrue())).fetchCount()) >
				0;
	}

	@Override
	public List<Job> getDeletedJobs(String siteId)
	{
		return runInTransaction(conn -> findJobs(conn, table.siteId().eq(siteId).and(table.deleted().isTrue()), Collectors.toList()));
	}

	@Override
	public List<Job> getBySiteIdAtLevel(
			String siteId,
			String level,
			boolean includeDeleted)
	{
		return runInTransaction(conn -> {
			BooleanExpression where = table.siteId().eq(siteId);

			if (isNotBlank(level))
			{
				where = where.and(table.parentName().eq(level));
			}
			else
			{
				where = where.and(table.parentName().isNull());
			}

			if (!includeDeleted)
			{
				where = where.and(table.deleted().isFalse());
			}

			return findJobs(conn, where, Collectors.toList());
		});
	}

	@Override
	public Optional<Job> find(
			String siteId,
			String name,
			@Nullable String parent)
	{
		return runInTransaction(conn -> {
			BooleanExpression where = table.siteId().eq(siteId).and(table.name().eq(name));

			if (isBlank(parent))
			{
				where = where.and(table.parentName().isNull());
			}
			else
			{
				where = where.and(table.parentName().eq(parent));
			}


			return findJobs(conn, where, Collectors.toList()).stream().findFirst();
		});
	}

	@Override
	public List<Job> find(String name)
	{
		return runInTransaction(conn -> findJobs(conn, table.name().eq(name).or(table.displayName().eq(name)), Collectors.toList()));
	}

	@Override
	public List<Job> search(String term)
	{
		return runInTransaction(conn -> {
			String name = "%" + term + "%";
			return findJobs(conn, table.name().likeIgnoreCase(name).or(table.displayName().likeIgnoreCase(name)), Collectors.toList());
		});
	}

	@Override
	public List<Job> search(
			String siteId,
			String term)
	{
		return runInTransaction(conn -> {
			String name = "%" + term + "%";
			BooleanExpression like = table.name().likeIgnoreCase(name).or(table.displayName().likeIgnoreCase(name));
			return findJobs(conn, table.siteId().eq(siteId).and(like), Collectors.toList());
		});
	}

	@Override
	public List<Job> findByHash(
			@Nullable String siteId,
			String hash)
	{
		return runInTransaction(conn -> {
			BooleanExpression where = table.hash().eq(hash);

			if (siteId != null)
			{
				where = where.and(table.siteId().eq(siteId));
			}

			return findJobs(conn, where, Collectors.toList());
		});
	}

	@Override
	public List<Job> findBySiteId(
			String siteId,
			int page,
			int size)
	{
		return runInTransaction(conn -> findJobs(conn, table.siteId().eq(siteId), size, page, Collectors.toList()));
	}

	@Override
	public void cleanupDeletedJobsWithoutBuilds()
	{
		databaseAccessor.runInTransaction(conn -> {
			List<String> ids = conn.select(table.id())
					.from(table)
					.where(table.deleted()
							       .isTrue()
							       .and(Expressions.asNumber(0L)
									            .eq(conn.select(Tables.BUILD.id().count())
											                .from(Tables.BUILD)
											                .where(Tables.BUILD.jobId().eq(table.id())))))
					.fetch();
			for (String id : ids)
			{
				DeletionHelper.deleteJob(conn, id);
			}
			return null;
		}, OnRollback.NOOP);
	}

	@Override
	public void cleanupOldDeletedJobs(LocalDateTime timestamp)
	{
		List<String> ids = databaseAccessor.runInTransaction(conn -> findOldDeletedJobIds(conn, timestamp), OnRollback.NOOP);
		logger.debug("Cleaning up {} deleted jobs.", ids.size());
		for (String id : ids)
		{
			try
			{
				databaseAccessor.runInTransaction(conn -> DeletionHelper.deleteJob(conn, id), OnRollback.NOOP);
			}
			catch (Throwable e)
			{
				logger.error("Failed to delete job {}", id, e);
			}
		}
	}

	@Override
	public List<String> findOldDeletedJobIds(LocalDateTime timestamp)
	{
		return databaseAccessor.runInTransaction(conn -> findOldDeletedJobIds(conn, timestamp), OnRollback.NOOP);
	}

	@Override
	public List<Job> getLatestActiveJobs(int limit)
	{
		// TODO Implement
		return new ArrayList<>();
	}

	@Nullable
	@Override
	public Job get(String id)
	{
		return runInTransaction(conn -> findJobs(conn, table.id().eq(id), 1, 0, Collectors.toList()).stream().findFirst().orElse(null));
	}

	@Override
	public void delete(String id)
	{
		runInTransaction(conn -> DeletionHelper.deleteJob(conn, id));
	}

	private List<String> findOldDeletedJobIds(
			DatabaseConnection conn,
			LocalDateTime timestamp)
	{
		return conn.select(table.id())
				.from(table)
				.where(table.deleted()
						       .isTrue()
						       .and(table.id()
								            .in(conn.select(Tables.BUILD.jobId())
										                .from(Tables.BUILD)
										                .where(Tables.BUILD.jobId()
												                       .eq(table.id())
												                       .and(Tables.BUILD.buildNumber().eq(table.lastBuild()))
												                       .and(Tables.BUILD.timestamp()
														                            .lt(TimeUtils.toEpochMilli(timestamp)))))))
				.fetch();
	}

	Set<Job> getJobs(
			DatabaseConnection conn,
			Set<String> jobIds,
			boolean includeIssueReferences)
	{
		Set<Job> jobs = findJobs(conn, table.id().in(jobIds), Collectors.toSet());
		if (includeIssueReferences)
		{
			for (Job job : jobs)
			{
				job.setIssueReferences(conn.select(new IssueReferenceProjection(issueReferenceProvider))
						                       .from(Tables.ISSUE_TO_BUILD)
						                       .where(Tables.ISSUE_TO_BUILD.jobId().eq(job.getId()))
						                       .fetch());
			}
		}
		return jobs;
	}

	<A, R extends Collection<Job>, C1 extends Collector<Job, A, R>> R findJobs(
			DatabaseConnection conn,
			Predicate where,
			C1 collector)
	{
		return findJobs(conn, where, -1, 0, collector);
	}

	<A, R extends Collection<Job>, C1 extends Collector<Job, A, R>> R findJobs(
			DatabaseConnection conn,
			Predicate where,
			int limit,
			int page,
			C1 collector)
	{
		A jobs = collector.supplier().get();
		try (CloseableIterable<Job> result = queryFactory.stream(conn, () -> {
			SQLQuery<Job> query = conn.select(new JobProjection())
					.from(table)
					.join(Tables.SITE)
					.on(table.siteId().eq(Tables.SITE.id()))
					.where(where);
			if (limit > 0)
			{
				return query.limit(limit).offset((long) page * limit);
			}
			else
			{
				return query;
			}
		}))
		{
			result.foreach(job -> collector.accumulator().accept(jobs, job));
		}

		return collector.finisher().apply(jobs);
	}
}
