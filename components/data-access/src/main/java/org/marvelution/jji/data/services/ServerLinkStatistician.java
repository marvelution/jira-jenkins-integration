/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.data.access.api.BuildDAO;
import org.marvelution.jji.data.access.api.IssueToBuildDAO;
import org.marvelution.jji.data.access.api.LinkStatisticsDAO;
import org.marvelution.jji.data.access.api.IssueReferenceProvider;
import org.marvelution.jji.data.services.api.LinkStatistician;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static org.marvelution.jji.api.features.FeatureFlags.DISABLE_ISSUE_INDEXING;

@Named
@ExportAsService(LinkStatistician.class)
public class ServerLinkStatistician
        extends BaseLinkStatistician
{

    private final Features features;
    private final IssueManager issueManager;
    private final IssueIndexingService issueIndexingService;
    private Instant lastIndexWarningLogTimestamp;

    @Inject
    public ServerLinkStatistician(
            IssueToBuildDAO issueToBuildDAO,
            BuildDAO buildDAO,
            IssueReferenceProvider issueReferenceProvider,
            LinkStatisticsDAO linkStatisticsDAO,
            Features features,
            @ComponentImport
            IssueManager issueManager,
            @ComponentImport
            IssueIndexingService issueIndexingService)
    {
        super(issueToBuildDAO, buildDAO, linkStatisticsDAO, issueReferenceProvider);
        this.features = features;
        this.issueManager = issueManager;
        this.issueIndexingService = issueIndexingService;
    }

    @Override
    protected void triggerIssueReindexing(Set<String> issueKeys)
    {
        if (features.isFeatureEnabled(DISABLE_ISSUE_INDEXING))
        {
            if (lastIndexWarningLogTimestamp == null || lastIndexWarningLogTimestamp.plus(Duration.ofHours(12))
                    .isAfter(Instant.now()))
            {
                lastIndexWarningLogTimestamp = Instant.now();
                logger.warn("Issue indexing is disabled, disable feature {} to enable indexing again.", DISABLE_ISSUE_INDEXING);
            }
        }
        else
        {
            try
            {
                Set<Issue> issues = issueKeys.stream()
                        .map(issueManager::getIssueObject)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toSet());
                if (!issues.isEmpty())
                {
                    logger.debug("Reindexing {} issues to update the Jenkins property", issues.size());
                    issueIndexingService.reIndexIssueObjects(issues, IssueIndexingParams.INDEX_ISSUE_ONLY, true);
                }
            }
            catch (IndexException e)
            {
                logger.warn("Unable to update index of issues {}", e.getMessage(), e);
            }
        }
    }
}
