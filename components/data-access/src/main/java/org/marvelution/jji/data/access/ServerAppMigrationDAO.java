/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.time.*;
import java.util.*;
import java.util.stream.*;
import javax.inject.Named;
import javax.inject.*;

import org.marvelution.jji.api.migration.*;
import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.data.access.querydsl.*;
import org.marvelution.jji.data.services.api.*;

import com.atlassian.jira.config.util.*;
import com.atlassian.jira.util.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import com.atlassian.pocketknife.api.querydsl.*;
import com.atlassian.pocketknife.api.querydsl.util.*;

import static org.apache.commons.lang3.StringUtils.stripEnd;

@Named
public class ServerAppMigrationDAO
		implements AppMigrationDAO<ServerAppMigration>
{

	private final DatabaseAccessor databaseAccessor;
	private final ConfigurationService configurationService;
	private final JiraHome jiraHome;

	@Inject
	public ServerAppMigrationDAO(
			DatabaseAccessor databaseAccessor,
			ConfigurationService configurationService,
			@ComponentImport JiraHome jiraHome)
	{
		this.databaseAccessor = databaseAccessor;
		this.configurationService = configurationService;
		this.jiraHome = jiraHome;
	}

	@Override
	public Optional<ServerAppMigration> findById(String id)
	{
		return Optional.ofNullable(databaseAccessor.runInTransaction(conn -> conn.select(new AppMigrationProjection())
				.from(Tables.APP_MIGRATION_TABLE)
				.where(Tables.APP_MIGRATION_TABLE.id().eq(id))
				.fetchOne(), OnRollback.NOOP));
	}

	@Override
	public List<ServerAppMigration> findByType(AppMigrationType... type)
	{
		String[] types = Stream.of(type).map(Enum::name).toArray(String[]::new);
		return databaseAccessor.runInTransaction(conn -> conn.select(new AppMigrationProjection())
				.from(Tables.APP_MIGRATION_TABLE)
				.where(Tables.APP_MIGRATION_TABLE.type().stringValue().in(types))
				.orderBy(Tables.APP_MIGRATION_TABLE.createTimestamp().desc())
				.fetch(), OnRollback.NOOP);
	}

	@Override
	public ServerAppMigration store(ServerAppMigration migration)
	{
		return databaseAccessor.runInTransaction(conn -> {
			conn.insert(Tables.APP_MIGRATION_TABLE)
					.set(Tables.APP_MIGRATION_TABLE.id(), migration.getId())
					.set(Tables.APP_MIGRATION_TABLE.name(), migration.getName())
					.set(Tables.APP_MIGRATION_TABLE.createTimestamp(),
					     migration.getCreatedAt().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli())
					.set((com.querydsl.core.types.Path) Tables.APP_MIGRATION_TABLE.type(), migration.getType().name())
					.set((com.querydsl.core.types.Path) Tables.APP_MIGRATION_TABLE.progressStatus(), migration.getProgress().name())
					.set(Tables.APP_MIGRATION_TABLE.eventJson(), migration.getEventJson())
					.set(Tables.APP_MIGRATION_TABLE.feedbackJson(), migration.getFeedbackJson())
					.execute();
			return migration;
		}, OnRollback.NOOP);
	}

	@Override
	public ServerAppMigration store(
			ServerAppMigration migration,
			InputStream file)
			throws IOException
	{
		try (OutputStream outputStream = createMigrationData(migration))
		{
			IOUtil.copy(file, outputStream);
		}
		return store(migration);
	}

	@Override
	public void updateFeedback(
			String id,
			Feedback feedback)
	{
		databaseAccessor.runInTransaction(conn -> conn.update(Tables.APP_MIGRATION_TABLE)
				.set(Tables.APP_MIGRATION_TABLE.feedbackJson(), ServerAppMigration.JSONB.toJson(feedback))
				.where(Tables.APP_MIGRATION_TABLE.id().eq(id))
				.execute(), OnRollback.NOOP);
	}

	@Override
	public void updateProgressStatus(
			String id,
			ProgressStatus progressStatus)
	{
		databaseAccessor.runInTransaction(conn -> conn.update(Tables.APP_MIGRATION_TABLE)
				.set((com.querydsl.core.types.Path) Tables.APP_MIGRATION_TABLE.progressStatus(), progressStatus.name())
				.where(Tables.APP_MIGRATION_TABLE.id().eq(id))
				.execute(), OnRollback.NOOP);
	}

	@Override
	public InputStream getMigrationData(ServerAppMigration migration)
			throws IOException
	{
		return Files.newInputStream(getDirectory().resolve(migration.getId()), StandardOpenOption.READ);
	}

	@Override
	public OutputStream createMigrationData(ServerAppMigration migration)
			throws IOException
	{
		return Files.newOutputStream(getDirectory().resolve(migration.getId()), StandardOpenOption.CREATE,
		                             StandardOpenOption.TRUNCATE_EXISTING);
	}

	private Path getDirectory()
			throws IOException
	{
		Path directory = jiraHome.getDataDirectory().toPath().resolve("./jenkins-migrations/");
		Files.createDirectories(directory);
		return directory;
	}

	@Override
	public URI getDownloadLink(ServerAppMigration migration)
	{
		String baseAPIUrl = stripEnd(configurationService.getBaseAPIUrl().toASCIIString(), "/") + "/";
		return URI.create(baseAPIUrl).resolve("./migration/" + migration.getId());
	}

	@Override
	public void cancelMigration(String id)
	{
		// This is not support in Server or DC, but no need to fail if called
	}

	@Override
	public boolean isMigrationCancelled(String id)
	{
		// This is not support in Server or DC, but no need to fail if called
		return false;
	}
}
