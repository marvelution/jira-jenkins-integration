/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import javax.inject.*;
import java.util.*;
import java.util.function.*;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.data.access.querydsl.*;
import org.marvelution.jji.model.*;

import com.atlassian.pocketknife.api.querydsl.*;
import com.atlassian.pocketknife.api.querydsl.util.*;
import com.querydsl.core.*;
import jakarta.json.bind.*;
import org.slf4j.*;

import static org.marvelution.jji.model.HasId.*;

@Named
public class DefaultLinkStatisticsDAO
        implements LinkStatisticsDAO, AutoCloseable
{

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultLinkStatisticsDAO.class);
    private final Jsonb jsonb = JsonbBuilder.create();
    private final DatabaseAccessor databaseAccessor;

    @Inject
    public DefaultLinkStatisticsDAO(DatabaseAccessor databaseAccessor)
    {
        this.databaseAccessor = databaseAccessor;
    }

    @Override
    public LinkStatistics getLinkStatistics(String issueKey)
    {
        return getLinkStatisticianState(issueKey).current();
    }

    @Override
    public LinkStatisticianState getLinkStatisticianState(String issueKey)
    {
        return Optional.ofNullable(databaseAccessor.runInTransaction(conn -> conn.select(new LinkStatisticianStateProjection(jsonb))
                        .from(Tables.LINK_STATISTICS_TABLE)
                        .where(Tables.LINK_STATISTICS_TABLE.issueKey()
                                .eq(issueKey))
                        .orderBy(Tables.LINK_STATISTICS_TABLE.id()
                                .asc())
                        .limit(1)
                        .fetch(), OnRollback.NOOP))
                .filter(list -> !list.isEmpty())
                .map(list -> list.get(0))
                .orElse(new LinkStatisticianStateProjection.State(issueKey));
    }

    @Override
    public int getLinkStatisticianStates(
            int page,
            int limit,
            BiConsumer<String, String> consumer)
    {
        List<Tuple> list = databaseAccessor.runInTransaction(conn -> conn.select(Tables.LINK_STATISTICS_TABLE.issueKey(),
                        Tables.LINK_STATISTICS_TABLE.json())
                .from(Tables.LINK_STATISTICS_TABLE)
                .orderBy(Tables.LINK_STATISTICS_TABLE.issueKey()
                        .asc())
                .offset((long) page * limit)
                .limit(limit)
                .fetch(), OnRollback.NOOP);
        list.forEach(t -> consumer.accept(t.get(Tables.LINK_STATISTICS_TABLE.issueKey()), t.get(Tables.LINK_STATISTICS_TABLE.json())));
        return list.size();
    }

    @Override
    public void storeLinkStatistics(
            String issueKey,
            boolean queued,
            LinkStatistics statistics)
    {
        String statisticsJson = jsonb.toJson(statistics);
        storeLinkStatistics(issueKey, queued, statisticsJson);
    }

    @Override
    public void importLinkStatistics(
            String issueKey,
            String statisticsJson)
    {
        storeLinkStatistics(issueKey, false, statisticsJson);
    }

    @Override
    public void clearLinkStatistics(String... issueKeys)
    {
        databaseAccessor.runInTransaction(conn -> conn.delete(Tables.LINK_STATISTICS_TABLE)
                .where(Tables.LINK_STATISTICS_TABLE.issueKey()
                        .in(issueKeys))
                .execute(), OnRollback.NOOP);
    }

    @Override
    public void clearLinkStatisticsForProject(String projectKey)
    {
        databaseAccessor.runInTransaction(conn -> conn.delete(Tables.LINK_STATISTICS_TABLE)
                .where(Tables.LINK_STATISTICS_TABLE.issueKey()
                        .like(projectKey + "-%"))
                .execute(), OnRollback.NOOP);
    }

    private void storeLinkStatistics(
            String issueKey,
            boolean queued,
            String statisticsJson)
    {
        databaseAccessor.runInTransaction(conn -> {
            List<String> ids = conn.select(Tables.LINK_STATISTICS_TABLE.id())
                    .from(Tables.LINK_STATISTICS_TABLE)
                    .where(Tables.LINK_STATISTICS_TABLE.issueKey()
                            .eq(issueKey))
                    .orderBy(Tables.LINK_STATISTICS_TABLE.id()
                            .asc())
                    .fetch();
            if (ids.isEmpty())
            {
                conn.insert(Tables.LINK_STATISTICS_TABLE)
                        .set(Tables.LINK_STATISTICS_TABLE.id(), newId())
                        .set(Tables.LINK_STATISTICS_TABLE.queued(), queued)
                        .set(Tables.LINK_STATISTICS_TABLE.issueKey(), issueKey)
                        .set(Tables.LINK_STATISTICS_TABLE.json(), statisticsJson)
                        .execute();
                LOGGER.debug("Inserted link statistics for {}: {}", issueKey, statisticsJson);
            }
            else
            {
                conn.update(Tables.LINK_STATISTICS_TABLE)
                        .set(Tables.LINK_STATISTICS_TABLE.json(), statisticsJson)
                        .set(Tables.LINK_STATISTICS_TABLE.queued(), queued)
                        .where(Tables.LINK_STATISTICS_TABLE.issueKey()
                                .eq(issueKey)
                                .and(Tables.LINK_STATISTICS_TABLE.id()
                                        .eq(ids.get(0))))
                        .execute();
                LOGGER.debug("Updated link statistics for {}: {}", issueKey, statisticsJson);
                if (ids.size() > 1)
                {
                    long count = conn.delete(Tables.LINK_STATISTICS_TABLE)
                            .where(Tables.LINK_STATISTICS_TABLE.issueKey()
                                    .eq(issueKey)
                                    .and(Tables.LINK_STATISTICS_TABLE.id()
                                            .ne(ids.get(0))))
                            .execute();
                    LOGGER.debug("Deleted {} link statistics duplicates for {}", count, issueKey);
                }
            }
            return null;
        }, OnRollback.NOOP);
    }

    @Override
    public void close()
            throws Exception
    {
        jsonb.close();
    }
}
