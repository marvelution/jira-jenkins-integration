/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import javax.inject.*;

import org.marvelution.jji.api.events.EventPublisher;
import org.marvelution.jji.api.text.*;
import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.events.*;

import com.atlassian.event.api.*;
import com.atlassian.jira.event.*;
import com.atlassian.plugin.spring.scanner.annotation.export.*;

/**
 * Default {@link SiteService} implementation.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
@ExportAsService(SiteService.class)
@EventComponent
public class DefaultSiteService
        extends BaseSiteService
{

    @Inject
    public DefaultSiteService(
            SiteDAO siteDAO,
            SiteClient siteClient,
            JobService jobService,
            SiteValidator siteValidator,
            EventPublisher eventPublisher,
            TextResolver textResolver,
            TunnelManager tunnelManager)
    {
        super(siteDAO, siteClient, jobService, siteValidator, eventPublisher, textResolver, tunnelManager);
    }

    @EventListener
    public void onProjectDeleted(ProjectDeletedEvent event)
    {
        siteDAO.deleteAll(event.getKey());
    }
}
