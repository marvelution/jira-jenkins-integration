/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.net.*;
import java.util.*;
import javax.annotation.*;
import javax.inject.*;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.data.access.querydsl.*;
import org.marvelution.jji.model.*;

import com.atlassian.pocketknife.api.querydsl.*;
import com.atlassian.pocketknife.api.querydsl.stream.*;
import com.querydsl.core.types.*;

/**
 * Default {@link SiteDAO} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultSiteDAO
		extends AbstractPocketKnifeDAO<Site, SiteTable>
		implements SiteDAO
{

	@Inject
	public DefaultSiteDAO(
			DatabaseAccessor databaseAccessor,
			StreamingQueryFactory queryFactory)
	{
		super(databaseAccessor, queryFactory, Tables.SITE);
	}

	@Nullable
	@Override
	public Site get(String id)
	{
		return runInTransaction(conn -> conn.select(new SiteProjection()).from(table).where(table.id().eq(id)).fetchOne());
	}

	@Override
	public List<Site> getAll()
	{
		return runInTransaction(conn -> conn.select(new SiteProjection()).from(table).orderBy(table.name().asc()).fetch());
	}

	@Override
	public Optional<Site> findByName(String name)
	{
		return findOne(table.name().eq(name));
	}

	@Override
	public Optional<Site> findByRpcUrl(URI rpcUri)
	{
		return findOne(table.rpcUrl().eq(rpcUri.toASCIIString()));
	}

	@Override
	public Optional<Site> findByDisplayUrl(URI displayUri)
	{
		return findOne(table.displayUrl().eq(displayUri.toASCIIString()));
	}

	private Optional<Site> findOne(Predicate where)
	{
		return Optional.ofNullable(runInTransaction(conn -> conn.select(new SiteProjection()).from(table).where(where).fetchOne()));
	}

	@Override
	public void delete(String id)
	{
		runInTransaction(conn -> DeletionHelper.deleteSite(conn, id));
	}

	@Override
	public void deleteAll()
	{
		runInTransaction(conn -> {
			for (String id : conn.select(table.id()).from(table).fetch())
			{
				DeletionHelper.deleteSite(conn, id);
			}
			return null;
		});
	}

	@Override
	public void deleteAll(String scope)
	{
		runInTransaction(conn -> {
			for (String id : conn.select(table.id()).from(table).where(table.scope().eq(scope)).fetch())
			{
				DeletionHelper.deleteSite(conn, id);
			}
			return null;
		});
	}

	@Override
	public void enableSite(
			String siteId,
			boolean enabled)
	{
		runInTransaction(conn -> {
			conn.update(table).set(table.enabled(), enabled).where(table.id().eq(siteId)).execute();
			return null;
		});
	}

	@Override
	public void enableNewJobs(
			String siteId,
			boolean enabled)
	{
		runInTransaction(conn -> {
			conn.update(table).set(table.autoLinkNewJobs(), enabled).where(table.id().eq(siteId)).execute();
			return null;
		});
	}
}
