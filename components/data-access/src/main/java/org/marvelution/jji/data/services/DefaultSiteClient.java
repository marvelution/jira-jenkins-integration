/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import javax.inject.*;
import javax.ws.rs.core.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.function.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.utils.*;

import com.atlassian.plugin.spring.scanner.annotation.export.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.*;
import jakarta.json.*;

import static java.util.Arrays.*;
import static java.util.Optional.*;

@Named
@ExportAsService(SiteClient.class)
public class DefaultSiteClient
        extends BaseSiteClient
{

    private static final int CALL_TIMEOUT = 50000;
    private final NonMarshallingRequestFactory<?> requestFactory;

    @Inject
    public DefaultSiteClient(
            ConfigurationService configurationService,
            TunnelManager tunnelManager,
            @ComponentImport
            NonMarshallingRequestFactory<?> requestFactory)
    {
        super(configurationService, tunnelManager);
        this.requestFactory = requestFactory;
    }

    @Override
    protected RestResponse doGet(SiteUrl siteUrl)
    {
        return executeRequest(siteUrl, Request.MethodType.GET, request -> {
        });
    }

    @Override
    protected RestResponse doPost(
            SiteUrl siteUrl,
            JsonObject json)
    {
        return executeRequest(siteUrl, Request.MethodType.POST, request -> {
            handleCrumbIfRequired(siteUrl, request::addHeader);
            request.setRequestBody(json.toString(), MediaType.APPLICATION_JSON);
        });
    }

    private RestResponse executeRequest(
            SiteUrl siteUrl,
            Request.MethodType methodType,
            Consumer<Request<?, ?>> requestCustomizer)
    {
        URI uri = siteUrl.url();
        Site site = siteUrl.site();

        Request<?, ?> request = requestFactory.createRequest(methodType, uri.toASCIIString());
        setHeaders(site, methodType.name(), uri, request::setHeader);
        if (siteUrl.auth() instanceof SiteAuthentication.BasicSiteAuthentication)
        {
            SiteAuthentication.BasicSiteAuthentication auth = (SiteAuthentication.BasicSiteAuthentication) siteUrl.auth();
            request.addBasicAuthentication(uri.getHost(), auth.getUser(), auth.getToken());
        }
        request.setConnectionTimeout(CALL_TIMEOUT);
        request.setSoTimeout(CALL_TIMEOUT);

        requestCustomizer.accept(request);

        RestResponse response;
        try
        {
            response = request.executeAndReturn(RestResponse::new);
        }
        catch (ResponseConnectTimeoutException | ResponseReadTimeoutException e)
        {
            logger.warn("Connection timeout occurred on {}", uri, e);
            response = new RestResponse("Unable to connect to Jenkins. Connection timed out.");
        }
        catch (ResponseException e)
        {
            logger.error("Failed to connect to {}; {}", uri, e.getMessage(), e);
            response = new RestResponse("Failed to execute request: " + e.getMessage());
        }
        if (response.hasAuthErrors())
        {
            logger.error("Authentication failure on {}", uri);
        }
        if (!response.isSuccessful() && logger.isDebugEnabled())
        {
            String responseBody = response.response.map(r -> {
                        try
                        {
                            return r.getResponseBodyAsString();
                        }
                        catch (ResponseException e)
                        {
                            return "Failed to read response body: " + e.getMessage();
                        }
                    })
                    .orElse("Empty response");
            logger.debug("Request {} was not successful [{}], and returned body: \n{}", uri, response.getStatusCode(), responseBody);
        }
        return response;
    }

    public static class RestResponse
            extends SiteResponse
    {

        private final Optional<Response> response;

        RestResponse(Response response)
        {
            this.response = of(response);
        }

        RestResponse(String... errors)
        {
            response = empty();
            this.errors.addAll(asList(errors));
        }

        @Override
        public boolean isSuccessful()
        {
            return response.map(Response::isSuccessful)
                           .orElse(false) && !hasAuthErrors() && !hasErrors();
        }

        @Override
        public int getStatusCode()
        {
            return response.map(Response::getStatusCode)
                    .orElse(-1);
        }

        @Override
        public String getStatusMessage()
        {
            return response.map(Response::getStatusText)
                    .orElse(null);
        }

        @Override
        public Optional<String> getHeader(String header)
        {
            return response.map(r -> r.getHeader(header))
                    .map(value -> {
                        if (value.contains(", "))
                        {
                            return value.split(", ")[0];
                        }
                        else
                        {
                            return value;
                        }
                    });
        }

        @Override
        protected Reader getResponseBodyReader()
                throws IOException
        {
            try
            {
                return new StringReader(response.orElseThrow(() -> new IllegalStateException("No response present."))
                        .getResponseBodyAsString());
            }
            catch (ResponseException e)
            {
                throw new IOException(e);
            }
        }
    }

}
