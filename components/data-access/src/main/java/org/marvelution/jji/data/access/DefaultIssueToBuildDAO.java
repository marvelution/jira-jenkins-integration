/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import javax.inject.*;
import java.util.*;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;

import com.atlassian.pocketknife.api.querydsl.*;
import com.atlassian.pocketknife.api.querydsl.stream.*;
import com.atlassian.pocketknife.api.querydsl.util.*;
import com.querydsl.core.*;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.dml.*;

import static java.util.Optional.*;
import static org.marvelution.jji.model.HasId.*;
import static org.marvelution.jji.utils.KeyExtractor.*;

@Named
public class DefaultIssueToBuildDAO
        implements IssueToBuildDAO
{

    private final DatabaseAccessor databaseAccessor;
    private final StreamingQueryFactory queryFactory;
    private final DefaultBuildDAO buildDAO;
    private final DefaultJobDAO jobDAO;
    private final IssueToBuildTable table = Tables.ISSUE_TO_BUILD;

    @Inject
    public DefaultIssueToBuildDAO(
            DatabaseAccessor databaseAccessor,
            StreamingQueryFactory queryFactory,
            DefaultBuildDAO buildDAO,
            DefaultJobDAO jobDAO)
    {
        this.databaseAccessor = databaseAccessor;
        this.queryFactory = queryFactory;
        this.buildDAO = buildDAO;
        this.jobDAO = jobDAO;
    }

    @Override
    public Set<String> getAllIssueKeysWithLinks(
            int limit,
            int page)
    {
        return databaseAccessor.runInTransaction(conn -> {
            Set<String> issueKeys = new HashSet<>();
            try (CloseableIterable<String> result = queryFactory.stream(conn,
                    () -> conn.select(table.issueKey())
                            .from(table)
                            .groupBy(table.issueKey())
                            .orderBy(table.issueKey()
                                    .asc())
                            .limit(limit)
                            .offset((long) page * limit)))
            {
                result.foreach(issueKeys::add);
            }
            return issueKeys;
        }, OnRollback.NOOP);
    }

    @Override
    public Set<String> getProjectKeysWithLinks()
    {
        return databaseAccessor.runInTransaction(conn -> {
            Set<String> projectKeys = new HashSet<>();
            try (CloseableIterable<String> result = queryFactory.stream(conn,
                    () -> conn.select(table.projectKey())
                            .from(table)
                            .groupBy(table.projectKey())
                            .orderBy(table.projectKey()
                                    .asc())))
            {
                result.foreach(projectKeys::add);
            }
            return projectKeys;
        }, OnRollback.NOOP);
    }

    @Override
    public int getLinkCountForIssue(String issueKey)
    {
        return (int) count(table.issueKey()
                .eq(issueKey));
    }

    @Override
    public int getLinkCountForProject(String projectKey)
    {
        return (int) count(table.projectKey()
                .eq(projectKey));
    }

    private long count(BooleanExpression where)
    {
        return databaseAccessor.runInTransaction(conn -> conn.select(table.id())
                .from(table)
                .where(where)
                .fetchCount(), OnRollback.NOOP);
    }

    @Override
    public Set<Build> getBuildsForIssue(
            String issueKey,
            int limit,
            boolean includeIssueReferences)
    {
        return databaseAccessor.runInTransaction(conn -> {

            Set<String> buildIds = new HashSet<>();
            try (CloseableIterable<String> result = queryFactory.stream(conn,
                    () -> conn.select(table.buildId())
                            .from(table)
                            .where(table.issueKey()
                                    .eq(issueKey))
                            .orderBy(table.buildTimestamp()
                                    .desc())
                            .limit(limit)))
            {
                result.foreach(buildIds::add);
            }

            return buildDAO.getBuilds(conn, buildIds, includeIssueReferences);
        }, OnRollback.NOOP);
    }

    @Override
    public Set<Build> getBuildsForLinkRequest(
            IssueLinkService.LinkRequest request,
            int limit,
            boolean includeIssueReferences)
    {
        return databaseAccessor.runInTransaction(conn -> {
            Predicate where;

            if (request == null)
            {
                where = Tables.TEST_RESULTS.id()
                        .isNotEmpty();
            }
            else
            {
                List<Predicate> clauses = new ArrayList<>();

                if (!request.getInProjectKeys()
                        .isEmpty())
                {
                    clauses.add(table.projectKey()
                            .in(request.getInProjectKeys()));
                }
                if (!request.getNotInProjectKeys()
                        .isEmpty())
                {
                    clauses.add(table.projectKey()
                            .in(request.getNotInProjectKeys())
                            .not());
                }
                if (!request.getInIssueKeys()
                        .isEmpty())
                {
                    clauses.add(table.issueKey()
                            .in(request.getInIssueKeys()));
                }
                if (!request.getNotInIssueKeys()
                        .isEmpty())
                {
                    clauses.add(table.issueKey()
                            .in(request.getNotInIssueKeys())
                            .not());
                }

                where = ExpressionUtils.allOf(clauses);
            }

            Predicate finalWhere = where;
            Set<String> buildIds = new HashSet<>();
            try (CloseableIterable<String> result = queryFactory.stream(conn,
                    () -> conn.select(table.buildId())
                            .from(table)
                            .where(finalWhere)
                            .orderBy(table.buildTimestamp()
                                    .desc())
                            .limit(limit)))
            {
                result.foreach(buildIds::add);
            }

            return buildDAO.getBuilds(conn, buildIds, includeIssueReferences);
        }, OnRollback.NOOP);
    }

    @Override
    public Set<Job> getJobsForIssue(
            boolean includeIssueReferences,
            String... issueKey)
    {
        return databaseAccessor.runInTransaction(conn -> {
            Set<String> jobIds = new HashSet<>();
            try (CloseableIterable<Tuple> result = queryFactory.stream(conn, () -> conn.select(table.jobId(),
                            table.buildTimestamp()
                                    .max())
                    .from(table)
                    .where(table.issueKey()
                            .in(issueKey))
                    .groupBy(table.jobId())
                    .orderBy(table.buildTimestamp()
                            .max()
                            .desc())))
            {
                result.map(tuple -> tuple.get(table.jobId()))
                        .foreach(jobIds::add);
            }

            return jobDAO.getJobs(conn, jobIds, includeIssueReferences);
        }, OnRollback.NOOP);
    }

    @Override
    public Set<String> getIssueKeys(Build build)
    {
        return databaseAccessor.runInTransaction(conn -> new HashSet<>(conn.select(table.issueKey())
                .from(table)
                .where(table.buildId()
                        .eq(build.getId()))
                .fetch()), OnRollback.NOOP);
    }

    @Override
    public Set<String> getIssueKeys(Job job)
    {
        return databaseAccessor.runInTransaction(conn -> new HashSet<>(conn.select(table.issueKey())
                .from(table)
                .where(table.jobId()
                        .eq(job.getId()))
                .fetch()), OnRollback.NOOP);
    }

    @Override
    public Set<IssueReference> getIssueLinks(Build build)
    {
        return getIssueLinks(table.buildId()
                .eq(build.getId()));
    }

    @Override
    public Set<IssueReference> getIssueLinks(Job job)
    {
        return getIssueLinks(table.jobId()
                .eq(job.getId()));
    }

    private Set<IssueReference> getIssueLinks(Predicate... where)
    {
        return databaseAccessor.runInTransaction(conn -> {
            Set<IssueReference> references = new HashSet<>();
            try (CloseableIterable<Tuple> result = queryFactory.stream(conn, () -> conn.select(table.issueKey(), table.projectKey())
                    .from(table)
                    .where(where)))
            {
                result.foreach(tuple -> references.add(new IssueReference().setIssueKey(tuple.get(table.issueKey()))
                        .setProjectKey(tuple.get(table.projectKey()))));
            }
            return references;
        }, OnRollback.NOOP);
    }

    @Override
    public void link(
            Build build,
            IssueReference issue)
    {
        databaseAccessor.runInTransaction(conn -> {
            conn.delete(table)
                    .where(table.buildId()
                                    .eq(build.getId()),
                            table.issueKey()
                                    .eq(issue.getIssueKey()))
                    .execute();

            return insertQuery(conn.insert(table), build, issue).execute();
        }, OnRollback.NOOP);
    }

    @Override
    public void relink(
            Build build,
            Set<IssueReference> references)
    {
        databaseAccessor.runInTransaction(conn -> {
            conn.delete(table)
                    .where(table.buildId()
                            .eq(build.getId()))
                    .execute();

            if (references != null && !references.isEmpty())
            {
                SQLInsertClause insert = conn.insert(table);
                references.forEach(reference -> insertQuery(insert, build, reference).addBatch());
                return insert.execute();
            }
            else
            {
                return 0;
            }
        }, OnRollback.NOOP);
    }

    private SQLInsertClause insertQuery(
            SQLInsertClause insert,
            Build build,
            IssueReference issue)
    {
        return insert.set(table.id(), newId())
                .set(table.buildId(), build.getId())
                .set(table.jobId(),
                        build.getJob()
                                .getId())
                .set(table.buildTimestamp(), build.getTimestamp())
                .set(table.issueKey(), issue.getIssueKey())
                .set(table.projectKey(), ofNullable(issue.getProjectKey()).orElse(extractProjectKeyFromIssueKey(issue.getIssueKey())));
    }

    @Override
    public void unlinkForIssue(String... issueKey)
    {
        delete(table.issueKey()
                .in(issueKey));
    }

    @Override
    public void unlinkForProject(String projectKey)
    {
        delete(table.projectKey()
                .eq(projectKey));
    }

    private void delete(BooleanExpression where)
    {
        databaseAccessor.runInTransaction(conn -> conn.delete(table)
                .where(where)
                .execute(), OnRollback.NOOP);
    }
}
