/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import java.util.*;

import org.marvelution.jji.model.*;

import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.*;

import static org.marvelution.jji.data.access.model.Entity.*;
import static org.marvelution.jji.data.access.model.v2.JobEntity.*;

/**
 * {@link Job} QueryDSL {@link Table}.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public class JobTable
		extends Table<Job, JobTable>
{

	private final StringPath id = createString(ID);
	private final StringPath siteId = createString(SITE_ID);
	private final StringPath hash = createString(HASH);
	private final BooleanPath deleted = createBoolean(DELETED);
	private final BooleanPath linked = createBoolean(LINKED);
	private final StringPath parentName = createString(PARENT_NAME);
	private final StringPath name = createString(NAME);
	private final StringPath displayName = createString(DISPLAY_NAME);
	private final StringPath urlName = createString(URL_NAME);
	private final StringPath description = createString(DESCRIPTION);
	private final NumberPath<Integer> lastBuild = createInteger(LAST_BUILD);
	private final NumberPath<Integer> oldestBuild = createInteger(OLDEST_BUILD);

	public JobTable(String namespace)
	{
		super(JobTable.class, namespace + TABLE_NAME, "job");
		createPrimaryKey(id);
	}

	@Override
	public StringPath id()
	{
		return id;
	}

	public StringPath siteId()
	{
		return siteId;
	}

	public StringPath hash()
	{
		return hash;
	}

	public BooleanPath deleted()
	{
		return deleted;
	}

	public BooleanPath linked()
	{
		return linked;
	}

	public StringPath parentName()
	{
		return parentName;
	}

	public StringPath name()
	{
		return name;
	}

	public StringPath displayName()
	{
		return displayName;
	}

	public StringPath urlName()
	{
		return urlName;
	}

	public StringPath description()
	{
		return description;
	}

	public NumberPath<Integer> lastBuild()
	{
		return lastBuild;
	}

	public NumberPath<Integer> oldestBuild()
	{
		return oldestBuild;
	}

	@Override
	public Map<Path<?>, Object> mapForInsert(
			RelationalPath<?> path,
			Job object)
	{
		Map<Path<?>, Object> bindings = mapForUpdate(path, object);
		bindings.put(id, object.getId());
		bindings.put(siteId, object.getSite().getId());
		return bindings;
	}

	@Override
	public Map<Path<?>, Object> mapForUpdate(
			RelationalPath<?> path,
			Job object)
	{
		Map<Path<?>, Object> bindings = new HashMap<>();
		bindings.put(hash, object.getHash());
		bindings.put(deleted, object.isDeleted());
		bindings.put(linked, object.isLinked());
		bindings.put(parentName, object.getParentName());
		bindings.put(name, object.getName());
		bindings.put(displayName, object.getDisplayNameOrNull());
		bindings.put(urlName, object.getUrlNameOrNull());
		bindings.put(description, object.getDescription());
		bindings.put(lastBuild, object.getLastBuild());
		bindings.put(oldestBuild, object.getOldestBuild());
		return bindings;
	}
}
