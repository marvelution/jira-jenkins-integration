/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import org.marvelution.jji.data.access.model.Entity;

import net.java.ao.*;
import net.java.ao.schema.*;

/**
 * @author Mark Rekveld
 * @since 4.5.0
 */
@Preload
@Table(IssueToBuildEntity.TABLE_NAME)
public interface IssueToBuildEntity
		extends Entity
{

	String TABLE_NAME = "ISSUE_TO_BUILD";

	String BUILD_ID = "BUILD_ID";
	String JOB_ID = "JOB_ID";
	String BUILD_TIMESTAMP = "BUILD_TIME_STAMP";
	String PROJECT_KEY = "PROJECT_KEY";
	String ISSUE_KEY = "ISSUE_KEY";

	@NotNull
	@Indexed
	BuildEntity getBuild();

	void setBuild(BuildEntity build);

	@NotNull
	@Indexed
	JobEntity getJob();

	void setJob(JobEntity job);

	@NotNull
	long getBuildTimeStamp();

	void setBuildTimeStamp(long timestamp);

	@NotNull
	@Indexed
	String getIssueKey();

	void setIssueKey(String issueKey);

	@Indexed
	String getProjectKey();

	void setProjectKey(String projectKey);
}
