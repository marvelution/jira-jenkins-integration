/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import org.marvelution.jji.api.migration.*;
import org.marvelution.jji.data.access.model.v3.*;

import com.atlassian.pocketknife.spi.querydsl.*;
import com.querydsl.core.types.dsl.*;

import static org.marvelution.jji.data.access.model.Entity.*;
import static org.marvelution.jji.data.access.model.v3.AppMigrationEntity.*;

public class AppMigrationTable
		extends EnhancedRelationalPathBase<AppMigrationTable>
{

	private final StringPath id = createString(ID);
	private final NumberPath<Long> createTimestamp = createLong(CREATE_TIMESTAMP);
	private final StringPath name = createString(NAME);
	private final EnumPath<AppMigrationType> type = createEnum(TYPE, AppMigrationType.class);
	private final EnumPath<ProgressStatus> progressStatus = createEnum(PROGRESS_STATUS, ProgressStatus.class);
	private final StringPath eventJson = createString(EVENT_JSON);
	private final StringPath feedbackJson = createString(FEEDBACK_JSON);

	public AppMigrationTable(String namespace)
	{
		super(AppMigrationTable.class, namespace + AppMigrationEntity.TABLE_NAME, "migration");
		createPrimaryKey(id);
	}

	public StringPath id()
	{
		return id;
	}

	public NumberPath<Long> createTimestamp()
	{
		return createTimestamp;
	}

	public StringPath name()
	{
		return name;
	}

	public EnumPath<AppMigrationType> type()
	{
		return type;
	}

	public EnumPath<ProgressStatus> progressStatus()
	{
		return progressStatus;
	}

	public StringPath eventJson()
	{
		return eventJson;
	}

	public StringPath feedbackJson()
	{
		return feedbackJson;
	}
}
