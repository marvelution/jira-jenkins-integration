/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import com.atlassian.pocketknife.spi.querydsl.*;
import com.querydsl.core.types.dsl.*;

import static org.marvelution.jji.data.access.model.v2.BuildToDeploymentEnvironmentEntity.*;

/**
 * Build To Deployment QueryDSL {@link Table}.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public class BuildToDeploymentEnvironmentTable
		extends EnhancedRelationalPathBase<BuildToDeploymentEnvironmentTable>
{

	private final StringPath id = createString(ID);
	private final StringPath buildId = createString(BUILD_ID);
	private final StringPath environmentId = createString(ENVIRONMENT_ID);

	public BuildToDeploymentEnvironmentTable(String namespace)
	{
		super(BuildToDeploymentEnvironmentTable.class, namespace + TABLE_NAME, "buildToDeployEnv");
		createPrimaryKey(id);
	}

	public StringPath id()
	{
		return id;
	}

	public StringPath buildId()
	{
		return buildId;
	}

	public StringPath environmentId()
	{
		return environmentId;
	}
}
