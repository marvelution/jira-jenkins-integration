/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import org.marvelution.jji.data.access.model.Entity;

import net.java.ao.*;
import net.java.ao.schema.*;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
@Preload
@Table(TestResultsEntity.TABLE_NAME)
public interface TestResultsEntity
		extends Entity
{

	String TABLE_NAME = "TEST_RESULTS";

	String BUILD_ID = "BUILD_ID";
	String SKIPPED = "SKIPPED";
	String FAILED = "FAILED";
	String TOTAL = "TOTAL";

	@NotNull
	BuildEntity getBuild();

	void setBuild(BuildEntity build);

	int getSkipped();

	void setSkipped(int skipped);

	int getFailed();

	void setFailed(int failed);

	int getTotal();

	void setTotal(int total);

}
