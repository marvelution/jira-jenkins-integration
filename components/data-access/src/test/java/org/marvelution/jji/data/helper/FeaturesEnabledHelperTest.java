/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.helper;

import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.*;
import org.marvelution.jji.api.*;
import org.marvelution.jji.api.features.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.license.*;
import org.marvelution.testing.*;

import com.atlassian.jira.issue.*;
import com.atlassian.jira.project.*;
import com.atlassian.jira.security.*;
import com.atlassian.jira.security.plugin.*;
import com.atlassian.jira.user.*;
import com.atlassian.plugin.*;
import com.atlassian.plugin.osgi.bridge.external.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.invocation.*;

import static com.atlassian.jira.permission.ProjectPermissions.*;
import static org.assertj.core.api.Assertions.*;
import static org.marvelution.jji.api.features.FeatureFlags.*;
import static org.marvelution.jji.data.helper.FeaturesEnabledHelper.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class FeaturesEnabledHelperTest
        extends TestSupport
{

    @Mock
    private IssueLinkService issueLinkService;
    @Mock
    private ConfigurationService configurationService;
    private Features features;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private LicenseHelper.Factory licenseHelperFactory;
    @Mock
    private LicenseHelper licenseHelper;
    @Mock
    private PluginRetrievalService pluginRetrievalService;
    @Mock
    private PluginAccessor pluginAccessor;
    private FeaturesEnabledHelper featuresEnabledHelper;

    static Stream<Arguments> dynamicArguments()
    {
        return Stream.of(Arguments.of(0, VIEW_DEV_TOOLS),
                Arguments.of(1, VIEW_DEV_TOOLS),
                Arguments.of(0, VIEW_JENKINS),
                Arguments.of(1, VIEW_JENKINS));
    }

    static Stream<Arguments> enabledArguments()
    {
        return Stream.of(Arguments.of(true, VIEW_DEV_TOOLS),
                Arguments.of(false, VIEW_DEV_TOOLS),
                Arguments.of(true, VIEW_JENKINS),
                Arguments.of(false, VIEW_JENKINS));
    }

    @BeforeEach
    void setUp()
    {
        AppState appState = new DefaultAppState(licenseHelper, pluginRetrievalService);
        features = new Features(new InMemoryFeatureStore())
        {
            @Override
            protected Set<FeatureFlag> loadFeatureFlags()
            {
                return Collections.singleton(new FeatureFlag(DYNAMIC_UI_ELEMENTS, false, true));
            }
        };
        featuresEnabledHelper = new FeaturesEnabledHelper(appState,
                features,
                issueLinkService,
                configurationService,
                authenticationContext,
                permissionManager);

        Plugin plugin = mock(Plugin.class);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(pluginRetrievalService.getPlugin()).thenReturn(plugin);
        when(licenseHelper.isActive()).thenReturn(true);
        when(authenticationContext.getLoggedInUser()).thenReturn(mock(ApplicationUser.class));
    }

    @ParameterizedTest
    @MethodSource("enabledArguments")
    void testAreFeaturesEnabledForProject_Permission(
            boolean enabled,
            ProjectPermissionKey permission)
    {
        when(permissionManager.hasPermission(any(ProjectPermissionKey.class), any(Project.class), any(ApplicationUser.class))).then(
                invocation -> {
                    ProjectPermissionKey key = invocation.getArgument(0);
                    return permission.equals(key);
                });
        if (enabled)
        {
            when(configurationService.isProjectEnabled("DEV")).thenReturn(true);
        }

        Project project = mockProject();

        assertThat(featuresEnabledHelper.areFeaturesEnabledForProject(project)).isEqualTo(enabled);
    }

    @ParameterizedTest
    @MethodSource("enabledArguments")
    void testAreFeaturesEnabledForIssue_Permission(
            boolean enabled,
            ProjectPermissionKey permission)
    {
        when(permissionManager.hasPermission(any(ProjectPermissionKey.class), any(Project.class), any(ApplicationUser.class))).then(
                invocation -> {
                    ProjectPermissionKey key = invocation.getArgument(0);
                    return permission.equals(key);
                });
        if (enabled)
        {
            when(configurationService.isProjectEnabled("DEV")).thenReturn(true);
        }

        Issue issue = mock(Issue.class);
        when(issue.getProjectObject()).then(this::mockProject);

        assertThat(featuresEnabledHelper.areFeaturesEnabledForIssue(issue)).isEqualTo(enabled);
    }

    @ParameterizedTest
    @MethodSource("enabledArguments")
    void testAreFeaturesEnabledForProject_DisabledProject(
            boolean enabled,
            ProjectPermissionKey permission)
    {
        when(configurationService.isProjectEnabled("DEV")).thenReturn(enabled);
        when(permissionManager.hasPermission(any(ProjectPermissionKey.class), any(Project.class), any(ApplicationUser.class))).then(
                invocation -> {
                    ProjectPermissionKey key = invocation.getArgument(0);
                    return permission.equals(key);
                });

        Project project = mockProject();

        assertThat(featuresEnabledHelper.areFeaturesEnabledForProject(project)).isEqualTo(enabled);
    }

    @ParameterizedTest
    @MethodSource("enabledArguments")
    void testAreFeaturesEnabledForIssue_DisabledProject(
            boolean enabled,
            ProjectPermissionKey permission)
    {
        when(configurationService.isProjectEnabled("DEV")).thenReturn(enabled);
        when(permissionManager.hasPermission(any(ProjectPermissionKey.class), any(Project.class), any(ApplicationUser.class))).then(
                invocation -> {
                    ProjectPermissionKey key = invocation.getArgument(0);
                    return permission.equals(key);
                });

        Issue issue = mock(Issue.class);
        when(issue.getProjectObject()).then(this::mockProject);

        assertThat(featuresEnabledHelper.areFeaturesEnabledForIssue(issue)).isEqualTo(enabled);
    }

    @ParameterizedTest
    @MethodSource("dynamicArguments")
    void testAreFeaturesEnabledForProject_Dynamic(
            int count,
            ProjectPermissionKey permission)
    {
        when(configurationService.isProjectEnabled("DEV")).thenReturn(true);
        when(permissionManager.hasPermission(any(ProjectPermissionKey.class), any(Project.class), any(ApplicationUser.class))).then(
                invocation -> {
                    ProjectPermissionKey key = invocation.getArgument(0);
                    return permission.equals(key);
                });
        features.enableFeature(DYNAMIC_UI_ELEMENTS);
        Project project = mockProject();
        when(issueLinkService.getBuildCountForProject("DEV")).thenReturn(count);

        assertThat(featuresEnabledHelper.areFeaturesEnabledForProject(project)).isEqualTo(count > 0);
    }

    @ParameterizedTest
    @MethodSource("dynamicArguments")
    void testAreFeaturesEnabledForIssue_Dynamic(
            int count,
            ProjectPermissionKey permission)
    {
        when(configurationService.isProjectEnabled("DEV")).thenReturn(true);
        when(permissionManager.hasPermission(any(ProjectPermissionKey.class), any(Project.class), any(ApplicationUser.class))).then(
                invocation -> {
                    ProjectPermissionKey key = invocation.getArgument(0);
                    return permission.equals(key);
                });
        features.enableFeature(DYNAMIC_UI_ELEMENTS);
        Issue issue = mock(Issue.class);
        when(issue.getKey()).thenReturn("DEV-1");
        when(issue.getProjectObject()).then(this::mockProject);
        when(issueLinkService.getBuildCountForIssue("DEV-1")).thenReturn(count);

        assertThat(featuresEnabledHelper.areFeaturesEnabledForIssue(issue)).isEqualTo(count > 0);
    }

    private Project mockProject(InvocationOnMock invocation)
    {
        return mockProject();
    }

    private Project mockProject()
    {
        Project project = mock(Project.class, withSettings().lenient());
        when(project.getKey()).thenReturn("DEV");
        return project;
    }
}
