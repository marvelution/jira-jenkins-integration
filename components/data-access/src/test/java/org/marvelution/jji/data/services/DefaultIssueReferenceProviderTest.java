/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.*;
import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.data.access.DefaultIssueReferenceProvider;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.testing.*;

import com.atlassian.jira.issue.*;
import com.atlassian.jira.issue.fields.*;
import com.atlassian.jira.project.*;
import com.atlassian.plugins.rest.api.json.JaxbJsonMarshaller;
import org.junit.jupiter.api.*;
import org.mockito.*;

import static org.marvelution.jji.utils.KeyExtractor.*;

import static java.util.stream.Collectors.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

class DefaultIssueReferenceProviderTest
		extends TestSupport
{

	@Mock
	private IssueManager issueManager;
	@Mock
	private FieldManager fieldManager;
	@Mock
	private JaxbJsonMarshaller jaxbJsonMarshaller;
	@Mock(strictness = Mock.Strictness.LENIENT)
	private ConfigurationService configurationService;
	private DefaultIssueReferenceProvider referenceProvider;

	@BeforeEach
	void setUp()
	{
		referenceProvider = new DefaultIssueReferenceProvider(configurationService, issueManager, fieldManager, jaxbJsonMarshaller);
		when(configurationService.getBaseUrl()).thenReturn(URI.create("https://jira.example.com"));
	}

	@Nested
	class GetIssueReference
	{

		private Map<String, MutableIssue> issues;

		@BeforeEach
		void setUp()
		{
			issues = new HashMap<>();
			when(issueManager.getIssueObject(anyString())).then(invocation -> issues.get(invocation.<String>getArgument(0)));
		}

		@Test
		void testGetIssueReference()
		{
			mockIssue("DEV-1", true);

			Optional<IssueReference> reference = referenceProvider.getIssueReference("DEV-1");

			assertThat(reference).isPresent().get().isEqualTo(new IssueReference().setIssueKey("DEV-1").setProjectKey("DEV"));
		}

		@Test
		void testGetIssueReference_NoProject()
		{
			mockIssue("DEV-1", false);

			Optional<IssueReference> reference = referenceProvider.getIssueReference("DEV-1");

			assertThat(reference).isPresent().get().isEqualTo(new IssueReference().setIssueKey("DEV-1").setProjectKey("DEV"));
		}

		@Test
		void testGetIssueReference_UnknownIssue()
		{
			Optional<IssueReference> reference = referenceProvider.getIssueReference("DEV-1");

			assertThat(reference).isNotPresent();
		}

		@Test
		void testGetIssueReferences()
		{
			mockIssue("DEV-1", false);
			mockIssue("DEV-2", true);

			Set<IssueReference> references = referenceProvider.getIssueReferences(Stream.of("DEV-1", "DEV-2", "DEV-200").collect(toSet()));

			assertThat(references).hasSize(2)
					.containsOnly(new IssueReference().setIssueKey("DEV-1").setProjectKey("DEV"),
					              new IssueReference().setIssueKey("DEV-2").setProjectKey("DEV"));
		}

		@Test
		void testGetIssueReferences_UnknownIssues()
		{
			Set<IssueReference> references = referenceProvider.getIssueReferences(Stream.of("DEV-1", "DEV-2", "DEV-200").collect(toSet()));

			assertThat(references).isEmpty();
		}

		private void mockIssue(
				String issueKey,
				boolean withProject)
		{
			MutableIssue issue = mock(MutableIssue.class);
			when(issue.getKey()).thenReturn(issueKey);
			if (withProject)
			{
				Project project = mock(Project.class);
				when(project.getKey()).thenReturn(extractProjectKeyFromIssueKey(issueKey));
				when(issue.getProjectObject()).thenReturn(project);
			}
			issues.put(issueKey, issue);
		}
	}

	@Test
	void testSetIssueUrl()
	{
		IssueReference reference = new IssueReference().setIssueKey("DEV-2").setProjectKey("DEV");
		referenceProvider.setIssueUrl(reference);

		assertThat(reference).isEqualTo(new IssueReference().setIssueKey("DEV-2").setProjectKey("DEV"))
				.extracting(IssueReference::getIssueUrl)
				.isEqualTo(URI.create("https://jira.example.com/browse/DEV-2"));
	}
}
