/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.admin;

import com.atlassian.jira.web.action.*;
import com.atlassian.plugin.*;
import com.atlassian.plugin.osgi.bridge.external.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;

/**
 * Simple {@link JiraWebActionSupport} to redirect to the documentation.
 *
 * @author Mark Rekveld
 * @since 3.9.0
 */
public class RedirectToDocs extends JiraWebActionSupport {

	private final PluginRetrievalService pluginRetrievalService;

	public RedirectToDocs(@ComponentImport PluginRetrievalService pluginRetrievalService) {
		this.pluginRetrievalService = pluginRetrievalService;
	}

	/**
	 * Redirect to documentation.
	 */
	@Override
	public String doDefault() {
		return forceRedirect(getDocsBaseUrl());
	}

	/**
	 * Redirect to release notes.
	 */
	public String doReleaseNotes() {
		return forceRedirect(getDocsBaseUrl() + "/release/jira/#" + getPluginInformation().getVersion());
	}

	/**
	 * Redirect to the quick start guide for Jenkins add-on installation steps.
	 */
	public String doQuickStartGuide() {
		return forceRedirect(getDocsBaseUrl() + "/guides/quick-start-guide");
	}

	/**
	 * Returns the base documentation url.
	 */
	private String getDocsBaseUrl() {
		return getPluginInformation().getParameters().get("documentation.url");
	}

	/**
	 * Returns the {@link PluginInformation}.
	 */
	private PluginInformation getPluginInformation() {
		return pluginRetrievalService.getPlugin().getPluginInformation();
	}
}
