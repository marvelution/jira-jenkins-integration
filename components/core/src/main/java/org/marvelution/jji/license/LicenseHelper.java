/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.license;

import com.atlassian.plugin.Plugin;

/**
 * API for checking add-on license states.
 *
 * @author Mark Rekveld
 * @since 3.7.0
 */
public interface LicenseHelper {

	/**
	 * Returns whether the license of the add-on is active.
	 * Defaults to {@code true} in case the add-on didn't mark the {@code atlassian-plugin.xml} as required.
	 */
	boolean isActive();

	/**
	 * Returns whether the license of the add-on is expired.
	 * Defaults to {@code false} in case the add-on didn't mark the {@code atlassian-plugin.xml} as required.
	 */
	boolean isExpired();

	/**
	 * Returns whether the license of the add-on is nearly expired.
	 * Defaults to {@code false} in case the add-on didn't mark the {@code atlassian-plugin.xml} as required.
	 */
	boolean isNearlyExpired();

	/**
	 * Factory for {@link LicenseHelper}s.
	 */
	@FunctionalInterface
	interface Factory {

		/**
		 * Returns a new {@link LicenseHelper} for the specified {@link Plugin}.
		 */
		LicenseHelper create(Plugin plugin);
	}
}
