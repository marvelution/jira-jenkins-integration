/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.HashMap;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link HashMap} implementation that removes Null characters from strings values.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public final class NullCharacterRemovingHashMap extends HashMap<String, Object> {

	private static final long serialVersionUID = 4485473832788525727L;
	private static final Logger LOGGER = LoggerFactory.getLogger(NullCharacterRemovingHashMap.class);
	private static final String NULL_CHARACTER = "\u0000";

	@Override
	public Object put(String key, @Nullable Object value) {
		Object transformedValue = value instanceof String ? removeNullCharacters((String) value) : value;
		return super.put(key, transformedValue);
	}

	private String removeNullCharacters(String inputText) {
		if (!inputText.contains(NULL_CHARACTER)) {
			return inputText;
		} else {
			LOGGER.debug("Removing null character from '{}'", inputText);
			return inputText.replace(NULL_CHARACTER, "");
		}
	}
}
