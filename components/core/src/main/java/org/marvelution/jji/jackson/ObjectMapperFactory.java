/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.jackson;

import java.text.SimpleDateFormat;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

import static com.fasterxml.jackson.databind.DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

@Named
public class ObjectMapperFactory
{
    private final List<Module> modules;
    private ObjectMapper objectMapper;

    @Inject
    public ObjectMapperFactory(List<Module> modules)
    {
        this.modules = modules;
    }

    public ObjectMapper create()
    {
        if (objectMapper == null)
        {
            objectMapper = JsonMapper.builder()
                    .serializationInclusion(JsonInclude.Include.NON_NULL)
                    .enable(ACCEPT_CASE_INSENSITIVE_ENUMS)
                    .disable(FAIL_ON_UNKNOWN_PROPERTIES)
                    .disable(WRITE_DATES_AS_TIMESTAMPS)
                    .disable(ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
                    .defaultDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"))
                    .addModules(new JavaTimeModule(),
                            new JaxbAnnotationModule().setPriority(JaxbAnnotationModule.Priority.PRIMARY),
                            new Jdk8Module(),
                            new ParameterNamesModule())
                    .addModules(modules)
                    .build();
        }
        return objectMapper;
    }
}
