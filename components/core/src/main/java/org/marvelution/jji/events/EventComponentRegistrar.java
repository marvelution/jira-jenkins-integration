/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.events;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;

/**
 * {@link DestructionAwareBeanPostProcessor} implementation to register and unregister {@link EventComponent}s.
 *
 * @author Mark Rekveld
 */
@Named
@Singleton
public class EventComponentRegistrar implements DestructionAwareBeanPostProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(EventComponentRegistrar.class);
	private final EventPublisher eventPublisher;
	private final PluginEventManager pluginEventManager;

	@Inject
	public EventComponentRegistrar(@ComponentImport EventPublisher eventPublisher, @ComponentImport PluginEventManager pluginEventManager) {
		this.eventPublisher = eventPublisher;
		this.pluginEventManager = pluginEventManager;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (isEventComponent(bean)) {
			LOGGER.debug("Registering bean {} of type {} with the eventPublisher", beanName, bean.getClass());
			eventPublisher.register(bean);
		}
		if (isPluginEventListener(bean)) {
			LOGGER.debug("Registering bean {} of type {} with the pluginEventManager", beanName, bean.getClass());
			pluginEventManager.register(bean);
		}
		return bean;
	}

	@Override
	public void postProcessBeforeDestruction(Object bean, String beanName) throws BeansException {
		if (isEventComponent(bean)) {
			LOGGER.debug("Unregistering bean {} of type {} from the eventPublisher", beanName, bean.getClass());
			eventPublisher.unregister(bean);
		}
		if (isPluginEventListener(bean)) {
			LOGGER.debug("Unregistering bean {} of type {} from the pluginEventManager", beanName, bean.getClass());
			pluginEventManager.unregister(bean);
		}
	}

	private boolean isEventComponent(Object bean) {
		return bean.getClass().isAnnotationPresent(EventComponent.class) || hasMethodsAnnotatedWith(bean.getClass(), EventListener.class);
	}

	private boolean isPluginEventListener(Object bean) {
		return hasMethodsAnnotatedWith(bean.getClass(), PluginEventListener.class);
	}

	private boolean hasMethodsAnnotatedWith(Class<?> type, Class<? extends Annotation> annotation) {
		if (type != Object.class) {
			for (Method method : type.getDeclaredMethods()) {
				if (method.isAnnotationPresent(annotation)) {
					return true;
				}
			}
			if (type.getSuperclass() != null) {
				return hasMethodsAnnotatedWith(type.getSuperclass(), annotation);
			}
		}
		return false;
	}
}
