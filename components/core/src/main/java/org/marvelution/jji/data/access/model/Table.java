/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model;

import java.util.*;

import org.marvelution.jji.model.*;

import com.atlassian.pocketknife.api.querydsl.*;
import com.atlassian.pocketknife.spi.querydsl.*;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.*;
import com.querydsl.sql.dml.*;

import static org.marvelution.jji.model.HasId.newId;

/**
 * Base QueryDSL {@link EnhancedRelationalPathBase} for model tables.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public abstract class Table<E extends HasId<E>, T extends Table<E, T>>
		extends EnhancedRelationalPathBase<T>
{

	public Table(
			Class<? extends T> type,
			String logicalTableName,
			String tableAlias)
	{
		super(type, logicalTableName, tableAlias);
	}

	public abstract StringPath id();

	public SQLInsertClause insert(DatabaseConnection conn, E object)
	{
		if (object.getId() == null)
		{
			object.setId(newId());
		}
		return conn.insert(this).populate(object, this::mapForInsert);
	}

	public SQLUpdateClause update(DatabaseConnection conn, E object)
	{
		return conn.update(this).populate(object, this::mapForUpdate).where(id().eq(object.getId()));
	}

	public abstract Map<Path<?>, Object> mapForInsert(RelationalPath<?> path, E object);

	public abstract Map<Path<?>, Object> mapForUpdate(RelationalPath<?> path, E object);
	
	public SQLDeleteClause delete(DatabaseConnection conn, String id) {
		return conn.delete(this).where(id().eq(id));
	}
}
