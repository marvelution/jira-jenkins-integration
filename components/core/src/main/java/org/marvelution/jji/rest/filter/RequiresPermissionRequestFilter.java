/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest.filter;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;

import org.marvelution.jji.rest.security.RequiresPermission;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.rest.api.security.exception.AuthenticationRequiredException;
import com.atlassian.plugins.rest.api.security.exception.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.requireNonNull;

public class RequiresPermissionRequestFilter
        implements ContainerRequestFilter
{

    private static final Logger LOGGER = LoggerFactory.getLogger(RequiresPermissionRequestFilter.class);
    private final JiraAuthenticationContext authenticationContext;
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;
    private final RequiresPermission requiresPermission;

    public RequiresPermissionRequestFilter(
            JiraAuthenticationContext authenticationContext,
            GlobalPermissionManager globalPermissionManager,
            PermissionManager permissionManager,
            RequiresPermission requiresPermission)
    {
        this.authenticationContext = requireNonNull(authenticationContext);
        this.globalPermissionManager = requireNonNull(globalPermissionManager);
        this.permissionManager = requireNonNull(permissionManager);
        this.requiresPermission = requiresPermission;
    }

    @Override
    public void filter(ContainerRequestContext requestContext)
            throws IOException
    {
        if (requiresPermission != null && requiresPermission.value().length > 0)
        {
            ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
            LOGGER.debug("Checking user {} has require permissions {}", loggedInUser, String.join(", ", requiresPermission.value()));
            if (loggedInUser == null)
            {
                throw new AuthenticationRequiredException();
            }
            else if (globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, loggedInUser))
            {
                LOGGER.debug("User {} has global administer permissions.", loggedInUser);
            }
            else
            {
                for (String permission : requiresPermission.value())
                {
                    if (!permissionManager.hasProjects(new ProjectPermissionKey(permission), loggedInUser))
                    {
                        LOGGER.debug("User {} doesn't have permissions {} on at least one project.", loggedInUser, permission);
                        throw new AuthorizationException("User " + loggedInUser + " doesn't have permissions " + permission);
                    }
                }
            }
        }
    }
}
