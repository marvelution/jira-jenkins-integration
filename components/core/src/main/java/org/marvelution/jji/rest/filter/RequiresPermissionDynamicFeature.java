/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest.filter;

import javax.inject.Inject;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;
import jakarta.annotation.Nullable;

import org.marvelution.jji.rest.security.RequiresPermission;

import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class RequiresPermissionDynamicFeature
        implements DynamicFeature
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RequiresPermissionDynamicFeature.class);
    private final JiraAuthenticationContext authenticationContext;
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;

    @Inject
    public RequiresPermissionDynamicFeature(
            @ComponentImport
            JiraAuthenticationContext authenticationContext,
            @ComponentImport
            GlobalPermissionManager globalPermissionManager,
            @ComponentImport
            PermissionManager permissionManager)
    {
        this.authenticationContext = authenticationContext;
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
    }

    @Override
    public void configure(
            ResourceInfo resourceInfo,
            FeatureContext context)
    {
        RequiresPermission requiresPermission = getRequiresPermission(resourceInfo);

        if (requiresPermission != null)
        {
            LOGGER.debug("Registering filter {} for {} with {}",
                    RequiresPermissionRequestFilter.class.getName(),
                    resourceInfo,
                    requiresPermission);
            context.register(new RequiresPermissionRequestFilter(authenticationContext,
                    globalPermissionManager,
                    permissionManager,
                    requiresPermission));
        }
    }

    @Nullable
    private static RequiresPermission getRequiresPermission(ResourceInfo resourceInfo)
    {
        RequiresPermission requiresPermission = null;
        if (resourceInfo.getResourceMethod()
                .isAnnotationPresent(RequiresPermission.class))
        {
            requiresPermission = resourceInfo.getResourceMethod()
                    .getAnnotation(RequiresPermission.class);
        }
        else if (resourceInfo.getResourceClass()
                .isAnnotationPresent(RequiresPermission.class))
        {
            requiresPermission = resourceInfo.getResourceClass()
                    .getAnnotation(RequiresPermission.class);
        }
        return requiresPermission;
    }
}
