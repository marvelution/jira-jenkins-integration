/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import org.marvelution.jji.data.access.model.Entity;

import com.atlassian.activeobjects.external.*;
import com.atlassian.activeobjects.spi.*;
import net.java.ao.*;
import org.slf4j.*;

import static org.marvelution.jji.model.HasId.*;

import static java.util.Arrays.*;
import static java.util.Optional.*;

/**
 * Base implementation for DAO based on {@link ActiveObjects}
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
public abstract class AbstractActiveObjectsDAO<E extends Entity>
{

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	protected final ActiveObjects activeObjects;
	private final Class<E> entityClass;

	protected AbstractActiveObjectsDAO(
			ActiveObjects activeObjects,
			Class<E> entityClass)
	{
		this.activeObjects = activeObjects;
		this.entityClass = entityClass;
	}

	/**
	 * @since 4.0.0
	 */
	protected String addQuotesIfNeeded(String string)
	{
		UnaryOperator<String> quotivy = UnaryOperator.identity();
		// Postgres really wants quotes around the column name, but AO doesn't do this correctly when using UPPER() so do is 'manually'.
		if (activeObjects.moduleMetaData().getDatabaseType() == DatabaseType.POSTGRESQL)
		{
			quotivy = s -> "\"" + s + "\"";
		}
		return quotivy.apply(string);
	}

	/**
	 * Returns an {@link Optional} reference to the {@link Entity} with the specified {@code id}
	 */
	protected Optional<E> getOne(String id)
	{
		return getOne(entityClass, id);
	}

	/**
	 * Returns an {@link Optional} reference to the {@link Entity} of the specified {@link Class entityType} with the specified {@code id}
	 *
	 * @since 4.1.0
	 */
	protected <OE extends Entity> Optional<OE> getOne(
			Class<OE> entityClass,
			String id)
	{
		return ofNullable(activeObjects.get(entityClass, id));
	}

	/**
	 * Returns an {@link Optional} reference to the {@link Entity} that is the first result of the specified {@link Query}
	 */
	protected Optional<E> findOne(Query query)
	{
		return find(query).findFirst();
	}

	/**
	 * Returns a {@link Stream} to {@link Entity Entities} that match the specified {@link Query}
	 */
	protected Stream<E> find(Query query)
	{
		return find(entityClass, query);
	}

	/**
	 * Returns a {@link Stream} to {@link Entity Entities} of the specified {@link Class entityType} that match the specified {@link Query}
	 *
	 * @since 4.1.0
	 */
	protected <EO extends RawEntity<String>> Stream<EO> find(
			Class<EO> entityType,
			Query query)
	{
		return stream(activeObjects.find(entityType, query));
	}

	/**
	 * Returns the number of entities related to the specified {@link Query}.
	 *
	 * @since 3.9.0
	 */
	protected int count(Query query)
	{
		return activeObjects.count(entityClass, query);
	}

	/**
	 * Inserts a new {@link Entity} using the specified {@link Map} of fields and return it.
	 */
	protected E insert(NullCharacterRemovingHashMap mapping)
	{
		return insert(entityClass, mapping);
	}

	/**
	 * Inserts a new {@link Entity} of the specified {@code entityType} using the specified {@link Map} of fields and return it.
	 */
	protected <OE extends Entity> OE insert(
			Class<OE> entityType,
			NullCharacterRemovingHashMap mapping)
	{
		if (!mapping.containsKey(Entity.ID))
		{
			mapping.put(Entity.ID, newId());
		}
		return activeObjects.create(entityType, mapping);
	}

	/**
	 * Update an {@link Entity} using the specified {@link Consumer updater} and return it in its new state.
	 */
	protected E update(
			String id,
			Consumer<E> updater)
	{
		return update(entityClass, id, updater);
	}

	/**
	 * Update an {@link Entity} of the specified {@code entityType} using the specified {@link Consumer updater} and return it in its new
	 * state.
	 */
	protected <OE extends Entity> OE update(
			Class<OE> entityType,
			String id,
			Consumer<OE> updater)
	{
		OE mapping = activeObjects.get(entityType, id);
		updater.accept(mapping);
		mapping.save();
		return mapping;
	}

	/**
	 * Delete a single {@link Entity} of the specified {@code entityType}, with the specified {@code id}
	 */
	protected <OE extends Entity> void delete(
			Class<OE> entityType,
			String id)
	{
		OE mapping = activeObjects.get(entityType, id);
		if (mapping != null)
		{
			deleteEntity(mapping);
		}
	}

	/**
	 * Delete all the {@link Entity} of the specified {@code entityType}, that match the specified {@link Query};
	 */
	protected <OE extends Entity> void delete(
			Class<OE> entityType,
			Query query)
	{
		deleteEntity(activeObjects.find(entityType, query));
	}

	protected void deleteOne(String id)
	{
		getOne(id).ifPresent(this::delete);
	}

	/**
	 * Delete all {@link Entity Entities}, and returns them, that match the specified {@code query}
	 */
	protected void delete(Query query)
	{
		stream(activeObjects.find(entityClass, query)).forEach(this::delete);
	}

	protected abstract void delete(E entity);

	/**
	 * Delete {@link Entity Entities} using a where criteria statement
	 */
	protected void deleteWithSQL(
			String criteria,
			Object... parameters)
	{
		activeObjects.deleteWithSQL(entityClass, criteria, parameters);
	}

	protected void deleteEntity(Entity... entity)
	{
		if (entity != null)
		{
			stream(entity).filter(Objects::nonNull)
					.peek(object -> logger.debug("Deleting entity {} with ID {}", object.getEntityType().getSimpleName(), object.getID()))
					.forEach(activeObjects::delete);
		}
	}
}
