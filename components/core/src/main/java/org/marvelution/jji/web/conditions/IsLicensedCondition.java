/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import java.util.Map;

import javax.inject.Inject;

import org.marvelution.jji.license.LicenseHelper;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

/**
 * {@link Condition} that checks the add-on license state.
 *
 * @author Mark Rekveld
 * @since 3.7.0
 */
public class IsLicensedCondition implements Condition {

	private final LicenseHelper licenseHelper;

	@Inject
	public IsLicensedCondition(LicenseHelper licenseHelper) {
		this.licenseHelper = licenseHelper;
	}

	@Override
	public void init(Map<String, String> params) throws PluginParseException {
	}

	@Override
	public boolean shouldDisplay(Map<String, Object> context) {
		return licenseHelper.isActive();
	}
}
