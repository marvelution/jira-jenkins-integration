/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import javax.inject.*;
import java.util.*;

import org.marvelution.jji.model.*;

import com.atlassian.jira.permission.*;
import com.atlassian.jira.project.*;
import com.atlassian.jira.security.*;
import com.atlassian.plugin.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import com.atlassian.plugin.web.*;

public class UserCanAdministerSite
        implements Condition
{

    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;
    private final JiraAuthenticationContext authenticationContext;
    private final ProjectManager projectManager;

    @Inject
    public UserCanAdministerSite(
            @ComponentImport
            GlobalPermissionManager globalPermissionManager,
            @ComponentImport
            PermissionManager permissionManager,
            @ComponentImport
            JiraAuthenticationContext authenticationContext,
            ProjectManager projectManager)
    {
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
        this.authenticationContext = authenticationContext;
        this.projectManager = projectManager;
    }

    @Override
    public void init(Map<String, String> params)
            throws PluginParseException
    {}

    @Override
    public boolean shouldDisplay(Map<String, Object> context)
    {
        if (globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, authenticationContext.getLoggedInUser()))
        {
            return true;
        }
        else
        {
            Site site = (Site) context.get("site");
            Project project = projectManager.getProjectObjByKey(site.getScope());
            return permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS,
                    project,
                    authenticationContext.getLoggedInUser());
        }
    }
}
