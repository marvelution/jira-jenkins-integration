/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.api.AppState;
import org.marvelution.jji.license.LicenseHelper;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@Named
public class DefaultAppState
        implements AppState
{

    private final LicenseHelper licenseHelper;
    private final PluginRetrievalService pluginRetrievalService;

    @Inject
    public DefaultAppState(
            LicenseHelper licenseHelper,
            @ComponentImport
            PluginRetrievalService pluginRetrievalService)
    {
        this.licenseHelper = licenseHelper;
        this.pluginRetrievalService = pluginRetrievalService;
    }

    @Override
    public String getKey()
    {
        return getPlugin().getKey();
    }

    @Override
    public boolean isEnabled()
    {
        return getPlugin().getPluginState() == PluginState.ENABLED && licenseHelper.isActive();
    }

    public String getCompleteModuleKey(String moduleKey)
    {
        return String.format("%s:%s", getKey(), moduleKey);
    }

    private Plugin getPlugin()
    {
        return pluginRetrievalService.getPlugin();
    }
}
