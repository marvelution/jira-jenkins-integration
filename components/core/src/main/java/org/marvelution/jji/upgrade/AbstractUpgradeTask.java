/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.*;
import java.util.regex.*;

import org.marvelution.jji.api.*;

import com.atlassian.sal.api.upgrade.*;

public abstract class AbstractUpgradeTask
		implements PluginUpgradeTask
{

	private static final Pattern CLASS_NAME_PATTERN = Pattern.compile("^T([\\d]+)_(.*)$");
	protected final AppState appState;
	private Integer buildNumber;
	private String description;

	public AbstractUpgradeTask(AppState appState)
	{
		this.appState = appState;
		Matcher matcher = CLASS_NAME_PATTERN.matcher(getClass().getSimpleName());
		if (matcher.matches())
		{
			buildNumber = Integer.parseInt(matcher.group(1));
			description = String.join(" ", matcher.group(2).split("(?=\\p{Lu})"));
		}
	}

	@Override
	public int getBuildNumber()
	{
		return Optional.ofNullable(buildNumber)
				.orElseThrow(() -> new IllegalStateException("Missing buildNumber for " + getClass().getName()));
	}

	@Override
	public String getShortDescription()
	{
		return Optional.ofNullable(description).orElseGet(getClass()::getSimpleName);
	}

	@Override
	public String getPluginKey()
	{
		return appState.getKey();
	}
}
