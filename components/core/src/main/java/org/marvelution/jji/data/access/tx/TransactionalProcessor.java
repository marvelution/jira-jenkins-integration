/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.tx;

import java.lang.reflect.*;
import javax.inject.*;

import org.marvelution.jji.data.access.*;

import com.atlassian.activeobjects.external.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import org.springframework.beans.factory.config.*;

import static java.util.Objects.*;

@Named
@Singleton
public class TransactionalProcessor
		implements BeanPostProcessor
{

	private final ActiveObjects activeObjects;

	@Inject
	public TransactionalProcessor(@ComponentImport ActiveObjects activeObjects)
	{
		this.activeObjects = requireNonNull(activeObjects, "activeObjects cannot be null");
	}

	@Override
	public Object postProcessBeforeInitialization(
			Object bean,
			String beanName)
	{
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(
			Object bean,
			String beanName)
	{
		return (bean instanceof AbstractActiveObjectsDAO) ? transactional(bean) : bean;
	}

	private Object transactional(Object bean)
	{
		Class<?> type = bean.getClass();
		return Proxy.newProxyInstance(type.getClassLoader(), type.getInterfaces(), new TransactionalProxy(activeObjects, bean));
	}
}
