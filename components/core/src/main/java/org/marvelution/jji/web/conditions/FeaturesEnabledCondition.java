/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import java.util.*;

import com.atlassian.jira.issue.*;
import com.atlassian.jira.project.*;
import com.atlassian.plugin.*;
import com.atlassian.plugin.web.*;

/**
 * @author Mark Rekveld
 * @since 4.1.0
 */
public class FeaturesEnabledCondition
		implements Condition
{

	public static final String CONTEXT_KEY_PROJECT = "project";
	public static final String CONTEXT_KEY_ISSUE = "issue";
	private final Helper helper;

	public FeaturesEnabledCondition(Helper helper)
	{
		this.helper = helper;
	}

	@Override
	public void init(Map<String, String> params)
			throws PluginParseException
	{}

	@Override
	public boolean shouldDisplay(Map<String, Object> context)
	{
		if (context.containsKey(CONTEXT_KEY_ISSUE))
		{
			return helper.areFeaturesEnabledForIssue((Issue) context.get(CONTEXT_KEY_ISSUE));
		}
		else if (context.containsKey(CONTEXT_KEY_PROJECT))
		{
			return helper.areFeaturesEnabledForProject((Project) context.get(CONTEXT_KEY_PROJECT));
		}
		else
		{
			return false;
		}
	}

	public interface Helper
	{
		boolean areFeaturesEnabledForIssue(Issue issue);

		boolean areFeaturesEnabledForProject(Project project);
	}
}
