/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import javax.inject.*;
import java.util.*;

import com.atlassian.jira.permission.*;
import com.atlassian.jira.security.*;
import com.atlassian.plugin.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import com.atlassian.plugin.web.*;

public class UserIsJiraSiteAdministrator
        implements Condition
{

    private final GlobalPermissionManager permissionManager;
    private final JiraAuthenticationContext authenticationContext;

    @Inject
    public UserIsJiraSiteAdministrator(
            @ComponentImport
            GlobalPermissionManager permissionManager,
            @ComponentImport
            JiraAuthenticationContext authenticationContext)
    {
        this.permissionManager = permissionManager;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public void init(Map<String, String> params)
            throws PluginParseException
    {}

    @Override
    public boolean shouldDisplay(Map<String, Object> context)
    {
        return permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, authenticationContext.getLoggedInUser());
    }
}
