/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest.filter;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.marvelution.jji.rest.security.AdminRequired;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.api.security.exception.AuthenticationRequiredException;
import com.atlassian.plugins.rest.api.security.exception.AuthorizationException;
import org.slf4j.LoggerFactory;

import static java.util.Objects.requireNonNull;

@Provider
@AdminRequired
public class AdminRequiredRequestFilter
        implements ContainerRequestFilter
{

    private final JiraAuthenticationContext authenticationContext;
    private final GlobalPermissionManager globalPermissionManager;

    @Inject
    public AdminRequiredRequestFilter(
            @ComponentImport
            JiraAuthenticationContext authenticationContext,
            @ComponentImport
            GlobalPermissionManager globalPermissionManager)
    {
        this.authenticationContext = requireNonNull(authenticationContext);
        this.globalPermissionManager = requireNonNull(globalPermissionManager);
    }

    @Override
    public void filter(ContainerRequestContext requestContext)
    {
        ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
        if (loggedInUser == null)
        {
            throw new AuthenticationRequiredException();
        }
        else if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, loggedInUser))
        {
            LoggerFactory.getLogger(AdminRequiredRequestFilter.class)
                    .error("{} do not have permission to access this resource", loggedInUser.getDisplayName());
            throw new AuthorizationException("No administrator privilege has been granted");
        }
    }
}
