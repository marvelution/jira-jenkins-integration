/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.features;

import java.util.*;
import javax.inject.*;

import org.marvelution.jji.api.features.*;

import com.atlassian.jira.config.properties.*;
import com.atlassian.plugin.osgi.bridge.external.*;
import com.atlassian.plugin.spring.scanner.annotation.export.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;

import static org.marvelution.jji.api.features.FeatureFlags.*;

@Named
@ExportAsService(Features.class)
public class ServerFeatures
		extends Features
{

	private final JiraProperties jiraProperties;
	private final PluginRetrievalService pluginRetrievalService;

	@Inject
	public ServerFeatures(
			FeatureStore featureStore,
			@ComponentImport JiraProperties jiraProperties,
			@ComponentImport PluginRetrievalService pluginRetrievalService)
	{
		super(featureStore);
		this.jiraProperties = jiraProperties;
		this.pluginRetrievalService = pluginRetrievalService;
	}

	@Override
	protected Set<FeatureFlag> loadFeatureFlags()
	{
		LOGGER.info("Loading features...");
		Set<FeatureFlag> features = new HashSet<>();
		LOGGER.debug("Loading Jira instance defined features...");
		jiraProperties.getProperties()
				.entrySet()
				.stream()
				.filter(property -> property.getKey() instanceof String)
				.filter(property -> ((String) property.getKey()).startsWith(FEATURE_FLAG_PROPERTY_PREFIX))
				.forEach(feature -> {
					String flag = ((String) feature.getKey()).substring(FEATURE_FLAG_PROPERTY_PREFIX.length());
					boolean state = Boolean.parseBoolean((String) feature.getValue());
					features.add(new FeatureFlag(flag, state));
				});
		LOGGER.debug("Loading add-on defined features...");
		pluginRetrievalService.getPlugin()
				.getPluginInformation()
				.getParameters()
				.entrySet()
				.stream()
				.filter(property -> property.getKey().startsWith(FEATURE_FLAG_PROPERTY_PREFIX))
				.forEach(feature -> {
					String flag = feature.getKey().substring(FEATURE_FLAG_PROPERTY_PREFIX.length());
					String[] values = feature.getValue().split(",");
					boolean state = Boolean.parseBoolean(values[0]);
					boolean overrideable = values.length != 2 || Boolean.parseBoolean(values[1]);
					features.add(new FeatureFlag(flag, state, overrideable));
				});
		LOGGER.info("Loaded {} feature flags.", features.size());
		return features;
	}
}
