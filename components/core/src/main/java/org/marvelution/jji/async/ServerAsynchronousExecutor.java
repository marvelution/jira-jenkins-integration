/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.async;

import java.util.concurrent.*;
import jakarta.annotation.Nonnull;
import javax.inject.Named;

import com.atlassian.jira.util.thread.JiraThreadLocalUtil;
import com.atlassian.jira.util.thread.JiraThreadLocalUtilImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;

/**
 * @author Mark Rekveld
 * @since 4.0.0
 */
@Named("asyncExecutor")
public class ServerAsynchronousExecutor
        implements Executor
{

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerAsynchronousExecutor.class);
    private final ExecutorService executorService;
    private final JiraThreadLocalUtil jiraThreadLocalUtil;

    public ServerAsynchronousExecutor()
    {
        executorService = new ThreadPoolExecutor(1,
                10,
                0,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                namedThreadFactory("AsyncExecutorServiceThread"),
                new ThreadPoolExecutor.CallerRunsPolicy());
        jiraThreadLocalUtil = new JiraThreadLocalUtilImpl();
    }

    @Override
    public void execute(
            @Nonnull
            Runnable command)
    {
        executorService.execute(() -> {
            jiraThreadLocalUtil.preCall();
            try
            {
                command.run();
            }
            finally
            {
                jiraThreadLocalUtil.postCall(LOGGER);
            }
        });
    }
}
