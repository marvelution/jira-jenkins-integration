/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.license;

import java.util.*;
import javax.inject.*;

import com.atlassian.plugin.*;
import com.atlassian.plugin.module.*;
import com.atlassian.plugin.osgi.bridge.external.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import com.atlassian.upm.api.license.*;
import com.atlassian.upm.api.license.entity.*;
import com.atlassian.upm.api.util.*;

@Named
public class DefaultLicenseHelper
		implements LicenseHelper
{

	static final String ATLASSIAN_LICENSING_ENABLED = "atlassian-licensing-enabled";
	private final PluginLicenseManager licenseManager;
	private final HostLicenseInformation hostLicenseInformation;
	private final boolean licensed;

	@Inject
	public DefaultLicenseHelper(
			@ComponentImport PluginRetrievalService pluginRetrievalService,
			@ComponentImport PluginLicenseManager licenseManager,
			@ComponentImport HostLicenseInformation hostLicenseInformation)
	{
		this(pluginRetrievalService.getPlugin(), Optional.of(licenseManager), Optional.of(hostLicenseInformation));
	}

	DefaultLicenseHelper(Plugin plugin)
	{
		this(plugin, Optional.empty(), Optional.empty());
	}

	private DefaultLicenseHelper(
			Plugin plugin,
			Optional<PluginLicenseManager> licenseManager,
			Optional<HostLicenseInformation> hostLicenseInformation)
	{
		this.licenseManager = licenseManager.orElseGet(() -> getContainerManager(plugin, PluginLicenseManager.class));
		this.hostLicenseInformation = hostLicenseInformation.orElseGet(() -> getContainerManager(plugin, HostLicenseInformation.class));
		licensed = Optional.of(plugin)
				.map(com.atlassian.plugin.Plugin::getPluginInformation)
				.map(PluginInformation::getParameters)
				.map(parameters -> parameters.getOrDefault(ATLASSIAN_LICENSING_ENABLED, "false"))
				.map(Boolean::parseBoolean)
				.orElse(Boolean.FALSE);

		if (licensed && this.licenseManager == null)
		{
			throw new IllegalArgumentException("missing PluginLicenseManager");
		}
	}

	private static <T> T getContainerManager(
			Plugin plugin,
			Class<T> managerClass)
	{
		return Optional.of(plugin)
				.filter(ContainerManagedPlugin.class::isInstance)
				.map(ContainerManagedPlugin.class::cast)
				.map(ContainerManagedPlugin::getContainerAccessor)
				.flatMap(containerAccessor -> containerAccessor.getBeansOfType(managerClass).stream().findFirst())
				.orElse(null);
	}

	@Override
	public boolean isActive()
	{
		if (licensed)
		{
			Optional<PluginLicense> license = getLicense();
			if (license.isEmpty()) {
				return false;
			}
			else if (hostLicenseInformation.isDataCenter())
			{
				return license.filter(PluginLicense::isValid).filter(PluginLicense::isActive).isPresent();
			}
			else
			{
				return license.map(PluginLicense::getError)
						.flatMap(this::asOptional)
						// Allow expired licenses and licenses that for older app versions.
						.filter(error -> error != LicenseError.EXPIRED && error != LicenseError.VERSION_MISMATCH)
						// If there is a different error then the license is not valid.
						.isEmpty();
			}
		}
		else
		{
			return true;
		}
	}

	@Override
	public boolean isExpired()
	{
		return getLicense().filter(PluginLicense::isMaintenanceExpired).isPresent();
	}

	@Override
	public boolean isNearlyExpired()
	{
		return getLicense().filter(license -> !license.isMaintenanceExpired())
				.flatMap(PluginLicense::getDurationBeforeMaintenanceExpiry)
				.filter(period -> {
					try
					{
						return period.toDays() <= 7;
					}
					catch (Exception e)
					{
						// Joda time throws an exception when the period has months or years in it.
						return false;
					}
				})
				.isPresent();
	}

	private Optional<PluginLicense> getLicense()
	{
		return Optional.ofNullable(licenseManager).map(PluginLicenseManager::getLicense).flatMap(this::asOptional);
	}

	@SuppressWarnings("deprecation")
    private <T> Optional<T> asOptional(Option<T> option)
	{
		return Optional.ofNullable(option).filter(Option::isDefined).map(Option::get);
	}
}
