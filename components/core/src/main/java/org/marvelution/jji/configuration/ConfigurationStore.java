/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.configuration;

import java.util.*;
import java.util.function.*;
import javax.annotation.*;

import com.atlassian.sal.api.pluginsettings.*;
import org.apache.commons.lang3.*;

/**
 * @author Mark Rekveld
 * @since 4.1.0
 */
public class ConfigurationStore
{

	private final PluginSettingsFactory pluginSettingsFactory;
	private final String prefix;
	private transient PluginSettings pluginSettings;

	public ConfigurationStore(
			PluginSettingsFactory pluginSettingsFactory,
			String prefix)
	{
		this.pluginSettingsFactory = pluginSettingsFactory;
		this.prefix = prefix;
	}

	public String get(String key)
	{
		return get(null, key);
	}

	public String get(
			@Nullable String scope,
			String key)
	{
		return (String) getPluginSettings(scope).get(getKey(key));
	}

	public <T> Optional<T> get(
			String key,
			Function<String, T> converter)
	{
		return get(null, key, converter);
	}

	public <T> Optional<T> get(
			@Nullable String scope,
			String key,
			Function<String, T> converter)
	{
		return Optional.ofNullable(get(scope, key)).map(converter);
	}

	public String put(
			String key,
			String value)
	{
		return put(null, key, value);
	}

	public String put(
			@Nullable String scope,
			String key,
			String value)
	{
		return (String) getPluginSettings(scope).put(getKey(key), value);
	}

	public String remove(
			String key)
	{
		return remove(null, key);
	}

	public String remove(
			@Nullable String scope,
			String key)
	{
		return (String) getPluginSettings(scope).remove(getKey(key));
	}

	private String getKey(String key)
	{
		if (key.startsWith(prefix))
		{
			return key;
		}
		else
		{
			return prefix + key;
		}
	}

	private PluginSettings getPluginSettings(@Nullable String scope)
	{
		if (StringUtils.isBlank(scope))
		{
			if (pluginSettings == null)
			{
				pluginSettings = pluginSettingsFactory.createGlobalSettings();
			}
			return pluginSettings;
		}
		else
		{
			return pluginSettingsFactory.createSettingsForKey(scope);
		}
	}
}
