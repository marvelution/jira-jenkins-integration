/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.cluster;

import java.util.*;
import java.util.concurrent.atomic.*;
import javax.annotation.*;

import org.marvelution.jji.events.*;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.cluster.*;
import com.atlassian.jira.event.cluster.*;
import org.slf4j.*;

/**
 * Base {@link ClusterMessageConsumer} that will register itself using pre/post construction hooks and node activation events.
 *
 * @author Mark Rekveld
 * @since 4.0.0
 */
@EventComponent
public abstract class AbstractClusterMessageConsumer implements ClusterMessageConsumer {

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	private final ClusterMessagingService messagingService;
	private final String channel;
	private final AtomicBoolean initialized = new AtomicBoolean();

	protected AbstractClusterMessageConsumer(ClusterMessagingService messagingService, String channel) {
		this.messagingService = messagingService;
		this.channel = channel;
	}

	@Override
	public void receive(String channel, String message, String senderId) {
		if (Objects.equals(this.channel, channel)) {
			logger.debug("Received message '{}' from {}.", message, senderId);
			onReceive(message, senderId);
		} else {
			logger.debug("Ignoring message from {} on channel {}", senderId, channel);
		}
	}

	protected abstract void onReceive(String message, String senderId);

	@PostConstruct
	public void initialize() {
		if (initialized.compareAndSet(false, true)) {
			logger.info("Registering message listener on channel {}.", channel);
			messagingService.registerListener(channel, this);
		}
	}

	@PreDestroy
	public void destroy() {
		if (initialized.compareAndSet(true, false)) {
			logger.info("Unregistering message listener from channel {}.", channel);
			messagingService.unregisterListener(channel, this);
		}
	}

	@EventListener
	public void nodeActivated(NodeActivatedEvent event) {
		logger.debug("Received node activated event {}", event);
		initialize();
	}

	@EventListener
	public void nodePassivating(NodePassivatingEvent event) {
		logger.debug("Received node passivating event {}", event);
		destroy();
	}
}
