/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.features;

import java.util.*;
import javax.inject.*;

import org.marvelution.jji.api.features.*;
import org.marvelution.jji.configuration.*;

import com.atlassian.sal.api.pluginsettings.*;

import static org.marvelution.jji.api.features.FeatureFlags.*;

@Named
public class ServerFeatureStore
		implements FeatureStore
{

	private final ConfigurationStore store;

	@Inject
	public ServerFeatureStore(PluginSettingsFactory pluginSettingsFactory)
	{
		store = new ConfigurationStore(pluginSettingsFactory, FEATURE_FLAG_PROPERTY_PREFIX);
	}

	@Override
	public Optional<Boolean> get(
			String scope,
			String key)
	{
		return store.get(key, Boolean::parseBoolean);
	}

	@Override
	public void set(
			String scope,
			String feature,
			boolean state)
	{
		store.put(feature, Boolean.toString(state));
	}

	@Override
	public void remove(
			String scope,
			String feature)
	{
		store.remove(feature);
	}
}
