/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.function.*;

import org.marvelution.jji.data.access.model.*;
import org.marvelution.jji.model.*;

import com.atlassian.pocketknife.api.querydsl.*;
import com.atlassian.pocketknife.api.querydsl.stream.*;
import com.atlassian.pocketknife.api.querydsl.util.*;
import com.querydsl.core.types.Predicate;
import org.slf4j.*;

/**
 * Base DAO using Atlassian PocketKnife QueryDSL.
 *
 * @author Mark Rekveld
 * @since 4.7.0
 */
public abstract class AbstractPocketKnifeDAO<E extends HasId<E>, T extends Table<E, T>>
{

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	protected final DatabaseAccessor databaseAccessor;
	protected final StreamingQueryFactory queryFactory;
	protected final T table;

	protected AbstractPocketKnifeDAO(
			DatabaseAccessor databaseAccessor,
			StreamingQueryFactory queryFactory,
			T table)
	{
		this.databaseAccessor = databaseAccessor;
		this.queryFactory = queryFactory;
		this.table = table;
	}

	protected long count(Predicate... where)
	{
		return runInTransaction(conn -> conn.select(table.id()).from(table).where(where).fetchCount());
	}

	public E save(E entity)
	{
		return databaseAccessor.runInTransaction(conn -> saveInTransaction(entity.copy(), conn), OnRollback.NOOP);
	}

	protected E saveInTransaction(
			E entity,
			DatabaseConnection conn)
	{
		long count = entity.getId() != null ? conn.select(table.id()).from(table).where(table.id().eq(entity.getId())).fetchCount() : 0;
		if (count == 0)
		{
			table.insert(conn, entity).execute();
		}
		else
		{
			table.update(conn, entity).execute();
		}
		return entity;
	}

	protected void delete(String id)
	{
		delete(table, id);
	}

	protected void delete(Predicate... where)
	{
		delete(table, where);
	}

	protected <T1 extends Table<?, ?>> void delete(
			T1 table,
			String id)
	{
		runInTransaction(conn -> table.delete(conn, id).execute());
	}

	protected <T1 extends Table<?, ?>> void delete(
			T1 table,
			Predicate... where)
	{
		runInTransaction(conn -> conn.delete(table).where(where).execute());
	}

	protected <R> R runInTransaction(Function<DatabaseConnection, R> callback)
	{
		return databaseAccessor.runInTransaction(callback, OnRollback.NOOP);
	}
}
