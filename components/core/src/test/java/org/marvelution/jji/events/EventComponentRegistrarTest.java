/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.events;

import org.marvelution.testing.TestSupport;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

/**
 * Tests for {@link EventComponentRegistrar}.
 *
 * @author Mark Rekveld
 * @since 3.7.0
 */
class EventComponentRegistrarTest
        extends TestSupport
{

    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private PluginEventManager pluginEventManager;
    private EventComponentRegistrar registrar;

    @BeforeEach
    void setUp()
    {
        registrar = new EventComponentRegistrar(eventPublisher, pluginEventManager);
    }

    @Test
    void testRegisterEventComponent()
    {
        testRegisterEventListener(new ClassEventListener());

        verifyNoMoreInteractions(eventPublisher);
        verifyNoInteractions(pluginEventManager);
    }

    @Test
    void testRegisterEventListener()
    {
        testRegisterEventListener(new MethodEventListener());

        verifyNoMoreInteractions(eventPublisher);
        verifyNoInteractions(pluginEventManager);
    }

    private void testRegisterEventListener(Object listener)
    {
        assertThat(registrar.postProcessBeforeInitialization(listener, "name"), is(listener));

        verifyNoInteractions(eventPublisher);

        assertThat(registrar.postProcessAfterInitialization(listener, "name"), is(listener));

        verify(eventPublisher).register(listener);

        registrar.postProcessBeforeDestruction(listener, "name");

        verify(eventPublisher).unregister(listener);
    }

    @Test
    void testRegisterPluginEventListener()
    {
        MethodPluginEventListener listener = new MethodPluginEventListener();

        assertThat(registrar.postProcessBeforeInitialization(listener, "name"), is(listener));

        verifyNoInteractions(pluginEventManager);

        assertThat(registrar.postProcessAfterInitialization(listener, "name"), is(listener));

        verify(pluginEventManager).register(listener);

        registrar.postProcessBeforeDestruction(listener, "name");

        verify(pluginEventManager).unregister(listener);
        verifyNoMoreInteractions(pluginEventManager);
        verifyNoInteractions(eventPublisher);
    }

    @Test
    void testRegisterCombinedEventListener()
    {
        CombinedMethodListener listener = new CombinedMethodListener();

        assertThat(registrar.postProcessBeforeInitialization(listener, "name"), is(listener));

        verifyNoInteractions(eventPublisher, pluginEventManager);

        assertThat(registrar.postProcessAfterInitialization(listener, "name"), is(listener));

        verify(eventPublisher).register(listener);
        verify(pluginEventManager).register(listener);

        registrar.postProcessBeforeDestruction(listener, "name");

        verify(eventPublisher).unregister(listener);
        verify(pluginEventManager).unregister(listener);

        verifyNoMoreInteractions(eventPublisher, pluginEventManager);
    }

    @EventComponent
    public static class ClassEventListener
    {}

    public static class MethodEventListener
    {

        @EventListener
        public void onEvent(Object event)
        {
        }
    }

    public static class MethodPluginEventListener
    {

        @PluginEventListener
        public void onEvent(Object event)
        {
        }
    }

    public static class CombinedMethodListener
    {

        @EventListener
        public void onEvent(Object event)
        {
        }

        @PluginEventListener
        public void onPluginEvent(Object event)
        {
        }
    }
}
