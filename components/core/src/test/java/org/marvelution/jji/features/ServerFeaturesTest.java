/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.features;

import java.util.*;

import org.marvelution.jji.api.features.*;
import org.marvelution.testing.*;

import com.atlassian.jira.config.properties.*;
import com.atlassian.plugin.*;
import com.atlassian.plugin.osgi.bridge.external.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import static org.marvelution.jji.api.features.FeatureFlags.*;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

class ServerFeaturesTest
		extends TestSupport
{

	@Mock
	private JiraProperties jiraProperties;
	@Mock
	private PluginRetrievalService pluginRetrievalService;
	@Mock
	private Plugin plugin;
	private ServerFeatures serverFeatures;

	@BeforeEach
	void setUp()
	{
		serverFeatures = new ServerFeatures(new InMemoryFeatureStore(), jiraProperties, pluginRetrievalService);
		doReturn(plugin).when(pluginRetrievalService).getPlugin();
	}

	@Test
	void testLoadingFeatures()
	{
		PluginInformation information = new PluginInformation();
		information.addParameter(FEATURE_FLAG_PROPERTY_PREFIX + DYNAMIC_UI_ELEMENTS, "true");
		information.addParameter(FEATURE_FLAG_PROPERTY_PREFIX + JIRA_JENKINS_AUTOMATION, "true,false");
		information.addParameter(FEATURE_FLAG_PROPERTY_PREFIX + "test-feature", "true");
		information.addParameter("atlassian-licensing-enabled", "true");
		information.addParameter("atlassian-data-center-status", "compatible");
		when(plugin.getPluginInformation()).thenReturn(information);

		Properties properties = new Properties();
		properties.put(FEATURE_FLAG_PROPERTY_PREFIX + "test-feature", "false");
		properties.put(FEATURE_FLAG_PROPERTY_PREFIX + DC_LOCAL_QUEUEING_DISABLED, "true");
		properties.put("some.other.key", "with its value");
		when(jiraProperties.getProperties()).thenReturn(properties);

		Set<String> features = serverFeatures.getAllEnabledFeatures();
		assertThat(features.size(), is(3));
		assertThat(features, hasItem(JIRA_JENKINS_AUTOMATION));
		assertThat(features, hasItem(DC_LOCAL_QUEUEING_DISABLED));
		assertThat(features, hasItem(DYNAMIC_UI_ELEMENTS));

		Set<FeatureFlag> systemFeatureFlags = serverFeatures.getSystemFeatureFlags();
		assertThat(systemFeatureFlags, hasSize(3));
		assertThat(systemFeatureFlags, hasItem(new FeatureFlag(JIRA_JENKINS_AUTOMATION, true, false)));
		assertThat(systemFeatureFlags, hasItem(new FeatureFlag(DC_LOCAL_QUEUEING_DISABLED, true, false)));
		assertThat(systemFeatureFlags, hasItem(new FeatureFlag("test-feature", false, false)));

		Set<FeatureFlag> siteFeatureFlags = serverFeatures.getSiteFeatureFlags();
		assertThat(siteFeatureFlags, hasSize(1));
		assertThat(siteFeatureFlags, hasItem(new FeatureFlag(DYNAMIC_UI_ELEMENTS, true, true)));
	}
}
