/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.*;

import org.marvelution.jji.api.*;
import org.marvelution.testing.*;

import com.atlassian.sal.api.message.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AbstractUpgradeTaskTest
		extends TestSupport
{

	private static final String TEST_ADDON_KEY = "test.addon.key";
	@Mock
	private AppState appState;
	private T100_UpgradeTaskTest task;

	@BeforeEach
	void setUp()
	{
		task = new T100_UpgradeTaskTest(appState);
	}

	@Test
	void testGetBuildNumber()
	{
		assertThat(task.getBuildNumber()).isEqualTo(100);
	}

	@Test
	void testGetShortDescription()
	{
		assertThat(task.getShortDescription()).isEqualTo("Upgrade Task Test");
	}

	@Test
	void testGetPluginKey()
	{
		when(appState.getKey()).thenReturn(TEST_ADDON_KEY);
		assertThat(task.getPluginKey()).isEqualTo(TEST_ADDON_KEY);
	}

	private static class T100_UpgradeTaskTest
			extends AbstractUpgradeTask
	{

		public T100_UpgradeTaskTest(AppState appState)
		{
			super(appState);
		}

		@Override
		public Collection<Message> doUpgrade()
		{
			return null;
		}
	}
}
