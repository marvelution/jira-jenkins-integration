/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.license;

import java.time.Duration;
import java.util.Optional;
import java.util.stream.Stream;

import org.marvelution.testing.TestSupport;

import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService;
import com.atlassian.upm.api.license.HostLicenseInformation;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.LicenseError;
import com.atlassian.upm.api.license.entity.PluginLicense;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.Mock;

import static com.atlassian.upm.api.util.Option.none;
import static com.atlassian.upm.api.util.Option.option;
import static java.util.Collections.singleton;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.marvelution.jji.license.DefaultLicenseHelper.ATLASSIAN_LICENSING_ENABLED;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link DefaultLicenseHelper}.
 *
 * @author Mark Rekveld
 * @since 3.7.0
 */
class DefaultLicenseHelperTest
        extends TestSupport
{

    @Mock
    private PluginLicenseManager licenseManager;
    @Mock
    private HostLicenseInformation hostLicenseInformation;
    @Mock
    private com.atlassian.plugin.module.ContainerManagedPlugin plugin;
    @Mock
    private ContainerAccessor containerAccessor;
    private PluginInformation pluginInformation;

    @BeforeEach
    void setUp()
    {
        pluginInformation = new PluginInformation();
        when(plugin.getPluginInformation()).thenReturn(pluginInformation);
    }

    static class InvalidLicenseErrors
            implements ArgumentsProvider
    {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context)
        {
            return Stream.of(LicenseError.values())
                    .filter(error -> error != LicenseError.EXPIRED && error != LicenseError.VERSION_MISMATCH)
                    .map(Arguments::of);
        }
    }

    static class ValidLicenseErrors
            implements ArgumentsProvider
    {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context)
        {
            return Stream.of(LicenseError.EXPIRED, LicenseError.VERSION_MISMATCH)
                    .map(Arguments::of);
        }
    }

    @Nested
    class Licensed
    {

        @Mock
        private PluginLicense license;
        private DefaultLicenseHelper licenseHelper;

        @BeforeEach
        void setUp()
        {
            pluginInformation.addParameter(ATLASSIAN_LICENSING_ENABLED, "true");
            when(plugin.getContainerAccessor()).thenReturn(containerAccessor);
            when(containerAccessor.getBeansOfType(PluginLicenseManager.class)).thenReturn(singleton(licenseManager));
            when(containerAccessor.getBeansOfType(HostLicenseInformation.class)).thenReturn(singleton(hostLicenseInformation));
            licenseHelper = new DefaultLicenseHelper(plugin);
        }

        @Test
        void testNoLicensePresent()
        {
            assertThat(licenseHelper.isActive(), is(false));
        }

        @Test
        void testNearlyExpiredLicense()
        {
            when(licenseManager.getLicense()).thenReturn(option(license));
            when(license.getDurationBeforeMaintenanceExpiry()).thenReturn(Optional.of(Duration.ofDays(7)))
                    .thenReturn(Optional.of(Duration.ofDays(8)));

            assertThat(licenseHelper.isNearlyExpired(), is(true));
            assertThat(licenseHelper.isNearlyExpired(), is(false));
        }

        @Test
        void testNearlyExpired_ExpiredLicense()
        {
            when(licenseManager.getLicense()).thenReturn(option(license));
            when(license.isMaintenanceExpired()).thenReturn(true);

            assertThat(licenseHelper.isNearlyExpired(), is(false));
        }

        @Test
        void testExpired_ExpiredLicense()
        {
            when(licenseManager.getLicense()).thenReturn(option(license));
            when(license.isMaintenanceExpired()).thenReturn(true);

            assertThat(licenseHelper.isExpired(), is(true));
        }

        @Test
        void testExpired()
        {
            when(licenseManager.getLicense()).thenReturn(option(license));
            when(license.isMaintenanceExpired()).thenReturn(false);

            assertThat(licenseHelper.isExpired(), is(false));
        }

        @Test
        void testIsActive_DataCenter()
        {
            when(hostLicenseInformation.isDataCenter()).thenReturn(true);
            when(licenseManager.getLicense()).thenReturn(option(license));

            when(license.isValid()).thenReturn(true);
            when(license.isActive()).thenReturn(true);

            assertThat(licenseHelper.isActive(), is(true));
        }

        @Test
        void testInvalidLicense_DataCenter()
        {
            when(hostLicenseInformation.isDataCenter()).thenReturn(true);
            when(licenseManager.getLicense()).thenReturn(option(license));
            when(license.isValid()).thenReturn(false);

            assertThat(licenseHelper.isActive(), is(false));
        }

        @Test
        void testInactiveLicense_DataCenter()
        {
            when(hostLicenseInformation.isDataCenter()).thenReturn(true);
            when(licenseManager.getLicense()).thenReturn(option(license));
            when(license.isValid()).thenReturn(true);
            when(license.isActive()).thenReturn(false);

            assertThat(licenseHelper.isActive(), is(false));
        }

        @ParameterizedTest
        @ArgumentsSource(InvalidLicenseErrors.class)
        void testInvalidLicense_Server(LicenseError error)
        {
            when(hostLicenseInformation.isDataCenter()).thenReturn(false);
            when(licenseManager.getLicense()).thenReturn(option(license));
            when(license.getError()).thenReturn(option(error));

            assertThat(licenseHelper.isActive(), is(false));
        }

        @ParameterizedTest
        @ArgumentsSource(ValidLicenseErrors.class)
        void testIsActive_Server(LicenseError error)
        {
            when(hostLicenseInformation.isDataCenter()).thenReturn(false);
            when(licenseManager.getLicense()).thenReturn(option(license));
            when(license.getError()).thenReturn(option(error));

            assertThat(licenseHelper.isActive(), is(true));
        }

        @Test
        void testIsActive_Server()
        {
            when(hostLicenseInformation.isDataCenter()).thenReturn(false);
            when(licenseManager.getLicense()).thenReturn(option(license));
            when(license.getError()).thenReturn(none());

            assertThat(licenseHelper.isActive(), is(true));
        }
    }

    @Nested
    class Unlicensed
    {

        private DefaultLicenseHelper licenseHelper;

        @BeforeEach
        void setUp()
        {
            pluginInformation.addParameter(ATLASSIAN_LICENSING_ENABLED, "false");
            licenseHelper = new DefaultLicenseHelper(plugin);
        }

        @Test
        void testActive()
        {
            assertThat(licenseHelper.isActive(), is(true));
        }

        @Test
        void testNearlyExpired()
        {
            assertThat(licenseHelper.isNearlyExpired(), is(false));
        }
    }

    @Nested
    class DependencyInjection
    {

        @Mock
        private PluginRetrievalService pluginRetrievalService;

        @BeforeEach
        void setUp()
        {
            pluginInformation.addParameter(ATLASSIAN_LICENSING_ENABLED, "false");
            when(pluginRetrievalService.getPlugin()).thenReturn(plugin);
        }

        @Test
        void testCreate()
        {
            DefaultLicenseHelper licenseHelper = new DefaultLicenseHelper(pluginRetrievalService, licenseManager, hostLicenseInformation);
            assertThat(licenseHelper.isActive(), is(true));
        }
    }

    @Nested
    class NotContainerManagedPlugin
    {

        @Test
        void testCreate_NotLicensed()
        {
            pluginInformation.addParameter(ATLASSIAN_LICENSING_ENABLED, "false");

            DefaultLicenseHelper licenseHelper = new DefaultLicenseHelper(plugin);
            assertThat(licenseHelper.isActive(), is(true));
        }

        @Test
        void testCreate_Licensed_MissingLicenseManager()
        {
            pluginInformation.addParameter(ATLASSIAN_LICENSING_ENABLED, "true");

            assertThrows(IllegalArgumentException.class, () -> new DefaultLicenseHelper(plugin), "missing PluginLicenseManager");
        }
    }

    @Nested
    class ContainerManagedPlugin
    {

        @BeforeEach
        void setUp()
        {
            when(plugin.getContainerAccessor()).thenReturn(containerAccessor);
            when(containerAccessor.getBeansOfType(PluginLicenseManager.class)).thenReturn(singleton(licenseManager));
            when(containerAccessor.getBeansOfType(HostLicenseInformation.class)).thenReturn(singleton(hostLicenseInformation));
        }

        @Test
        void testCreate_NotLicensed()
        {
            pluginInformation.addParameter(ATLASSIAN_LICENSING_ENABLED, "false");

            DefaultLicenseHelper licenseHelper = new DefaultLicenseHelper(plugin);
            assertThat(licenseHelper.isActive(), is(true));
        }

        @Test
        void testCreate_Licensed()
        {
            pluginInformation.addParameter(ATLASSIAN_LICENSING_ENABLED, "true");

            DefaultLicenseHelper licenseHelper = new DefaultLicenseHelper(plugin);
            assertThat(licenseHelper.isActive(), is(false));
        }
    }
}
