/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.marvelution.jji.api.AppState;
import org.marvelution.jji.license.LicenseHelper;
import org.marvelution.testing.TestSupport;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Answers;
import org.mockito.Mock;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

class DefaultAppStateTest
        extends TestSupport
{

    @Mock
    private LicenseHelper licenseHelper;
    @Mock
    private PluginRetrievalService pluginRetrievalService;
    private AppState appState;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Plugin plugin;

    @BeforeEach
    void setUp()
    {
        appState = new DefaultAppState(licenseHelper, pluginRetrievalService);
        when(pluginRetrievalService.getPlugin()).thenReturn(plugin);
    }

    @PluginStateTest
    void testIsEnabled(PluginState state)
    {
        when(plugin.getPluginState()).thenReturn(state);
        if (state == PluginState.ENABLED)
        {
            when(licenseHelper.isActive()).thenReturn(true);
        }

        assertThat(appState.isEnabled(), is(state == PluginState.ENABLED));
    }

    @Target(METHOD)
    @Retention(RUNTIME)
    @Documented
    @ParameterizedTest(name = "{0}")
    @EnumSource(PluginState.class)
    @interface PluginStateTest
    {}
}
