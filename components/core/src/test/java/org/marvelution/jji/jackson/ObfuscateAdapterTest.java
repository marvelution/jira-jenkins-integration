/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.jackson;

import java.io.IOException;

import org.marvelution.jji.model.ObfuscationTestSupport;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import org.junit.jupiter.api.BeforeEach;

import static com.fasterxml.jackson.databind.AnnotationIntrospector.pair;

public class ObfuscateAdapterTest
        extends ObfuscationTestSupport
{

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp()
    {
        objectMapper = new ObjectMapper();
        // Add Jackson default and JAX-B introspectors
        objectMapper.setAnnotationIntrospector(pair(new JacksonAnnotationIntrospector(),
                new JaxbAnnotationIntrospector(objectMapper.getTypeFactory())));
    }

    @Override
    protected String serialize(Object value)
            throws IOException
    {
        return objectMapper.writeValueAsString(value);
    }

    @Override
    protected <T> T deserialize(
            String serialized,
            Class<T> type)
            throws IOException
    {
        return objectMapper.readValue(serialized, type);
    }
}
