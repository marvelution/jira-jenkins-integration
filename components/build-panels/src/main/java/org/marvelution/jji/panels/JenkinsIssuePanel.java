/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.panels;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

import org.marvelution.jji.data.helper.BuildPanelModelHelper;
import org.marvelution.jji.data.helper.FeaturesEnabledHelper;
import org.marvelution.jji.data.helper.PanelBuild;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.IssueReference;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel3;
import com.atlassian.jira.plugin.issuetabpanel.GetActionsRequest;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.plugin.issuetabpanel.ShowPanelRequest;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.model.WebPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Collections.singletonList;
import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsFirst;
import static java.util.stream.Collectors.toList;

/**
 * The {@link AbstractIssueTabPanel3} implementation to get all the {@link Build}s related to the {@link Issue}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JenkinsIssuePanel
        extends AbstractIssueTabPanel3
{

    private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsIssuePanel.class);
    private final FeaturesEnabledHelper featuresEnabledHelper;
    private final BuildPanelModelHelper buildPanelModelHelper;
    private final DynamicWebInterfaceManager webInterfaceManager;

    public JenkinsIssuePanel(
            FeaturesEnabledHelper featuresEnabledHelper,
            BuildPanelModelHelper buildPanelModelHelper,
            @ComponentImport
            DynamicWebInterfaceManager webInterfaceManager)
    {
        this.featuresEnabledHelper = featuresEnabledHelper;
        this.buildPanelModelHelper = buildPanelModelHelper;
        this.webInterfaceManager = webInterfaceManager;
    }

    @Override
    public boolean showPanel(ShowPanelRequest showPanelRequest)
    {
        return featuresEnabledHelper.areFeaturesEnabledForIssue(showPanelRequest.issue());
    }

    @Override
    public List<IssueAction> getActions(GetActionsRequest getActionsRequest)
    {
        Issue issue = getActionsRequest.issue();
        LOGGER.debug("Looking for builds related to issue [{}]", issue.getKey());
        List<IssueAction> actions = buildPanelModelHelper.relatedBuilds(issue.getKey())
                .stream()
                .map(build -> getBuildActionHtml(build).map(html -> new BuildAction(html, build.getDate()))
                        .orElse(null))
                .filter(Objects::nonNull)
                .sorted(nullsFirst(comparing(BuildAction::getTimePerformed)))
                .collect(toList());
        return actions.isEmpty() ? singletonList(BuildAction.NO_BUILDS_ACTION) : actions;
    }

    /**
     * Generate an HTML View for the {@link Build} given
     */
    private Optional<String> getBuildActionHtml(PanelBuild build)
    {
        Map<String, Object> context = new HashMap<>();
        context.put("build", build);
        if (build.getIssueReferences()
                    .size() > 5)
        {
            context.put("projectKeys",
                    build.getIssueReferences()
                            .stream()
                            .map(IssueReference::getProjectKey)
                            .distinct()
                            .sorted()
                            .toList());
        }
        try
        {
            StringWriter writer = new StringWriter();
            for (WebPanel panel : webInterfaceManager.getDisplayableWebPanels("jenkins-panel-build-item", context))
            {
                panel.writeHtml(writer, context);
            }
            return Optional.of(writer.toString());
        }
        catch (IOException e)
        {
            LOGGER.warn("Failed to render html for build {} -> {}", build.getId(), e.getMessage());
            return Optional.empty();
        }
    }
}
