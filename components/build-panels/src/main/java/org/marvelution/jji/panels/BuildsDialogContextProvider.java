/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.panels;

import javax.inject.*;
import java.util.*;

import org.marvelution.jji.data.helper.*;

import com.atlassian.jira.issue.*;
import com.atlassian.plugin.*;
import com.atlassian.plugin.web.*;

import static java.util.Objects.*;

/**
 * {@link ContextProvider} for the dev status panel.
 *
 * @author Mark Rekveld
 * @since 3.5.0
 */
public class BuildsDialogContextProvider
        implements ContextProvider
{

    private final BuildPanelModelHelper helper;

    @Inject
    public BuildsDialogContextProvider(BuildPanelModelHelper helper)
    {
        this.helper = helper;
    }

    @Override
    public void init(Map<String, String> params)
            throws PluginParseException
    {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context)
    {
        Issue issue = requireNonNull(getIssue(context), "missing issue in panel context");
        return helper.modelForBuildsDialog(issue.getKey(), (String) context.get("tab"));
    }

    private Issue getIssue(Map<String, Object> context)
    {
        return (Issue) context.get("issue");
    }
}
