/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
require(['marvelution/form-dialog', 'jquery'], function (FormDialog, jQuery) {
    jQuery(document).on("click", ".jenkins_trigger_build", function (e) {
        e.preventDefault();
        const dialog = new FormDialog({
            id: "jenkins-trigger-build-dialog",
            refreshAfterSubmit: false,
            ajaxOptions: {
                url: this.href,
                data: {
                    decorator: "dialog",
                    inline: "false"
                }
            },
            onSuccessfulSubmit: function (data, xhr, textStatus, smartAjaxResult) {
                const form = this;
                $.each(data.triggers, function (index, trigger) {
                    let actions = '';
                    if (trigger.link) {
                        actions = '<ul class="aui-nav-actions-list"><li><a class="aui-button aui-button-link" href="' +
                            trigger.link+ '">' + AJS.I18n.getText('details') + '</a></ul>';
                    }
                    form.showMessage(trigger.message + actions, trigger.type.toLowerCase(), false);
                });
            }
        });
        dialog.show();
    });
});
