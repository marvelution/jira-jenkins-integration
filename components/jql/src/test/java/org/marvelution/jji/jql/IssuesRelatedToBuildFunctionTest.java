/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.jql;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.IssueLinkService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.testing.TestSupport;

import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static java.util.Arrays.stream;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Stream.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class IssuesRelatedToBuildFunctionTest
        extends TestSupport
{

    @Mock
    private ApplicationUser user;
    @Mock
    private QueryCreationContext queryCreationContext;
    @Mock
    private TerminalClause terminalClause;
    @Mock
    private JobService jobService;
    @Mock
    private BuildService buildService;
    @Mock
    private IssueLinkService issueLinkService;
    private IssuesRelatedToBuildFunction function;

    @BeforeEach
    void setUp()
    {
        I18nHelper i18nHelper = mock(I18nHelper.class,
                invocation -> stream(invocation.getArguments()).map(String::valueOf)
                        .collect(joining(".")));
        function = new IssuesRelatedToBuildFunction(jobService, buildService, issueLinkService)
        {
            @Override
            protected I18nHelper getI18n()
            {
                return i18nHelper;
            }
        };
    }

    private FunctionOperand newFunctionOperand(String... arguments)
    {
        return new FunctionOperand("issuesRelatedToBuild", arguments);
    }


    @Test
    void testValidate_InvalidArgumentCount()
    {
        // Verify no arguments
        MessageSet messages = function.validate(user, newFunctionOperand(), terminalClause);
        assertThat(messages.getErrorMessages(), hasSize(1));
        assertThat(messages.getErrorMessages(), hasItem("jql.invalid.number.of.arguments.1"));

        // Verify to many arguments
        messages = function.validate(user, newFunctionOperand("1", "2", "3", "4"), terminalClause);
        assertThat(messages.getErrorMessages(), hasSize(1));
        assertThat(messages.getErrorMessages(), hasItem("jql.invalid.number.of.arguments.1"));

        verifyNoInteractions(jobService, buildService);
    }

    @Nested
    class BuildRange
    {

        @Test
        void testValidate_UnknownJob()
        {
            MessageSet messages = function.validate(user, newFunctionOperand("unknown", "1", "10"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(1));
            assertThat(messages.getErrorMessages(), hasItem("jql.unknown.job.unknown"));
        }

        @Test
        void testValidate_InvalidRangeStart()
        {
            when(jobService.find("job")).thenReturn(singletonList(new Job().setId("1")
                    .setName("job")));

            MessageSet messages = function.validate(user, newFunctionOperand("job", "-1", "10"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(1));
            assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.range"));
            messages = function.validate(user, newFunctionOperand("job", "nan", "10"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(2));
            assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.nan"));
            assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.range"));
        }

        @Test
        void testValidate_InvalidRangeEnd()
        {
            when(jobService.find("job")).thenReturn(singletonList(new Job().setId("1")
                    .setName("job")));

            MessageSet messages = function.validate(user, newFunctionOperand("job", "1", "-10"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(1));
            assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.range"));
            messages = function.validate(user, newFunctionOperand("job", "1", "nan"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(2));
            assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.nan"));
            assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.range"));
        }

        @Test
        void testValidate_RangeEndBeforeStart()
        {
            when(jobService.find("job")).thenReturn(singletonList(new Job().setId("1")
                    .setName("job")));

            MessageSet messages = function.validate(user, newFunctionOperand("job", "10", "1"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(1));
            assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.range"));
        }

        @Test
        void testValidate()
        {
            when(jobService.find("job")).thenReturn(singletonList(new Job().setId("1")
                    .setName("job")));

            MessageSet messages = function.validate(user, newFunctionOperand("job", "1", "10"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(0));
        }

        @Test
        void testGetValues()
        {
            Job job = new Job().setId("1")
                    .setName("job");
            when(jobService.find("job")).thenReturn(singletonList(job));
            Set<Build> builds = of(1, 2, 3, 4).map(number -> new Build().setId("number")
                            .setJob(job)
                            .setNumber(number))
                    .collect(toSet());
            when(buildService.getAllInRange(eq(job), eq(1), eq(10))).thenReturn(builds);
            when(issueLinkService.getRelatedIssueKeys(any(Build.class))).then(invocation -> {
                int number = invocation.<Build>getArgument(0)
                        .getNumber();
                return of(number, number * 10).map(n -> "ISSUE-" + n)
                        .collect(toSet());
            });

            FunctionOperand functionOperand = newFunctionOperand("job", "1", "10");
            List<QueryLiteral> values = function.getValues(queryCreationContext, functionOperand, terminalClause);
            assertThat(values, hasSize(builds.size() * 2));
            builds.forEach(build -> {
                assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-" + build.getNumber())));
                assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-" + (build.getNumber() * 10))));
            });

            verify(jobService).find("job");
            verify(buildService).getAllInRange(eq(job), eq(1), eq(10));
            verify(issueLinkService, times(4)).getRelatedIssueKeys(any(Build.class));
            verifyNoMoreInteractions(buildService, jobService);
        }
    }

    @Nested
    class BuildByNumber
    {

        @Test
        void testValidate()
        {
            when(jobService.find("job")).thenReturn(singletonList(new Job().setId("1")
                    .setName("job")));

            MessageSet messages = function.validate(user, newFunctionOperand("job", "1"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(0));
        }

        @Test
        void testValidate_LastBuild()
        {
            when(jobService.find("job")).thenReturn(singletonList(new Job().setId("1")
                    .setName("job")));

            MessageSet messages = function.validate(user, newFunctionOperand("job", "lastBuild"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(0));
        }

        @Test
        void testValidate_UnknownJob()
        {
            // Verify an unknown job
            MessageSet messages = function.validate(user, newFunctionOperand("unknown", "1"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(1));
            assertThat(messages.getErrorMessages(), hasItem("jql.unknown.job.unknown"));
        }

        @Test
        void testValidate_InvalidBuild()
        {
            when(jobService.find("job")).thenReturn(singletonList(new Job().setId("1")
                    .setName("job")));

            MessageSet messages = function.validate(user, newFunctionOperand("job", "-1"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(1));
            assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.-1"));
            messages = function.validate(user, newFunctionOperand("job", "nan"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(1));
            assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.nan"));
        }

        @Test
        void testGetValues()
        {
            Job job = new Job().setId("1")
                    .setName("job")
                    .setLastBuild(10);
            when(jobService.find("job")).thenReturn(singletonList(job));
            when(buildService.get(eq(job), anyInt())).then(invocation -> {
                int number = invocation.getArgument(1);
                return Optional.of(new Build().setId("number")
                        .setJob(job)
                        .setNumber(number));
            });
            when(issueLinkService.getRelatedIssueKeys(any(Build.class))).then(invocation -> {
                int number = invocation.<Build>getArgument(0)
                        .getNumber();
                return of(number, number * 10).map(n -> "ISSUE-" + n)
                        .collect(toSet());
            });

            FunctionOperand functionOperand = newFunctionOperand("job", "1");
            List<QueryLiteral> values = function.getValues(queryCreationContext, functionOperand, terminalClause);
            assertThat(values, hasSize(2));
            assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-1")));
            assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-10")));

            functionOperand = newFunctionOperand("job", "lastBuild");
            values = function.getValues(queryCreationContext, functionOperand, terminalClause);
            assertThat(values, hasSize(2));
            assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-10")));
            assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-100")));
        }
    }

    @Nested
    class BuildById
    {

        @Test
        void testValidate()
        {
            when(buildService.get("1")).thenReturn(Optional.of(new Build().setId("1")
                    .setJob(new Job().setId("1"))
                    .setNumber(1)));

            MessageSet messages = function.validate(user, newFunctionOperand("1"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(0));
        }

        @Test
        void testValidate_UnknownId()
        {
            // Verify an unknown build id
            MessageSet messages = function.validate(user, newFunctionOperand("2"), terminalClause);
            assertThat(messages.getErrorMessages(), hasSize(1));
            assertThat(messages.getErrorMessages(), hasItem("jql.unknown.build.2"));
        }

        @Test
        void testGetValues()
        {
            Build build = new Build().setId("1")
                    .setJob(new Job().setId("1"))
                    .setNumber(1);
            when(buildService.get("1")).thenReturn(Optional.of(build));
            when(issueLinkService.getRelatedIssueKeys(build)).thenReturn(singleton("ISSUE-1"));

            FunctionOperand functionOperand = newFunctionOperand("1");
            List<QueryLiteral> values = function.getValues(queryCreationContext, functionOperand, terminalClause);
            assertThat(values, hasSize(1));
            assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-1")));
        }
    }
}
