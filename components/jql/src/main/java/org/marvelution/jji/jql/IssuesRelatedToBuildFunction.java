/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.jql;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Nonnull;

import org.marvelution.jji.data.services.api.IssueLinkService;
import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.model.Build;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;

import static java.lang.Integer.parseInt;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;

/**
 * JQL Function to located issues related to a Jenkins Build
 *
 * @author Mark Rekveld
 * @since 1.1.1
 */
public class IssuesRelatedToBuildFunction extends AbstractJqlFunction {

	private final JobService jobService;
	private final BuildService buildService;
	private final IssueLinkService issueLinkService;

	public IssuesRelatedToBuildFunction(JobService jobService, BuildService buildService, IssueLinkService issueLinkService) {
		this.jobService = jobService;
		this.buildService = buildService;
		this.issueLinkService = issueLinkService;
	}

	@Override
	@Nonnull
	public MessageSet validate(ApplicationUser user, @Nonnull FunctionOperand functionOperand, @Nonnull TerminalClause terminalClause) {
		List<String> arguments = functionOperand.getArgs();
		switch (arguments.size()) {
			case 3:
				return validateBuildRange(arguments);
			case 2:
				return validateBuildByNumber(arguments);
			case 1:
				return validateBuildById(arguments);
			default:
				MessageSet messages = new MessageSetImpl();
				messages.addErrorMessage(getI18n().getText("jql.invalid.number.of.arguments", getMinimumNumberOfExpectedArguments()));
				return messages;
		}
	}

	@Override
	@Nonnull
	public List<QueryLiteral> getValues(@Nonnull QueryCreationContext queryCreationContext, @Nonnull final FunctionOperand functionOperand,
	                                    @Nonnull TerminalClause terminalClause) {
		List<String> arguments = functionOperand.getArgs();
		List<Build> builds;
		if (arguments.size() == 3) {
			builds = getBuildsByRange(arguments);
		} else if (arguments.size() == 2) {
			builds = getBuildsByNumber(arguments);
		} else {
			builds = buildService.get(arguments.get(0))
			                     .map(Collections::singletonList)
			                     .orElseGet(ArrayList::new);
		}
		return unmodifiableList(builds.stream()
		                              .filter(Objects::nonNull)
		                              .flatMap(build -> issueLinkService.getRelatedIssueKeys(build).stream())
		                              .map(issueKey -> new QueryLiteral(functionOperand, issueKey))
		                              .collect(toList()));
	}

	private MessageSet validateBuildRange(List<String> arguments) {
		MessageSet messages = new MessageSetImpl();
		if (jobService.find(arguments.get(0)).isEmpty()) {
			messages.addErrorMessage(getI18n().getText("jql.unknown.job", arguments.get(0)));
		}
		int start = 0, end = 0;
		try {
			start = parseInt(arguments.get(1));
		} catch (NumberFormatException e) {
			messages.addErrorMessage(getI18n().getText("jql.invalid.build.number", arguments.get(1)));
		}
		try {
			end = parseInt(arguments.get(2));
		} catch (NumberFormatException e) {
			messages.addErrorMessage(getI18n().getText("jql.invalid.build.number", arguments.get(2)));
		}
		if (start <= 0 || end <= 0 || end <= start) {
			messages.addErrorMessage(getI18n().getText("jql.invalid.build.number.range"));
		}
		return messages;
	}

	private List<Build> getBuildsByRange(List<String> arguments) {
		return jobService.find(arguments.get(0)).stream()
		                 .flatMap(job -> buildService.getAllInRange(job, parseInt(arguments.get(1)), parseInt(arguments.get(2))).stream())
		                 .collect(toList());
	}

	private MessageSet validateBuildByNumber(List<String> arguments) {
		MessageSet messages = new MessageSetImpl();
		if (jobService.find(arguments.get(0)).isEmpty()) {
			messages.addErrorMessage(getI18n().getText("jql.unknown.job", arguments.get(0)));
		}
		if (!"lastBuild".equals(arguments.get(1))) {
			try {
				if (parseInt(arguments.get(1)) <= 0) {
					messages.addErrorMessage(getI18n().getText("jql.invalid.build.number", arguments.get(1)));
				}
			} catch (NumberFormatException e) {
				messages.addErrorMessage(getI18n().getText("jql.invalid.build.number", arguments.get(1)));
			}
		}
		return messages;
	}

	private List<Build> getBuildsByNumber(List<String> arguments) {
		int buildNumber = "lastBuild".equals(arguments.get(1)) ? -1 : parseInt(arguments.get(1));
		return jobService.find(arguments.get(0)).stream()
		                 .map(job -> buildService.get(job, buildNumber == -1 ? job.getLastBuild() : buildNumber))
		                 .filter(Optional::isPresent)
		                 .map(Optional::get)
		                 .collect(toList());
	}

	private MessageSet validateBuildById(List<String> arguments) {
		MessageSet messages = new MessageSetImpl();
		if (!buildService.get(arguments.get(0)).isPresent()) {
			messages.addErrorMessage(getI18n().getText("jql.unknown.build", arguments.get(0)));
		}
		return messages;
	}

	@Override
	public int getMinimumNumberOfExpectedArguments() {
		return 1;
	}

	@Override
	@Nonnull
	public JiraDataType getDataType() {
		return JiraDataTypes.ISSUE;
	}

}
