/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.data.containers;

import org.testcontainers.containers.*;
import org.testcontainers.utility.*;

import static org.marvelution.jji.test.data.TestContainerJdbcConfiguration.*;

public class JiraOracleContainer
		extends OracleContainer
{

	@SuppressWarnings("resource")
	public JiraOracleContainer()
	{
		super(DockerImageName.parse("gvenzl/oracle-xe").withTag("21-slim-faststart"));
		withDatabaseName(DEFAULT_DB);
		withUsername(DEFAULT_USER);
		withPassword(DEFAULT_PASSWORD);
	}
}
