/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.data;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;
import java.util.function.Function;

import com.atlassian.pocketknife.api.querydsl.*;
import com.atlassian.pocketknife.api.querydsl.configuration.ConfigurationEnrichment;
import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import com.atlassian.pocketknife.api.querydsl.util.OnRollback;
import com.atlassian.pocketknife.internal.querydsl.DatabaseAccessorImpl;
import com.atlassian.pocketknife.internal.querydsl.DatabaseConnectionConverterImpl;
import com.atlassian.pocketknife.internal.querydsl.EitherAwareDatabaseAccessorImpl;
import com.atlassian.pocketknife.internal.querydsl.OptionalAwareDatabaseAccessorImpl;
import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearerImpl;
import com.atlassian.pocketknife.internal.querydsl.configuration.ConfigurationEnrichmentImpl;
import com.atlassian.pocketknife.internal.querydsl.dialect.DefaultDialectConfiguration;
import com.atlassian.pocketknife.internal.querydsl.schema.DefaultSchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.JdbcTableInspector;
import com.atlassian.pocketknife.internal.querydsl.schema.ProductSchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.SchemaProvider;
import com.atlassian.pocketknife.test.util.querydsl.StandaloneTransactionalExecutorFactory;
import io.atlassian.fugue.Either;
import io.atlassian.fugue.Option;
import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.JdbcConfiguration;

public class StandaloneActiveObjectsDatabaseAccessor
        implements DatabaseAccessor, OptionalAwareDatabaseAccessor, EitherAwareDatabaseAccessor
{

    private final EntityManager entityManager;
    private final JdbcConfiguration jdbc;
    private final StandaloneTransactionalExecutorFactory executorFactory;
    private final DatabaseAccessor delegateDatabaseAccessor;
    private final OptionalAwareDatabaseAccessor optionalAwareDatabaseAccessor;
    private final EitherAwareDatabaseAccessor eitherAwareDatabaseAccessor;

    public StandaloneActiveObjectsDatabaseAccessor(
            EntityManager entityManager,
            JdbcConfiguration jdbc)
    {
        this.entityManager = entityManager;
        this.jdbc = jdbc;
        executorFactory = new StandaloneTransactionalExecutorFactory(Option.option(jdbc.getSchema()));
        DatabaseConnectionConverter connectionConverter = getDatabaseConnectionConverter();
        delegateDatabaseAccessor = new DatabaseAccessorImpl(connectionConverter, executorFactory, () -> {});
        optionalAwareDatabaseAccessor = new OptionalAwareDatabaseAccessorImpl(this);
        eitherAwareDatabaseAccessor = new EitherAwareDatabaseAccessorImpl(this);
    }

    private DatabaseConnectionConverter getDatabaseConnectionConverter()
    {
        SchemaProvider schemaProvider = new DefaultSchemaProvider(new ProductSchemaProvider(executorFactory),
                new JdbcTableInspector(),
                new PKQCacheClearerImpl(null));
        ConfigurationEnrichment configurationEnrichment = new ConfigurationEnrichmentImpl();
        DialectProvider dialectProvider = new DefaultDialectConfiguration(schemaProvider, configurationEnrichment);
        return new DatabaseConnectionConverterImpl(dialectProvider);
    }

    @Override
    public <T> T runInNewTransaction(
            Function<DatabaseConnection, T> callback,
            OnRollback onRollback)
    {
        return doWithConnection((connection) -> delegateCall(connection, callback, true, onRollback));
    }

    @Override
    public <T> T runInTransaction(
            Function<DatabaseConnection, T> callback,
            OnRollback onRollback)
    {
        return doWithConnection((connection) -> delegateCall(connection, callback, false, onRollback));
    }

    @Override
    public <L, R> Either<L, R> runInNewEitherAwareTransaction(
            Function<DatabaseConnection, Either<L, R>> callback,
            OnRollback onRollback)
    {
        return eitherAwareDatabaseAccessor.runInNewEitherAwareTransaction(callback, onRollback);
    }

    @Override
    public <L, R> Either<L, R> runInEitherAwareTransaction(
            Function<DatabaseConnection, Either<L, R>> callback,
            OnRollback onRollback)
    {
        return eitherAwareDatabaseAccessor.runInEitherAwareTransaction(callback, onRollback);
    }

    @Override
    public <T> Optional<T> runInNewOptionalAwareTransaction(
            Function<DatabaseConnection, Optional<T>> callback,
            OnRollback onRollback)
    {
        return optionalAwareDatabaseAccessor.runInNewOptionalAwareTransaction(callback, onRollback);
    }

    @Override
    public <T> Optional<T> runInOptionalAwareTransaction(
            Function<DatabaseConnection, Optional<T>> callback,
            OnRollback onRollback)
    {
        return optionalAwareDatabaseAccessor.runInOptionalAwareTransaction(callback, onRollback);
    }

    private <T> T delegateCall(
            Connection connection,
            Function<DatabaseConnection, T> callback,
            boolean requireNew,
            OnRollback onRollback)
    {
        try
        {
            executorFactory.setConnection(Option.some(connection));
            if (requireNew)
            {
                return delegateDatabaseAccessor.runInNewTransaction(callback, onRollback);
            }
            else
            {
                return delegateDatabaseAccessor.runInTransaction(callback, onRollback);
            }
        }
        finally
        {
            executorFactory.setConnection(Option.none());
        }
    }

    public <T> T doWithConnection(Function<Connection, T> callback)
    {
        try
        {
            Connection connection = getConnection();
            Throwable exception = null;

            T result;
            try
            {
                result = callback.apply(connection);
            }
            catch (Throwable e)
            {
                exception = e;
                throw e;
            }
            finally
            {
                if (connection != null)
                {
                    if (exception != null)
                    {
                        try
                        {
                            connection.close();
                        }
                        catch (Throwable e)
                        {
                            exception.addSuppressed(e);
                        }
                    }
                    else
                    {
                        connection.close();
                    }
                }

            }
            return result;
        }
        catch (SQLException e)
        {
            throw throwConnectionException(e, "Error closing the connection:'%s' user='%s' password='%s'");
        }
    }

    private Connection getConnection()
    {
        try
        {
            return entityManager.getProvider()
                    .getConnection();
        }
        catch (SQLException e)
        {
            throw throwConnectionException(e, "unable to connect to database url:'%s' user='%s' password='%s'");
        }
    }

    private RuntimeException throwConnectionException(
            SQLException exception,
            String format)
    {
        return new RuntimeException(String.format(format, jdbc.getUrl(), jdbc.getUsername(), jdbc.getPassword()), exception);
    }
}
