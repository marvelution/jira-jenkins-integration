/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.data;

import java.io.*;
import java.lang.annotation.*;
import java.sql.*;
import java.util.*;
import javax.inject.Provider;

import org.marvelution.jji.test.data.converters.*;

import com.atlassian.activeobjects.external.*;
import com.atlassian.activeobjects.test.*;
import com.atlassian.pocketknife.api.querydsl.*;
import com.atlassian.pocketknife.api.querydsl.stream.*;
import com.atlassian.pocketknife.internal.querydsl.stream.*;
import com.google.inject.*;
import net.java.ao.*;
import net.java.ao.builder.*;
import net.java.ao.schema.*;
import net.java.ao.test.converters.NameConverters;
import net.java.ao.test.jdbc.*;
import org.junit.jupiter.api.extension.*;

public class ActiveObjectsExtension
		implements ParameterResolver, BeforeEachCallback
{

	private static final ExtensionContext.Namespace NAMESPACE = ExtensionContext.Namespace.create(ActiveObjectsExtension.class);

	@Override
	public boolean supportsParameter(
			ParameterContext parameterContext,
			ExtensionContext extensionContext)
	{
		Class<?> type = parameterContext.getParameter().getType();
		if (type == EntityManager.class || type == EntityManagerContext.class)
		{
			return true;
		}
		try
		{
			getOrCreateEntityManagerFactory(extensionContext).getInjector().getBinding(type);
			return true;
		}
		catch (ConfigurationException ignored)
		{
		}
		return false;
	}

	@Override
	public Object resolveParameter(
			ParameterContext parameterContext,
			ExtensionContext extensionContext)
	{
		EntityManagerContext entityManagerContext = getOrCreateEntityManagerFactory(extensionContext);
		Class<?> type = parameterContext.getParameter().getType();

		if (type == EntityManagerContext.class)
		{
			return entityManagerContext;
		}
		else if (type == EntityManager.class)
		{
			return entityManagerContext.getOrCreateEntityManager();
		}

		try
		{
			return entityManagerContext.getInjector().getInstance(type);
		}
		catch (Exception e)
		{
			throw new ParameterResolutionException("Failed to construct a " + type, e);
		}
	}

	@Override
	public void beforeEach(ExtensionContext context)
	{
		getOrCreateEntityManagerFactory(context).beforeTest(context);
	}

	private EntityManagerContext getOrCreateEntityManagerFactory(ExtensionContext extensionContext)
	{
		ExtensionContext.Store store = extensionContext.getRoot().getStore(NAMESPACE);
		return store.getOrComputeIfAbsent(extensionContext.getRequiredTestClass(), EntityManagerContext::new, EntityManagerContext.class);
	}

	@NameConverters(table = PrefixesTableNameConverter.class)
	@Jdbc(TestContainerJdbcConfiguration.class)
	@Data
	public static class EntityManagerContext
			implements ExtensionContext.Store.CloseableResource, Provider<EntityManager>, com.google.inject.Module
	{

		private final JdbcConfiguration jdbc;
		private final TableNameConverter tableNameConverter;
		private final FieldNameConverter fieldNameConverter;
		private final SequenceNameConverter sequenceNameConverter;
		private final TriggerNameConverter triggerNameConverter;
		private final IndexNameConverter indexNameConverter;
		private final UniqueNameConverter uniqueNameConverter;
		private final Class<? extends DatabaseUpdater> databaseUpdaterClass;
		private EntityManager entityManager;
		private Injector injector;

		EntityManagerContext(Class<?> testClass)
		{
			jdbc = newInstance(getAnnotation(testClass, Jdbc.class).value());
			NameConverters nameConverters = getAnnotation(testClass, NameConverters.class);
			tableNameConverter = newInstance(nameConverters.table());
			fieldNameConverter = newInstance(nameConverters.field());
			sequenceNameConverter = newInstance(nameConverters.sequence());
			triggerNameConverter = newInstance(nameConverters.trigger());
			indexNameConverter = newInstance(nameConverters.index());
			uniqueNameConverter = newInstance(nameConverters.unique());
			databaseUpdaterClass = getAnnotation(testClass, Data.class).value();
		}

		private static <A extends Annotation> A getAnnotation(
				Class<?> testClass,
				Class<A> annotationClass)
		{
			if (testClass.isAnnotationPresent(annotationClass))
			{
				return testClass.getAnnotation(annotationClass);
			}
			return EntityManagerContext.class.getAnnotation(annotationClass);
		}

		private static <T> T newInstance(Class<T> type)
		{
			try
			{
				return type.getDeclaredConstructor().newInstance();
			}
			catch (Exception e)
			{
				throw new RuntimeException(e);
			}
		}

		@Override
		public void close()
				throws Exception
		{
			Optional.ofNullable(entityManager).map(EntityManager::getProvider).ifPresent(DatabaseProvider::dispose);
			entityManager = null;
			if (jdbc instanceof Closeable)
			{
				((Closeable) jdbc).close();
			}
		}

		@Override
		public EntityManager get()
		{
			return getOrCreateEntityManager();
		}

		EntityManager getOrCreateEntityManager()
		{
			if (entityManager == null)
			{
				jdbc.init();

				EntityManagerBuilderWithDatabaseProperties entityManagerBuilder = createEntityManagerBuilder();

				entityManager = entityManagerBuilder.build();
			}

			return entityManager;
		}

		private EntityManagerBuilderWithDatabaseProperties createEntityManagerBuilder()
		{
			EntityManagerBuilderWithDatabaseProperties entityManagerBuilder = EntityManagerBuilder.url(jdbc.getUrl())
					.username(jdbc.getUsername())
					.password(jdbc.getPassword())
					.schema(jdbc.getSchema())
					.auto();

			if (tableNameConverter != null)
			{
				entityManagerBuilder = entityManagerBuilder.tableNameConverter(tableNameConverter);
			}
			if (fieldNameConverter != null)
			{
				entityManagerBuilder = entityManagerBuilder.fieldNameConverter(fieldNameConverter);
			}
			if (sequenceNameConverter != null)
			{
				entityManagerBuilder = entityManagerBuilder.sequenceNameConverter(sequenceNameConverter);
			}
			if (triggerNameConverter != null)
			{
				entityManagerBuilder = entityManagerBuilder.triggerNameConverter(triggerNameConverter);
			}
			if (indexNameConverter != null)
			{
				entityManagerBuilder = entityManagerBuilder.indexNameConverter(indexNameConverter);
			}
			if (uniqueNameConverter != null)
			{
				entityManagerBuilder = entityManagerBuilder.uniqueNameConverter(uniqueNameConverter);
			}
			return entityManagerBuilder;
		}

		void beforeTest(ExtensionContext context)
		{
			EntityManager entityManager = getOrCreateEntityManager();
			try (Connection connection = entityManager.getProvider().getConnection())
			{
				connection.setAutoCommit(false);
				entityManager.migrateDestructively();
				getInjector().getInstance(databaseUpdaterClass).update(entityManager);
				connection.commit();
			}
			catch (Exception e)
			{
				throw new RuntimeException("Failed to start active objects transaction.", e);
			}
		}

		@Override
		public void configure(Binder binder)
		{
			binder.bind(ActiveObjects.class).toProvider(() -> new TestActiveObjects(getOrCreateEntityManager())).in(Scopes.SINGLETON);
			binder.bind(EntityManagerBuilderWithDatabaseProperties.class).toProvider(this::createEntityManagerBuilder);
			binder.bind(DatabaseAccessor.class)
					.toProvider(() -> new StandaloneActiveObjectsDatabaseAccessor(getOrCreateEntityManager(), jdbc))
					.in(Scopes.SINGLETON);
			binder.bind(StreamingQueryFactory.class).to(StreamingQueryFactoryImpl.class);
		}

		Injector getInjector()
		{
			if (injector == null)
			{
				injector = Guice.createInjector(this);
			}
			return injector;
		}
	}
}
