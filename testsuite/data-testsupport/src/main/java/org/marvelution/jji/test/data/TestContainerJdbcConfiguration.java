/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.data;

import java.io.*;
import java.util.*;

import org.marvelution.jji.test.data.containers.*;

import net.java.ao.test.*;
import net.java.ao.test.jdbc.*;
import org.testcontainers.containers.*;

import static org.testcontainers.containers.MSSQLServerContainer.*;
import static org.testcontainers.containers.MySQLContainer.*;
import static org.testcontainers.containers.PostgreSQLContainer.*;

/**
 * @author Mark Rekveld
 * @since 4.2.0
 */
public class TestContainerJdbcConfiguration
		implements JdbcConfiguration, Closeable
{

	public static final String DEFAULT_DB = "test";
	public static final String DEFAULT_USER = "test";
	public static final String DEFAULT_PASSWORD = "test";
	private JdbcConfiguration jdbcConfiguration;

	@Override
	public String getUrl()
	{
		return jdbcConfiguration.getUrl();
	}

	@Override
	public String getSchema()
	{
		return jdbcConfiguration.getSchema();
	}

	@Override
	public String getUsername()
	{
		return jdbcConfiguration.getUsername();
	}

	@Override
	public String getPassword()
	{
		return jdbcConfiguration.getPassword();
	}

	@Override
	public void init()
	{
		if (jdbcConfiguration == null)
		{
			boolean inPipelines = Boolean.parseBoolean(System.getenv("CI"));
			String db = ConfigurationProperties.get("ao.test.database", "hsql");
			switch (db)
			{
				case "hsql":
					jdbcConfiguration = new Hsql();
					break;
				case "mysql":
					if (inPipelines)
					{
						jdbcConfiguration = new MySql("jdbc:mysql://localhost:" + MYSQL_PORT + "/" + DEFAULT_DB, DEFAULT_USER,
						                              DEFAULT_PASSWORD, DEFAULT_DB);
					}
					else
					{
						jdbcConfiguration = new ContainerJdbcConfiguration(new JiraMySQLContainer(), DEFAULT_DB);
					}
					break;
				case "postgres":
					if (inPipelines)
					{
						jdbcConfiguration = new Postgres(
								"jdbc:postgresql://localhost:" + POSTGRESQL_PORT + "/" + DEFAULT_DB + "?loggerLevel=OFF", DEFAULT_USER,
								DEFAULT_PASSWORD, "public");
					}
					else
					{
						jdbcConfiguration = new ContainerJdbcConfiguration(new JiraPostgreSQLContainer(), "public");
					}
					break;
				case "mssql":
					if (inPipelines)
					{
						jdbcConfiguration = new SqlServer("jdbc:sqlserver://localhost:" + MS_SQL_SERVER_PORT, "SA",
						                                  "A_Str0ng_Required_Password", "guest");
					}
					else
					{
						jdbcConfiguration = new ContainerJdbcConfiguration(new JiraMSSQLServerContainer(), "guest");
					}
					break;
				case "oracle":
					if (inPipelines)
					{
						jdbcConfiguration = new Oracle("jdbc:oracle:thin:@localhost:1521/" + DEFAULT_DB, DEFAULT_USER, DEFAULT_PASSWORD,
						                               DEFAULT_DB.toUpperCase(Locale.ENGLISH));
						break;
					}
					else
					{
						jdbcConfiguration = new ContainerJdbcConfiguration(new JiraOracleContainer(), "TEST");
						break;
					}
				default:
					throw new IllegalStateException("Unsupported database type set " + db);
			}

			jdbcConfiguration.init();
		}
	}

	@Override
	public void close()
			throws IOException
	{
		if (jdbcConfiguration instanceof Closeable)
		{
			((Closeable) jdbcConfiguration).close();
		}
	}

	private static class ContainerJdbcConfiguration
			implements JdbcConfiguration, Closeable
	{

		private final JdbcDatabaseContainer<?> databaseContainer;
		private final String schema;

		private ContainerJdbcConfiguration(
				JdbcDatabaseContainer<?> databaseContainer,
				String schema)
		{
			this.databaseContainer = databaseContainer;
			this.schema = schema;
		}

		@Override
		public String getUrl()
		{
			return databaseContainer.getJdbcUrl();
		}

		@Override
		public String getSchema()
		{
			return schema;
		}

		@Override
		public String getUsername()
		{
			return databaseContainer.getUsername();
		}

		@Override
		public String getPassword()
		{
			return databaseContainer.getPassword();
		}

		@Override
		public void init()
		{
			databaseContainer.start();
		}

		@Override
		public void close()
		{
			databaseContainer.stop();
		}
	}
}
