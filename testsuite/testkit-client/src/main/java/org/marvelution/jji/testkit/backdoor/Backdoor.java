/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

public class Backdoor
        extends com.atlassian.jira.testkit.client.Backdoor
{

    private final BackdoorEnvironmentData environmentData;
    private final ConfigurationControl configurationControl;
    private final SiteControl siteControl;
    private final JobControl jobControl;
    private final BuildControl buildControl;
    private final CacheControl cacheControl;
    private final RawControl rawControl;
    private final GeneratorControl generatorControl;
    private final UserControl userControl;
    private final AgileBoardControl agileBoardControl;
    private final ExtendedPluginsControl extendedPluginsControl;
    private final TestTracerControl testTracerControl;
    private final LinkStatsControl linkStatsControl;
    private final Map<Class<? extends RestApiClient<?>>, RestApiClient<?>> clients = new ConcurrentHashMap<>();
    private final Function<Class<? extends RestApiClient<?>>, RestApiClient<?>> clientCreator = type -> {
        try
        {
            return type.getConstructor(JIRAEnvironmentData.class)
                    .newInstance(environmentData());
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    };

    public Backdoor()
    {
        this(new BackdoorEnvironmentData());
    }

    public Backdoor(BackdoorEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
        configurationControl = new ConfigurationControl(environmentData);
        siteControl = new SiteControl(environmentData);
        jobControl = new JobControl(environmentData, siteControl);
        buildControl = new BuildControl(environmentData, jobControl);
        cacheControl = new CacheControl(environmentData);
        rawControl = new RawControl(environmentData);
        generatorControl = new GeneratorControl(environmentData);
        userControl = new UserControl(environmentData);
        agileBoardControl = new AgileBoardControl(environmentData);
        extendedPluginsControl = new ExtendedPluginsControl(environmentData);
        testTracerControl = new TestTracerControl(environmentData);
        linkStatsControl = new LinkStatsControl(environmentData);
    }

    public BackdoorEnvironmentData environmentData()
    {
        return environmentData;
    }

    public ConfigurationControl configuration()
    {
        return configurationControl;
    }

    public SiteControl sites()
    {
        return siteControl;
    }

    public JobControl jobs()
    {
        return jobControl;
    }

    public BuildControl builds()
    {
        return buildControl;
    }

    public CacheControl cache()
    {
        return cacheControl;
    }

    public GeneratorControl generator()
    {
        return generatorControl;
    }

    @Override
    public ExtendedPluginsControl plugins()
    {
        return extendedPluginsControl;
    }

    @Override
    public RawControl rawRestApiControl()
    {
        return rawControl;
    }

    public UserControl users()
    {
        return userControl;
    }

    public AgileBoardControl agileBoards()
    {
        return agileBoardControl;
    }

    public TestTracerControl testTracer()
    {
        return testTracerControl;
    }

    public LinkStatsControl linkStats()
    {
        return linkStatsControl;
    }

    public <RC extends RestApiClient<RC>> RC createRestClient(Class<RC> type)
    {
        return type.cast(clients.computeIfAbsent(type, clientCreator));
    }
}
