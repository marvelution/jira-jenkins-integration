/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor.rs;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

/**
 * @author Mark Rekveld
 * @since 3.7.0
 */
@Provider
@Consumes({"application/vnd.atl.plugins+json"})
@Produces({"application/vnd.atl.plugins+json"})
public class AtlassianPluginJsonProvider
        extends JacksonJaxbJsonProvider
{

    public AtlassianPluginJsonProvider()
    {
        this(null, DEFAULT_ANNOTATIONS);
    }

    public AtlassianPluginJsonProvider(Annotations... annotationsToUse)
    {
        this(null, annotationsToUse);
    }

    public AtlassianPluginJsonProvider(
            ObjectMapper mapper,
            Annotations[] annotationsToUse)
    {
        super(mapper, annotationsToUse);
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
}
