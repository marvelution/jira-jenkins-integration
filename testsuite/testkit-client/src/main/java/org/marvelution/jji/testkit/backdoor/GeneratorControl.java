/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import javax.ws.rs.core.Response;
import jakarta.json.Json;
import jakarta.json.JsonObjectBuilder;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;

import static org.apache.commons.lang3.StringUtils.capitalize;

/**
 * {@link BackdoorControl} for generating data in Jira.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class GeneratorControl
        extends BackdoorControl<GeneratorControl>
{

    public static final String GENERIC_PROJECT_PREFIX = "Test Project ";
    private static final AtomicInteger PROJECT_COUNTER = new AtomicInteger(1);
    private static final String GREENHOPPER_ADDON_KEY = "com.pyxis.greenhopper.jira";
    private static final String SCRUM_SOFTWARE_PROJECT_TEMPLATE_KEY = GREENHOPPER_ADDON_KEY + ":gh-scrum-template";
    private static final String KANBAN_SOFTWARE_PROJECT_TEMPLATE_KEY = GREENHOPPER_ADDON_KEY + ":gh-kanban-template";
    private static final String BASIC_SOFTWARE_PROJECT_TEMPLATE_KEY = GREENHOPPER_ADDON_KEY + ":basic-software-development-template";
    private static final String PROJECT_MANAGEMENT_PROJECT_TEMPLATE_KEY =
            "com.atlassian.jira-core-project-templates:jira-core-project-management";
    private final ProjectClient restClient;

    GeneratorControl(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.restClient = new ProjectClient(environmentData);
    }

    public Project generateScrumProject()
    {
        return generateScrumProject(GENERIC_PROJECT_PREFIX + PROJECT_COUNTER.getAndIncrement());
    }

    public Project generateScrumProject(String name)
    {
        return generateProject(SCRUM_SOFTWARE_PROJECT_TEMPLATE_KEY, name);
    }

    public Project generateKanBanProject()
    {
        return generateKanBanProject(GENERIC_PROJECT_PREFIX + PROJECT_COUNTER.getAndIncrement());
    }

    public Project generateKanBanProject(String name)
    {
        return generateProject(KANBAN_SOFTWARE_PROJECT_TEMPLATE_KEY, name);
    }

    public Project generateBasicProject()
    {
        return generateBasicProject(GENERIC_PROJECT_PREFIX + PROJECT_COUNTER.getAndIncrement());
    }

    public Project generateBasicProject(String name)
    {
        return generateProject(BASIC_SOFTWARE_PROJECT_TEMPLATE_KEY, name);
    }

    public Project generateBusinessProject()
    {
        return generateBusinessProject(GENERIC_PROJECT_PREFIX + PROJECT_COUNTER.getAndIncrement());
    }

    public Project generateBusinessProject(String name)
    {
        return generateProject(PROJECT_MANAGEMENT_PROJECT_TEMPLATE_KEY, name);
    }

    public Project generateProject(
            String templateKey,
            String name)
    {
        String projectName = capitalize(name);
        String projectKey = getProjectKeyForName(projectName);
        restClient.getProjects()
                .stream()
                .filter(p -> p.key.equals(projectKey))
                .forEach(project -> restClient.delete(project.key));
        JsonObjectBuilder json = Json.createObjectBuilder();
        json.add("key", projectKey);
        json.add("name", projectName);
        json.add("lead", "admin");
        json.add("projectTemplateKey", templateKey);
        Response response = restClient.create(json.build()
                .toString());
        if (response.getStatus() == Response.Status.CREATED.getStatusCode())
        {
            return response.readEntity(Project.class);
        }
        else
        {
            throw new IllegalStateException("Failed to create project " + name);
        }
    }

    public Project getOrGenerateProject(
            String name,
            Function<String, Project> generator)
    {
        Project project = null;
        try
        {
            project = restClient.get(projectKeyForName(name));
        }
        catch (Exception e)
        {
            logger.log(e);
        }
        if (project == null)
        {
            project = generator.apply(name);
        }
        return project;
    }

    public static String projectKeyForName(String projectName)
    {
        return getProjectKeyForName(capitalize(projectName));
    }

    private static String getProjectKeyForName(String projectName)
    {
        String projectKey = projectName.replaceAll("[^A-Z0-9]", "");
        if (projectKey.length() > 10)
        {
            projectKey = projectKey.substring(0, 10);
        }
        return projectKey;
    }
}
