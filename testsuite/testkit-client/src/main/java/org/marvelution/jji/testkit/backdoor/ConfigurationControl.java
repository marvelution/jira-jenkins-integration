/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import java.net.URI;
import java.util.Set;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;

import org.marvelution.jji.api.features.FeatureFlag;
import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.model.request.EnabledState;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import static java.util.stream.Collectors.toSet;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * @author Mark Rekveld
 * @since 2.3.0
 */
public class ConfigurationControl
        extends BackdoorControl<ConfigurationControl>
{

    ConfigurationControl(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * @since 3.3.0
     */
    public String getInstanceName()
    {
        return createConfigurationResource().path("instanceName")
                .request()
                .get(String.class);
    }

    /**
     * @since 3.3.0
     */
    public URI getBaseUrl()
    {
        return URI.create(createConfigurationResource().path("baseUrl")
                .request()
                .get(String.class));
    }

    /**
     * @since 3.3.0
     */
    public URI getBaseAPIUrl()
    {
        return URI.create(createConfigurationResource().path("baseAPIUrl")
                .request()
                .get(String.class));
    }

    /**
     * @since 3.3.0
     */
    public int getMaxBuildsPerPage()
    {
        return getMaxBuildsPerPage("");
    }

    /**
     * @since 5.13.0
     */
    public int getMaxBuildsPerPage(String scope)
    {
        WebTarget resource = createConfigurationResource().path("maxBuildsPerPage");
        if (scope != null)
        {
            resource = resource.queryParam("scope", scope);
        }
        return resource.request()
                .get(Integer.class);
    }

    public Configuration getConfiguration()
    {
        return getConfiguration("");
    }

    /**
     * @since 5.13.0
     */
    public Configuration getConfiguration(String scope)
    {
        WebTarget resource = createConfigurationResource();
        if (scope != null)
        {
            resource = resource.queryParam("scope", scope);
        }
        return resource.request()
                .get(Configuration.class);
    }

    public Configuration saveConfiguration(Configuration configuration)
    {
        return createConfigurationResource().request()
                .accept(APPLICATION_JSON_TYPE)
                .post(Entity.json(configuration), Configuration.class);
    }

    /**
     * @since 3.0.0
     */
    public void clearConfiguration()
    {
        createConfigurationResource().request()
                .delete();
    }

    /**
     * @since 4.1.0
     */
    public void enableFeature(String feature)
    {
        setFeature(feature, true);
    }

    /**
     * @since 4.1.0
     */
    public void disableFeature(String feature)
    {
        setFeature(feature, false);
    }

    /**
     * @since 4.1.0
     */
    private void setFeature(
            String feature,
            boolean enabled)
    {
        EnabledState enabledState = new EnabledState();
        enabledState.setEnabled(enabled);
        createConfigurationResource().path("features")
                .path(feature)
                .request()
                .post(Entity.json(enabledState));
    }

    /**
     * @since 4.1.0
     */
    public Set<FeatureFlag> getSiteFeatureFlags()
    {
        return getFeatureFlags("site");
    }

    /**
     * @since 4.1.0
     */
    public Set<FeatureFlag> getSystemFeatureFlags()
    {
        return getFeatureFlags("system");
    }

    /**
     * @since 4.1.0
     */
    private Set<FeatureFlag> getFeatureFlags(String scope)
    {
        return createConfigurationResource().path("features")
                .path(scope)
                .request()
                .get(new GenericType<Set<TempFeatureFlag>>() {})
                .stream()
                .map(temp -> new FeatureFlag(temp.feature, temp.enabled, temp.overrideable))
                .collect(toSet());
    }

    public static class TempFeatureFlag
    {

        public String feature;
        public boolean enabled;
        public boolean overrideable;
    }
}
