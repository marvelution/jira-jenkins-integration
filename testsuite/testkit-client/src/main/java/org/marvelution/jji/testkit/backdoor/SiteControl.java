/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import java.net.URI;
import java.util.List;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;

import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteConnectionType;
import org.marvelution.jji.model.SiteType;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import static java.lang.String.valueOf;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class SiteControl
        extends BackdoorControl<SiteControl>
{

    private static final GenericType<List<Site>> LIST_GENERIC_TYPE = new GenericType<List<Site>>() {};

    SiteControl(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public List<Site> getSites()
    {
        return createSiteResource().request()
                .get(LIST_GENERIC_TYPE);
    }

    public void clearSites()
    {
        createSiteResource().request()
                .delete();
    }

    public Site getSite(String siteId)
    {
        return getSite(siteId, true);
    }

    public Site getSite(
            String siteId,
            boolean includeJobs)
    {
        return createSiteResource().path(siteId)
                .queryParam("includeJobs", valueOf(includeJobs))
                .request()
                .get(Site.class);
    }

    public void populateSharedSecret(Site site)
    {
        String sharedSecret = createSiteResource().path(site.getId())
                .path("sharedSecret")
                .request()
                .get(String.class);
        site.setSharedSecret(sharedSecret);
    }

    public Site addPublicSite(
            SiteType type,
            String name,
            URI rpcUrl)
    {
        return addPublicSite(type, name, rpcUrl, false);
    }

    public Site addPublicSite(
            SiteType type,
            String name,
            URI rpcUrl,
            boolean autoLinkNewJobs)
    {
        return addSite(type, name, rpcUrl, false, autoLinkNewJobs);
    }

    public Site addFirewalledSite(
            SiteType type,
            String name,
            URI rpcUrl)
    {
        return addFirewalledSite(type, name, rpcUrl, false);
    }

    public Site addFirewalledSite(
            SiteType type,
            String name,
            URI rpcUrl,
            boolean autoLinkNewJobs)
    {
        return addSite(type, name, rpcUrl, true, autoLinkNewJobs);
    }

    private Site addSite(
            SiteType type,
            String name,
            URI rpcUrl,
            boolean firewalled,
            boolean autoLinkNewJobs)
    {
        Site site = new Site().setType(type)
                .setName(name)
                .setRpcUrl(rpcUrl)
                .setConnectionType(firewalled ? SiteConnectionType.INACCESSIBLE : SiteConnectionType.ACCESSIBLE)
                .setAutoLinkNewJobs(autoLinkNewJobs);
        return saveSite(site);
    }

    public Site saveSite(Site site)
    {
        return createSiteResource().request()
                .accept(APPLICATION_JSON_TYPE)
                .post(Entity.json(site.copy()), Site.class);
    }

    public void syncSite(Site site)
    {
        createSiteResource().path(site.getId())
                .path("sync")
                .request()
                .put(Entity.json(""));
    }
}
