/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import java.nio.charset.StandardCharsets;
import java.util.Objects;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;

import org.marvelution.jji.testkit.backdoor.model.PluginLicense;
import org.marvelution.jji.testkit.backdoor.model.PluginSummary;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.PluginsControl;
import org.apache.commons.io.IOUtils;

/**
 * Extended {@link PluginsControl}.
 *
 * @author Mark Rekveld
 * @since 3.7.0
 */
public class ExtendedPluginsControl
        extends PluginsControl
{

    public ExtendedPluginsControl(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public boolean isPluginUsingLicensing(String pluginKey)
    {
        return createResource(pluginKey, "summary").get(PluginSummary.class).usesLicensing;
    }

    public boolean isPluginLicensed(String pluginKey)
    {
        PluginLicense pluginLicense = getPluginLicense(pluginKey);
        return pluginLicense.valid && pluginLicense.active;
    }

    public PluginLicense getPluginLicense(String pluginKey)
    {
        return createResource(pluginKey, "license").get(PluginLicense.class);
    }

    public void clearPluginLicense(String pluginKey)
    {
        createResource(pluginKey, "license").delete();
    }

    private Invocation.Builder createResource(
            String pluginKey,
            String... paths)
    {
        WebTarget resource = createResourceForPath("plugins").path(pluginKey + "-key");
        for (String path : paths)
        {
            resource = resource.path(path);
        }
        return resource.request()
                .accept("application/vnd.atl.plugins+json");
    }

    @Override
    public void setPluginLicense(
            String pluginKey,
            String license)
    {
        try
        {
            String rawLicense;
            if (license.startsWith("/licenses/") && license.endsWith(".lic"))
            {
                rawLicense = IOUtils.toString(Objects.requireNonNull(getClass().getResourceAsStream(license)), StandardCharsets.UTF_8);
            }
            else
            {
                rawLicense = license;
            }

            super.setPluginLicense(pluginKey, rawLicense);
        }
        catch (Exception e)
        {
            throw new AssertionError(e);
        }
    }
}
