/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;

import com.atlassian.jira.testkit.client.util.*;

import static org.apache.commons.lang3.StringUtils.*;

/**
 * {@link AbstractEnvironmentData} implementation for the {@link Backdoor}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class BackdoorEnvironmentData
		extends AbstractEnvironmentData
{

	private static final String DEFAULT_BASEURL = "http://localhost:2990/jira";
	private static final String DEFAULT_CONTEXT = "/jira";
	private static final String DEFAULT_EDITION = "enterprise";

	private static final String JIRA_BASEURL = "jira.baseUrl";
	private static final String JIRA_TMP = "jira.tmp";
	private static final String JIRA_XML_DATA_LOCATION = "jira.xml.data.location";
	private static final String JIRA_CONTEXT = "jira.context";
	private static final String JIRA_EDITION = "jira.edition";

	private URL baseUrl;

	public BackdoorEnvironmentData()
	{
		super(new Properties());
	}

	public String getRestApiModule()
	{
		return getEnvironmentProperty("rest.api.module", "api");
	}

	public String getAddonKey()
	{
		return getEnvironmentProperty("addon.key", "");
	}

	public boolean isServer()
	{
		return getEdition().equalsIgnoreCase("server");
	}

	public boolean isDataCenter()
	{
		return getEdition().equalsIgnoreCase("data-center");
	}

	@Override
	public String getEdition()
	{
		return getEnvironmentProperty(JIRA_EDITION, DEFAULT_EDITION);
	}

	@Override
	public String getContext()
	{
		return getEnvironmentProperty(JIRA_CONTEXT, DEFAULT_CONTEXT);
	}

	@Override
	public URL getBaseUrl()
	{
		if (baseUrl == null)
		{
			try
			{
				baseUrl = URI.create(getEnvironmentProperty(JIRA_BASEURL, DEFAULT_BASEURL)).toURL();
			}
			catch (MalformedURLException e)
			{
				throw new IllegalStateException("Malformed Jira baseUrl", e);
			}
		}
		return baseUrl;
	}

	@Override
	public File getXMLDataLocation()
	{
		return resolveFileLocation(JIRA_XML_DATA_LOCATION);
	}

	@Override
	public File getWorkingDirectory()
	{
		return resolveFileLocation(JIRA_TMP);
	}

	@Override
	public File getJIRAHomeLocation()
	{
		return getWorkingDirectory();
	}

	private File resolveFileLocation(final String key)
	{
		String property = getEnvironmentProperty(key, null);
		Path path;
		if (isNotBlank(property))
		{
			path = Paths.get(property);
		}
		else
		{
			try
			{
				path = Files.createTempFile(key, null);
			}
			catch (Exception e)
			{
				throw new IllegalStateException("Filed to create a temporary directory for key " + key, e);
			}
		}
		try
		{
			path = path.toRealPath();
		}
		catch (IOException ignored)
		{
		}
		return path.toFile();
	}
}
