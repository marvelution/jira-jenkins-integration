/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.jerseyclient.ApacheClientFactoryImpl;
import com.atlassian.jira.testkit.client.jerseyclient.JerseyClientFactory;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;

class BackdoorControl<T extends BackdoorControl<T>>
        extends com.atlassian.jira.testkit.client.BackdoorControl<T>
{
    private static final ThreadLocal<Client> client = ThreadLocal.withInitial(() -> {
        ClientConfig config = new ClientConfig();
        config.property("jersey.config.client.suppressHttpComplianceValidation", true);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModules(new JavaTimeModule(),
                new JodaModule(),
                new JaxbAnnotationModule().setPriority(JaxbAnnotationModule.Priority.PRIMARY),
                new Jdk8Module(),
                new ParameterNamesModule());
        JacksonJaxbJsonProvider jacksonProvider = new JacksonJaxbJsonProvider();
        jacksonProvider.setMapper(objectMapper);
        jacksonProvider.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        config.register(jacksonProvider);
        config.register(MultiPartWriter.class);
        JerseyClientFactory clientFactory = new ApacheClientFactoryImpl(config);
        Client client = clientFactory.create();
        client.register(new LoggingFeature());

        client.register(RestApiClient.BackdoorLoggingFilter.class);
        client.register(RestApiClient.JsonMediaTypeFilter.class);
        return client;
    });

    BackdoorControl(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    @Override
    protected String getRestModulePath()
    {
        return "jenkins-test";
    }

    WebTarget createConfigurationResource()
    {
        return createResource().path("configuration");
    }

    WebTarget createSiteResource()
    {
        return createResource().path("site");
    }

    WebTarget createJobResource()
    {
        return createResource().path("job");
    }

    WebTarget createBuildResource()
    {
        return createResource().path("build");
    }

    WebTarget createUserResource()
    {
        return createResource().path("user");
    }

    @Override
    protected WebTarget resourceRoot(String url)
    {
        return resourceRoot(client.get(), url);
    }
}
