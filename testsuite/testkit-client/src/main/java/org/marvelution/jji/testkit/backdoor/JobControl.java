/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.NoSuchElementException;
import javax.ws.rs.client.Entity;

import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class JobControl
        extends BackdoorControl<JobControl>
{

    private final SiteControl siteControl;

    JobControl(
            JIRAEnvironmentData environmentData,
            SiteControl siteControl)
    {
        super(environmentData);
        this.siteControl = siteControl;
    }

    public Job getJob(String jobId)
    {
        return createJobResource().path(jobId)
                .request()
                .get(Job.class);
    }

    public Job addJob(String name)
    {
        Site site = siteControl.getSites()
                .stream()
                .findFirst()
                .orElseThrow(NoSuchElementException::new);
        return addJob(site, name);
    }

    public Job addJob(
            Site site,
            String name)
    {
        return saveJob(new Job().setName(name)
                .setUrlName(URLEncoder.encode(name, StandardCharsets.UTF_8))
                .setSite(site));
    }

    public Job saveJob(Job job)
    {
        return createJobResource().request()
                .accept(APPLICATION_JSON_TYPE)
                .post(Entity.json(job.copy()), Job.class);
    }

    public void syncJob(Job job)
    {
        createJobResource().path(job.getId())
                .path("sync")
                .request()
                .put(Entity.json(""));
    }
}
