/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor.rest;

import java.util.Collections;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.LinkStatistician;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@Named
@UnrestrictedAccess
@Path("linkStats")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorLinkStatsResource
{

    private final LinkStatistician linkStatistician;

    @Inject
    public BackdoorLinkStatsResource(
            @ComponentImport
            LinkStatistician linkStatistician)
    {
        this.linkStatistician = linkStatistician;
    }

    @POST
    @Path("{issueKey}")
    public void calculateStats(
            @PathParam("issueKey")
            String issueKey,
            @QueryParam("reIndex")
            @DefaultValue("false")
            boolean reIndex)
    {
        linkStatistician.calculate(new LinkStatistician.Request().setIssueKeys(Collections.singleton(issueKey))
                .setReindexIssue(reIndex));
    }
}
