/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor.rest;

import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import org.slf4j.LoggerFactory;

/**
 * @author Mark Rekveld
 * @since 4.7.0
 */
@Named
@UnrestrictedAccess
@Path("test-tracer")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorTestTracerResource
{
    private static final String DEFAULT_LOGGER = "org.marvelution.jji.backdoor.rest.BackdoorTestTracerResource";

    @POST
    @Path("info")
    public void info(
            @QueryParam("logger")
            @DefaultValue(DEFAULT_LOGGER)
            String logger,
            String message)
    {
        LoggerFactory.getLogger(logger)
                .info(message);
    }

    @POST
    @Path("error")
    public void error(
            @QueryParam("logger")
            @DefaultValue(DEFAULT_LOGGER)
            String logger,
            String message)
    {
        LoggerFactory.getLogger(logger)
                .error(message);
    }
}
