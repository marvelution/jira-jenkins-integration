/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor.rest;

import java.util.List;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.jji.data.synchronization.api.trigger.ManualSynchronizationTrigger;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
@Named
@UnrestrictedAccess
@Path("site")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorSiteResource
{

    private final SiteService siteService;
    private final JobService jobService;
    private final SynchronizationService synchronizationService;

    @Inject
    public BackdoorSiteResource(
            @ComponentImport
            SiteService siteService,
            @ComponentImport
            JobService jobService,
            @ComponentImport
            SynchronizationService synchronizationService)
    {
        this.siteService = siteService;
        this.jobService = jobService;
        this.synchronizationService = synchronizationService;
    }

    @GET
    public List<Site> getAll()
    {
        return siteService.getAll(true);
    }

    @POST
    public Site save(Site site)
    {
        return siteService.save(site);
    }

    @DELETE
    public void clear()
    {
        siteService.getAll()
                .forEach(site -> {
                    try
                    {
                        siteService.delete(site);
                    }
                    catch (Exception ignore)
                    {
                        // already deleted by async job
                    }
                });
    }

    @GET
    @Path("{siteId}")
    @Nullable
    public Site get(
            @PathParam("siteId")
            String siteId,
            @QueryParam("includeJobs")
            boolean includeJobs)
    {
        return siteService.get(siteId)
                .map(site -> {
                    jobService.getRootJobsOnSite(site)
                            .forEach(job -> addJobsToSite(site, job));
                    return site;
                })
                .orElse(null);
    }

    private void addJobsToSite(
            Site site,
            Job job)
    {
        site.addJob(job);
        jobService.getSubJobs(job)
                .forEach(j -> addJobsToSite(site, j));
    }

    @GET
    @Path("{siteId}/sharedSecret")
    @Nullable
    public String getSharedSecret(
            @PathParam("siteId")
            String siteId)
    {
        return siteService.get(siteId)
                .map(Site::getSharedSecret)
                .orElse(null);
    }

    @DELETE
    @Path("{siteId}")
    public void delete(
            @PathParam("siteId")
            String siteId)
    {
        siteService.get(siteId)
                .ifPresent(siteService::delete);
    }

    @PUT
    @Path("{siteId}/sync")
    public void synchronize(
            @PathParam("siteId")
            String siteId)
    {
        siteService.get(siteId)
                .ifPresent(syncable -> synchronizationService.synchronize(syncable, new ManualSynchronizationTrigger("backdoor")));
    }
}
