/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor.rest;

import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.api.features.FeatureFlag;
import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.model.request.EnabledState;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

import static java.util.stream.Collectors.toSet;

@Named
@UnrestrictedAccess
@Path("configuration")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorConfigurationResource
{

    private final ConfigurationService configurationService;
    private final Features features;

    @Inject
    public BackdoorConfigurationResource(
            @ComponentImport
            ConfigurationService configurationService,
            @ComponentImport
            Features features)
    {
        this.configurationService = configurationService;
        this.features = features;
    }

    @GET
    @Path("instanceName")
    public String getInstanceName()
    {
        return configurationService.getInstanceName();
    }

    @GET
    @Path("baseUrl")
    public String getBaseUrl()
    {
        return configurationService.getBaseUrl()
                .toASCIIString();
    }

    @GET
    @Path("baseAPIUrl")
    public String getBaseAPIUrl()
    {
        return configurationService.getBaseAPIUrl()
                .toASCIIString();
    }

    @GET
    @Path("maxBuildsPerPage")
    public Integer getMaxBuildsPerPage(
            @QueryParam("scope")
            @DefaultValue("")
            String scope)
    {
        return configurationService.getMaximumBuildsPerPage(scope);
    }

    @GET
    public Configuration getConfiguration(
            @QueryParam("scope")
            @DefaultValue("")
            String scope)
    {
        return configurationService.getConfiguration(scope);
    }

    @POST
    public Configuration savePluginConfiguration(Configuration configuration)
    {
        configurationService.saveConfiguration(configuration);
        return configurationService.getConfiguration(configuration.getScope());
    }

    @DELETE
    public void clearConfiguration()
    {
        configurationService.resetToDefaults();
    }

    @POST
    @Path("features/{feature}")
    public void setFeature(
            @PathParam("feature")
            String feature,
            EnabledState enabledState)
    {
        features.setFeatureState(feature, enabledState.isEnabled());
    }

    @GET
    @Path("features/site")
    public Set<TempFeatureFlag> getSiteFeatureFlags()
    {
        return features.getSiteFeatureFlags()
                .stream()
                .map(TempFeatureFlag::from)
                .collect(toSet());
    }

    @GET
    @Path("features/system")
    public Set<TempFeatureFlag> getSystemFeatureFlags()
    {
        return features.getSystemFeatureFlags()
                .stream()
                .map(TempFeatureFlag::from)
                .collect(toSet());
    }

    public static class TempFeatureFlag
    {

        public String feature;
        public boolean enabled;
        public boolean overrideable;

        public static TempFeatureFlag from(FeatureFlag flag)
        {
            TempFeatureFlag temp = new TempFeatureFlag();
            temp.feature = flag.getFeature();
            temp.enabled = flag.isEnabled();
            temp.overrideable = flag.isOverrideable();
            return temp;
        }
    }
}
