/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor.rest;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.jji.data.synchronization.api.trigger.ManualSynchronizationTrigger;
import org.marvelution.jji.model.Job;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
@Named
@UnrestrictedAccess
@Path("job")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorJobResource
{

    private final JobService jobService;
    private final SynchronizationService synchronizationService;

    @Inject
    public BackdoorJobResource(
            @ComponentImport
            JobService jobService,
            @ComponentImport
            SynchronizationService synchronizationService)
    {
        this.jobService = jobService;
        this.synchronizationService = synchronizationService;
    }

    @POST
    public Job save(Job job)
    {
        return jobService.save(job);
    }

    @Nullable
    @GET
    @Path("{jobId}")
    public Job get(
            @PathParam("jobId")
            String jobId)
    {
        return jobService.get(jobId)
                .orElse(null);
    }

    @DELETE
    @Path("{jobId}")
    public void delete(
            @PathParam("jobId")
            String jobId)
    {
        jobService.get(jobId)
                .ifPresent(jobService::delete);
    }

    @PUT
    @Path("{jobId}/sync")
    public void synchronize(
            @PathParam("jobId")
            String jobId)
    {
        jobService.get(jobId)
                .ifPresent(syncable -> synchronizationService.synchronize(syncable, new ManualSynchronizationTrigger("backdoor")));
    }
}
