/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor.rest;

import java.util.Date;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@Named
@UnrestrictedAccess
@Path("user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorUserResource
{

    private final UserManager userManager;
    private final DateTimeFormatter dateTimeFormatter;

    @Inject
    public BackdoorUserResource(
            @ComponentImport
            UserManager userManager,
            @ComponentImport
            DateTimeFormatter dateTimeFormatter)
    {
        this.userManager = userManager;
        this.dateTimeFormatter = dateTimeFormatter;
    }

    @GET
    @Path("{user}/format/{style}")
    public String formatTimestamp(
            @PathParam("user")
            String username,
            @PathParam("style")
            DateTimeStyle style,
            @QueryParam("timestamp")
            long timestamp)
    {
        return dateTimeFormatter.forUser(userManager.getUserByName(username))
                .withStyle(style)
                .format(new Date(timestamp));
    }

    @GET
    @Path("{user}/id")
    public Long getUserId(
            @PathParam("user")
            String username)
    {
        return Optional.of(username)
                .map(userManager::getUserByName)
                .map(ApplicationUser::getId)
                .orElse(null);
    }

    @GET
    @Path("{user}/displayName")
    public String getUserDisplayName(
            @PathParam("user")
            String username)
    {
        return Optional.of(username)
                .map(userManager::getUserByName)
                .map(ApplicationUser::getDisplayName)
                .orElse(null);
    }
}
