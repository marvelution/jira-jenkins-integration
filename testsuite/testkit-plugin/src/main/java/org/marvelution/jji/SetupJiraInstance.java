/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji;

import javax.inject.*;

import org.marvelution.jji.data.services.api.model.PermissionKeys;

import com.atlassian.crowd.embedded.api.*;
import com.atlassian.crowd.exception.*;
import com.atlassian.event.api.*;
import com.atlassian.jira.event.*;
import com.atlassian.jira.exception.*;
import com.atlassian.jira.permission.*;
import com.atlassian.jira.security.groups.*;
import com.atlassian.jira.security.plugin.*;
import com.atlassian.jira.user.*;
import com.atlassian.jira.user.util.*;
import com.atlassian.plugin.spring.scanner.annotation.export.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.*;
import com.atlassian.sal.api.lifecycle.*;
import org.slf4j.*;

import static org.apache.commons.lang3.StringUtils.*;

@Named
@ExportAsService(LifecycleAware.class)
public class SetupJiraInstance
        implements LifecycleAware
{

    private static final Logger LOGGER = LoggerFactory.getLogger(SetupJiraInstance.class);
    private final UserManager userManager;
    private final GroupManager groupManager;
    private final PermissionSchemeService permissionSchemeService;
    private final EventPublisher eventPublisher;

    @Inject
    public SetupJiraInstance(
            @ComponentImport
            UserManager userManager,
            @ComponentImport
            GroupManager groupManager,
            @ComponentImport
            PermissionSchemeService permissionSchemeService,
            @ComponentImport
            EventPublisher eventPublisher)
    {
        this.userManager = userManager;
        this.groupManager = groupManager;
        this.permissionSchemeService = permissionSchemeService;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void onStart()
    {
        eventPublisher.register(this);
        ApplicationUser admin = getApplicationUser("admin");
        ApplicationUser user = getApplicationUser("user");
        ApplicationUser superUser = getApplicationUser("superuser");
        Group group = groupManager.getGroup("jira-software-users");
        try
        {
            groupManager.addUserToGroup(user, group);
            groupManager.addUserToGroup(superUser, group);
            LOGGER.info("Added {} and {} to {}", user, superUser, group);
        }
        catch (GroupNotFoundException | UserNotFoundException e)
        {
            throw new IllegalStateException("User and group should exist", e);
        }
        catch (OperationNotPermittedException | OperationFailedException e)
        {
            throw new IllegalStateException("not allowed to update group memberships", e);
        }
        PermissionScheme permissionScheme = permissionSchemeService.getPermissionScheme(admin, 0L)
                .get();
        grantSuperUserPermissions(admin, superUser, permissionScheme);
    }

    @Override
    public void onStop()
    {
        eventPublisher.unregister(this);
    }

    @EventListener
    public void onProjectCreated(ProjectCreatedEvent event)
    {
        ApplicationUser admin = getApplicationUser("admin");
        PermissionScheme permissionScheme = permissionSchemeService.getSchemeAssignedToProject(admin, event.getId())
                .get();
        grantSuperUserPermissions(admin, getApplicationUser("superuser"), permissionScheme);
    }

    private void grantSuperUserPermissions(
            ApplicationUser admin,
            ApplicationUser superUser,
            PermissionScheme permissionScheme)
    {
        PermissionSchemeInput permissionSchemeInput = PermissionSchemeInput.builder(permissionScheme)
                .addPermission(PermissionGrantInput.newGrant(PermissionHolder.holder(JiraPermissionHolderType.USER,
                        superUser.getUsername()), new ProjectPermissionKey(PermissionKeys.TRIGGER_JENKINS_BUILD)))
                .addPermission(PermissionGrantInput.newGrant(PermissionHolder.holder(JiraPermissionHolderType.USER,
                        superUser.getUsername()), ProjectPermissions.ADMINISTER_PROJECTS))
                .build();
        permissionSchemeService.updatePermissionScheme(admin, permissionScheme.getId(), permissionSchemeInput);
        LOGGER.info("Added trigger-build-permission to {} in scheme {}", superUser, permissionScheme);
    }

    private ApplicationUser getApplicationUser(String username)
    {
        ApplicationUser user = userManager.getUserByName(username);
        if (user == null)
        {
            try
            {
                user = userManager.createUser(new UserDetails(username, capitalize(username)).withPassword(username)
                        .withEmail(username + "@example.com"));
            }
            catch (CreateException | PermissionException e)
            {
                throw new IllegalStateException("Failed to create user " + username, e);
            }
        }
        LOGGER.info("Resolved {} -> {}", username, user);
        return user;
    }
}
