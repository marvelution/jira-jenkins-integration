/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test;

import java.lang.reflect.*;

import org.marvelution.jji.test.authz.*;
import org.marvelution.jji.testkit.backdoor.*;

import org.junit.jupiter.api.*;

public interface BaseTest
{

    String ANONYMOUS = AuthzTest.ANONYMOUS;
    String USER = AuthzTest.USER;
    String SUPER_USER = AuthzTest.SUPER_USER;
    String ADMIN = AuthzTest.ADMIN;

    Backdoor backdoor = new Backdoor();

    @BeforeEach
    default void logStart(TestInfo testInfo)
    {
        backdoor.logControl()
                .info("--- TEST " + formatTestInfo(testInfo) + " STARTED ---");
    }

    @AfterEach
    default void logEnd(TestInfo testInfo)
    {
        backdoor.logControl()
                .info(" --- TEST " + formatTestInfo(testInfo) + " FINISHED ---");
    }

    default String formatTestInfo(TestInfo testInfo)
    {
        return String.format("%s (%s.%s)",
                testInfo.getDisplayName(),
                testInfo.getTestClass()
                        .map(Class::getName)
                        .orElse("unknown"),
                testInfo.getTestMethod()
                        .map(Method::getName)
                        .orElse("unknown"));
    }
}
