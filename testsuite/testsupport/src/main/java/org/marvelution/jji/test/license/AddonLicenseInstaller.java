/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.license;

import java.util.*;
import java.util.concurrent.*;

import org.marvelution.jji.testkit.backdoor.*;

import org.apache.commons.lang3.*;
import org.junit.jupiter.api.extension.*;
import org.slf4j.*;

/**
 * @author Mark Rekveld
 * @since 3.7.0
 */
public class AddonLicenseInstaller
		extends LicensedExtension
		implements BeforeAllCallback, BeforeEachCallback
{

	private static final Logger LOGGER = LoggerFactory.getLogger(AddonLicenseInstaller.class);
	private final Backdoor backdoor = new Backdoor();

	@Override
	public void beforeAll(ExtensionContext context)
	{
		setPluginLicense(context);
	}

	@Override
	public void beforeEach(ExtensionContext context)
	{
		setPluginLicense(context);
	}

	@Override
	protected License defaultLicense()
	{
		return License.NEARLY_EXPIRED;
	}

	private void setPluginLicense(ExtensionContext context)
	{
		Optional<String> addonKey = Optional.ofNullable(System.getProperty("addon.key")).filter(StringUtils::isNotBlank);
		if (addonKey.isPresent() && backdoor.plugins().isPluginUsingLicensing(addonKey.get()))
		{
			License license = getLicenseFromContext(context);
			if (license == License.UNLICENSED)
			{
				LOGGER.info("Clearing Addon license  for addon {}", addonKey.get());
				backdoor.plugins().clearPluginLicense(addonKey.get());
			}
			else
			{
				String licenseFilename = "app-3hours.lic";
				if (license == License.EXPIRED)
				{
					licenseFilename = "app-10seconds.lic";
				}
				String licenseFile = "/licenses/" + backdoor.environmentData().getEdition() + "/" + licenseFilename;
				LOGGER.info("Setting Addon {} license to {} for {}", addonKey.get(), licenseFile, context.getDisplayName());
				backdoor.plugins().setPluginLicense(addonKey.get(), licenseFile);

				if (license == License.EXPIRED)
				{
					LOGGER.info("Waiting for 11 seconds to expire the installed license...");
					try
					{
						TimeUnit.SECONDS.sleep(11);
					}
					catch (InterruptedException ignored)
					{
						LOGGER.warn("Got interrupted waiting to expire the license.");
					}
				}
			}
		}
	}
}
