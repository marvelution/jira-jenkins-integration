/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.rest;

import javax.ws.rs.core.Response;

import org.hamcrest.Description;
import org.hamcrest.DiagnosingMatcher;
import org.hamcrest.Matcher;

/**
 * Common project specific Matchers
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class Matchers
{

    /**
     * Creates a matcher that matches if the examined object response status matches {@link Response.Status#OK}.
     */
    public static Matcher<Response> successful()
    {
        return status(Response.Status.OK);
    }

    /**
     * Creates a matcher that matches if the examined object response status matches {@link Response.Status#NO_CONTENT}.
     */
    public static Matcher<Response> noContent()
    {
        return status(Response.Status.NO_CONTENT);
    }

    /**
     * Creates a matcher that matches if the examined object response status matches {@link Response.Status#BAD_REQUEST}.
     */
    public static Matcher<Response> badRequest()
    {
        return status(Response.Status.BAD_REQUEST);
    }

    /**
     * Creates a matcher that matches if the examined object response status matches {@link Response.Status#NOT_FOUND}.
     */
    public static Matcher<Response> notFound()
    {
        return status(Response.Status.NOT_FOUND);
    }

    /**
     * Creates a matcher that matches if the examined object response status matches {@link Response.Status#FORBIDDEN}.
     */
    public static Matcher<Response> forbidden()
    {
        return status(Response.Status.FORBIDDEN);
    }

    /**
     * Creates a matcher that matches if the examined object response status matches {@link Response.Status#UNAUTHORIZED}.
     */
    public static Matcher<Response> unauthorized()
    {
        return status(Response.Status.UNAUTHORIZED);
    }

    /**
     * Creates a matcher that matches if the examined object response status matches the specified {@link Response.Status expected}
     * status.
     */
    public static Matcher<Response> status(final Response.Status expected)
    {
        return new DiagnosingMatcher<>()
        {

            @Override
            public void describeTo(Description description)
            {
                description.appendText("response with status ")
                        .appendValue(expected.getStatusCode())
                        .appendText(" ")
                        .appendText(expected.getReasonPhrase());
            }

            @Override
            protected boolean matches(
                    Object item,
                    Description mismatchDescription)
            {
                if (item == null)
                {
                    mismatchDescription.appendText("null");
                    return false;
                }
                if (!(item instanceof Response response))
                {
                    mismatchDescription.appendValue(item)
                            .appendValue(" is a " + item.getClass()
                                    .getName());
                    return false;
                }
                if (response.getStatusInfo()
                            .getStatusCode() != expected.getStatusCode())
                {
                    mismatchDescription.appendText("resulted in " + response.getStatusInfo());
                    return false;
                }
                return true;
            }

        };
    }

}
