/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.rest;

import java.net.URI;
import java.net.URISyntaxException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.UriBuilder;

import org.marvelution.jji.test.AbstractIntegrationTest;
import org.marvelution.jji.testkit.backdoor.BackdoorEnvironmentData;

import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.jerseyclient.ApacheClientFactoryImpl;
import com.atlassian.jira.testkit.client.jerseyclient.JerseyClientFactory;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.module.jakarta.xmlbind.JakartaXmlBindAnnotationModule;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;

/**
 * Base Integration Testcase for REST resource testing
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public abstract class AbstractResourceTest
        extends AbstractIntegrationTest
        implements WebTargetTest
{

    private static ThreadLocal<Client> client = ThreadLocal.withInitial(() -> {
        ClientConfig config = new ClientConfig();
        config.property("jersey.config.client.suppressHttpComplianceValidation", true);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModules(new JavaTimeModule(),
                new JodaModule(),
                new JaxbAnnotationModule().setPriority(JaxbAnnotationModule.Priority.PRIMARY),
                new JakartaXmlBindAnnotationModule().setPriority(JakartaXmlBindAnnotationModule.Priority.SECONDARY),
                new Jdk8Module(),
                new ParameterNamesModule());
        JacksonJaxbJsonProvider jacksonProvider = new JacksonJaxbJsonProvider();
        jacksonProvider.setMapper(objectMapper);
        jacksonProvider.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        config.register(jacksonProvider);
        config.register(MultiPartWriter.class);
        JerseyClientFactory clientFactory = new ApacheClientFactoryImpl(config);
        Client client = clientFactory.create();
        client.register(new LoggingFeature());

        client.register(RestApiClient.BackdoorLoggingFilter.class);
        client.register(RestApiClient.JsonMediaTypeFilter.class);
        return client;
    });
    private Feature auth = HttpAuthenticationFeature.basic(ADMIN, ADMIN);

    protected void anonymous()
    {
        auth = null;
    }

    protected void asAdmin()
    {
        loginWith(ADMIN);
    }

    protected void asUser()
    {
        loginWith(USER);
    }

    protected void asSuperUser()
    {
        loginWith(SUPER_USER);
    }

    protected void loginWith(String username)
    {
        loginWith(username, username);
    }

    protected void loginWith(
            String username,
            String password)
    {
        loginWith(HttpAuthenticationFeature.basic(username, password));
    }

    protected void loginWith(ClientRequestFilter authFilter)
    {
        loginWith(context -> {
            context.register(authFilter);
            return true;
        });
    }

    protected void loginWith(Feature filter)
    {
        auth = filter;
    }

    @Override
    public UriBuilder uriBuilder()
    {
        try
        {
            BackdoorEnvironmentData environmentData = backdoor.environmentData();
            return UriBuilder.fromUri(environmentData.getBaseUrl()
                            .toURI())
                    .path("rest")
                    .path(environmentData.getRestApiModule())
                    .path("latest");
        }
        catch (URISyntaxException e)
        {
            throw new IllegalStateException("Malformed base url " + e.getMessage(), e);
        }
    }

    @Override
    public WebTarget resource(URI uri)
    {
        WebTarget resource = client().target(uri);
        if (auth != null)
        {
            resource.register(auth);
        }
        return resource;
    }

    private Client client()
    {
        return client.get();
    }
}
