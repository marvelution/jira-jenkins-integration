/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test;

import org.marvelution.jji.testkit.backdoor.*;

import org.junit.jupiter.api.extension.*;

/**
 * @author Mark Rekveld
 * @since 4.7.0
 */
public class TestTracer
		implements BeforeEachCallback, AfterEachCallback
{

	private final Backdoor backdoor = new Backdoor();

	@Override
	public void beforeEach(ExtensionContext context)
	{
		backdoor.testTracer().testStarted(context.getRequiredTestClass(), context.getDisplayName());
	}

	@Override
	public void afterEach(ExtensionContext context)
	{
		if (context.getExecutionException().isPresent())
		{
			backdoor.testTracer().testFailed(context.getRequiredTestClass(), context.getDisplayName());
		}
		else
		{
			backdoor.testTracer().testSucceeded(context.getRequiredTestClass(), context.getDisplayName());
		}
	}
}
