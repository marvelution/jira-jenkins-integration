/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.license;

import java.util.stream.*;

import org.marvelution.jji.test.condition.*;
import org.marvelution.jji.testkit.backdoor.*;

/**
 * @author Mark Rekveld
 * @since 3.10.0
 */
public class ServerSupportedLicenses
		implements SupportedLicenses
{

	private final Backdoor backdoor = new Backdoor();

	@Override
	public Stream<License> licenses()
	{
		String addonKey = System.getProperty("addon.key");
		if (backdoor.plugins().isPluginUsingLicensing(addonKey))
		{
			// Data center has no 10 second license, so don't test expired state, tests will take 1m longer per test
			return Stream.of(License.UNLICENSED, License.EXPIRED, License.NEARLY_EXPIRED);
		}
		else
		{
			return Stream.of(License.ACTIVE);
		}
	}
}
