/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.condition;

import java.util.Objects;
import java.util.Optional;

import org.marvelution.jji.testkit.backdoor.Backdoor;

import org.junit.jupiter.api.extension.ConditionEvaluationResult;
import org.junit.jupiter.api.extension.ExecutionCondition;
import org.junit.jupiter.api.extension.ExtensionContext;

import static org.junit.jupiter.api.extension.ConditionEvaluationResult.disabled;
import static org.junit.jupiter.api.extension.ConditionEvaluationResult.enabled;
import static org.junit.platform.commons.util.AnnotationUtils.findAnnotation;

/**
 * @author Mark Rekveld
 * @see EnabledIfAddonUsesLicensing
 * @since 3.7.0
 */
public class EnabledIfAddonUsesLicensingCondition implements ExecutionCondition {

	private static final ConditionEvaluationResult ENABLED_BY_DEFAULT = enabled("@EnabledIfAddonUsesLicensing is not present");
	private final Backdoor backdoor = new Backdoor();

	@Override
	public ConditionEvaluationResult evaluateExecutionCondition(ExtensionContext context) {
		Optional<EnabledIfAddonUsesLicensing> optional = findAnnotation(context.getElement(), EnabledIfAddonUsesLicensing.class);

		if (!optional.isPresent()) {
			return ENABLED_BY_DEFAULT;
		}

		EnabledIfAddonUsesLicensing annotation = optional.get();

		String addonKey;
		if (Objects.equals(annotation.addonKey(), EnabledIfAddonUsesLicensing.LOOKUP)) {
			addonKey = System.getProperty(annotation.addonKeyProperty());
		} else {
			addonKey = annotation.addonKey();
		}

		if (backdoor.plugins().isPluginUsingLicensing(addonKey)) {
			return enabled("Add-on with key '" + addonKey + "' uses licensing.");
		} else {
			return disabled("Add-on with key '" + addonKey + "' doesn't use licensing.");
		}
	}
}
