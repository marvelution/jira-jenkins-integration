/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.rest;

import java.lang.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import static org.marvelution.jji.test.AbstractIntegrationTest.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ParameterizedTest(name = "user: {0}")
@ArgumentsSource(ResourceAuthzTest.AuthzArgumentSource.class)
public @interface ResourceAuthzTest
{

    Response.Status anonymous() default Response.Status.UNAUTHORIZED;

    Response.Status user() default Response.Status.FORBIDDEN;

    Response.Status superUser() default Response.Status.FORBIDDEN;

    Response.Status administrator() default Response.Status.OK;

    class AuthzArgumentSource
            implements ArgumentsProvider
    {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context)
        {
            ResourceAuthzTest config = context.getElement()
                    .filter(element -> element.isAnnotationPresent(ResourceAuthzTest.class))
                    .map(element -> element.getAnnotation(ResourceAuthzTest.class))
                    .orElseThrow(() -> new AssertionError("missing ResourceAuthzTest annotation"));
            List<Arguments> arguments = new ArrayList<>();
            arguments.add(Arguments.of(ANONYMOUS, config.anonymous()));
            arguments.add(Arguments.of(USER, config.user()));
            arguments.add(Arguments.of(ADMIN, config.administrator()));
            if (!config.user()
                    .equals(config.superUser()))
            {
                arguments.add(Arguments.of(SUPER_USER, config.superUser()));
            }
            return arguments.stream();
        }
    }
}
