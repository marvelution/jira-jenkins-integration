/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test;

import org.marvelution.jji.test.license.*;
import org.marvelution.testing.*;

import org.junit.jupiter.api.extension.*;

/**
 * Base Integration Test
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
@ExtendWith({ AddonLicenseInstaller.class, TestTracer.class })
public abstract class AbstractIntegrationTest
		extends TestSupport
		implements BaseTest
{}
