/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.rest;

import java.util.function.Supplier;
import java.util.logging.Logger;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.Response;

import org.assertj.core.api.Assertions;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.logging.LoggingFeature;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.marvelution.jji.test.rest.AbstractResourceAuthzTest.Method.*;

public abstract class AbstractResourceAuthzTest
        extends AbstractResourceTest
{

    @BeforeEach
    public void setUpAnonymousDefaultAuth()
    {
        anonymous();
    }

    protected void testAuthzGet(
            WebTarget resource,
            AuthzPair authz)
    {
        testAuthz(GET, resource, authz);
    }

    protected void testAuthzGet(
            WebTarget resource,
            Entity<?> entity,
            AuthzPair authz)
    {
        testAuthz(GET, resource, () -> entity, authz);
    }

    protected void testAuthzPost(
            WebTarget resource,
            AuthzPair authz)
    {
        testAuthzPost(resource, Entity.json(""), authz);
    }

    protected void testAuthzPost(
            WebTarget resource,
            Entity<?> entity,
            AuthzPair authz)
    {
        testAuthz(POST, resource, () -> entity, authz);
    }

    protected void testAuthzPut(
            WebTarget resource,
            AuthzPair authz)
    {
        testAuthzPut(resource, Entity.json(""), authz);
    }

    protected void testAuthzPut(
            WebTarget resource,
            Entity<?> entity,
            AuthzPair authz)
    {
        testAuthz(PUT, resource, () -> entity, authz);
    }

    protected void testAuthzDelete(
            WebTarget resource,
            AuthzPair authz)
    {
        testAuthzDelete(resource, Entity.json(""), authz);
    }

    protected void testAuthzDelete(
            WebTarget resource,
            Entity<?> entity,
            AuthzPair authz)
    {
        testAuthz(DELETE, resource, () -> entity, authz);
    }

    private void testAuthz(
            Method method,
            WebTarget resource,
            AuthzPair authz)
    {
        testAuthz(method, resource, () -> null, authz);
    }

    private void testAuthz(
            Method method,
            WebTarget resource,
            Supplier<Entity<?>> entitySupplier,
            AuthzPair authz)
    {
        if (authz.feature != null)
        {
            // Add the required feature for this authz test
            resource.register(authz.feature);
        }
        resource.register(new LoggingFeature(Logger.getLogger(getClass().getName())));
        try (Response response = resource.request()
                .method(method.name(), entitySupplier.get(), Response.class))
        {
            Assertions.assertThat(response.getStatus())
                    .as(authz.description)
                    .isEqualTo(authz.expectedResponseStatus.getStatusCode());
        }
        catch (ClientErrorException e)
        {
            Assertions.assertThat(e.getResponse()
                            .getStatus())
                    .isEqualTo(authz.expectedResponseStatus.getStatusCode());
        }
    }

    protected AuthzPair anonymous(Response.Status expectedResponseStatus)
    {
        return new AuthzPair(ANONYMOUS, expectedResponseStatus);
    }

    protected AuthzPair user(Response.Status expectedResponseStatus)
    {
        return new AuthzPair(USER, expectedResponseStatus);
    }

    protected AuthzPair superuser(Response.Status expectedResponseStatus)
    {
        return new AuthzPair(SUPER_USER, expectedResponseStatus);
    }

    protected AuthzPair administrator(Response.Status expectedResponseStatus)
    {
        return new AuthzPair(ADMIN, expectedResponseStatus);
    }

    protected AuthzPair authzPair(
            String username,
            Response.Status expectedResponseStatus)
    {
        return new AuthzPair(username, expectedResponseStatus);
    }

    protected AuthzPair authzPair(
            String username,
            int expectedResponseStatus)
    {
        return new AuthzPair(username, Response.Status.fromStatusCode(expectedResponseStatus));
    }

    protected AuthzPair filtered(
            Feature feature,
            Response.Status expectedResponseStatus,
            String description)
    {
        return new AuthzPair(feature, expectedResponseStatus, description);
    }

    protected AuthzPair filtered(
            ClientRequestFilter filter,
            Response.Status expectedResponseStatus,
            String description)
    {
        return new AuthzPair(context -> {
            context.register(filter);
            return true;
        }, expectedResponseStatus, description);
    }

    @BeforeAll
    static void setUpAuthProject()
    {
        backdoor.generator()
                .getOrGenerateProject("AuthzTestProject",
                        name -> backdoor.generator()
                                .generateScrumProject(name));
    }

    protected enum Method
    {
        GET,
        PUT,
        POST,
        DELETE
    }

    protected static class AuthzPair
    {

        final Feature feature;
        final Response.Status expectedResponseStatus;
        final String description;

        private AuthzPair(
                String userpass,
                Response.Status expectedResponseStatus)
        {
            this(isNotBlank(userpass) ? HttpAuthenticationFeature.basic(userpass, userpass) : null,
                    expectedResponseStatus,
                    "checking " + userpass);
        }

        private AuthzPair(
                Feature feature,
                Response.Status expectedResponseStatus,
                String description)
        {
            this.feature = feature;
            this.expectedResponseStatus = expectedResponseStatus;
            this.description = description;
        }
    }
}
