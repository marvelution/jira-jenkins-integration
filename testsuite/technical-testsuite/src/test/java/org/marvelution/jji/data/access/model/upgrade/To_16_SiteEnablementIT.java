/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.upgrade;

import java.net.*;
import javax.annotation.*;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.marvelution.jji.data.access.*;
import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.access.model.v2.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.synctoken.utils.*;
import org.marvelution.jji.test.data.*;
import org.marvelution.testing.*;
import org.marvelution.testing.inject.*;

import com.atlassian.activeobjects.external.*;
import com.google.inject.*;
import com.google.inject.Module;
import net.java.ao.*;
import net.java.ao.schema.*;
import net.java.ao.test.jdbc.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;

import static org.marvelution.jji.data.access.model.v2.SiteEntity.*;
import static org.marvelution.jji.data.access.model.v2.SiteEntity.SHARED_SECRET;
import static org.marvelution.jji.model.SiteType.JENKINS;

import static net.java.ao.schema.StringLength.MAX_LENGTH;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith({ ActiveObjectsExtension.class, InjectorExtension.class })
@Data(To_16_SiteEnablementIT.SetupTestData.class)
public class To_16_SiteEnablementIT
		extends TestSupport
		implements Module
{

	private final ActiveObjectsExtension.EntityManagerContext entityManagerContext;
	@Inject
	private ActiveObjects activeObjects;
	@Inject
	private SiteDAO siteDAO;
	@Inject
	private To_16_SiteEnablement underTest;

	public To_16_SiteEnablementIT(ActiveObjectsExtension.EntityManagerContext entityManagerContext)
	{
		this.entityManagerContext = entityManagerContext;
	}

	@Override
	public void configure(Binder binder)
	{
		binder.install(new DataAccessModule(entityManagerContext));
		binder.bind(To_16_SiteEnablement.class).in(Singleton.class);
	}

	@Test
	void testUpgrade()
	{
		underTest.upgrade(ModelVersion.valueOf("15"), activeObjects);

		Site site = siteDAO.get("site-1");
		assertThat(site).isNotNull().extracting(Site::isEnabled).isEqualTo(true);
	}

	public static class SetupTestData
			implements DatabaseUpdater
	{

		@Override
		public void update(EntityManager entityManager)
				throws Exception
		{
			entityManager.migrate(SiteV2Entity.class);

			entityManager.create(SiteEntity.class, new DBParam("ID", "site-1"), new DBParam(SiteEntity.NAME, "Jenkins"),
			                     new DBParam(SITE_TYPE, JENKINS), new DBParam(RPC_URL, URI.create("http://localhost:8080/")),
			                     new DBParam(AUTO_LINK_NEW_JOBS, true), new DBParam(JENKINS_PLUGIN_INSTALLED, true),
			                     new DBParam(SHARED_SECRET, SharedSecretGenerator.generate()));
		}
	}

	@Preload
	@Table(org.marvelution.jji.data.access.model.v2.SiteEntity.TABLE_NAME)
	public interface SiteV2Entity
			extends org.marvelution.jji.data.access.model.Entity
	{

		@NotNull
		String getName();

		void setName(String name);

		@NotNull
		@StringLength(MAX_LENGTH)
		String getSharedSecret();

		void setSharedSecret(String sharedSecret);

		@NotNull
		SiteType getSiteType();

		void setSiteType(SiteType siteType);

		@NotNull
		URI getRpcUrl();

		void setRpcUrl(URI rpcUrl);

		@Nullable
		URI getDisplayUrl();

		void setDisplayUrl(@Nullable URI displayUrl);

		SiteAuthenticationType getSiteAuthentication();

		void setSiteAuthentication(SiteAuthenticationType siteAuthenticationType);

		@Nullable
		String getUser();

		void setUser(@Nullable String user);

		@Nullable
		String getUserToken();

		void setUserToken(@Nullable String userToken);

		boolean isUseCrumbs();

		void setUseCrumbs(boolean useCrumbs);

		boolean isAutoLinkNewJobs();

		void setAutoLinkNewJobs(boolean autoLinkNewJobs);

		boolean isJenkinsPluginInstalled();

		void setJenkinsPluginInstalled(boolean jenkinsPluginInstalled);

		boolean isRegistrationComplete();

		void setRegistrationComplete(boolean registrationComplete);

		boolean isFirewalled();

		void setFirewalled(boolean firewalled);
	}
}
