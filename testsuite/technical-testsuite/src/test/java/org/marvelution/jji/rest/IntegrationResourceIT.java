/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.marvelution.jji.model.*;
import org.marvelution.jji.synctoken.jersey.SyncTokenClientFilter;
import org.marvelution.jji.test.rest.AbstractResourceTest;
import org.marvelution.testing.wiremock.FileSource;
import org.marvelution.testing.wiremock.WireMockOptions;
import org.marvelution.testing.wiremock.WireMockServer;

import com.atlassian.jira.testkit.client.restclient.Project;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import org.assertj.core.api.HamcrestCondition;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static com.github.tomakehurst.wiremock.core.WireMockApp.FILES_ROOT;
import static com.github.tomakehurst.wiremock.http.RequestMethod.GET;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.marvelution.jji.data.services.IntegrationApiService.SYNC_RESULT_HEADER;
import static org.marvelution.jji.model.Matchers.equalTo;
import static org.marvelution.jji.test.rest.Matchers.status;
import static org.marvelution.testing.wiremock.FileSourceType.UNDER_CLASSPATH;

class IntegrationResourceIT
        extends AbstractResourceTest
        implements WebResources
{

    private static final GenericType<Map<String, String>> LINKS_TYPE = new GenericType<Map<String, String>>() {};
    private WireMockServer jenkins;
    private Job job;

    @BeforeEach
    void setUp(
            @WireMockOptions(fileSource = @FileSource(type = UNDER_CLASSPATH,
                                                      path = "IntegrationResourceIT"))
            WireMockServer jenkins)
    {
        this.jenkins = jenkins;
        Site site = backdoor.sites()
                .addPublicSite(SiteType.JENKINS, getTestMethodName(), this.jenkins.serverUri(), true);
        job = backdoor.jobs()
                .saveJob(new Job().setSite(site)
                        .setName("jira-jenkins-integration")
                        .setLinked(true));
        backdoor.sites()
                .populateSharedSecret(job.getSite());
        loginWith(new SyncTokenClientFilter(job.getSite()
                .getId(),
                job.getSite()
                        .getSharedSecret(),
                backdoor.environmentData()
                        .getContext()));
    }

    @AfterEach
    void teardown()
    {
        backdoor.sites()
                .clearSites();
    }

    @Test
    void testGetBuildLinks()
            throws Exception
    {
        Project project = backdoor.generator()
                .generateScrumProject(getTestMethodName());
        String issueKey = backdoor.issues()
                .createIssue(project.key, "Build Link Test")
                .key();
        Build build = backdoor.builds()
                .addBuild(job);
        Map<String, String> links = integrationResource(build, "links").request()
                .get(LINKS_TYPE);
        assertThat(links.isEmpty()).isTrue();
        backdoor.builds()
                .linkBuildToIssue(build, issueKey);
        links = integrationResource(build, "links").request()
                .get(LINKS_TYPE);
        assertThat(links).isNotEmpty()
                .hasSize(1)
                .containsEntry(issueKey, UriBuilder.fromUri(backdoor.environmentData()
                                .getBaseUrl()
                                .toURI())
                        .path("browse")
                        .path(issueKey)
                        .build()
                        .toASCIIString());
    }

    @Test
    void testGetBuildLinks_UnknownJob()
    {
        Map<String, String> links = integrationResource(new Job().setName("unknown-job"), 1, "links").request()
                .get(LINKS_TYPE);
        assertThat(links).isEmpty();
    }

    @Test
    void testGetBuildLinks_UnknownBuild()
    {
        Map<String, String> links = integrationResource(job, 100, "links").request()
                .get(LINKS_TYPE);
        assertThat(links).isEmpty();
    }

    @Test
    void testMarkJobAsDeleted()
    {
        Job job = backdoor.jobs()
                .addJob(getTestMethodName());
        testMarkJobAsDeleted(job);

        await().atMost(5, SECONDS)
                .untilAsserted(() -> assertThat(backdoor.jobs()
                        .getJob(job.getId())
                        .isDeleted()).isTrue());
    }

    @Test
    void testMarkJobAsDeleted_UnknownJob()
    {
        testMarkJobAsDeleted(new Job().setName("unknown-job"));
    }

    @Test
    void testMarkBuildAsDeleted()
    {
        Job job = backdoor.jobs()
                .addJob(getTestMethodName());
        Build build = backdoor.builds()
                .addBuild(job, Result.SUCCESS);

        testMarkJobAsDeleted(job, build.getNumber());

        await().atMost(5, SECONDS)
                .untilAsserted(() -> assertThat(backdoor.builds()
                        .getBuild(build.getId())
                        .isDeleted()).isTrue());
    }

    @Test
    void testMarkBuildAsDeleted_UnknownJob()
    {
        testMarkJobAsDeleted(new Job().setName("unknown-job"), 1);
    }

    @Test
    void testMarkBuildAsDeleted_UnknownBuild()
    {
        testMarkJobAsDeleted(job, 10);
    }

    private void testMarkJobAsDeleted(Job job)
    {
        testMarkJobAsDeleted(job, -1);
    }

    private void testMarkJobAsDeleted(
            Job job,
            int build)
    {
        Response response = integrationResource(job, build).request()
                .delete(Response.class);
        assertThat(response).is(HamcrestCondition.matching(status(Response.Status.NO_CONTENT)));
    }

    @Test
    void testGetRegistrationDetails()
    {
        Response response = integrationResource("register",
                job.getSite()
                        .getId()).request()
                .get(Response.class);
        assertThat(response).is(HamcrestCondition.matching(status(Response.Status.OK)));
        Map<Object, Object> details = response.readEntity(new GenericType<Map<Object, Object>>() {});
        assertThat(details).containsEntry("url",
                        backdoor.configuration()
                                .getBaseAPIUrl()
                                .toASCIIString())
                .containsEntry("name",
                        backdoor.configuration()
                                .getInstanceName())
                .containsEntry("identifier",
                        job.getSite()
                                .getId())
                .containsEntry("sharedSecret",
                        job.getSite()
                                .getSharedSecret())
                .containsEntry("firewalled",
                        job.getSite()
                                .isInaccessibleSite())
                .containsEntry("tunneled",
                        job.getSite()
                                .isTunneledSite());
    }

    @Test
    void testGetRegistrationDetails_UnknownSite()
    {
        Response response = integrationResource("register", "unknown-id").request()
                .get(Response.class);
        assertThat(response).is(HamcrestCondition.matching(status(Response.Status.UNAUTHORIZED)));
    }

    @Test
    void testCompleteRegistration()
    {
        assertThat(job.getSite()
                .isRegistrationComplete()).isFalse();
        Response response = integrationResource("register",
                job.getSite()
                        .getId()).request()
                .post(null, Response.class);
        assertThat(response).is(HamcrestCondition.matching(status(Response.Status.ACCEPTED)));
        assertThat(backdoor.sites()
                .getSite(job.getSite()
                        .getId())
                .isRegistrationComplete()).isTrue();
    }

    @Test
    void testCompleteRegistration_UnknownSite()
    {
        Response response = integrationResource("register", "unknown-id").request()
                .post(null, Response.class);
        assertThat(response).is(HamcrestCondition.matching(status(Response.Status.UNAUTHORIZED)));
    }

    @Nested
    class SynchronizeByPut
    {

        @Test
        void testSynchronizeJob()
        {
            Response response = testSynchronize(job);
            assertThat(response).is(HamcrestCondition.matching(status(Response.Status.ACCEPTED)));
            assertThat(response.getHeaders()
                    .getFirst(SYNC_RESULT_HEADER)).isNotNull();

            assertSynchronizationRequest();
        }

        @Test
        void testSynchronizeJob_UnknownJob()
        {
            Response response = testSynchronize(new Job().setName("unknown-job"));
            assertThat(response).is(HamcrestCondition.matching(status(Response.Status.ACCEPTED)));
            assertThat(response.getHeaders()
                    .getFirst(SYNC_RESULT_HEADER)).isNotNull();
        }

        private Response testSynchronize(Job job)
        {
            return testSynchronize(job, -1);
        }

        @Test
        void testSynchronizeBuild()
        {
            Response response = testSynchronize(job, 1);
            assertThat(response).is(HamcrestCondition.matching(status(Response.Status.ACCEPTED)));
            assertThat(response.getHeaders()
                    .getFirst(SYNC_RESULT_HEADER)).isNotNull();

            assertSynchronizationRequest();
        }

        @Test
        void testSynchronizeBuild_UnknownJob()
        {
            Response response = testSynchronize(new Job().setName("unknown-job"), 1);
            assertThat(response).is(HamcrestCondition.matching(status(Response.Status.ACCEPTED)));
            assertThat(response.getHeaders()
                    .getFirst(SYNC_RESULT_HEADER)).isNotNull();
        }

        private Response testSynchronize(
                Job job,
                int build)
        {
            Response response = integrationResource(job, build).request(MediaType.APPLICATION_JSON)
                    .put(Entity.json(""), Response.class);
            assertThat(response.getStatusInfo()
                    .getFamily()).as("Expecting successful put; %s", response.getStatus())
                    .isEqualTo(Response.Status.Family.SUCCESSFUL);
            return response;
        }

        @Test
        void testSynchronizeBuild_UnknownJobWithParents()
        {
            List<String> parentHashes = new ArrayList<>();
            parentHashes.add(sha1Hex(job.getUrlName()));
            Response response = integrationResource(new Job().setName("unknown-job"), 1).request()
                    .put(Entity.json(parentHashes), Response.class);
            assertThat(response).is(HamcrestCondition.matching(status(Response.Status.ACCEPTED)));
            assertThat(response.getHeaders()
                    .getFirst(SYNC_RESULT_HEADER)).isNotNull();

            assertSynchronizationRequest();
        }

        private void assertSynchronizationRequest()
        {
            await().atMost(30, SECONDS)
                    .until(this::findJobRequests,
                            hasItems(loggedRequestFor("/job/jira-jenkins-integration/api/json/"),
                                    loggedRequestFor("/job/jira-jenkins-integration/1/api/json/")));
        }

        private List<LoggedRequest> findJobRequests()
        {
            return jenkins.findAll(new RequestPatternBuilder(GET, urlPathMatching("/job/jira-jenkins-integration/([0-9]+/)?api/json/")));
        }

        private Matcher<LoggedRequest> loggedRequestFor(String url)
        {
            return new TypeSafeDiagnosingMatcher<LoggedRequest>()
            {
                @Override
                protected boolean matchesSafely(
                        LoggedRequest item,
                        Description mismatchDescription)
                {
                    if (urlPathEqualTo(url).match(item.getUrl())
                            .isExactMatch())
                    {
                        return true;
                    }
                    mismatchDescription.appendText("has url ")
                            .appendValue(item.getUrl());
                    return false;
                }

                @Override
                public void describeTo(Description description)
                {
                    description.appendText("request to ")
                            .appendValue(url);
                }
            };
        }
    }

    @Nested
    class SynchronizeByPost
    {

        @Test
        void testSynchronizeJob()
        {
            Response response = testSynchronize(job, "jira-jenkins-integration.json");
            assertThat(response).is(HamcrestCondition.matching(status(Response.Status.ACCEPTED)));
            assertThat(response.getHeaders()
                    .getFirst(SYNC_RESULT_HEADER)).isNotNull();

            await().atMost(30, SECONDS)
                    .until(() -> backdoor.jobs()
                                    .getJob(job.getId()),
                            equalTo(new Job().setSite(job.getSite())
                                    .setId(job.getId())
                                    .setName("jira-jenkins-integration")
                                    .setLastBuild(1)
                                    .setOldestBuild(1)
                                    .setLinked(true)));
        }

        @Test
        void testSynchronizeJob_UnknownJob()
        {
            Response response = testSynchronize(new Job().setName("jenkins-jira-integration"), "jenkins-jira-integration.json");
            assertThat(response).is(HamcrestCondition.matching(status(Response.Status.NO_CONTENT)));
            assertThat(response.getHeaders()
                    .getFirst(SYNC_RESULT_HEADER)).isNull();

            assertThat(backdoor.sites()
                    .getSite(job.getSite()
                            .getId(), true)
                    .getJobs()).haveAtLeastOne(HamcrestCondition.matching(equalTo(new Job().setSite(job.getSite())
                    .setName("jenkins-jira-integration")
                    .setLastBuild(1)
                    .setOldestBuild(1)
                    .setLinked(true), true)));
        }

        @Test
        void testSynchronizeJob_RenamedToExistingJob()
        {
            Response response = testSynchronize(new Job().setName("unknown-job"), "jira-jenkins-integration.json");
            assertThat(response).is(HamcrestCondition.matching(status(Response.Status.NO_CONTENT)));
            assertThat(response.getHeaders()
                    .getFirst(SYNC_RESULT_HEADER)).isNull();

            assertThat(backdoor.sites()
                    .getSite(job.getSite()
                            .getId(), true)
                    .getJobs()).haveAtLeastOne(HamcrestCondition.matching(equalTo(new Job().setSite(job.getSite())
                    .setId(job.getId())
                    .setName("jira-jenkins-integration")
                    .setLastBuild(1)
                    .setOldestBuild(1)
                    .setLinked(true))));
        }

        private Response testSynchronize(
                Job job,
                String resource)
        {
            return testSynchronize(job, -1, resource);
        }

        @Test
        void testSynchronizeBuild()
        {
            Response response = testSynchronize(job, 1, "jira-jenkins-integration-build.json");
            assertThat(response).is(HamcrestCondition.matching(status(Response.Status.ACCEPTED)));
            assertThat(response.getHeaders()
                    .getFirst(SYNC_RESULT_HEADER)).isNotNull();

            await().atMost(30, SECONDS)
                    .until(() -> backdoor.jobs()
                                    .getJob(job.getId()),
                            equalTo(new Job().setSite(job.getSite())
                                    .setId(job.getId())
                                    .setName("jira-jenkins-integration")
                                    .setLastBuild(1)
                                    .setLinked(true)));
            await().atMost(30, SECONDS)
                    .until(() -> backdoor.builds()
                                    .getBuilds(job),
                            hasItem(equalTo(new Build().setJob(job)
                                    .setNumber(1)
                                    .setDescription("My build description")
                                    .setCause("Started by an SCM change")
                                    .setResult(Result.FAILURE)
                                    .setDuration(135329L)
                                    .setTimestamp(1355405446078L)
                                    .setTestResults(new TestResults().setTotal(3121)
                                            .setFailed(0)
                                            .setSkipped(0)), true)));
        }

        @Test
        void testSynchronizeBuild_UnknownJob()
        {
            Response response = testSynchronize(new Job().setName("unknown"), 1, "jira-jenkins-integration-build.json");
            assertThat(response).is(HamcrestCondition.matching(status(Response.Status.ACCEPTED)));
            assertThat(response.getHeaders()
                    .getFirst(SYNC_RESULT_HEADER)).isNotNull();

            await().atMost(30, SECONDS)
                    .until(() -> backdoor.sites()
                                    .getSite(job.getSite()
                                            .getId(), true)
                                    .getJobs(),
                            hasItem(equalTo(new Job().setSite(job.getSite())
                                    .setName("jira-jenkins-integration")
                                    .setLastBuild(1)
                                    .setLinked(true), true)));
            await().atMost(30, SECONDS)
                    .until(() -> backdoor.builds()
                                    .getBuilds(job),
                            hasItem(equalTo(new Build().setJob(job)
                                    .setNumber(1)
                                    .setDescription("My build description")
                                    .setCause("Started by an SCM change")
                                    .setResult(Result.FAILURE)
                                    .setDuration(135329L)
                                    .setTimestamp(1355405446078L)
                                    .setTestResults(new TestResults().setTotal(3121)
                                            .setFailed(0)
                                            .setSkipped(0)), true)));
        }

        @Test
        void testSynchronizeBuild_UnknownJobWithoutParentInBuildData()
        {
            Response response = testSynchronize(new Job().setName("unknown"), 1, "jira-jenkins-integration-build-no-parent.json");
            assertThat(response).is(HamcrestCondition.matching(status(Response.Status.NO_CONTENT)));
            assertThat(response.getHeaders()
                    .getFirst(SYNC_RESULT_HEADER)).isNull();
        }

        private Response testSynchronize(
                Job job,
                int buildNumber,
                String resource)
        {
            String json = jenkins.getOptions()
                    .filesRoot()
                    .child(FILES_ROOT)
                    .getTextFileNamed(resource)
                    .readContentsAsString();
            return integrationResource(job, buildNumber).request()
                    .post(Entity.json(json), Response.class);
        }
    }
}
