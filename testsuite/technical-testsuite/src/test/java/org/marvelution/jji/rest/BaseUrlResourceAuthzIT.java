/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.URI;
import javax.ws.rs.core.Response;

import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;
import org.marvelution.jji.synctoken.jersey.SyncTokenClientFilter;
import org.marvelution.jji.test.rest.AbstractResourceAuthzTest;
import org.marvelution.jji.test.rest.ResourceAuthzTest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Integration Authz Testcase for the base-url REST resource.
 *
 * @author Mark Rekveld
 * @since 3.2.1
 */
class BaseUrlResourceAuthzIT
        extends AbstractResourceAuthzTest
        implements WebResources
{

    private Site site;

    @BeforeEach
    void setUp()
    {
        site = backdoor.sites()
                .addFirewalledSite(SiteType.JENKINS, "Local Test", URI.create("http://localhost:8080"));
        backdoor.sites()
                .populateSharedSecret(site);
    }

    @AfterEach
    void tearDown()
    {
        backdoor.sites()
                .clearSites();
    }

    @ResourceAuthzTest(user = Response.Status.UNAUTHORIZED,
                       superUser = Response.Status.UNAUTHORIZED,
                       administrator = Response.Status.UNAUTHORIZED)
    void testGetBaseUrl(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzGet(baseUrlResource(), authzPair(username, expectedStatus));
    }

    @Test
    void testGetBaseUrl()
    {
        testAuthzGet(baseUrlResource(),
                filtered(new SyncTokenClientFilter(site.getId(),
                        site.getSharedSecret(),
                        backdoor.environmentData()
                                .getContext()), Response.Status.OK, "sync-token " + site.getName()));
    }
}
