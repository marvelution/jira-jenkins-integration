/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.marvelution.jji.model.*;
import org.marvelution.jji.model.request.TriggerBuildsRequest;
import org.marvelution.jji.test.rest.AbstractResourceAuthzTest;
import org.marvelution.testing.wiremock.WireMockOptions;
import org.marvelution.testing.wiremock.WireMockServer;

import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.VersionClient;
import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.marvelution.jji.data.services.BaseSiteClient.X_JIRA_INTEGRATION;
import static org.marvelution.jji.data.services.api.ConfigurationService.PARAMETER_ISSUE_FIELDS;
import static org.marvelution.jji.data.services.api.ConfigurationService.USE_USER_ID;

public class JobResourceIT
        extends AbstractResourceAuthzTest
        implements WebResources
{

    private WireMockServer jenkins;
    private Job job;
    private String issueKey;
    private Project project;

    @BeforeEach
    void setUp(
            @WireMockOptions
            WireMockServer jenkins)
    {
        this.jenkins = jenkins;
        this.jenkins.expect(RequestMethod.GET,
                urlPathEqualTo("/plugin/jira-integration/ping.html"),
                aResponse().withStatus(200)
                        .withHeader(X_JIRA_INTEGRATION, "5.2.0"));
        this.jenkins.expect(RequestMethod.POST, urlPathEqualTo("/jji/register/"), aResponse().withStatus(200));
        Site site = backdoor.sites()
                .saveSite(new Site().setType(SiteType.JENKINS)
                        .setName(getTestMethodName())
                        .setRpcUrl(jenkins.serverUri())
                        .setAccessibleSite()
                        .setAutoLinkNewJobs(true)
                        .setJenkinsPluginInstalled(true));
        job = backdoor.jobs()
                .saveJob(new Job().setSite(site)
                        .setName(getTestMethodName())
                        .setLinked(true));
        backdoor.configuration()
                .saveConfiguration(new Configuration().addConfiguration(ConfigurationSetting.forKey(PARAMETER_ISSUE_FIELDS)
                                .setValue("creator,labels,fixVersions"))
                        .addConfiguration(ConfigurationSetting.forKey(USE_USER_ID)
                                .setValue(false)));
        project = backdoor.generator()
                .getOrGenerateProject(getTestMethodName(),
                        name -> backdoor.generator()
                                .generateScrumProject(name));
        issueKey = backdoor.issues()
                .createIssue(project.key, getTestMethodName(), "admin").key;
    }

    @AfterEach
    void cleanup()
    {
        backdoor.sites()
                .clearSites();
    }

    @Test
    void testTriggerBuild()
    {
        com.atlassian.jira.testkit.client.restclient.Version version = backdoor.createRestClient(VersionClient.class)
                .create(new com.atlassian.jira.testkit.client.restclient.Version().name("Version 2.0")
                        .projectId(Long.parseLong(project.id)));
        backdoor.issues()
                .addFixVersion(issueKey, version.name);
        backdoor.issues()
                .addLabel(issueKey, "support");

        TriggerBuildsRequest request = new TriggerBuildsRequest().setIssueKey(issueKey)
                .setJobs(Stream.of(job.getId())
                        .collect(Collectors.toSet()));

        asSuperUser();
        Response response = jobResource("build").request()
                .accept(APPLICATION_JSON_TYPE)
                .post(Entity.json(request), Response.class);

        assertThat(response.getStatus()).isEqualTo(200);

        RequestPatternBuilder requestPatternBuilder = new RequestPatternBuilder(RequestMethod.POST,
                urlPathEqualTo("/job/" + getTestMethodName() + "/jji/build/"));
        jenkins.verify(requestPatternBuilder);

        URI baseUrl = backdoor.configuration()
                .getBaseUrl();
        String superUserName = backdoor.users()
                .displayName(SUPER_USER);
        String adminName = backdoor.users()
                .displayName(ADMIN);
        List<LoggedRequest> requests = jenkins.findAll(requestPatternBuilder);
        assertThat(requests).hasSize(1)
                .first()
                .extracting(LoggedRequest::getBodyAsString)
                .isEqualTo("{\"by\":\"" + superUserName + "\",\"issueUrl\":\"" + baseUrl + "/browse/" + issueKey + "\",\"issueKey\":\"" +
                           issueKey + "\",\"parameters\":[{\"name\":\"Labels\",\"value\":\"support\"}," +
                           "{\"name\":\"Fix_Version_s\",\"value\":\"Version 2.0\"},{\"name\":\"Creator\",\"value\":\"" + adminName +
                           "\"}]}");
    }

    @Test
    void testTriggerBuildEmptyFields()
    {
        TriggerBuildsRequest request = new TriggerBuildsRequest().setIssueKey(issueKey)
                .setJobs(Stream.of(job.getId())
                        .collect(Collectors.toSet()));

        asSuperUser();
        Response response = jobResource("build").request()
                .accept(APPLICATION_JSON_TYPE)
                .post(Entity.json(request), Response.class);

        assertThat(response.getStatus()).isEqualTo(200);

        RequestPatternBuilder requestPatternBuilder = new RequestPatternBuilder(RequestMethod.POST,
                urlPathEqualTo("/job/" + getTestMethodName() + "/jji/build/"));
        jenkins.verify(requestPatternBuilder);

        URI baseUrl = backdoor.configuration()
                .getBaseUrl();
        String superUserName = backdoor.users()
                .displayName(SUPER_USER);
        String adminName = backdoor.users()
                .displayName(ADMIN);
        List<LoggedRequest> requests = jenkins.findAll(requestPatternBuilder);
        assertThat(requests).hasSize(1)
                .first()
                .extracting(LoggedRequest::getBodyAsString)
                .isEqualTo("{\"by\":\"" + superUserName + "\",\"issueUrl\":\"" + baseUrl + "/browse/" + issueKey + "\",\"issueKey\":\"" +
                           issueKey + "\",\"parameters\":[{\"name\":\"Creator\",\"value\":\"" + adminName + "\"}]}");
    }

    @Test
    void testTriggerBuildUserId()
    {
        backdoor.configuration()
                .saveConfiguration(new Configuration().addConfiguration(ConfigurationSetting.forKey(PARAMETER_ISSUE_FIELDS)
                                .setValue("creator,labels,fixVersions"))
                        .addConfiguration(ConfigurationSetting.forKey(USE_USER_ID)
                                .setValue(true)));
        TriggerBuildsRequest request = new TriggerBuildsRequest().setIssueKey(issueKey)
                .setJobs(Stream.of(job.getId())
                        .collect(Collectors.toSet()));

        asSuperUser();
        Response response = jobResource("build").request()
                .accept(APPLICATION_JSON_TYPE)
                .post(Entity.json(request), Response.class);

        assertThat(response.getStatus()).isEqualTo(200);

        RequestPatternBuilder requestPatternBuilder = new RequestPatternBuilder(RequestMethod.POST,
                urlPathEqualTo("/job/" + getTestMethodName() + "/jji/build/"));
        jenkins.verify(requestPatternBuilder);

        URI baseUrl = backdoor.configuration()
                .getBaseUrl();
        String superUserId = String.valueOf(backdoor.users()
                .userId(SUPER_USER));
        String adminId = String.valueOf(backdoor.users()
                .userId(ADMIN));
        List<LoggedRequest> requests = jenkins.findAll(requestPatternBuilder);
        assertThat(requests).hasSize(1)
                .first()
                .extracting(LoggedRequest::getBodyAsString)
                .isEqualTo("{\"by\":\"" + superUserId + "\",\"issueUrl\":\"" + baseUrl + "/browse/" + issueKey + "\",\"issueKey\":\"" +
                           issueKey + "\",\"parameters\":[{\"name\":\"Creator\",\"value\":\"" + adminId + "\"}]}");
    }
}
