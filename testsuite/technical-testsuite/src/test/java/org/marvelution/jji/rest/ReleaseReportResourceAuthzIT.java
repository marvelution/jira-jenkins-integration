/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import javax.ws.rs.core.Response;

import org.marvelution.jji.test.rest.AbstractResourceAuthzTest;
import org.marvelution.jji.test.rest.ResourceAuthzTest;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Project;
import org.junit.jupiter.api.BeforeEach;

/**
 * Integration Authz Testcase for the release report REST resource.
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
class ReleaseReportResourceAuthzIT
        extends AbstractResourceAuthzTest
        implements WebResources
{

    private IssueCreateResponse issue;

    @BeforeEach
    void setUp()
    {
        Project project = backdoor.generator()
                .generateScrumProject(getTestMethodName());
        issue = backdoor.issues()
                .createIssue(project.key, "Authz Test: " + getTestMethodName());
    }

    @ResourceAuthzTest(user = Response.Status.OK,
                       superUser = Response.Status.OK)
    void testBuildInformation(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzGet(releaseReportResource().path("build-info/" + issue.key), authzPair(username, expectedStatus));
    }
}
