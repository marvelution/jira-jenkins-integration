/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.*;
import java.util.ArrayList;
import java.util.*;
import java.util.concurrent.*;

import org.marvelution.jji.api.events.*;
import org.marvelution.jji.data.access.*;
import org.marvelution.jji.data.access.api.IssueReferenceProvider;
import org.marvelution.jji.model.*;
import org.marvelution.jji.test.data.*;

import com.atlassian.jira.event.*;
import com.atlassian.jira.event.issue.*;
import com.atlassian.jira.event.type.*;
import com.atlassian.jira.issue.*;
import com.atlassian.jira.project.*;
import com.google.inject.*;
import com.google.inject.util.*;
import net.java.ao.test.jdbc.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;

import static org.marvelution.jji.utils.KeyExtractor.*;

import static java.util.Arrays.stream;
import static java.util.Objects.*;
import static java.util.stream.Collectors.*;
import static org.assertj.core.api.Assertions.*;
import static org.awaitility.Awaitility.*;
import static org.mockito.Mockito.*;

@ExtendWith(ActiveObjectsExtension.class)
@Data(ModelDatabaseUpdater.class)
class DefaultIssueLinkServiceIT
		extends AbstractBaseIssueLinkServiceIT<DefaultIssueLinkService>
{

	private final ActiveObjectsExtension.EntityManagerContext entityManagerContext;
	private InMemoryEventPublisher eventPublisher;
	@Inject
	private IssueReferenceProvider issueReferenceProvider;
	private Set<IssueReference> issueReferences;

	public DefaultIssueLinkServiceIT(ActiveObjectsExtension.EntityManagerContext entityManagerContext)
	{
		this.entityManagerContext = entityManagerContext;
	}

	@Override
	public void configure(Binder binder)
	{
		eventPublisher = new InMemoryEventPublisher();
		binder.install(Modules.override(new DataAccessModule(entityManagerContext)).with(new org.marvelution.testing.inject.AbstractModule()
		{
			@Override
			protected void configure()
			{
				bindMock(IssueReferenceProvider.class, withSettings().lenient());
				bind(EventPublisher.class).toInstance(eventPublisher);
			}
		}));
		binder.install(new DataServicesModule());
	}

	static class InMemoryEventPublisher
			implements EventPublisher
	{

		private final List<Object> events = new ArrayList<>();

		@Override
		public void publish(Object event)
		{
			events.add(event);
		}

		void assertEventPublished(Object event)
		{
			await().atMost(10, TimeUnit.SECONDS).untilAsserted(() -> assertThat(events).contains(event));
		}
	}

	@Override
	protected <T> void assertEventPublished(
			EventPublisher eventPublisher,
			T event)
	{
		this.eventPublisher.assertEventPublished(event);
	}

	@BeforeEach
	@SuppressWarnings("unchecked")
	void setUpMocks()
	{
		issueReferences = new HashSet<>();
		when(issueReferenceProvider.getIssueReference(anyString())).then(invocation -> {
			String issueKey = invocation.getArgument(0);
			return issueReferences.stream().filter(reference -> reference.getIssueKey().equals(issueKey)).findFirst();
		});
		when(issueReferenceProvider.getIssueReferences(any(Set.class))).then(invocation -> {
			Set<String> issueKeys = invocation.getArgument(0);
			return issueReferences.stream().filter(reference -> issueKeys.contains(reference.getIssueKey())).collect(toSet());
		});
		doAnswer(invocation -> {
			IssueReference reference = invocation.getArgument(0);
			reference.setIssueUrl(URI.create("https://jira.example.com/browse/" + reference.getIssueKey()));
			return null;
		}).when(issueReferenceProvider).setIssueUrl(any(IssueReference.class));
	}

	@Override
	protected void setupIssueReference(String... issueKeys)
	{
		requireNonNull(issueKeys);
		stream(issueKeys).map(key -> new IssueReference().setIssueKey(key).setProjectKey(extractProjectKeyFromIssueKey(key)))
				.forEach(issueReferences::add);
	}

	@Test
	void testOnIssueEvent()
	{
		Build build = setupBuild(createBuild(job), new IssueReference().setIssueKey("PR-1").setProjectKey("PR"),
		                         new IssueReference().setIssueKey("PR-2").setProjectKey("PR"),
		                         new IssueReference().setIssueKey("PR-3").setProjectKey("PR"));

		assertThat(issueLinkService.getRelatedIssueKeys(build)).hasSize(3).containsOnly("PR-1", "PR-2", "PR-3");

		Issue issue = mock(Issue.class);
		when(issue.getKey()).thenReturn("PR-1");
		issueLinkService.onIssueEvent(new IssueEvent(issue, null, null, EventType.ISSUE_UPDATED_ID));

		assertThat(issueLinkService.getRelatedIssueKeys(build)).hasSize(3).containsOnly("PR-1", "PR-2", "PR-3");

		issueLinkService.onIssueEvent(new IssueEvent(issue, null, null, EventType.ISSUE_DELETED_ID));

		assertThat(issueLinkService.getRelatedIssueKeys(build)).hasSize(2).containsOnly("PR-2", "PR-3");
	}

	@Test
	void testOnProjectDeleted()
	{
		Build build = setupBuild(createBuild(job), new IssueReference().setIssueKey("PR-1").setProjectKey("PR"),
		                         new IssueReference().setIssueKey("PR-2").setProjectKey("PR"),
		                         new IssueReference().setIssueKey("OP-1").setProjectKey("OP"));

		assertThat(issueLinkService.getRelatedIssueKeys(build)).containsOnly("PR-1", "PR-2", "OP-1");

		Project project = mock(Project.class);
		when(project.getKey()).thenReturn("PR");
		issueLinkService.onProjectDeleted(new ProjectDeletedEvent(null, project));

		assertThat(issueLinkService.getRelatedIssueKeys(build)).containsOnly("OP-1");
	}
}
