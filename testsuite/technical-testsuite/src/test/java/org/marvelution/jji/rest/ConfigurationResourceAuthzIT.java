/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.model.request.EnabledState;
import org.marvelution.jji.model.request.EvaluateParameterIssueFieldsRequest;
import org.marvelution.jji.test.condition.DisabledIfLiteApp;
import org.marvelution.jji.test.rest.AbstractResourceAuthzTest;
import org.marvelution.jji.test.rest.ResourceAuthzTest;

import static org.marvelution.jji.api.features.FeatureFlags.DYNAMIC_UI_ELEMENTS;

class ConfigurationResourceAuthzIT
        extends AbstractResourceAuthzTest
        implements WebResources
{

    @ResourceAuthzTest
    void testGetConfiguration(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzGet(configurationResource(), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testSaveConfiguration(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzPost(configurationResource(), Entity.json(new Configuration()), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.OK)
    void testEvaluateParameterIssueFields(
            String username,
            Response.Status expectedStatus)
    {
        EvaluateParameterIssueFieldsRequest request = new EvaluateParameterIssueFieldsRequest();
        request.setIssueKey("TEST-1");
        request.setFields("");
        request.setNames("");
        request.setMappers("");
        testAuthzPut(configurationResource("parameter-issue-fields"), Entity.json(request), authzPair(username, expectedStatus));
    }

    @DisabledIfLiteApp
    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testToggleFeature(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzPost(featureResource(DYNAMIC_UI_ELEMENTS), Entity.json(new EnabledState()), authzPair(username, expectedStatus));
    }
}
