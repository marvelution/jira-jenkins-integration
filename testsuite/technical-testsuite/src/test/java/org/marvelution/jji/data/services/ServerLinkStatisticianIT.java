/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.*;
import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.api.features.*;
import org.marvelution.jji.data.access.*;
import org.marvelution.jji.data.access.api.IssueReferenceProvider;
import org.marvelution.jji.model.*;
import org.marvelution.jji.test.data.*;
import org.marvelution.testing.inject.AbstractModule;

import com.atlassian.jira.issue.*;
import com.atlassian.jira.issue.index.*;
import com.google.inject.*;
import com.google.inject.util.*;
import net.java.ao.test.jdbc.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;

import static org.marvelution.jji.data.services.DataServicesModule.*;
import static org.marvelution.jji.model.Result.*;
import static org.marvelution.jji.utils.KeyExtractor.*;

import static java.util.Objects.*;
import static java.util.stream.Collectors.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(ActiveObjectsExtension.class)
@Data(ModelDatabaseUpdater.class)
public class ServerLinkStatisticianIT
		extends AbstractBaseLinkStatisticianIT<ServerLinkStatistician>
{

	private final ActiveObjectsExtension.EntityManagerContext entityManagerContext;
	@Inject
	private Features features;
	@Inject
	private IssueReferenceProvider issueReferenceProvider;
	@Inject
	private IssueManager issueManager;
	@Inject
	private IssueIndexingService issueIndexingService;
	private Set<IssueReference> issueReferences;
	private Set<FeatureFlag> featureFlags;

	public ServerLinkStatisticianIT(ActiveObjectsExtension.EntityManagerContext entityManagerContext)
	{
		this.entityManagerContext = entityManagerContext;
	}

	@Override
	public void configure(Binder binder)
	{
		featureFlags = new HashSet<>();
		featureFlags.add(new FeatureFlag(FeatureFlags.DISABLE_ISSUE_INDEXING, false, true));
		binder.install(Modules.override(new DataAccessModule(entityManagerContext)).with(new org.marvelution.testing.inject.AbstractModule()
		{
			@Override
			protected void configure()
			{
				bindMock(IssueReferenceProvider.class, withSettings().lenient());
			}
		}));
		binder.install(Modules.override(new DataServicesModule()).with(new AbstractModule()
		{
			@Override
			protected void configure()
			{
				bind(FEATUREFLAGS_TYPE).toInstance(featureFlags);
			}
		}));
	}

	@BeforeEach
	@SuppressWarnings("unchecked")
	void setUpMocks()
	{
		issueReferences = new HashSet<>();
		when(issueReferenceProvider.getIssueReference(anyString())).then(invocation -> {
			String issueKey = invocation.getArgument(0);
			return issueReferences.stream().filter(reference -> reference.getIssueKey().equals(issueKey)).findFirst();
		});
		when(issueReferenceProvider.getIssueReferences(any(Set.class))).then(invocation -> {
			Set<String> issueKeys = invocation.getArgument(0);
			return issueReferences.stream().filter(reference -> issueKeys.contains(reference.getIssueKey())).collect(toSet());
		});
		doAnswer(invocation -> {
			IssueReference reference = invocation.getArgument(0);
			reference.setIssueUrl(URI.create("https://jira.example.com/browse/" + reference.getIssueKey()));
			return null;
		}).when(issueReferenceProvider).setIssueUrl(any(IssueReference.class));
	}

	@Override
	protected IssueReference[] setupIssueReference(String... issueKeys)
	{
		requireNonNull(issueKeys);
		return Stream.of(issueKeys)
				.map(key -> new IssueReference().setIssueKey(key).setProjectKey(extractProjectKeyFromIssueKey(key)))
				.peek(issueReferences::add)
				.toArray(IssueReference[]::new);
	}

	@Test
	void testReindexingOnStatisticsChange()
			throws IndexException
	{
		Build build = setupBuild(createBuild(job));
		setupIssueReference("PR-1", "PR-2");
		setupLink(build, new IssueReference().setIssueKey("PR-1"));
		when(issueManager.getIssueObject(anyString())).then(invocation -> {
			String issueKey = invocation.getArgument(0);
			if (issueReferences.stream().anyMatch(issueReference -> issueReference.getIssueKey().equals(issueKey)))
			{
				return mock(MutableIssue.class);
			}
			else
			{
				return null;
			}
		});

		calculateStats(true, "PR-1");

		assertThat(linkStatisticsDAO.getLinkStatistics("PR-1")).extracting(LinkStatistics::getTotal, LinkStatistics::getWorstResult)
				.containsOnly(1L, SUCCESS);

		verify(issueManager).getIssueObject("PR-1");
		verify(issueIndexingService).reIndexIssueObjects(anySet(), eq(IssueIndexingParams.INDEX_ISSUE_ONLY), eq(true));
	}


	@Test
	void testDontReindexingOnStatisticsChangeWhenIndexingIsDisabled()
	{
		features.enableFeature(FeatureFlags.DISABLE_ISSUE_INDEXING);

		Build build = setupBuild(createBuild(job));
		setupIssueReference("PR-1", "PR-2");
		setupLink(build, new IssueReference().setIssueKey("PR-1"));

		calculateStats(true, "PR-1");

		assertThat(linkStatisticsDAO.getLinkStatistics("PR-1")).extracting(LinkStatistics::getTotal, LinkStatistics::getWorstResult)
				.containsOnly(1L, SUCCESS);

		verifyNoMoreInteractions(issueManager, issueIndexingService);
	}
}
