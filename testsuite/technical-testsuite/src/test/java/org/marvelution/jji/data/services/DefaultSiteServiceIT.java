/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.*;
import javax.inject.Inject;

import org.marvelution.jji.data.access.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.test.data.*;
import org.marvelution.jji.utils.*;

import com.atlassian.sal.api.net.*;
import com.atlassian.sal.testresources.net.*;
import com.google.inject.*;
import net.java.ao.test.jdbc.*;
import org.junit.jupiter.api.extension.*;

import static org.marvelution.jji.data.services.BaseSiteClient.*;

/**
 * Tests for {@link DefaultSiteService}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@ExtendWith(ActiveObjectsExtension.class)
@Data(ModelDatabaseUpdater.class)
public class DefaultSiteServiceIT
		extends AbstractBaseSiteServiceIT<DefaultSiteService>
{

	private final ActiveObjectsExtension.EntityManagerContext entityManagerContext;
	@Inject
	private NonMarshallingMockRequestFactory requestFactory;

	public DefaultSiteServiceIT(ActiveObjectsExtension.EntityManagerContext entityManagerContext)
	{
		this.entityManagerContext = entityManagerContext;
	}

	@Override
	public void configure(Binder binder)
	{
		binder.install(new DataAccessModule(entityManagerContext));
		binder.install(new DataServicesModule());
	}

	@Override
	protected void setupSiteClient(
			Site site,
			Status status,
			boolean pluginInstalled)
	{
		setupStatusRequest(site, status);
		setupPluginRequest(site, pluginInstalled);
	}

	private void setupStatusRequest(
			Site site,
			Status status)
	{
		String url = SiteUrl.syncUrl().site(site).api().url().toASCIIString();
		MockRequest request = new MockRequest(Request.MethodType.GET, url);
		if (status == Status.ONLINE || status == Status.NOT_ACCESSIBLE)
		{
			MockResponse response = new MockResponse();
			response.setSuccessful(true);
			response.setStatusCode(status == Status.ONLINE ? 200 : 401);
			response.setStatusText(status.name());
			request.setResponse(response);
		}
		requestFactory.addRequest(url, request);
	}

	private void setupPluginRequest(
			Site site,
			boolean pluginInstalled)
	{
		String url = SiteUrl.syncUrl().site(site).path("plugin/jira-integration/ping.html").url().toASCIIString();
		MockRequest request = new MockRequest(Request.MethodType.GET, url);
		MockResponse response = new MockResponse();
		response.setSuccessful(pluginInstalled);
		if (pluginInstalled)
		{
			response.setStatusCode(200);
			Map<String, String> headers = new HashMap<>();
			headers.put(X_JIRA_INTEGRATION, "5.2.0");
			response.setHeaders(headers);
			setupRegistrationRequest(site);
		}
		else
		{
			response.setStatusCode(404);
		}
		request.setResponse(response);
		requestFactory.addRequest(url, request);
	}

	private void setupRegistrationRequest(Site site)
	{
		String url = SiteUrl.syncUrl().site(site).path("jji/register/").url().toASCIIString();
		MockRequest request = new MockRequest(Request.MethodType.POST, url);
		MockResponse response = new MockResponse();
		response.setSuccessful(true);
		response.setStatusCode(200);
		request.setResponse(response);
		requestFactory.addRequest(url, request);
	}
}
