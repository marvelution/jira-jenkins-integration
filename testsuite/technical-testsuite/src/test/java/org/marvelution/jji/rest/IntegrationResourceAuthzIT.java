/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.lang.annotation.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.Response;

import org.marvelution.jji.Headers;
import org.marvelution.jji.events.JobNotificationType;
import org.marvelution.jji.model.*;
import org.marvelution.jji.synctoken.jersey.SyncTokenClientFilter;
import org.marvelution.jji.test.rest.AbstractResourceAuthzTest;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class IntegrationResourceAuthzIT
        extends AbstractResourceAuthzTest
        implements WebResources
{

    private static Site site;
    private static Job job;

    @MissingSyncTokenTest
    void testGetBuildLinks_MissingSyncToken(String username)
    {
        testAuthzGet(integrationResource(job, 1, "links"), authzPair(username, Response.Status.UNAUTHORIZED));
    }

    @Test
    void testGetBuildLinks()
    {
        testAuthzGet(integrationResource(job, 1, "links"),
                filtered(new SyncTokenClientFilter(site.getId(),
                        site.getSharedSecret(),
                        backdoor.environmentData()
                                .getContext()), Response.Status.OK, "sync-token " + site.getName()));
    }

    @MissingSyncTokenTest
    void testMarkJobDeleted_MissingSyncToken(String username)
    {
        testAuthzDelete(integrationResource(job), authzPair(username, Response.Status.UNAUTHORIZED));
    }

    @Test
    void testMarkJobDeleted()
    {
        testAuthzDelete(integrationResource(job),
                filtered(new SyncTokenClientFilter(site.getId(),
                        site.getSharedSecret(),
                        backdoor.environmentData()
                                .getContext()), Response.Status.NO_CONTENT, "sync-token " + site.getName()));
    }

    @MissingSyncTokenTest
    void testMarkBuildDeleted_MissingSyncToken(String username)
    {
        testAuthzDelete(integrationResource(job, 1), authzPair(username, Response.Status.UNAUTHORIZED));
    }

    @Test
    void testMarkBuildDeleted()
    {
        testAuthzDelete(integrationResource(job, 1),
                filtered(new SyncTokenClientFilter(site.getId(),
                        site.getSharedSecret(),
                        backdoor.environmentData()
                                .getContext()), Response.Status.NO_CONTENT, "sync-token " + site.getName()));
    }

    @MissingSyncTokenTest
    void testGetTunnelDetails_MissingSyncToken(String username)
    {
        testAuthzGet(integrationResource("tunnel", site.getId()), authzPair(username, Response.Status.UNAUTHORIZED));
    }

    @Test
    void testGetTunnelDetails()
    {
        testAuthzGet(integrationResource("tunnel", site.getId()),
                filtered(new SyncTokenClientFilter(site.getId(),
                        site.getSharedSecret(),
                        backdoor.environmentData()
                                .getContext()), Response.Status.NOT_FOUND, "sync-token " + site.getName()));
    }

    @MissingSyncTokenTest
    void testGetRegistrationDetails_MissingSyncToken(String username)
    {
        testAuthzGet(integrationResource("register", site.getId()), authzPair(username, Response.Status.UNAUTHORIZED));
    }

    @Test
    void testGetRegistrationDetails()
    {
        testAuthzGet(integrationResource("register", site.getId()),
                filtered(new SyncTokenClientFilter(site.getId(),
                        site.getSharedSecret(),
                        backdoor.environmentData()
                                .getContext()), Response.Status.OK, "sync-token " + site.getName()));
    }

    @MissingSyncTokenTest
    void testCompleteRegistration_MissingSyncToken(String username)
    {
        testAuthzPost(integrationResource("register", site.getId()), authzPair(username, Response.Status.UNAUTHORIZED));
    }

    @Test
    void testCompleteRegistration()
    {
        testAuthzPost(integrationResource("register", site.getId()),
                filtered(new SyncTokenClientFilter(site.getId(),
                        site.getSharedSecret(),
                        backdoor.environmentData()
                                .getContext()), Response.Status.ACCEPTED, "sync-token " + site.getName()));
    }

    private static Stream<Arguments> jobNotificationTypes()
    {
        List<String> types = new ArrayList<>();
        Stream.of(JobNotificationType.values())
                .forEach(type -> {
                    types.add(type.name());
                    types.add(type.value());
                });
        return types.stream()
                .map(Arguments::of);
    }

    @BeforeAll
    static void setup()
    {
        site = backdoor.sites()
                .addFirewalledSite(SiteType.JENKINS, "Local Test", URI.create("http://localhost:8080"));
        job = backdoor.jobs()
                .addJob(site, "test-job");
        backdoor.sites()
                .populateSharedSecret(site);
    }

    @AfterAll
    static void teardown()
    {
        backdoor.sites()
                .clearSites();
    }

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @ParameterizedTest(name = "user: {0}")
    @ValueSource(strings = {ANONYMOUS,
            USER,
            ADMIN})
    @interface MissingSyncTokenTest
    {}

    @Nested
    class SynchronizeByPut
    {

        @MissingSyncTokenTest
        void testSynchronizeJob_MissingSyncToken(String username)
        {
            testSynchronizeJob(authzPair(username, Response.Status.UNAUTHORIZED), null);
        }

        @ParameterizedTest
        @MethodSource("org.marvelution.jji.rest.IntegrationResourceAuthzIT#jobNotificationTypes")
        void testSynchronizeJob(String notificationType)
        {
            testSynchronizeJob(filtered(new SyncTokenClientFilter(site.getId(),
                    site.getSharedSecret(),
                    backdoor.environmentData()
                            .getContext()), Response.Status.NO_CONTENT, "sync-token " + site.getName()), notificationType);
        }

        private void testSynchronizeJob(
                AuthzPair authzPair,
                String notificationType)
        {
            testAuthzPut(integrationResource(job).register((Feature) context -> {
                context.register((ClientRequestFilter) requestContext -> requestContext.getHeaders()
                        .add(Headers.NOTIFICATION_TYPE, notificationType));
                return true;
            }), authzPair);
        }

        @MissingSyncTokenTest
        void testSynchronizeBuild_MissingSyncToken(String username)
        {
            testSynchronizeBuild(authzPair(username, Response.Status.UNAUTHORIZED));
        }

        @Test
        void testSynchronizeBuild()
        {
            testSynchronizeBuild(filtered(new SyncTokenClientFilter(site.getId(),
                    site.getSharedSecret(),
                    backdoor.environmentData()
                            .getContext()), Response.Status.NO_CONTENT, "sync-token " + site.getName()));
        }

        private void testSynchronizeBuild(AuthzPair authzPair)
        {
            testAuthzPut(integrationResource(job, 1), authzPair);
        }

        @MissingSyncTokenTest
        void testSynchronizeBuildWithParents_MissingSyncToken(String username)
        {
            testSynchronizeBuildWithParents(authzPair(username, Response.Status.UNAUTHORIZED));
        }

        @Test
        void testSynchronizeBuildWithParents()
        {
            testSynchronizeBuildWithParents(filtered(new SyncTokenClientFilter(site.getId(),
                    site.getSharedSecret(),
                    backdoor.environmentData()
                            .getContext()), Response.Status.NO_CONTENT, "sync-token " + site.getName()));
        }

        private void testSynchronizeBuildWithParents(AuthzPair authzPair)
        {
            List<String> parentHashes = new ArrayList<>();
            parentHashes.add("parent-1");
            parentHashes.add("parent-2");
            testAuthzPut(integrationResource(new Job().setName("unknown-job"), 1), Entity.json(parentHashes), authzPair);
        }
    }

    @Nested
    class SynchronizeByPost
    {

        @MissingSyncTokenTest
        void testSynchronizeJob_MissingSyncToken(String username)
        {
            testSynchronizeJob(authzPair(username, Response.Status.UNAUTHORIZED), null);
        }

        @ParameterizedTest
        @MethodSource("org.marvelution.jji.rest.IntegrationResourceAuthzIT#jobNotificationTypes")
        void testSynchronizeJob(String notificationType)
        {
            testSynchronizeJob(filtered(new SyncTokenClientFilter(site.getId(),
                    site.getSharedSecret(),
                    backdoor.environmentData()
                            .getContext()), Response.Status.NO_CONTENT, "sync-token " + site.getName()), notificationType);
        }

        private void testSynchronizeJob(
                AuthzPair authzPair,
                String notificationType)
        {
            Map<String, Object> json = new HashMap<>();
            json.put("name", job.getName());
            json.put("url",
                    job.getDisplayUrl()
                            .toASCIIString());
            testAuthzPost(integrationResource(job).register((Feature) context -> {
                context.register((ClientRequestFilter) requestContext -> requestContext.getHeaders()
                        .add(Headers.NOTIFICATION_TYPE, notificationType));
                return true;
            }), Entity.json(json), authzPair);
        }

        @MissingSyncTokenTest
        void testSynchronizeBuild_MissingSyncToken(String username)
        {
            testSynchronizeBuild(authzPair(username, Response.Status.UNAUTHORIZED));
        }

        @Test
        void testSynchronizeBuild()
        {
            testSynchronizeBuild(filtered(new SyncTokenClientFilter(site.getId(),
                    site.getSharedSecret(),
                    backdoor.environmentData()
                            .getContext()), Response.Status.NO_CONTENT, "sync-token " + site.getName()));
        }

        private void testSynchronizeBuild(AuthzPair authzPair)
        {
            Map<String, Object> json = new HashMap<>();
            json.put("number", 1);
            json.put("result", Result.SUCCESS.key());
            json.put("url",
                    new Build().setJob(job)
                            .setNumber(1)
                            .getDisplayUrl()
                            .toASCIIString());
            testAuthzPost(integrationResource(job), Entity.json(json), authzPair);
        }
    }
}
