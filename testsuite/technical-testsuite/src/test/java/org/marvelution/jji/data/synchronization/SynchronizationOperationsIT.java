/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.io.InputStreamReader;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import jakarta.json.*;

import org.marvelution.jji.model.*;
import org.marvelution.jji.model.request.ConnectSiteRequest;
import org.marvelution.jji.rest.WebResources;
import org.marvelution.jji.test.rest.AbstractResourceTest;
import org.marvelution.testing.wiremock.WireMockOptions;
import org.marvelution.testing.wiremock.WireMockServer;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static java.util.stream.IntStream.range;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author Mark Rekveld
 * @since 2.1.0
 */
class SynchronizationOperationsIT
        extends AbstractResourceTest
        implements WebResources
{

    @AfterEach
    void tearDown()
    {
        backdoor.sites()
                .clearSites();
    }

    private Site createAndAddSite(
            WireMockServer jenkins,
            int expectedJobCount)
    {
        Site added = siteResource().request()
                .post(Entity.json(new Site().setType(SiteType.JENKINS)
                        .setName(getTestMethodName())
                        .setRpcUrl(jenkins.serverUri())
                        .setAuthenticationType(SiteAuthenticationType.BASIC)
                        .setUser("admin")
                        .setToken("admin")
                        .setAutoLinkNewJobs(true)), Site.class);
        try (Response response = siteResource(added, "connect").request()
                .post(Entity.json(new ConnectSiteRequest().setMethod(ConnectSiteRequest.ConnectMethod.casc)), Response.class))
        {
            assertThat(response.getStatus(), is(Response.Status.ACCEPTED.getStatusCode()));
            await().atMost(30, TimeUnit.SECONDS)
                    .pollInterval(1, TimeUnit.SECONDS)
                    .until(() -> backdoor.sites()
                                         .getSite(added.getId())
                                         .getJobs()
                                         .size() == expectedJobCount);
            Site site = backdoor.sites()
                    .getSite(added.getId());
            backdoor.sites()
                    .populateSharedSecret(site);
            return site;
        }
    }

    @Test
    void testFolderJobSynchronization(
            @WireMockOptions
            WireMockServer jenkins)
    {
        Site site = createAndAddSite(jenkins, 5);
        assertThat(site.isJenkinsPluginInstalled(), is(false));
        assertThat(site.isUseCrumbs(), is(false));
        assertThat(site.getJobs(), hasSize(5));
        Assertions.assertThat(site.getJobs())
                .hasSize(5)
                .allMatch(Job::isLinked);
        assertThat(site.getJobs()
                .stream()
                .filter(Job::isLinked)
                .count(), is(5L));
        site.getJobs()
                .stream()
                .filter(job -> job.getName()
                        .equals("Cave"))
                .forEach(job -> await(job.getUrlName()).atMost(30, TimeUnit.SECONDS)
                        .pollInterval(1, TimeUnit.SECONDS)
                        .until(() -> !backdoor.jobs()
                                .getJob(job.getId())
                                .isDeleted()));
        site.getJobs()
                .parallelStream()
                .filter(job -> !job.getName()
                        .equals("Cave"))
                .forEach(job -> await(job.getUrlName()).atMost(30, TimeUnit.SECONDS)
                        .pollInterval(1, TimeUnit.SECONDS)
                        .until(() -> backdoor.jobs()
                                .getJob(job.getId())
                                .isDeleted()));
    }

    @Test
    void testBuildSynchronization(
            @WireMockOptions
            WireMockServer jenkins)
    {
        Project project = backdoor.generator()
                .generateScrumProject(getTestMethodName());
        IssueCreateResponse issue = backdoor.issues()
                .createIssue(project.key, getTestMethodName());
        assertThat(issue.key(), is("TBS-1"));
        Site site = createAndAddSite(jenkins, 1);
        assertThat(site.isJenkinsPluginInstalled(), is(false));
        assertThat(site.isUseCrumbs(), is(false));
        assertThat(site.getJobs(), hasSize(1));
        Job job = site.getJobs()
                .get(0);
        assertThat(job, is(notNullValue()));
        assertThat(job.getName(), is("Cave"));
        assertThat(job.getDisplayName(), is("Cave"));
        assertThat(job.getUrlName(), is("Cave"));
        assertThat(job.isLinked(), is(true));
        assertThat(job.isDeleted(), is(false));
        await(job.getUrlName() + " builds").atMost(30, TimeUnit.SECONDS)
                .pollInterval(1, TimeUnit.SECONDS)
                .until(() -> backdoor.builds()
                        .getBuilds(job), hasSize(1));
        Set<Build> builds = backdoor.builds()
                .getBuilds(job);
        assertThat(builds, hasSize(1));
        Build build = builds.iterator()
                .next();
        assertThat(build, is(notNullValue()));
        assertThat(build.getId(), is(notNullValue()));
        assertThat(build.getJob()
                .getId(), is(job.getId()));
        assertThat(build.getNumber(), is(1));
        assertThat(build.getDisplayNameOrNull(), is(nullValue()));
        assertThat(build.getDescription(), is("My build description"));
        assertThat(build.isDeleted(), is(false));
        assertThat(build.getCause(), is("Started by an SCM change"));
        assertThat(build.getResult(), is(Result.FAILURE));
        assertThat(build.getDuration(), is(135329L));
        assertThat(build.getTimestamp(), is(1355405446078L));
        TestResults testResults = build.getTestResults();
        assertThat(testResults, is(notNullValue()));
        assertThat(testResults.getSkipped(), is(40));
        assertThat(testResults.getFailed(), is(10));
        assertThat(testResults.getTotal(), is(3121));
        Set<String> relatedIssueKeys = backdoor.builds()
                .getRelatedIssueKeys(build);
        assertThat(relatedIssueKeys, hasSize(1));
        assertThat(relatedIssueKeys, hasItem("TBS-1"));
    }

    @ParameterizedTest(name = "{0} builds")
    @ValueSource(ints = {100,
            200,
            300,
            400,
            500})
    void testSynchronizeJobWithLoadsOfBuilds(
            int count,
            @WireMockOptions(optionsClass = LoadOfBuildsOptions.class)
            WireMockServer jenkins)
    {
        LoadsOfBuildsResponseTransformer.buildCount.set(count);

        Site site = createAndAddSite(jenkins, 1);
        assertThat(site.isJenkinsPluginInstalled(), is(false));
        assertThat(site.isUseCrumbs(), is(false));
        assertThat(site.getJobs(), hasSize(1));
        assertThat(site.getJobs()
                .stream()
                .filter(Job::isLinked)
                .count(), is(1L));

        Job job = site.getJobs()
                .get(0);

        await().atMost(5, TimeUnit.MINUTES)
                .until(() -> backdoor.builds()
                        .getBuilds(job), hasSize(count));
    }

    public static class LoadOfBuildsOptions
            extends WireMockConfiguration
    {

        public LoadOfBuildsOptions()
        {
            dynamicPort();
            usingFilesUnderClasspath("SynchronizationOperationsIT/testSynchronizeJobWithLoadsOfBuilds");
            extensions(new LoadsOfBuildsResponseTransformer());
        }
    }

    public static class LoadsOfBuildsResponseTransformer
            extends ResponseTransformer
    {

        static final AtomicInteger buildCount = new AtomicInteger();

        @Override
        public String getName()
        {
            return "loads-of-builds";
        }

        @Override
        public com.github.tomakehurst.wiremock.http.Response transform(
                Request request,
                com.github.tomakehurst.wiremock.http.Response response,
                FileSource files,
                Parameters parameters)
        {
            Pattern pattern = Pattern.compile("/job/Cave(/(\\d+))?/api/json/(.*)?");
            Matcher matcher = pattern.matcher(request.getUrl());

            if (matcher.matches())
            {
                try (JsonReader reader = Json.createReader(new InputStreamReader(response.getBodyStream())))
                {
                    JsonObject json = reader.read()
                            .asJsonObject();
                    JsonObjectBuilder jsonBuilder = Json.createObjectBuilder(json);
                    String group = matcher.group(2);
                    if (group == null)
                    {
                        String url = json.getString("url");

                        int count = buildCount.get();
                        jsonBuilder.add("firstBuild",
                                Json.createObjectBuilder()
                                        .add("number", 1)
                                        .add("url", url + "1/"));
                        jsonBuilder.add("lastBuild",
                                Json.createObjectBuilder()
                                        .add("number", count)
                                        .add("url", url + count + "/"));

                        JsonArrayBuilder builds = Json.createArrayBuilder();
                        range(1, count + 1).mapToObj(n -> Json.createObjectBuilder()
                                        .add("number", n)
                                        .add("url", url + n + "/"))
                                .forEach(builds::add);
                        jsonBuilder.add("builds", builds);
                    }
                    else
                    {
                        int number = Integer.parseInt(group);
                        jsonBuilder.add("number", number)
                                .add("url", json.getString("url") + number + "/");
                    }
                    return com.github.tomakehurst.wiremock.http.Response.Builder.like(response)
                            .body(jsonBuilder.build()
                                    .toString())
                            .build();
                }
            }
            else
            {
                return response;
            }
        }
    }
}
