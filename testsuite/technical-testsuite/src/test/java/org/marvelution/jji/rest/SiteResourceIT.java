/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.URI;
import java.util.List;
import java.util.Map;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.marvelution.jji.model.*;
import org.marvelution.jji.model.request.ConnectSiteRequest;
import org.marvelution.jji.test.rest.AbstractResourceTest;
import org.marvelution.jji.validation.api.ErrorMessages;
import org.marvelution.testing.wiremock.WireMockOptions;
import org.marvelution.testing.wiremock.WireMockServer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.marvelution.jji.data.Matchers.errorForField;
import static org.marvelution.jji.model.Matchers.equalTo;
import static org.marvelution.jji.test.rest.Matchers.*;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
class SiteResourceIT
        extends AbstractResourceTest
        implements WebResources
{

    private static final TypeReference<Map<String, Object>> MAP_TYPE_REFERENCE = new TypeReference<Map<String, Object>>() {};
    private WireMockServer jenkins;

    @BeforeEach
    void setUp(
            @WireMockOptions
            WireMockServer jenkins)
    {
        this.jenkins = jenkins;
    }

    @AfterEach
    void tearDown()
    {
        backdoor.sites()
                .clearSites();
    }

    @Test
    void testAddSite()
    {
        Site site = new Site();

        // try adding a site without details
        Response response = siteResource().request()
                .post(Entity.json(site), Response.class);
        assertThat(response, badRequest());
        ErrorMessages errors = response.readEntity(ErrorMessages.class);
        assertThat(errors.getErrors(), hasSize(3));
        assertThat(errors.getErrors(), hasItem(errorForField("name")));
        assertThat(errors.getErrors(), hasItem(errorForField("type")));
        assertThat(errors.getErrors(), hasItem(errorForField("rpcUrl")));

        // try adding a site with invalid rpcUrl
        site.setType(SiteType.JENKINS);
        site.setName("Added CI");
        site.setRpcUrl(URI.create("localhost"));
        site.setDisplayUrl(URI.create("jenkins.example.com"));
        site.setAuthenticationType(SiteAuthenticationType.BASIC);
        site.setUser("bob");
        site.setToken("bob");
        response = siteResource().request()
                .post(Entity.json(site), Response.class);
        assertThat(response, status(Response.Status.BAD_REQUEST));
        errors = response.readEntity(ErrorMessages.class);
        assertThat(errors.getErrors(), hasSize(2));
        assertThat(errors.getErrors(), hasItem(errorForField("rpcUrl")));
        assertThat(errors.getErrors(), hasItem(errorForField("displayUrl")));

        // try adding a site with valid rpcUrl
        site.setRpcUrl(jenkins.serverUri());
        site.setDisplayUrl(URI.create("https://jenkins.example.com/"));
        response = siteResource().request()
                .post(Entity.json(site), Response.class);
        assertThat(response, successful());

        // verify returned site object
        final Site entity = response.readEntity(Site.class);
        assertThat(entity, is(notNullValue()));
        assertThat(entity.getId(), is(notNullValue()));
        assertThat(entity.getName(), is("Added CI"));
        assertThat(Obfuscate.isObfuscated(entity.getSharedSecret()), is(true));
        assertThat(entity.getType(), is(SiteType.JENKINS));
        assertThat(entity.getRpcUrl(), is(jenkins.serverUri()));
        assertThat(entity.getDisplayUrl(), is(URI.create("https://jenkins.example.com/")));
        assertThat(entity.getAuthenticationType(), is(SiteAuthenticationType.BASIC));
        assertThat(entity.getUser(), is("bob"));
        assertThat(Obfuscate.isObfuscated(entity.getToken()), is(true));
        assertThat(entity.isJenkinsPluginInstalled(), is(true));
        assertThat(entity.isUseCrumbs(), is(false));

        siteResource(entity, "connect").request()
                .post(Entity.json(new ConnectSiteRequest().setMethod(ConnectSiteRequest.ConnectMethod.casc)));

        // verify that jobs are synchronized after the entity is added
        assertThat(entity,
                is(equalTo(backdoor.sites()
                        .getSite(entity.getId()))));
        await().atMost(20, SECONDS)
                .until(() -> backdoor.sites()
                        .getSite(entity.getId())
                        .getJobs(), hasSize(3));
        List<Job> jobs = backdoor.sites()
                .getSite(entity.getId())
                .getJobs();
        assertThat(jobs, hasSize(3));
        assertThat(jobs.stream()
                .filter(Job::isLinked)
                .count(), is(0L));
    }

    @Test
    void testUpdateSite()
    {
        Site site = backdoor.sites()
                .addFirewalledSite(SiteType.JENKINS, "Updated CI", URI.create("http://localhost:8080/"))
                .setRpcUrl(jenkins.serverUri())
                .setAccessibleSite()
                .setAuthenticationType(SiteAuthenticationType.BASIC)
                .setUser("bob")
                .setToken("bob")
                .setAutoLinkNewJobs(true);
        Site entity = siteResource(site).request()
                .post(Entity.json(site), Site.class);
        assertThat(entity.isJenkinsPluginInstalled(), is(true));
        assertThat(entity,
                is(equalTo(backdoor.sites()
                        .getSite(site.getId()))));
        await().atMost(20, SECONDS)
                .until(() -> backdoor.sites()
                        .getSite(entity.getId())
                        .getJobs(), hasSize(3));
        Site updated = backdoor.sites()
                .getSite(entity.getId());
        assertThat(updated.isJenkinsPluginInstalled(), is(true));
        assertThat(updated.isUseCrumbs(), is(false));
        assertThat(updated.getJobs(), hasSize(3));
        assertThat(updated.getJobs()
                .stream()
                .filter(Job::isLinked)
                .count(), is(3L));
    }

    @Test
    void testRegisterAndUnregisterWithSite()
            throws Exception
    {
        // Add new Site.
        Site site = siteResource().request()
                .post(Entity.json(new Site().setType(SiteType.JENKINS)
                        .setName("Register - Unregister")
                        .setRpcUrl(jenkins.serverUri())), Site.class);
        assertThat(Obfuscate.isObfuscated(site.getSharedSecret()), is(true));
        siteResource(site, "connect").request()
                .post(Entity.json(new ConnectSiteRequest().setMethod(ConnectSiteRequest.ConnectMethod.automatic)
                        .setAdminUser("admin")
                        .setAdminToken("admin")));

        // Get Site SharedSecret - is obfuscated in REST API.
        backdoor.sites()
                .populateSharedSecret(site);

        // Verify registration request.
        List<LoggedRequest> requests = getLoggedRequests(postRequestedFor(urlPathEqualTo("/jji/register/")));
        LoggedRequest registerRequest = requests.get(0);
        Map<String, Object> registerBody = new ObjectMapper().readValue(registerRequest.getBody(), MAP_TYPE_REFERENCE);
        assertThat(registerBody.get("url"),
                is(backdoor.configuration()
                        .getBaseAPIUrl()
                        .toASCIIString()));
        assertThat(registerBody.get("name"),
                is(backdoor.configuration()
                        .getInstanceName()));
        assertThat(registerBody.get("identifier"), is(site.getId()));
        assertThat(registerBody.get("sharedSecret"), is(site.getSharedSecret()));

        // Delete the site to trigger unregistration.
        siteResource(site).request()
                .delete();

        // Verify unregistration request.
        requests = getLoggedRequests(postRequestedFor(urlPathEqualTo("/jji/unregister/")));
        LoggedRequest unregisterRequest = requests.get(0);
        Map<String, Object> unregisterBody = new ObjectMapper().readValue(unregisterRequest.getBody(), MAP_TYPE_REFERENCE);
        assertThat(unregisterBody.get("url"),
                is(backdoor.configuration()
                        .getBaseAPIUrl()
                        .toASCIIString()));
    }

    private List<LoggedRequest> getLoggedRequests(
            RequestPatternBuilder requestPatternBuilder)
    {
        await().atMost(30, SECONDS)
                .pollInterval(1, SECONDS)
                .until(() -> jenkins.findAll(requestPatternBuilder)
                        .size(), greaterThanOrEqualTo(1));
        return jenkins.findAll(requestPatternBuilder);
    }
}
