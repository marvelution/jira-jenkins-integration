/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.time.LocalDateTime;
import java.util.List;

import org.marvelution.jji.data.access.api.SynchronizationResultDAO;
import org.marvelution.jji.data.synchronization.api.trigger.ManualSynchronizationTrigger;
import org.marvelution.jji.model.OperationId;
import org.marvelution.jji.test.data.ActiveObjectsExtension;
import org.marvelution.testing.inject.InjectorExtension;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.util.OnRollback;
import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.google.inject.Binder;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Scopes;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import net.java.ao.test.jdbc.Data;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.marvelution.jji.data.access.model.Tables.DEFAULT_AO_NAMESPACE;
import static org.marvelution.jji.data.access.model.v2.SynchronizationResultEntity.*;

@ExtendWith({ActiveObjectsExtension.class,
        InjectorExtension.class})
@Data(ModelDatabaseUpdater.class)
public class DefaultSynchronizationResultDAOIT
        extends AbstractDAOTest
        implements Module
{

    private final ActiveObjectsExtension.EntityManagerContext entityManagerContext;
    @Inject
    private SynchronizationResultDAO resultDAO;
    @Inject
    private DatabaseAccessor databaseAccessor;

    public DefaultSynchronizationResultDAOIT(
            ActiveObjectsExtension.EntityManagerContext entityManagerContext)
    {
        this.entityManagerContext = entityManagerContext;
    }

    @Override
    public void configure(Binder binder)
    {
        binder.install(new DataAccessModule(entityManagerContext));
        binder.bind(SynchronizationResultDAO.class)
                .to(DefaultSynchronizationResultDAO.class)
                .in(Scopes.SINGLETON);
    }

    @Test
    void testFindAllNonFinishedResultIds()
    {
        DefaultSynchronizationResult result1 = resultDAO.create(OperationId.of("test-id-12"), new ManualSynchronizationTrigger("test"));
        DefaultSynchronizationResult result2 = resultDAO.create(OperationId.of("test-id-54"), new ManualSynchronizationTrigger("test"));

        assertThat(resultDAO.findAllNonFinishedResultIds()).hasSize(2)
                .containsOnly(result1.getId(), result2.getId());

        result1.finished();

        assertThat(resultDAO.findAllNonFinishedResultIds()).hasSize(1)
                .containsOnly(result2.getId());
    }

    @Test
    void testDeleteAllQueuedBefore()
    {
        databaseAccessor.runInTransaction(conn -> {
            conn.insert(ResultTable.TABLE)
                    .set(ResultTable.TABLE.id, "result-id-1")
                    .set(ResultTable.TABLE.operationId, "test-id-12")
                    .set(ResultTable.TABLE.triggerReason, "{}")
                    .set(ResultTable.TABLE.timestamp, System.currentTimeMillis())
                    .set(ResultTable.TABLE.queuedTimestamp, System.currentTimeMillis() - 5000)
                    .addBatch()
                    .set(ResultTable.TABLE.id, "result-id-2")
                    .set(ResultTable.TABLE.operationId, "test-id-34")
                    .set(ResultTable.TABLE.triggerReason, "{}")
                    .set(ResultTable.TABLE.timestamp, System.currentTimeMillis())
                    .set(ResultTable.TABLE.queuedTimestamp, System.currentTimeMillis() + 5000)
                    .addBatch()
                    .execute();
            return null;
        }, OnRollback.NOOP);

        resultDAO.deleteAllQueuedBefore(LocalDateTime.now());

        List<String> ids = databaseAccessor.runInTransaction(conn -> conn.select(ResultTable.TABLE.id)
                .from(ResultTable.TABLE)
                .fetch(), OnRollback.NOOP);
        assertThat(ids).hasSize(1)
                .containsOnly("result-id-2");
    }

    private static class ResultTable
            extends EnhancedRelationalPathBase<ResultTable>
    {

        static final ResultTable TABLE = new ResultTable();

        StringPath id = createString(ID);
        StringPath operationId = createString(OPERATION_ID);
        StringPath triggerReason = createString(TRIGGER_REASON);
        NumberPath<Long> timestamp = createLong(CREATE_TIMESTAMP);
        NumberPath<Long> queuedTimestamp = createLong(TIMESTAMP_QUEUED);

        public ResultTable()
        {
            super(ResultTable.class, DEFAULT_AO_NAMESPACE + TABLE_NAME, "results");
        }
    }
}
