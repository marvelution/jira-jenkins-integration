/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.marvelution.jji.api.migration.ExportOptions;
import org.marvelution.jji.api.migration.ImportOptions;
import org.marvelution.jji.data.migration.ConfigurationData;
import org.marvelution.jji.data.migration.MigrationData;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.test.rest.AbstractResourceAuthzTest;
import org.marvelution.jji.test.rest.ResourceAuthzTest;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;

/**
 * Integration Authz Testcase for the app migration REST resource.
 *
 * @author Mark Rekveld
 * @since 5.6.0
 */
class AppMigrationResourceAuthzIT
        extends AbstractResourceAuthzTest
        implements WebResources
{

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testExport(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzPost(exportResource(),
                Entity.json(new ExportOptions().setName("Test")
                        .setIncludeConfiguration(true)
                        .setIncludeIntegration(true)),
                authzPair(username, expectedStatus));
    }


    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testImport(
            String username,
            Response.Status expectedStatus)
            throws IOException
    {
        ImportOptions options = new ImportOptions().setName("Test")
                .setBlankImport(true)
                .setIncludeConfiguration(true)
                .setIncludeIntegration(true);
        Path file = Files.createTempFile("migration-test-", ".tmp");
        try (OutputStream output = Files.newOutputStream(file, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING))
        {
            MigrationData.Builder dataBuilder = MigrationData.newBuilder();
            ConfigurationData.Builder configurationBuilder = dataBuilder.getConfigurationBuilder();
            configurationBuilder.addSettingsBuilder()
                    .setKey(ConfigurationService.MAX_BUILDS_PER_PAGE)
                    .setValue("100");
            configurationBuilder.addSettingsBuilder()
                    .setKey(ConfigurationService.USE_USER_ID)
                    .setValue("true");
            configurationBuilder.addSettingsBuilder()
                    .setKey(ConfigurationService.DELETED_DATA_RETENTION_PERIOD)
                    .setValue("10");
            dataBuilder.build()
                    .writeTo(output);
        }
        MultiPart data = new FormDataMultiPart().bodyPart(new FormDataBodyPart("options", options, MediaType.APPLICATION_JSON_TYPE))
                .bodyPart(new FileDataBodyPart("importFile", file.toFile()));
        testAuthzPost(importResource(), Entity.entity(data, data.getMediaType()), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NOT_FOUND)
    void testGetFile(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzGet(migrationFileResource("migration-id"), authzPair(username, expectedStatus));
    }
}
