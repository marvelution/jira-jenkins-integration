/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.URI;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;
import org.marvelution.jji.model.request.ConnectSiteRequest;
import org.marvelution.jji.model.request.EnabledState;
import org.marvelution.jji.test.condition.DisabledIfLiteApp;
import org.marvelution.jji.test.rest.AbstractResourceAuthzTest;
import org.marvelution.jji.test.rest.ResourceAuthzTest;

import com.atlassian.jira.testkit.client.restclient.Project;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Integration Authz Testcase for the site REST resource
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
class SiteResourceAuthzIT
        extends AbstractResourceAuthzTest
        implements WebResources
{

    private Site site;

    @BeforeEach
    void setUp()
    {
        site = backdoor.sites()
                .addFirewalledSite(SiteType.JENKINS, "Jenkins", URI.create("http://localhost:8080"));
    }

    @AfterEach
    void tearDown()
    {
        backdoor.sites()
                .clearSites();
    }

    @ResourceAuthzTest
    void testGetAll(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzGet(siteResource(), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest
    void testGetSite(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzGet(siteResource(site), authzPair(username, expectedStatus));
    }

    @Test
    void testRead_Anonymous()
    {
        testAuthzPost(siteResource(), Entity.json(new Site().setName("Name")), anonymous(Response.Status.UNAUTHORIZED));
    }

    @Test
    void testRead_AuthenticatedUser()
    {
        testAuthzPost(siteResource(), Entity.json(new Site().setName("Name")), user(Response.Status.FORBIDDEN));
    }

    @Test
    void testUpdate_Anonymous()
    {
        testAuthzPost(siteResource(site), Entity.json(site.setName("Updated name")), anonymous(Response.Status.UNAUTHORIZED));
    }

    @Test
    void testUpdate_AuthenticatedUser()
    {
        testAuthzPost(siteResource(site), Entity.json(site.setName("Updated name")), user(Response.Status.FORBIDDEN));
    }

    @Test
    void testDelete_Anonymous()
    {
        testAuthzDelete(siteResource(site), anonymous(Response.Status.UNAUTHORIZED));
    }

    @Test
    void testDelete_AuthenticatedUser()
    {
        testAuthzDelete(siteResource(site), user(Response.Status.FORBIDDEN));
    }

    @ResourceAuthzTest(administrator = Response.Status.ACCEPTED)
    void testConnectSite(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzPost(siteResource(site, "connect"),
                Entity.json(new ConnectSiteRequest().setMethod(ConnectSiteRequest.ConnectMethod.casc)),
                authzPair(username, expectedStatus));
    }

    @DisabledIfLiteApp
    @ResourceAuthzTest(superUser = Response.Status.ACCEPTED,
                       administrator = Response.Status.ACCEPTED)
    void testConnectScopedSite(
            String username,
            Response.Status expectedStatus)
    {
        Project project = backdoor.generator()
                .generateScrumProject();
        backdoor.sites()
                .saveSite(site.setScope(project.key));
        testAuthzPost(siteResource(site, "connect"),
                Entity.json(new ConnectSiteRequest().setMethod(ConnectSiteRequest.ConnectMethod.casc)),
                authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testSyncJobList(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzPost(siteResource(site, "sync"), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testEnableAutoLink(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzPost(siteResource(site, "autolink/true"), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testEnableAutoLink_State(
            String username,
            Response.Status expectedStatus)
    {
        EnabledState state = new EnabledState();
        state.setEnabled(true);
        testAuthzPost(siteResource(site, "autolink"), Entity.json(state), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testEnableSite(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzPost(siteResource(site, "enable/true"), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testEnableSite_State(
            String username,
            Response.Status expectedStatus)
    {
        EnabledState state = new EnabledState();
        state.setEnabled(true);
        testAuthzPost(siteResource(site, "enable"), Entity.json(state), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest
    void testSiteStatus(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzGet(siteResource(site, "status"), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testRemoveJobs_Anonymous(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzDelete(siteResource(site, "jobs"), authzPair(username, expectedStatus));
    }
}
