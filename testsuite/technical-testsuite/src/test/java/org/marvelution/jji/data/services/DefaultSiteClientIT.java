/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.Objects;

import org.marvelution.jji.api.events.EventPublisher;
import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.testing.inject.AbstractModule;

import com.atlassian.jira.user.util.UserManager;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.core.net.HttpClientRequestFactory;
import com.google.inject.Binder;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import org.mockito.internal.configuration.GlobalConfiguration;

import static org.mockito.Mockito.withSettings;

/**
 * Tests for {@link DefaultSiteClient}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class DefaultSiteClientIT
        extends AbstractBaseSiteClientIT<DefaultSiteClient>
{

    @Inject
    private ConfigurationService configurationService;

    @Override
    public void configure(Binder binder)
    {
        binder.install(new Module());
    }

    @Override
    protected String baseRpcUrl()
    {
        return configurationService.getBaseAPIUrl()
                .toASCIIString();
    }

    private static class Module
            extends AbstractModule
    {
        @Override
        protected void configure()
        {
            bind(ConfigurationService.class).to(DefaultConfigurationService.class)
                    .in(Scopes.SINGLETON);
            bind(TextResolver.class).to(DataServicesModule.TestTextResolver.class)
                    .in(Scopes.SINGLETON);
            bindMock(ApplicationProperties.class, withSettings().defaultAnswer(invocation -> {
                if (Objects.equals(invocation.getMethod()
                        .getName(), "getBaseUrl"))
                {
                    return "http://jira.example.com";
                }
                else if (Objects.equals(invocation.getMethod()
                        .getName(), "getDisplayName"))
                {
                    return "Jira Server";
                }
                else
                {
                    return new GlobalConfiguration().getDefaultAnswer()
                            .answer(invocation);
                }
            }));
            bindMock(DatabaseAccessor.class);
            bindMock(UserManager.class);
            bindMock(EventPublisher.class);
            bind(HttpClientRequestFactory.class).in(Scopes.SINGLETON);
            bind(new TypeLiteral<NonMarshallingRequestFactory<?>>() {}).to(HttpClientRequestFactory.class);
            bind(TunnelManager.class).to(NoOpTunnelManager.class);

            bind(BaseSiteClient.class).to(DefaultSiteClient.class);
        }
    }
}
