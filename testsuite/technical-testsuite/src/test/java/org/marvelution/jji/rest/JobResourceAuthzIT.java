/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.URI;
import java.util.Collections;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;
import org.marvelution.jji.model.request.BulkRequest;
import org.marvelution.jji.model.request.EnabledState;
import org.marvelution.jji.model.request.TriggerBuildsRequest;
import org.marvelution.jji.test.rest.AbstractResourceAuthzTest;
import org.marvelution.jji.test.rest.ResourceAuthzTest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

class JobResourceAuthzIT
        extends AbstractResourceAuthzTest
        implements WebResources
{

    private Job job;

    @BeforeEach
    void setUp()
    {
        Site site = backdoor.sites()
                .addFirewalledSite(SiteType.JENKINS, "Jenkins", URI.create("http://localhost:8080"));
        job = backdoor.jobs()
                .addJob(site, getTestMethodName());
    }

    @AfterEach
    void tearDown()
    {
        backdoor.sites()
                .clearSites();
    }

    @ResourceAuthzTest
    void testGetJob(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzGet(jobResource(job), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest
    void testSearch(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzGet(jobResource("search").queryParam("term", "job"), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testRemoveJob(
            String username,
            Response.Status expectedStatus)
    {
        job.setDeleted(true);
        backdoor.jobs()
                .saveJob(job);
        testAuthzDelete(jobResource(job), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testSyncJob(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzPut(jobResource(job, "sync"), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testEnableJobLink(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzPost(jobResource(job, "link", "true"), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testEnableJobLink_State(
            String username,
            Response.Status expectedStatus)
    {
        EnabledState enabledState = new EnabledState();
        enabledState.setEnabled(true);
        testAuthzPost(jobResource(job, "link"), Entity.json(enabledState), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testRemoveAllBuilds(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzDelete(jobResource(job, "builds"), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testRebuildJobCache(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzPost(jobResource(job, "rebuild"), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(user = Response.Status.FORBIDDEN,
                       superUser = Response.Status.OK,
                       administrator = Response.Status.OK)
    void testFindTriggerableJobs(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzGet(jobResource("buildable"), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(user = Response.Status.FORBIDDEN,
                       superUser = Response.Status.OK,
                       administrator = Response.Status.OK)
    void testTriggerBuild(
            String username,
            Response.Status expectedStatus)
    {
        TriggerBuildsRequest request = new TriggerBuildsRequest();
        request.setIssueKey("TEST-1");
        request.setJobs(Collections.singleton("job-1"));
        testAuthzPost(jobResource("build"), Entity.json(request), authzPair(username, expectedStatus));
    }

    @ResourceAuthzTest(administrator = Response.Status.NO_CONTENT)
    void testBulkRequest(
            String username,
            Response.Status expectedStatus)
    {
        testAuthzPost(jobResource("bulk"), Entity.json(new BulkRequest()), authzPair(username, expectedStatus));
    }
}
