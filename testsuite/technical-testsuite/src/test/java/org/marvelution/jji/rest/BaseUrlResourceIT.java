/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.*;

import org.marvelution.jji.model.*;
import org.marvelution.jji.synctoken.jersey.*;
import org.marvelution.jji.test.rest.*;

import org.junit.jupiter.api.*;

import static java.util.Optional.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

/**
 * @author Mark Rekveld
 * @since 3.2.1
 */
class BaseUrlResourceIT
		extends AbstractResourceTest
		implements WebResources
{

	@BeforeEach
	void setUp()
	{
		Site site = backdoor.sites().addFirewalledSite(SiteType.JENKINS, "Local Test", URI.create("http://localhost:8080"));
		backdoor.sites().populateSharedSecret(site);

		loginWith(new SyncTokenClientFilter(site.getId(), site.getSharedSecret(), backdoor.environmentData().getContext()));
	}

	@AfterEach
	void tearDown()
	{
		backdoor.sites().clearSites();
	}

	@Test
	void testGetBaseUrl()
	{
		assertThat(backdoor.environmentData().getBaseUrl().toExternalForm(), is(baseUrlResource().request().get(String.class)));
	}
}
