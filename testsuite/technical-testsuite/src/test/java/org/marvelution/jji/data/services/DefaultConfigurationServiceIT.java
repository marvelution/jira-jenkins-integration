/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.*;

import org.marvelution.jji.data.access.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.test.data.*;
import org.marvelution.testing.inject.*;

import com.atlassian.sal.api.*;
import com.google.inject.*;
import net.java.ao.test.jdbc.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;

import static org.assertj.core.api.Assertions.*;
import static org.marvelution.jji.data.services.DefaultConfigurationService.*;
import static org.mockito.Mockito.*;

@ExtendWith({ActiveObjectsExtension.class,
        InjectorExtension.class})
@Data(ModelDatabaseUpdater.class)
class DefaultConfigurationServiceIT
        extends AbstractBaseConfigurationServiceTest
{

    private final ActiveObjectsExtension.EntityManagerContext entityManagerContext;

    @Inject
    private ApplicationProperties applicationProperties;

    public DefaultConfigurationServiceIT(ActiveObjectsExtension.EntityManagerContext entityManagerContext)
    {
        this.entityManagerContext = entityManagerContext;
    }

    @Override
    public void configure(Binder binder)
    {
        binder.install(new DataAccessModule(entityManagerContext));
        binder.install(new DataServicesModule());
    }

    @Test
    void testGetInstanceName()
    {
        when(applicationProperties.getDisplayName()).thenReturn("Jira Software");

        assertThat(configurationService.getInstanceName()).isEqualTo("Jira Software");
    }

    @Test
    void testGetBaseUrl()
    {
        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn("http://jira.example.com");

        assertThat(configurationService.getBaseUrl()).isEqualTo(URI.create("http://jira.example.com"));
    }

    @Test
    void testGetBaseAPIUrl()
    {
        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn("http://jira.example.com");

        assertThat(configurationService.getBaseAPIUrl()).isEqualTo(URI.create("http://jira.example.com/rest/jenkins/latest"));
    }

    @Test
    void testSetBaseAPIUrl()
    {
        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn("http://jira.example.com");
        configurationService.saveConfigurationSetting(ConfigurationSetting.forKey(JIRA_BASE_RPC_URL)
                .setValue("https://an.other.address"));

        Assertions.assertThat(configurationService.getConfigurationSetting(JIRA_BASE_RPC_URL)
                        .getValue())
                .isEqualTo("https://an.other.address");
    }

    @Test
    void testGetConfiguration()
    {
        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn("http://jira.example.com");

        configurationService.resetToDefaults();

        Configuration configuration = configurationService.getConfiguration();

        assertThat(configuration.getConfiguration()).hasSize(10)
                .containsOnly(ConfigurationSetting.forKey(JIRA_BASE_URL)
                                .setValue("http://jira.example.com")
                                .setOverridable(false),
                        ConfigurationSetting.forKey(JIRA_BASE_RPC_URL)
                                .setValue("http://jira.example.com"),
                        ConfigurationSetting.forKey(MAX_BUILDS_PER_PAGE)
                                .setValue(DEFAULT_MAX_BUILDS_PER_PAGE),
                        ConfigurationSetting.forKey(USE_USER_ID)
                                .setValue(DEFAULT_USE_USER_ID),
                        ConfigurationSetting.forKey(PARAMETER_ISSUE_FIELDS)
                                .setValue(""),
                        ConfigurationSetting.forKey(PARAMETER_ISSUE_FIELD_NAMES)
                                .setValue(""),
                        ConfigurationSetting.forKey(PARAMETER_ISSUE_FIELD_MAPPERS)
                                .setValue(""),
                        ConfigurationSetting.forKey(DELETED_DATA_RETENTION_PERIOD)
                                .setValue(DEFAULT_DELETED_DATA_RETENTION_PERIOD),
                        ConfigurationSetting.forKey(AUDIT_DATA_RETENTION_PERIOD)
                                .setValue(DEFAULT_AUDIT_DATA_RETENTION_PERIOD),
                        ConfigurationSetting.forKey(ENABLED_PROJECT_KEYS)
                                .setValue(""));
    }

    @Test
    void testGetScopedConfiguration()
    {
        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn("http://jira.example.com");

        configurationService.resetToDefaults();
        configurationService.saveConfiguration(new Configuration().setScope("TEST")
                .addConfiguration(ConfigurationSetting.forKey(MAX_BUILDS_PER_PAGE)
                        .setValue(20)));


        Configuration configuration = configurationService.getConfiguration("TEST");

        assertThat(configuration.getConfiguration()).hasSize(10)
                .containsOnly(ConfigurationSetting.forKey(JIRA_BASE_URL)
                                .setValue("http://jira.example.com")
                                .setOverridable(false),
                        ConfigurationSetting.forKey(JIRA_BASE_RPC_URL)
                                .setValue("http://jira.example.com"),
                        ConfigurationSetting.forKey(MAX_BUILDS_PER_PAGE)
                                .setScope("TEST")
                                .setValue(20),
                        ConfigurationSetting.forKey(USE_USER_ID)
                                .setValue(DEFAULT_USE_USER_ID),
                        ConfigurationSetting.forKey(PARAMETER_ISSUE_FIELDS)
                                .setValue(""),
                        ConfigurationSetting.forKey(PARAMETER_ISSUE_FIELD_NAMES)
                                .setValue(""),
                        ConfigurationSetting.forKey(PARAMETER_ISSUE_FIELD_MAPPERS)
                                .setValue(""),
                        ConfigurationSetting.forKey(DELETED_DATA_RETENTION_PERIOD)
                                .setValue(DEFAULT_DELETED_DATA_RETENTION_PERIOD),
                        ConfigurationSetting.forKey(AUDIT_DATA_RETENTION_PERIOD)
                                .setValue(DEFAULT_AUDIT_DATA_RETENTION_PERIOD),
                        ConfigurationSetting.forKey(ENABLED_PROJECT_KEYS)
                                .setValue(""));
    }

    @Test
    void testResetJiraBaseRpcURL()
    {
        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn("http://localhost:2990/jira");

        assertThat(configurationService.getConfigurationSetting(JIRA_BASE_RPC_URL)).isEqualTo(ConfigurationSetting.forKey(JIRA_BASE_RPC_URL)
                .setValue("http://localhost:2990/jira"));

        assertThat(configurationService.getConfiguration()
                .getConfiguration(JIRA_BASE_RPC_URL)).get()
                .isEqualTo(ConfigurationSetting.forKey(JIRA_BASE_RPC_URL)
                        .setValue("http://localhost:2990/jira"));

        configurationService.saveConfigurationSetting(ConfigurationSetting.forKey(JIRA_BASE_RPC_URL)
                .setValue("http://jira.example.com"));

        assertThat(configurationService.getConfigurationSetting(JIRA_BASE_RPC_URL)).isEqualTo(ConfigurationSetting.forKey(JIRA_BASE_RPC_URL)
                .setValue("http://jira.example.com"));

        assertThat(configurationService.getConfiguration()
                .getConfiguration(JIRA_BASE_RPC_URL)).get()
                .isEqualTo(ConfigurationSetting.forKey(JIRA_BASE_RPC_URL)
                        .setValue("http://jira.example.com"));

        configurationService.saveConfigurationSetting(ConfigurationSetting.forKey(JIRA_BASE_RPC_URL)
                .setValue(""));

        assertThat(configurationService.getConfigurationSetting(JIRA_BASE_RPC_URL)).isEqualTo(ConfigurationSetting.forKey(JIRA_BASE_RPC_URL)
                .setValue("http://localhost:2990/jira"));

        assertThat(configurationService.getConfiguration()
                .getConfiguration(JIRA_BASE_RPC_URL)).get()
                .isEqualTo(ConfigurationSetting.forKey(JIRA_BASE_RPC_URL)
                        .setValue("http://localhost:2990/jira"));
    }
}
