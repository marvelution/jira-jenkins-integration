/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.*;
import javax.inject.Named;

import org.marvelution.jji.api.features.*;
import org.marvelution.jji.api.text.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.*;
import org.marvelution.testing.inject.AbstractModule;

import com.atlassian.jira.issue.index.*;
import com.atlassian.sal.api.net.*;
import com.google.inject.Inject;
import com.google.inject.*;
import com.google.inject.name.*;

public class DataServicesModule
		extends AbstractModule
{

	public static final Key<Set<FeatureFlag>> FEATUREFLAGS_TYPE = Key.get(new TypeLiteral<Set<FeatureFlag>>() {},
	                                                                      Names.named("featureflags"));

	@Override
	protected void configure()
	{
		bind(SiteService.class).to(DefaultSiteService.class).in(Scopes.SINGLETON);
		bind(JobService.class).to(DefaultJobService.class).in(Scopes.SINGLETON);
		bind(BuildService.class).to(DefaultBuildService.class).in(Scopes.SINGLETON);
		bind(IssueLinkService.class).to(DefaultIssueLinkService.class).in(Scopes.SINGLETON);
		bind(LinkStatistician.class).to(ServerLinkStatistician.class).in(Scopes.SINGLETON);
		bind(SiteClient.class).to(DefaultSiteClient.class).in(Scopes.SINGLETON);
		bind(SiteValidator.class).to(DefaultSiteValidator.class).in(Scopes.SINGLETON);
		bind(TunnelManager.class).to(NoOpTunnelManager.class);

		bind(Features.class).to(TestFeatures.class).in(Scopes.SINGLETON);
		bind(FEATUREFLAGS_TYPE).toInstance(new HashSet<>());

		bind(NonMarshallingMockRequestFactory.class).in(Scopes.SINGLETON);
		bind(new TypeLiteral<NonMarshallingRequestFactory<?>>() {}).to(NonMarshallingMockRequestFactory.class);

		bindMock(SynchronizationService.class);

		bindMock(IssueIndexingService.class);
	}

	public static class TestFeatures
			extends Features
	{

		private final Set<FeatureFlag> featureFlags;

		@Inject
		public TestFeatures(@Named("featureflags") Set<FeatureFlag> featureFlags)
		{
			super(new InMemoryFeatureStore());
			this.featureFlags = featureFlags;
		}

		@Override
		protected Set<FeatureFlag> loadFeatureFlags()
		{
			return featureFlags;
		}
	}

	public static class TestTextResolver
			implements TextResolver
	{

		@Override
		public String getText(
				String key,
				Object... arguments)
		{
			return key;
		}

		@Override
		public String getText(
				Locale locale,
				String key,
				Object... arguments)
		{
			return key;
		}
	}
}
