/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nullable;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.test.rest.WebTargetTest;

import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;

public interface WebResources
        extends WebTargetTest
{

    default WebTarget siteResource(String... path)
    {
        return siteResource(null, path);
    }

    default WebTarget siteResource(
            @Nullable
            Site site,
            String... path)
    {
        Map<String, Object> values = new HashMap<>();
        UriBuilder uriBuilder = uriBuilder().path("site");
        if (site != null)
        {
            uriBuilder.path("{siteId}");
            values.put("siteId", site.getId());
        }
        if (path != null)
        {
            for (String p : path)
            {
                uriBuilder.path(p);
            }
        }
        return resource(uriBuilder.buildFromMap(values));
    }

    default WebTarget jobResource(String... path)
    {
        return jobResource(null, path);
    }

    default WebTarget jobResource(
            @Nullable
            Job job,
            String... path)
    {
        Map<String, Object> values = new HashMap<>();
        UriBuilder uriBuilder = uriBuilder().path("job");
        if (job != null)
        {
            uriBuilder.path("{jobId}");
            values.put("jobId", job.getId());
        }
        if (path != null)
        {
            for (String p : path)
            {
                uriBuilder.path(p);
            }
        }
        return resource(uriBuilder.buildFromMap(values));
    }

    default WebTarget jobSearch(String term)
    {
        return resource(uriBuilder().path("job")
                .path("/search")
                .queryParam("term", term)
                .build());
    }

    default WebTarget integrationResource(
            Job job,
            String... path)
    {
        return integrationResource(job, -1, path);
    }

    default WebTarget integrationResource(
            Job job,
            int buildNumber,
            String... path)
    {
        Map<String, Object> values = new HashMap<>();
        UriBuilder uriBuilder = uriBuilder().path("integration")
                .path("{jobHash}");
        values.put("jobHash", sha1Hex(job.getUrlName()));
        if (buildNumber > -1)
        {
            uriBuilder.path("{buildNumber}");
            values.put("buildNumber", buildNumber);
        }
        if (path != null)
        {
            for (String p : path)
            {
                uriBuilder.path(p);
            }
        }
        return resource(uriBuilder.buildFromMap(values));
    }

    default WebTarget integrationResource(
            Build build,
            String... path)
    {
        return integrationResource(build.getJob(), build.getNumber(), path);
    }

    default WebTarget integrationResource(String... path)
    {
        UriBuilder uriBuilder = uriBuilder().path("integration");
        if (path != null)
        {
            for (String p : path)
            {
                uriBuilder.path(p);
            }
        }
        return resource(uriBuilder.build());
    }

    default WebTarget configurationResource(String... path)
    {
        UriBuilder uriBuilder = uriBuilder().path("configuration");
        if (path != null)
        {
            for (String p : path)
            {
                uriBuilder.path(p);
            }
        }
        return resource(uriBuilder.build());
    }

    default WebTarget featureResource(String feature)
    {
        return configurationResource("features", feature);
    }

    default WebTarget releaseReportResource()
    {
        return resource(uriBuilder().path("release-report")
                .build());
    }

    default WebTarget baseUrlResource()
    {
        return resource(uriBuilder().path("base-url")
                .build());
    }

    default WebTarget migrationResource()
    {
        return resource(uriBuilder().path("migration")
                .build());
    }

    default WebTarget exportResource()
    {
        return migrationResource().path("export");
    }

    default WebTarget importResource()
    {
        return migrationResource().path("import");
    }

    default WebTarget migrationFileResource(String id)
    {
        return migrationResource().path(id);
    }
}
