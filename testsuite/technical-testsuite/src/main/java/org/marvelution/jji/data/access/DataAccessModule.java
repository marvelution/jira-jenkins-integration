/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.Objects;
import java.util.concurrent.Executor;

import org.marvelution.jji.api.events.EventPublisher;
import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.services.DataServicesModule;
import org.marvelution.jji.data.services.DefaultConfigurationService;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.test.data.ActiveObjectsExtension;
import org.marvelution.testing.inject.AbstractModule;

import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugins.rest.api.json.JaxbJsonMarshaller;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.testresources.pluginsettings.MockPluginSettingsFactory;
import com.google.inject.Scopes;
import org.mockito.internal.configuration.GlobalConfiguration;

import static org.mockito.Mockito.withSettings;

public class DataAccessModule
        extends AbstractModule
{

    private final ActiveObjectsExtension.EntityManagerContext entityManagerContext;

    public DataAccessModule(ActiveObjectsExtension.EntityManagerContext entityManagerContext)
    {
        this.entityManagerContext = entityManagerContext;
    }

    @Override
    protected void configure()
    {
        binder().install(entityManagerContext);
        bind(SiteDAO.class).to(DefaultSiteDAO.class)
                .in(Scopes.SINGLETON);
        bind(JobDAO.class).to(DefaultJobDAO.class)
                .in(Scopes.SINGLETON);
        bind(BuildDAO.class).to(DefaultBuildDAO.class)
                .in(Scopes.SINGLETON);
        bind(IssueToBuildDAO.class).to(DefaultIssueToBuildDAO.class)
                .in(Scopes.SINGLETON);
        bind(LinkStatisticsDAO.class).to(DefaultLinkStatisticsDAO.class)
                .in(Scopes.SINGLETON);

        bind(IssueReferenceProvider.class).to(DefaultIssueReferenceProvider.class)
                .in(Scopes.SINGLETON);
        bind(ConfigurationService.class).to(DefaultConfigurationService.class)
                .in(Scopes.SINGLETON);
        bind(TextResolver.class).to(DataServicesModule.TestTextResolver.class)
                .in(Scopes.SINGLETON);
        bind(PluginSettingsFactory.class).to(MockPluginSettingsFactory.class);
        bindMock(ApplicationProperties.class, withSettings().defaultAnswer(invocation -> {
            if (Objects.equals(invocation.getMethod()
                    .getName(), "getBaseUrl"))
            {
                return "http://localhost:2990/jira";
            }
            else if (Objects.equals(invocation.getMethod()
                    .getName(), "getDisplayName"))
            {
                return "Jira";
            }
            else
            {
                return new GlobalConfiguration().getDefaultAnswer()
                        .answer(invocation);
            }
        }));
        bind(Executor.class).toInstance(Runnable::run);
        bindMock(IssueManager.class);
        bindMock(FieldManager.class);
        bindMock(UserManager.class);
        bindMock(JiraHome.class);
        bindMock(EventPublisher.class);
        bindMock(JaxbJsonMarshaller.class);
    }
}
