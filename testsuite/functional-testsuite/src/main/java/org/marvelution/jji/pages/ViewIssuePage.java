/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.pages;

import org.marvelution.jji.web.tests.panel.DevStatusPanel;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class ViewIssuePage
{

    private static final String URL_PREFIX = "./browse/";
    private final Page page;

    public ViewIssuePage(Page page)
    {
        this.page = page;
    }

    public void navigateTo(String issueKey)
    {
        page.navigate(URL_PREFIX + issueKey);
    }

    public Locator header()
    {
        return page.locator(".issue-header-content");
    }

    public Locator actionsSort()
    {
        return page.locator(".issue-activity-sort-link.ajax-activity-content");
    }

    public Locator actionsContainer()
    {
        return page.locator("#issue_actions_container");
    }

    public Locator summary()
    {
        return header().locator("#summary-val");
    }

    public Locator issueKey()
    {
        return header().locator(".issue-link");
    }

    public DevStatusPanel devstatus()
    {
        return new DevStatusPanel(page.locator("#jenkins-devstatus"));
    }

    public Locator moreOperations()
    {
        return page.locator("#opsbar-operations_more");
    }

    public Locator triggerBuildAction()
    {
        return page.locator("#jenkins_trigger_build");
    }

    public Locator jenkinsTabPanelLink()
    {
        return page.locator("#jenkins-issue-panel");
    }

    public Locator jenkinsTabPanelMessage()
    {
        return actionsContainer().locator(".message-container");
    }

    public Locator jenkinsTabPanelBuild(int index)
    {
        return actionsContainer().locator(".jenkins-build")
                .nth(index);
    }

    public static ViewIssuePage navigateTo(
            Page page,
            String issueKey)
    {
        ViewIssuePage viewIssuePage = new ViewIssuePage(page);
        viewIssuePage.navigateTo(issueKey);
        return viewIssuePage;
    }
}
