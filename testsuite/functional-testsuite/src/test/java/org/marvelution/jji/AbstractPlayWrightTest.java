/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji;

import java.util.regex.Pattern;

import org.marvelution.jji.test.BaseTest;
import org.marvelution.jji.test.license.AddonLicenseInstaller;
import org.marvelution.testing.TestSupport;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.LocatorAssertions;
import com.microsoft.playwright.options.AriaRole;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

@ExtendWith(AddonLicenseInstaller.class)
public class AbstractPlayWrightTest
        extends TestSupport
        implements BaseTest
{
    protected static final int LOGIN_TIMEOUT = 30000;

    @AfterEach
    void reset()
    {
        backdoor.sites()
                .clearSites();
        backdoor.configuration()
                .clearConfiguration();
    }

    public void loginAsAdmin(Page page)
    {
        login(page, ADMIN);
    }

    public void login(
            Page page,
            String userpass)
    {
        login(page, userpass, userpass);
    }

    public void login(
            Page page,
            String username,
            String password)
    {
        login(page, username, password, false);
    }

    private void login(
            Page page,
            String username,
            String password,
            boolean retry)
    {
        if (!page.url()
                .contains("/login.jsp"))
        {
            page.navigate("./login.jsp");
        }
        page.locator("input[name$=username]")
                .fill(username);
        page.locator("input[name$=password]")
                .fill(password);
        page.locator("button[type=submit]")
                .click();
        page.waitForURL(Pattern.compile("(.*)/secure/Dashboard.jspa(.*)"), new Page.WaitForURLOptions().setTimeout(LOGIN_TIMEOUT));
        Locator loginButton = page.locator(".login-link");
        if (loginButton.count() > 0)
        {
            if (retry)
            {
                throw new AssertionError("Failed to login");
            }
            loginButton.click();
            page.waitForURL(Pattern.compile("(.*)/login.jsp(.*)"), new Page.WaitForURLOptions().setTimeout(LOGIN_TIMEOUT));
            login(page, username, password, true);
        }
        assertThat(page.locator("#header-details-user-fullname")).isVisible(new LocatorAssertions.IsVisibleOptions().setVisible(true)
                .setTimeout(LOGIN_TIMEOUT));
    }

    public void logout(Page page)
    {
        page.locator("#header-details-user-fullname")
                .click();
        page.locator("#log_out")
                .click();
        page.waitForURL(Pattern.compile("(.*)/secure/Logout!default.jspa(.*)"), new Page.WaitForURLOptions().setTimeout(LOGIN_TIMEOUT));
        assertThat(page.getByRole(AriaRole.MAIN)).containsText("logged out",
                new LocatorAssertions.ContainsTextOptions().setTimeout(LOGIN_TIMEOUT));
    }
}
