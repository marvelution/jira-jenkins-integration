/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.admin;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.marvelution.jji.AbstractPlayWrightTest;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.web.tests.admin.SiteManagementTests;

import com.atlassian.jira.testkit.client.restclient.Project;
import com.codeborne.selenide.Configuration;
import com.microsoft.playwright.Page;
import org.junit.jupiter.api.AfterEach;

import static org.marvelution.jji.web.conditions.Conditions.createAttributeMap;

abstract class AbstractSiteManagementIT
        extends AbstractPlayWrightTest
        implements SiteManagementTests
{

    protected String existingProjectKey;

    @AfterEach
    void tearDown()
    {
        existingProjectKey = null;
    }

    @Override
    public Site getSite(Site site)
    {
        return backdoor.sites()
                .getSite(site.getId(), true);
    }

    @Override
    public List<Site> getSites()
    {
        return backdoor.sites()
                .getSites();
    }

    @Override
    public Job getJob(Job job)
    {
        return backdoor.jobs()
                .getJob(job.getId());
    }

    @Override
    public Site saveSite(
            Site site,
            Job... jobs)
    {
        Site saved = backdoor.sites()
                .saveSite(site);
        if (jobs != null)
        {
            Stream.of(jobs)
                    .map(job -> job.setSite(saved))
                    .map(job -> backdoor.jobs()
                            .saveJob(job))
                    .forEach(saved::addJob);
        }
        return saved;
    }

    @Override
    public void navigateToManageSites(Page page)
    {
        page.navigate("./secure/admin/jji/sites");
    }

    @Override
    public void navigateDirectToSite(
            Page page,
            Site site)
    {
        page.navigate("./secure/admin/jji/sites/" + site.getId());
    }

    @Override
    public void navigateDirectToSiteInScope(
            Page page,
            Site site,
            String scope)
    {
        page.navigate("./secure/jji-config/" + scope + "/sites/" + site.getId());
    }

    @Override
    public String getExistingProjectKey()
    {
        if (existingProjectKey == null)
        {
            Project project = backdoor.generator()
                    .getOrGenerateProject(getTestMethodName(),
                            name -> backdoor.generator()
                                    .generateScrumProject(name));
            existingProjectKey = project.key;
        }
        return existingProjectKey;
    }

    @Override
    public void navigateToProjectSites(
            Page page,
            String projectKey)
    {
        page.navigate("./secure/jji-config/" + projectKey + "/sites");
    }

    @Override
    public String createRestUriForSite(String siteId)
    {
        return createRelativeUrl("/rest/jenkins/latest/site" + (siteId != null ? "/" + siteId : ""));
    }

    @Override
    public String createFormActionForSite(String siteId)
    {
        return createAbsoluteUrl("/rest/jenkins/latest/site/" + (siteId != null ? siteId : ""));
    }

    @Override
    public String createRestUriForJob(String jobId)
    {
        return createRelativeUrl("/rest/jenkins/latest/job/" + jobId);
    }

    @Override
    public Map<String, String> cleanupJobsButtonAttributes(Site site)
    {
        return createAttributeMap("data-dialog-module",
                "delete-site-jobs",
                "data-dialog-url",
                createRestUriForSite(null),
                "data-dialog-module-siteid",
                site.getId(),
                "data-dialog-message",
                "Successfully triggered cleaning up site '" + site.getName() + "'.");
    }

    @Override
    public Map<String, String> manageJobsButtonAttributes(Site site)
    {
        return createAttributeMap("data-dialog-module",
                "manage-site-jobs",
                "data-dialog-url",
                createRestUriForSite(null),
                "data-dialog-module-siteid",
                site.getId());
    }

    @Override
    public Map<String, String> editSiteButtonAttributes(Site site)
    {
        return createAttributeMap("data-portal",
                createRestUriForSite("wizard/" + site.getId()),
                "data-portal-header",
                "Manage " + site.getName(),
                "data-portal-button-done-text",
                "Save");
    }

    @Override
    public Map<String, String> siteConnectionButtonAttributes(Site site)
    {
        return createAttributeMap("data-portal",
                createRestUriForSite("wizard/" + site.getId()) + "/connect",
                "data-portal-header",
                "Manage Connection with " + site.getName(),
                "data-portal-button-done-text",
                "Connect");
    }

    String createAbsoluteUrl(String url)
    {
        return Configuration.baseUrl + url;
    }

    String createRelativeUrl(String url)
    {
        return backdoor.environmentData()
                       .getContext() + url;
    }
}
