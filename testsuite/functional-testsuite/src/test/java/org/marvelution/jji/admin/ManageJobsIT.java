/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.admin;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.web.tests.admin.ManageJobsTests;
import org.marvelution.testing.wiremock.FileSource;
import org.marvelution.testing.wiremock.FileSourceType;
import org.marvelution.testing.wiremock.WireMockOptions;

import com.microsoft.playwright.Page;
import org.junit.jupiter.api.BeforeEach;

@WireMockOptions(fileSource = @FileSource(type = FileSourceType.UNDER_TEST_CLASS))
class ManageJobsIT
        extends AbstractSiteManagementIT
        implements ManageJobsTests
{
    @Override
    public Job saveJob(Job job)
    {
        return backdoor.jobs()
                .saveJob(job);
    }

    @Override
    public Build saveBuild(Build build)
    {
        return backdoor.builds()
                .saveBuild(build);
    }

    @Override
    public void navigateDirectToJob(
            Page page,
            Job job)
    {
        page.navigate("./secure/admin/jji/jobs/" + job.getId());
    }

    @BeforeEach
    public void login(Page page)
    {
        loginAsAdmin(page);
    }
}
