/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.admin;

import java.net.URI;

import org.marvelution.jji.AbstractPlayWrightTest;
import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.model.ConfigurationSetting;
import org.marvelution.jji.test.license.License;
import org.marvelution.jji.web.tests.admin.ConfigurationTests;

import com.atlassian.jira.testkit.client.restclient.Project;
import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;

class ConfigurationIT
        extends AbstractPlayWrightTest
        implements ConfigurationTests
{
    @BeforeEach
    void setUp(Page page)
    {
        loginAsAdmin(page);
        backdoor.configuration()
                .clearConfiguration();
    }

    @Override
    public Configuration getConfiguration(String scope)
    {
        return backdoor.configuration()
                .getConfiguration(scope);
    }

    @Override
    public void openProjectConfigurationPage(Page page)
    {
        Project project = backdoor.generator()
                .getOrGenerateProject(getTestMethodName(),
                        name -> backdoor.generator()
                                .generateScrumProject(name));
        page.navigate("./secure/jji-config/" + project.key + "/configuration");
    }

    @Override
    public void additionalProjectConfiguration(
            Page page,
            Configuration configuration,
            License license)
    {}

    @Override
    public void openConfigurationPage(Page page)
    {
        page.navigate("./secure/admin/jji/configuration");
    }

    @Override
    public void additionalConfiguration(
            Page page,
            Configuration configuration,
            License license)
    {
        // Assert default configuration
        Locator jiraBaseRpcUrl = page.locator("input[name=jira_base_rpc_url]");
        assertThat(jiraBaseRpcUrl).hasValue(configuration.requireConfiguration("jira_base_url")
                .getValue());

        if (license.valid() && page.locator("#save-configuration-button")
                .isEnabled())
        {
            // Assert validation error on invalid rpc url
            jiraBaseRpcUrl.fill("localhost:2990/jira");
            page.locator("#save-configuration-button")
                    .click();
            assertThat(page.locator(".field-group", new Page.LocatorOptions().setHas(page.locator("input[name=jira_base_rpc_url]")))
                    .locator(".error")).containsText("Invalid URL, missing host.");

            // Assert submission of configuration hides page
            jiraBaseRpcUrl.fill("https://jira.example.com");
            page.locator("input[name=max_builds_per_page]")
                    .fill("200");
            page.locator("#save-configuration-button")
                    .click();

            // assert configuration is stored
            await().atMost(10, SECONDS)
                    .untilAsserted(() -> Assertions.assertThat(backdoor.configuration()
                                    .getConfiguration()
                                    .getConfiguration("jira_base_rpc_url")
                                    .map(ConfigurationSetting::getValue))
                            .isPresent()
                            .get()
                            .isEqualTo("https://jira.example.com"));
            Assertions.assertThat(backdoor.configuration()
                            .getBaseAPIUrl())
                    .isEqualTo(URI.create("https://jira.example.com/rest/jenkins/latest"));
            Assertions.assertThat(backdoor.configuration()
                            .getMaxBuildsPerPage())
                    .isEqualTo(200);

            // Assert new configuration is shown when opening page again
            page.reload();

            jiraBaseRpcUrl = page.locator("input[name=jira_base_rpc_url]");
            assertThat(jiraBaseRpcUrl).hasValue("https://jira.example.com");
            assertThat(page.locator("input[name=max_builds_per_page]")).hasValue("200");

            // Assert empty rpc url resets to base url
            jiraBaseRpcUrl.click(new Locator.ClickOptions().setPosition(1, 1));
            jiraBaseRpcUrl.dblclick(new Locator.DblclickOptions().setPosition(1, 1));
            jiraBaseRpcUrl.fill("");
            page.locator("#save-configuration-button")
                    .click();

            page.reload();
            assertThat(page.locator("input[name=jira_base_rpc_url]")).hasValue(configuration.requireConfiguration("jira_base_url")
                    .getValue());

            await().atMost(10, SECONDS)
                    .pollInterval(1, SECONDS)
                    .untilAsserted(() -> Assertions.assertThat(backdoor.configuration()
                                    .getConfiguration()
                                    .getConfiguration("jira_base_rpc_url")
                                    .map(ConfigurationSetting::getValue))
                            .isPresent()
                            .get()
                            .isEqualTo(backdoor.configuration()
                                    .getBaseUrl()
                                    .toASCIIString()));
            Assertions.assertThat(backdoor.configuration()
                            .getMaxBuildsPerPage())
                    .isEqualTo(200);
        }
    }
}
