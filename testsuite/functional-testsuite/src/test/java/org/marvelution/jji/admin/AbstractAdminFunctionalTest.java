/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.admin;

import org.marvelution.jji.AbstractFunctionalTest;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;
import org.marvelution.jji.pages.JenkinsSitesPage;
import org.marvelution.testing.wiremock.WireMockServer;

import org.junit.jupiter.api.AfterAll;

import static org.hamcrest.Matchers.hasSize;
import static org.marvelution.jji.web.elements.FlagsAndTips.removeAllFlagsAndTips;

public class AbstractAdminFunctionalTest
        extends AbstractFunctionalTest
{

    Site addSite(
            SiteType type,
            WireMockServer jenkins)
    {
        return addSite(type, jenkins, 0);
    }

    Site addSite(
            SiteType type,
            WireMockServer jenkins,
            int expectedJobs)
    {
        Site site = backdoor.sites()
                .addPublicSite(type, getTestMethodName(), jenkins.serverUri());
        if (!site.isInaccessibleSite())
        {
            backdoor.sites()
                    .syncSite(site);
        }
        if (expectedJobs > 0)
        {
            await().until(() -> backdoor.sites()
                    .getSite(site.getId(), true)
                    .getJobs(), hasSize(expectedJobs));
        }
        return site;
    }

    void navigateToJenkinsSites()
    {
        refreshOrOpen(JenkinsSitesPage.URL);
        removeAllFlagsAndTips();
    }

    @AfterAll
    public static void logout()
    {
        logoutQuietly();
    }
}
