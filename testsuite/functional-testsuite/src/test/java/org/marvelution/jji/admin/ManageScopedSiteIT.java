/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.admin;

import org.marvelution.jji.test.condition.*;
import org.marvelution.jji.web.tests.admin.*;
import org.marvelution.testing.wiremock.*;

import org.junit.jupiter.api.*;

@Disabled("Not using Playwright yet")
@DisabledIfLiteApp
@WireMockOptions(fileSource = @FileSource(path = "ManageSiteIT",
                                          type = FileSourceType.UNDER_CLASSPATH))
class ManageScopedSiteIT
        extends AbstractSiteManagementIT
        implements ManageScopedSiteTests
{

    @BeforeEach
    public void login()
    {
        login(null, SUPER_USER);
    }
}
