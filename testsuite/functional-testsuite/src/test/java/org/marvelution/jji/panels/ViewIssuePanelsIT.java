/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.panels;

import java.net.URI;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.model.*;
import org.marvelution.jji.pages.ViewIssuePage;
import org.marvelution.jji.test.condition.DisabledIfLiteApp;
import org.marvelution.jji.web.tests.panel.*;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;
import static org.marvelution.jji.api.features.FeatureFlags.DYNAMIC_UI_ELEMENTS;

public class ViewIssuePanelsIT
        extends AbstractPanelIT
        implements DevStatusPanelTests, BuildsDialogTests, TriggerBuildDialogTests
{

    @Override
    public DevStatusPanel openDevStatusPanel(
            Page page,
            String issueKey)
    {
        ViewIssuePage issuePage = ViewIssuePage.navigateTo(page, issueKey);
        return issuePage.devstatus();
    }

    @Override
    public Locator openBuildsDialog(
            Page page,
            String issueKey)
    {
        DevStatusPanel status = openDevStatusPanel(page, issueKey);
        status.buildStats()
                .button()
                .click();
        return page.locator("#jenkins-builds-dialog");
    }

    @Override
    public TriggerBuildDialog openBuildTriggerDialog(
            Page page,
            String issueKey)
    {
        ViewIssuePage issuePage = ViewIssuePage.navigateTo(page, issueKey);

        issuePage.moreOperations()
                .click();
        issuePage.triggerBuildAction()
                .click();
        return new TriggerBuildDialog(page.locator(".jira-dialog-content"));
    }

    @Test
    void testTriggerBuildOperationVisibility(Page page)
    {
        String issueKey = getExistingIssueKey();

        ViewIssuePage issuePage = ViewIssuePage.navigateTo(page, issueKey);
        issuePage.moreOperations()
                .click();
        assertThat(issuePage.triggerBuildAction()).isVisible();

        logout(page);
        login(page, USER);

        issuePage = ViewIssuePage.navigateTo(page, issueKey);
        issuePage.moreOperations()
                .click();
        assertThat(issuePage.triggerBuildAction()).not()
                .isVisible();
    }

    @Test
    void testCIBuildTabLessThenSixBuildLinksRendersRelatedIssueLinks(Page page)
    {
        testCIBuildTabRelatedLinksRendering(page, 3);
    }

    @Test
    void testCIBuildTabMoreThenFiveBuildLinksRendersRelatedProjectLinks(Page page)
    {
        testCIBuildTabRelatedLinksRendering(page, 10);
    }

    private void testCIBuildTabRelatedLinksRendering(
            Page page,
            int issueCount)
    {
        Site site = backdoor.sites()
                .addFirewalledSite(SiteType.JENKINS, getTestMethodName(), URI.create("http://localhost:8080"), false);
        Job job = backdoor.jobs()
                .addJob(site, "Test-Job");
        Build build = backdoor.builds()
                .addBuild(job);
        backdoor.jobs()
                .saveJob(job);

        Project project = backdoor.generator()
                .generateScrumProject(getTestMethodName());
        List<IssueReference> issues = new ArrayList<>();
        for (int i = 1; i <= issueCount; i++)
        {
            IssueCreateResponse issue = backdoor.issues()
                    .createIssue(project.key, "Test Issue " + i);
            backdoor.builds()
                    .linkBuildToIssue(build, issue.key);
            issues.add(new IssueReference().setIssueKey(issue.key)
                    .setProjectKey(project.key));
        }
        IssueReference issue = issues.get(0);

        ViewIssuePage issuePage = ViewIssuePage.navigateTo(page, issue.getIssueKey());

        assertThat(issuePage.issueKey()).hasText(issue.getIssueKey());
        assertThat(issuePage.jenkinsTabPanelLink()).isVisible();
        issuePage.jenkinsTabPanelLink()
                .click();
        assertThat(issuePage.jenkinsTabPanelMessage()).not()
                .isVisible();

        issues.sort(Comparator.comparing(IssueReference::getIssueKey));
        assertBuild(issuePage.jenkinsTabPanelBuild(0), build, issues);
    }

    private void assertBuild(
            Locator panelBuild,
            Build build,
            List<IssueReference> issues)
    {
        assertThat(panelBuild.locator(".header")).hasText(build.getDisplayName());

        Locator relations = panelBuild.locator(".related-issues");
        Locator links = relations.locator("li")
                .locator("a");

        if (issues.size() > 5)
        {
            List<String> projects = issues.stream()
                    .map(IssueReference::getProjectKey)
                    .distinct()
                    .sorted()
                    .toList();
            assertThat(relations).containsText("Related Projects");
            assertThat(links).hasCount(projects.size());
            assertThat(links).hasText(projects.toArray(String[]::new));
            for (int i = 0; i < projects.size(); i++)
            {
                String href = backdoor.environmentData()
                                      .getBaseUrl() + "issues/?jql=project%20%3D%20" + projects.get(i) +
                              "%20AND%20issue%20in%20issuesRelatedToBuild(" + build.getId() + ")";
                assertThat(links.nth(i)).hasAttribute("href", href);
            }
        }
        else
        {
            assertThat(relations).containsText("Related Issues");
            assertThat(links).hasCount(issues.size());
            assertThat(links).hasText(issues.stream()
                    .map(IssueReference::getIssueKey)
                    .sorted()
                    .toArray(String[]::new));
            for (int i = 0; i < issues.size(); i++)
            {
                String href = backdoor.environmentData()
                                      .getBaseUrl() + "browse/" + issues.get(i)
                                      .getIssueKey();
                assertThat(links.nth(i)).hasAttribute("href", href);
            }
        }
    }

    @Test
    void testPanelVisibilityOnDisabledProject(Page page)
    {
        Project project = backdoor.generator()
                .generateBasicProject();
        String issue = getExistingIssueKey();
        createPanelJobData(issue);

        ViewIssuePage issuePage = ViewIssuePage.navigateTo(page, issue);

        assertThat(issuePage.devstatus()
                .self()).isVisible();

        Configuration configuration = backdoor.configuration()
                .getConfiguration();
        configuration.addConfiguration(ConfigurationSetting.forKey(ConfigurationService.ENABLED_PROJECT_KEYS)
                .setValue(project.key));
        backdoor.configuration()
                .saveConfiguration(configuration);

        issuePage = ViewIssuePage.navigateTo(page, issue);

        assertThat(issuePage.devstatus()
                .self()).not()
                .isVisible();
    }

    @AfterEach
    void tearDown()
    {
        backdoor.configuration()
                .disableFeature(DYNAMIC_UI_ELEMENTS);
    }

    @Test
    @DisabledIfLiteApp
    void testDynamicUIElements(Page page)
    {
        Project project = backdoor.generator()
                .generateScrumProject(getTestMethodName());
        IssueCreateResponse issue = backdoor.issues()
                .createIssue(project.key, "Test Issue");

        Site site = backdoor.sites()
                .addFirewalledSite(SiteType.JENKINS, getTestMethodName(), URI.create("http://localhost:8080"), false);
        Job job = backdoor.jobs()
                .addJob(site, "Test-Job");
        job.setLinked(true);
        job.setLastBuild(10);
        backdoor.jobs()
                .saveJob(job);
        Build build = backdoor.builds()
                .addBuild(job, "SCM triggered build", Result.UNSTABLE);
        build.setDuration(TimeUnit.SECONDS.toMillis(510));
        build.setTimestamp(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2));
        backdoor.builds()
                .saveBuild(build);

        backdoor.configuration()
                .disableFeature(DYNAMIC_UI_ELEMENTS);
        ViewIssuePage issuePage = ViewIssuePage.navigateTo(page, issue.key);

        assertThat(issuePage.issueKey()).hasText(issue.key);
        assertThat(issuePage.jenkinsTabPanelLink()).isVisible();
        assertThat(issuePage.devstatus()
                .self()).isVisible();

        backdoor.configuration()
                .enableFeature(DYNAMIC_UI_ELEMENTS);
        issuePage = ViewIssuePage.navigateTo(page, issue.key);

        assertThat(issuePage.issueKey()).hasText(issue.key);
        assertThat(issuePage.jenkinsTabPanelLink()).not()
                .isVisible();
        assertThat(issuePage.devstatus()
                .self()).not()
                .isVisible();

        backdoor.builds()
                .linkBuildToIssue(build, issue.key);

        issuePage = ViewIssuePage.navigateTo(page, issue.key);

        assertThat(issuePage.issueKey()).hasText(issue.key);
        assertThat(issuePage.jenkinsTabPanelLink()).isVisible();
        assertThat(issuePage.devstatus()
                .self()).isVisible();
    }
}
