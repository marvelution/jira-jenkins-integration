/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji;

import org.marvelution.jji.elements.LoginPage;
import org.marvelution.jji.elements.UserOptions;
import org.marvelution.jji.test.BaseTest;
import org.marvelution.jji.test.license.AddonLicenseInstaller;
import org.marvelution.testing.FunctionalTestSupport;

import com.codeborne.selenide.Configuration;
import org.awaitility.Awaitility;
import org.awaitility.core.ConditionFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.switchTo;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.marvelution.jji.web.elements.FlagsAndTips.removeAllFlagsAndTips;

/**
 * Base test class for all Functional tests driven by Selenium and Selenide.
 *
 * @author Mark  Rekveld
 * @since 2.0.0
 */
@ExtendWith(AddonLicenseInstaller.class)
public abstract class AbstractFunctionalTest
        extends FunctionalTestSupport
        implements BaseTest
{

    private static final String JIRA_BASE_URL = "jira.baseUrl";

    @AfterEach
    public void reset()
    {
        backdoor.sites()
                .clearSites();
        backdoor.configuration()
                .clearConfiguration();
    }

    @Override
    protected void refreshOrOpen(String url)
    {
        refreshOrOpen(url, true);
    }

    protected void refreshOrOpen(
            String url,
            boolean hideFlagsAndTips)
    {
        super.refreshOrOpen(url);
        try
        {
            switchTo().alert()
                    .accept();
        }
        catch (Throwable ignore)
        {
            // no alert to close, all good.
        }
        if (hideFlagsAndTips)
        {
            removeAllFlagsAndTips();
        }
    }

    protected ConditionFactory await()
    {
        return Awaitility.await()
                .atMost(30, SECONDS)
                .pollInterval(1, SECONDS);
    }

    @BeforeAll
    public static void correctBaseUrl()
    {
        String jiraBaseUrl = backdoor.applicationProperties()
                .getString(JIRA_BASE_URL);
        if (!jiraBaseUrl.equals(Configuration.baseUrl))
        {
            backdoor.applicationProperties()
                    .setString(JIRA_BASE_URL, Configuration.baseUrl);
        }
    }

    protected static void loginAsAdmin()
    {
        login(ADMIN);
    }

    protected static void login(String userpass)
    {
        login(userpass, userpass);
    }

    protected static void login(
            String username,
            String password)
    {
        if (!LoginPage.username()
                .is(visible))
        {
            open(LoginPage.URL);
        }
        LoginPage.username()
                .shouldBe(visible)
                .setValue(username);
        LoginPage.password()
                .setValue(password);
        LoginPage.login()
                .click();
        LoginPage.username()
                .shouldNotBe(visible);
        removeAllFlagsAndTips();
    }

    protected static void logoutQuietly()
    {
        try
        {
            removeAllFlagsAndTips();
            new UserOptions().open()
                    .logout()
                    .shouldBe(visible)
                    .click();
        }
        catch (Throwable ignore)
        {
        }
    }
}
