/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.panels;

import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.time.temporal.Temporal;
import java.util.Date;

import org.marvelution.jji.AbstractPlayWrightTest;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.web.tests.panel.PanelTests;

import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.microsoft.playwright.Page;
import org.junit.jupiter.api.BeforeEach;

abstract class AbstractPanelIT
        extends AbstractPlayWrightTest
        implements PanelTests
{

    private String existingIssueKey;

    @BeforeEach
    void setupExistingIssueAndLogin(Page page)
    {
        Project project = backdoor.generator()
                .getOrGenerateProject(getTestMethodName(),
                        name -> backdoor.generator()
                                .generateScrumProject(name));
        existingIssueKey = backdoor.issues()
                .createIssue(project.key, getTestMethodName()).key;
        login(page, SUPER_USER);
    }

    @Override
    public Site saveSite(Site site)
    {
        return backdoor.sites()
                .saveSite(site);
    }

    @Override
    public Job saveJob(Job job)
    {
        return backdoor.jobs()
                .saveJob(job);
    }

    @Override
    public Build saveBuild(Build build)
    {
        return backdoor.builds()
                .saveBuild(build);
    }

    @Override
    public void linkBuildToIssue(
            Build build,
            String issueKey)
    {
        backdoor.builds()
                .linkBuildToIssue(build, issueKey);
    }

    @Override
    public String getExistingIssueKey()
    {
        return existingIssueKey;
    }

    @Override
    public String formatDate(
            Temporal temporal,
            ZoneId zoneId)
    {
        long nanos = temporal.getLong(ChronoField.NANO_OF_SECOND);
        long epochSeconds = temporal.getLong(ChronoField.INSTANT_SECONDS);
        return backdoor.users()
                .formatDate(USER, DateTimeStyle.RELATIVE, Date.from(Instant.ofEpochSecond(epochSeconds, nanos)));
    }

    @Override
    public void runLinkStatistician(String issueKey)
    {
        backdoor.linkStats()
                .calculateStats(issueKey, true);
    }
}
