/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.admin;

import org.marvelution.jji.AbstractPlayWrightTest;
import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.test.condition.DisabledIfLiteApp;
import org.marvelution.jji.test.license.License;
import org.marvelution.jji.web.tests.admin.ScopedConfigurationTests;

import com.atlassian.jira.testkit.client.restclient.Project;
import com.microsoft.playwright.Page;
import org.junit.jupiter.api.BeforeEach;

@DisabledIfLiteApp
class ScopedConfigurationIT
        extends AbstractPlayWrightTest
        implements ScopedConfigurationTests
{

    @BeforeEach
    void setUp()
    {
        backdoor.configuration()
                .clearConfiguration();
    }

    @Override
    public Configuration getConfiguration(String scope)
    {
        return backdoor.configuration()
                .getConfiguration(scope);
    }

    @Override
    public void openProjectConfigurationPage(Page page)
    {
        Project project = backdoor.generator()
                .getOrGenerateProject(getTestMethodName(),
                        name -> backdoor.generator()
                                .generateScrumProject(name));
        page.navigate("./secure/jji-config/" + project.key + "/configuration");
    }

    @Override
    public void additionalProjectConfiguration(
            Page page,
            Configuration configuration,
            License license)
    {}

    @BeforeEach
    public void login(Page page)
    {
        login(page, SUPER_USER);
    }
}
