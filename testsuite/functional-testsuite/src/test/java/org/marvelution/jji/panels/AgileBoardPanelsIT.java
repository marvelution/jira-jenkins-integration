/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.panels;

import org.marvelution.jji.testkit.backdoor.model.AgileBoard;
import org.marvelution.jji.web.tests.panel.DevStatusPanel;
import org.marvelution.jji.web.tests.panel.BuildsDialogTests;
import org.marvelution.jji.web.tests.panel.DevStatusPanelTests;

import com.atlassian.jira.issue.IssueKey;
import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.LocatorAssertions;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class AgileBoardPanelsIT
        extends AbstractPanelIT
        implements DevStatusPanelTests, BuildsDialogTests
{

    @Override
    public Locator openBuildsDialog(
            Page page,
            String issueKey)
    {
        DevStatusPanel status = openDevStatusPanel(page, issueKey);
        status.buildStats()
                .button()
                .click();
        Locator dialogContent = page.locator(".jira-dialog-content");
        assertThat(dialogContent).isVisible();
        return dialogContent;
    }

    @Override
    public DevStatusPanel openDevStatusPanel(
            Page page,
            String issueKey)
    {
        navigateToAgileBoard(page, issueKey);
        Locator locator = page.locator("[id$=-jenkins-agile-devstatus]");
        assertThat(locator).isVisible(new LocatorAssertions.IsVisibleOptions().setVisible(true)
                .setTimeout(10000));
        return new DevStatusPanel(locator);
    }

    private void navigateToAgileBoard(
            Page page,
            String issueKey)
    {
        AgileBoard board = backdoor.agileBoards()
                .getBoardsForProject(IssueKey.from(issueKey)
                        .getProjectKey())
                .stream()
                .findFirst()
                .orElseThrow();
        page.navigate("./secure/RapidBoard.jspa?rapidView=" + board.id + "&view=planning&selectedIssue=" + issueKey);
    }
}
