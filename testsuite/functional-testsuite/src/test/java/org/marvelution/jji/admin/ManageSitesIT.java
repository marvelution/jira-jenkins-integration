/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.admin;

import java.util.*;

import org.marvelution.jji.model.*;
import org.marvelution.jji.web.tests.admin.*;
import org.marvelution.testing.wiremock.*;

import org.junit.jupiter.api.*;

import static org.marvelution.jji.web.conditions.Conditions.*;

@Disabled("Not using Playwright yet")
@WireMockOptions(fileSource = @FileSource(type = FileSourceType.UNDER_TEST_CLASS))
class ManageSitesIT
		extends AbstractSiteManagementIT
		implements ManageSitesTests
{

	@BeforeEach
	public void login()
	{
		loginAsAdmin(null);
	}

	@Override
	public Map<String, String> addSiteButtonAttributes()
	{
		return createAttributeMap("data-portal",
				createRestUriForSite("wizard"),
				"data-portal-header",
				"Connect a Site",
				"data-portal-button-done-text",
				"Connect");
	}

	@Override
	public Map<String, String> deleteSiteButtonAttributes(Site site)
	{
		return createAttributeMap("data-dialog-module", "delete-site", "data-dialog-module-siteid", site.getId(), "data-dialog-url",
		                          createRestUriForSite(null), "data-dialog-message",
		                          "Successfully triggered deletion of site '" + site.getName() + "'.");
	}
}
