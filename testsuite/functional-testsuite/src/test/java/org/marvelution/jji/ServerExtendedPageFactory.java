/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji;

import org.marvelution.jji.web.tests.playwright.ExtendedPage;
import org.marvelution.jji.web.tests.playwright.ExtendedPageFactory;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.Response;
import org.junit.jupiter.api.extension.ExtensionContext;

public class ServerExtendedPageFactory
        implements ExtendedPageFactory
{
    @Override
    public Page extend(
            Page page,
            ExtensionContext extensionContext)
    {
        return new ServerPage(page, extensionContext);
    }

    public static class ServerPage
            extends ExtendedPage
    {
        ServerPage(
                Page delegate,
                ExtensionContext extensionContext)
        {
            super(delegate, extensionContext);
        }

        @Override
        public Response navigate(String url)
        {
            return navigate(url, null);
        }

        @Override
        public Response navigate(
                String url,
                NavigateOptions options)
        {
            Response response = super.navigate(url, options);
            addStyleTag(new Page.AddStyleTagOptions().setContent("""
                                                                 #aui-flag-container,
                                                                 .jira-help-tip,
                                                                 #theme-switcher-discovery-card
                                                                 {
                                                                   display:none !important;
                                                                 }
                                                                 """));
            return response;
        }
    }
}
